<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"
    xmlns:saxon="http://saxon.sf.net/"  
    xmlns:localfunc="http://tmobile.com/xsl/function/namespace"
   exclude-result-prefixes="xsd soap  saxon"
>
  <xsl:output method="xml"/>
  <xsl:param name="maskElements"/>
  
  <xsl:variable name="maskElementsVar" select="if ($maskElements instance of node()) then $maskElements else document($maskElements)"/>
  <xsl:variable name="xmlElemsToMask" select="for $varSensitiveElement in  ($maskElementsVar//*:SensitiveElement) return (saxon:evaluate($varSensitiveElement/*:maskElementXPath),  $varSensitiveElement)"/>
 
    <xsl:template match="@*|node()">
	    <xsl:copy>
	      <xsl:apply-templates select="@*|node()"/>
	    </xsl:copy>
  </xsl:template>

    <xsl:template match="//*[count(. intersect $xmlElemsToMask) > 0]"> 
        <xsl:variable name="curElem" select="current()"/>
        
       <xsl:variable name="index1" select="index-of($xmlElemsToMask, $curElem)"/>
       
       <xsl:variable name="indexcur" select="number(localfunc:getCurrentElemIndex($index1[1], $curElem, $xmlElemsToMask))"/>
       
       <xsl:variable name="index" select="number(localfunc:getMaskElemIndex($indexcur, $xmlElemsToMask))"/>
       
       <xsl:variable name="allNonAttrsToMask" select="$maskElementsVar//*:SensitiveElement[*:maskElementXPath = $xmlElemsToMask[$index]/*:maskElementXPath][./*:maskElementAttributeName[string-length(.) = 0]]"/>
       <xsl:variable name="allAttrsToMask" select="$maskElementsVar//*:SensitiveElement[*:maskElementXPath = $xmlElemsToMask[$index]/*:maskElementXPath][./*:maskElementAttributeName[string-length(.) > 0]]"/>
       <xsl:variable name="elemValue">
           <xsl:choose>   
		  <xsl:when test="count($allNonAttrsToMask) gt  0">
			 <xsl:variable name="unmaskPrefixLen" select="$allNonAttrsToMask[1]/*:unmaskedPrefixLength"/>
			 <xsl:variable name="unmaskSuffixLen" select="$allNonAttrsToMask[1]/*:unmaskedSuffixLength"/>
			 <xsl:variable name="valueForMask" select="$curElem/text()"/>
			 <xsl:value-of select="localfunc:maskValue($unmaskPrefixLen, $unmaskSuffixLen, $valueForMask)"/>		  
		  </xsl:when>
		  <xsl:otherwise>
		  <xsl:value-of select="$curElem/text()"/>
		  </xsl:otherwise>
	   </xsl:choose>
	</xsl:variable>
	<xsl:copy>
		<xsl:choose>
		   <xsl:when test="count($allAttrsToMask) gt  0">
		       <xsl:for-each select="$curElem/@*">
		           <xsl:attribute name="{local-name(.)}">
		              <xsl:choose>
				      <xsl:when test="local-name(.) = $allAttrsToMask/*:maskElementAttributeName">
				         <xsl:variable name="attrName" select="local-name(.)"/>
				         <xsl:variable name="unmaskPrefixLen" select="$allAttrsToMask[*:maskElementAttributeName = $attrName]/*:unmaskedPrefixLength"/>
				         <xsl:variable name="unmaskSuffixLen" select="$allAttrsToMask[*:maskElementAttributeName = $attrName]/*:unmaskedSuffixLength"/>
				         <xsl:variable name="valueForMask" select="."/>
					 <xsl:value-of select="localfunc:maskValue($unmaskPrefixLen, $unmaskSuffixLen, $valueForMask)"/>
				      </xsl:when>
				      <xsl:otherwise>
					  <xsl:value-of select="."/>    
				      </xsl:otherwise>
			    </xsl:choose>
			</xsl:attribute>
			</xsl:for-each>
		       <xsl:value-of select="$elemValue"/>
		       <xsl:apply-templates select="*"/>
		   </xsl:when>
		   <xsl:otherwise>
		       <xsl:apply-templates select="@*"/>
		       <xsl:value-of select="$elemValue"/> 
		       <xsl:apply-templates select="*"/>		       
		   </xsl:otherwise>
		</xsl:choose>
		 
	</xsl:copy>
  </xsl:template>

  <xsl:function name="localfunc:getMaskElemIndex">
    <xsl:param name="IndexVar"/>
    <xsl:param name="maskElems"/>
    <xsl:sequence select="if (local-name($maskElems[number($IndexVar)][1]) = 'SensitiveElement') then $IndexVar else 
    	localfunc:getMaskElemIndex($IndexVar + 1, $maskElems)"/>   
  </xsl:function>
  
  <xsl:function name="localfunc:getCurrentElemIndex">
    <xsl:param name="IndexVar"/>
    <xsl:param name="curElement"/>
    <xsl:param name="maskElems"/>
    <xsl:variable name="maskElemVar" select="$maskElems[number($IndexVar)][1]"/>
    <xsl:variable name="maskElemNextVar" select="$maskElems[number($IndexVar + 1)][1]"/>
    <xsl:variable name="maskElemVarXPath" select="$maskElemNextVar/*:maskElementXPath"/>
    <xsl:variable name="subStringVar" select="substring-after($maskElemVarXPath, local-name($curElement))"/>
   
    <xsl:sequence select="if ((local-name($maskElemVar) = local-name($curElement)) and  ((number(string-length($subStringVar)) lt 1)  or
          (count(saxon:evaluate(concat('$p1', $subStringVar), $curElement)) gt 0))
        )   
    then $IndexVar  else 
    	localfunc:getCurrentElemIndex($IndexVar + 1, $curElement, $maskElems)"/>   
  </xsl:function>

  <xsl:function name="localfunc:maskValue">
    <xsl:param name="unmaskPrefixLength"/>
    <xsl:param name="unmaskSuffixLength"/>
    <xsl:param name="valueToMask"/>
         <xsl:variable name="zero" select="'0'"/>

        <xsl:variable name="strLen" select="string-length($valueToMask)"/>
        <xsl:variable name="unmaskLen" select="$unmaskPrefixLength + $unmaskSuffixLength"/>
        
        <xsl:variable name="outputVar">
           <xsl:if test="($unmaskLen lt $strLen)">
               <xsl:if test="($unmaskPrefixLength gt $zero)">
                   <xsl:value-of select="substring($valueToMask, 1, $unmaskPrefixLength)"/>
               </xsl:if>
               <xsl:variable name="toMask" select="substring($valueToMask, $unmaskPrefixLength + 1, ($strLen - $unmaskLen))"/>
               <xsl:value-of select="replace($toMask, '.', '*')"/>
               <xsl:if test="$unmaskSuffixLength gt $zero">
                   <xsl:value-of select="substring($valueToMask, $strLen - $unmaskSuffixLength + 1 , $unmaskSuffixLength)"/>
               </xsl:if>               
           </xsl:if>
           <xsl:if test="$unmaskLen ge $strLen">
               <xsl:value-of select="$valueToMask"/>
           </xsl:if>          
        </xsl:variable>
        <xsl:sequence select="$outputVar"/>
   
  </xsl:function>
  
</xsl:stylesheet>
