package com.tmobile.u2.service;

import com.t_mobile.erp.services.logistics.MTBlockingStatusCheckResponse;
import com.tmobile.u2.exception.TMobileProcessException;

import targetsappi.applications.sappi.endpoints.device.validateimeisoap_start_input.ValidateIMEISoapStartInput;

public interface IValidateIMEISoapService {

	void executeMethod(ValidateIMEISoapStartInput validateIMEISoapStartInput,
			MTBlockingStatusCheckResponse mtBlockingStatusCheckResponse) throws TMobileProcessException;

}
