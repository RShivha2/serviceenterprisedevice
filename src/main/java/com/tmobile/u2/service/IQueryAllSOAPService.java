package com.tmobile.u2.service;

import com.example.namespaces.tns._1588947392231.QueryAllSOAPStartInput;
import com.example.namespaces.tns._1588947392232.QueryAllSOAPEndOutput;
import com.tmobile.u2.exception.TMobileProcessException;

public interface IQueryAllSOAPService {

	void executeMethod(QueryAllSOAPStartInput queryAllSOAPStartInput, QueryAllSOAPEndOutput queryAllSOAPEndOutput) throws TMobileProcessException;

}
