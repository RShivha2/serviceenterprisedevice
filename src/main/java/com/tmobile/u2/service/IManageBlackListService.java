package com.tmobile.u2.service;

import com.tmobile.services.productmanagement.deviceenterprise.v1.ManageBlackListRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.ManageBlackListResponse;
import com.tmobile.u2.exception.TMobileProcessException;

/*************************************************************************
 * 
 * TMOBILE CONFIDENTIAL
 * _________________________________________________________________________________
 * 
 * TMOBILE is a trademark of TMOBILE Company.
 *
 *Copyright © 2021 TMOBILE. All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of TMOBILE
 * and its suppliers, if any. The intellectual and technical concepts contained
 * herein are proprietary to TMOBILE and its suppliers and may be covered by U.S.
 * and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this
 * material is strictly forbidden unless prior written permission is obtained
 * from TMOBILE.
 *
 *************************************************************************/
// Author : Generated by ATMA ®
// Revision History : 
public interface IManageBlackListService  {
   
    public void executeMethod(ManageBlackListRequest manageBlackListRequest, ManageBlackListResponse manageBlackListResponse) throws TMobileProcessException;
}