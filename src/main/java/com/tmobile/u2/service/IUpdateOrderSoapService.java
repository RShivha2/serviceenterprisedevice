package com.tmobile.u2.service;

import com.oracle.xmlns.apps.mdm.customer.UpdateOrderOutput;
import com.t_mobile.u2.xml.tmolineordersio.ListOfTMOLineOrdersIO;
import com.tmobile.u2.exception.TMobileProcessException;

public interface IUpdateOrderSoapService {

	void executeMethod(ListOfTMOLineOrdersIO listOfTMOLineOrdersIO, UpdateOrderOutput updateorderOutput) throws TMobileProcessException;

}
