package com.tmobile.u2.service;

import com.t_mobile.csi.device.queryimeiwarranty.CSIQueryIMEIWarrantyRequestType;
import com.t_mobile.csi.device.queryimeiwarranty.CSIQueryIMEIWarrantyResponseType;
import com.tmobile.u2.exception.TMobileProcessException;

public interface IQueryIMEIWarrantySQLService {

	void executeMethod(CSIQueryIMEIWarrantyRequestType queryIMEIWarrantyRequest,
			CSIQueryIMEIWarrantyResponseType queryIMEIWarrantyResponse) throws TMobileProcessException;

}
