package com.tmobile.u2.service;

import com.t_mobile.csi.device.getimei.CSIGetIMEIRequestType;
import com.t_mobile.csi.device.getimei.CSIGetIMEIResponseType;
import com.tmobile.u2.exception.TMobileProcessException;

public interface IGetIMEISQLService {

	void executeMethod(CSIGetIMEIRequestType csiGetIMEIRequest, CSIGetIMEIResponseType csiGetIMEIResponse) throws TMobileProcessException;

}
