package com.tmobile.u2.service;

import com.t_mobile.schema.networkmanagement.union.resources.schemas.devicespecifications.DeviceSpecificationDetails;
import com.tmobile.u2.exception.TMobileProcessException;

import targetunion.applications.union.endpoints.getdevicespecificationsrest_start_input.GetDeviceSpecificationsRequest;

public interface IGetDeviceSpecificationsRESTService {

	void executeMethod(GetDeviceSpecificationsRequest getDeviceSpecificationsRequest,
			DeviceSpecificationDetails deviceSpecificationDetails) throws TMobileProcessException;

}
