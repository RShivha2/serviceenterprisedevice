package com.tmobile.u2.service;

import com.tmobile.u2.exception.TMobileProcessException;

import targetsappi.applications.sappi.endpoints.device.manageblacklistsoap_end_output.ManageBlackListSoapEndOutput;
import targetsappi.applications.sappi.endpoints.device.manageblacklistwholesalesoap_start_input.Event;

public interface IManageBlackListSoapService {

	void executeMethod(Event event, ManageBlackListSoapEndOutput manageBlackListSoapEndOutput) throws TMobileProcessException;

}
