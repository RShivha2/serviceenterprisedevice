package com.tmobile.u2.service;

import com.t_mobile.schema.apple.verifyorder.VerifyOrderRequesttype;
import com.t_mobile.schema.apple.verifyorder.VerifyOrderResponsetype;
import com.tmobile.u2.exception.TMobileProcessException;

public interface IVerifyOrderRESTService {

	void executeMethod(VerifyOrderRequesttype verifyOrderRequest, VerifyOrderResponsetype verifyOrderResponse) throws TMobileProcessException;

}
