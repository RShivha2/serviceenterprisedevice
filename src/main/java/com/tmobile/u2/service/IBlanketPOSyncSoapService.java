package com.tmobile.u2.service;

import com.tmobile.u2.exception.TMobileProcessException;

import targetsappi.applications.sappi.endpoints.order.blanketposyncsoap_end_output.BlanketPOSyncSoapEndOutput;
import targetsappi.applications.sappi.endpoints.order.blanketposyncsoap_start_input.BlanketPOSyncSoapStartInput;

public interface IBlanketPOSyncSoapService {

	void executeMethod(BlanketPOSyncSoapStartInput blanketPOSyncSoapStartInput, BlanketPOSyncSoapEndOutput blanketPOSyncSoapEndOutput) throws TMobileProcessException;

}
