package com.tmobile.u2.service;

import com.tmobile.u2.exception.TMobileProcessException;

import targetsappi.applications.sappi.endpoints.device.manageblacklistwholesalesoap_end_output.ManageBlackListWholesaleSoapEndOutput;
import targetsappi.applications.sappi.endpoints.device.manageblacklistwholesalesoap_start_input.Event;

public interface IManageBlackListWholesaleSoapService {

	void executeMethod(Event event, ManageBlackListWholesaleSoapEndOutput manageBlackListWholesaleSoapEndOutput) throws TMobileProcessException;

}
