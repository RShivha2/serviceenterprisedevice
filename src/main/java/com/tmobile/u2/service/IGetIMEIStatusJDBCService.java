package com.tmobile.u2.service;

import com.tmobile.u2.exception.TMobileProcessException;

import targetserialization.applications.serialization.endpoints.device.getimeistatusjdbc_end_output.GetIMEIStatusJDBCEndOutput;
import targetserialization.applications.serialization.endpoints.device.getimeistatusjdbc_start_input.GetIMEIStatusJDBCStartInput;

public interface IGetIMEIStatusJDBCService {

	void executeMethod(GetIMEIStatusJDBCStartInput getIMEIStatusJDBCStartInput,
			GetIMEIStatusJDBCEndOutput getIMEIStatusJDBCEndOutput) throws TMobileProcessException;

}
