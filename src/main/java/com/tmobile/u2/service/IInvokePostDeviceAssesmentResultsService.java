package com.tmobile.u2.service;

import com.tmobile.services.productmanagement.deviceenterprise.v1.PostDeviceAssesmentResultsRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.PostDeviceAssesmentResultsResponse;
import com.tmobile.u2.exception.TMobileProcessException;

public interface IInvokePostDeviceAssesmentResultsService {

	void executeMethod(PostDeviceAssesmentResultsRequest postDeviceAssesmentResultsRequest,
			PostDeviceAssesmentResultsResponse postDeviceAssesmentResultsResponse) throws TMobileProcessException;

}
