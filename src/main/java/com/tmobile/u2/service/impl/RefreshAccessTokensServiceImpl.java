package com.tmobile.u2.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.example.gsx_accesstoken.GSXAccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.namespaces.tns._1593722054053.Element;
import com.tibco.tns.bw.activity.jsonparser.xsd.output.GetAccessTokenOutput;
import com.tmobile.u2.exception.TMobileProcessException;
import com.tmobile.u2.service.IGetAccessTokenService;
import com.tmobile.u2.service.IRefreshAccessTokensService;
import com.tmobile.u2.util.ModuleServiceContext;

/*************************************************************************
 * 
 * TMOBILE CONFIDENTIAL
 * _________________________________________________________________________________
 * 
 * TMOBILE is a trademark of TMOBILE Company.
 *
 * Copyright © 2021 TMOBILE. All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of
 * TMOBILE and its suppliers, if any. The intellectual and technical concepts
 * contained herein are proprietary to TMOBILE and its suppliers and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by
 * trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from TMOBILE.
 *
 *************************************************************************/
// Author : Generated by ATMA ®
// Revision History :  
@Service
public class RefreshAccessTokensServiceImpl implements IRefreshAccessTokensService {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(RefreshAccessTokensServiceImpl.class);

	@Autowired
	private ModuleServiceContext moduleServiceContext;

	public void setModuleServiceContext(ModuleServiceContext moduleServiceContext) {
		this.moduleServiceContext = moduleServiceContext;
	}

	@Autowired
	private IGetAccessTokenService iGetAccessTokenService;

	public void setGetAccessTokenService(IGetAccessTokenService iGetAccessTokenService) {
		this.iGetAccessTokenService = iGetAccessTokenService;
	}

	/**
	 * Method end
	 * 
	 * @param Element element ,GetAccessTokenOutput getAccessTokenOutput
	 *                ,GSXAccessToken gSXAccessToken ,AuthenticationInfo
	 *                authenticationInfo
	 * @return void
	 * @throws TMobileProcessException
	 */
	@SuppressWarnings("unused")
	private void end(Element element, GetAccessTokenOutput getAccessTokenOutput, GSXAccessToken gSXAccessToken)
			throws TMobileProcessException {
		if (StringUtils.isNotEmpty(getAccessTokenOutput.getAccessToken())) {
			if (getAccessTokenOutput.getAccessToken() != null) {
				element.setAccessToken(getAccessTokenOutput.getAccessToken());
			}
		} else {
			if (gSXAccessToken.getRoot().getAccessToken() != null) {
				element.setAccessToken(gSXAccessToken.getRoot().getAccessToken());
			}
		}
	}

	/**
	 * Method callGetaccesstokens
	 * 
	 * @param GetAccessTokenOutput getAccessTokenOutput
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void callGetAccessTokens(GetAccessTokenOutput getAccessTokenOutput) throws TMobileProcessException {
		iGetAccessTokenService.executeMethod(getAccessTokenOutput);
	}

	/**
	 * Method setAccessTokens
	 * 
	 * @param GetAccessTokenOutput getAccessTokenOutput ,GSXAccessToken
	 *                             gSXAccessToken
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void setAccessTokens(GetAccessTokenOutput getAccessTokenOutput, GSXAccessToken gSXAccessToken)
			throws TMobileProcessException {
		org.example.gsx_accesstoken.Root root = gSXAccessToken.getRoot();
		if (getAccessTokenOutput.getAccessToken() != null) {
			root.setAccessToken(getAccessTokenOutput.getAccessToken());
		}
		callGetAccessTokens(getAccessTokenOutput);
		root.setExpiresIn(String.valueOf(Long.parseLong(getAccessTokenOutput.getExpiresIn()) * 1000));
		moduleServiceContext.putRequestValue("GsxAccesstokens", gSXAccessToken);
	}

	/**
	 * Method executeMethod
	 * 
	 * @param Element element
	 * @return void
	 * @throws TMobileProcessException
	 */
	public void executeMethod(Element element) throws TMobileProcessException {
		GSXAccessToken gSXAccessToken = new GSXAccessToken();
		GetAccessTokenOutput getAccessTokenOutput = new GetAccessTokenOutput();
		setAccessTokens(getAccessTokenOutput, gSXAccessToken);
	}

}