package com.tmobile.u2.service.impl;

import java.time.LocalDateTime;
import java.util.Arrays;

import javax.xml.bind.JAXBElement;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apple.gsxws.elements.global.LogoutRequestType;
import com.apple.gsxws.elements.global.LogoutRequestWrapper;
import com.apple.gsxws.elements.global.LogoutResponseWrapper;
import com.apple.gsxws.types.global.GsxUserSessionType;
import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.Root;
import com.tmobile.services.schema.dataobject.responsestatus.gsperrorhandlerlogger.DebugData;
import com.tmobile.services.schema.dataobject.responsestatus.gsperrorhandlerlogger.ErrorShortMessage;
import com.tmobile.services.schema.dataobject.responsestatus.gsperrorhandlerlogger.ProcessStack;
import com.tmobile.services.schema.dataobject.responsestatus.gsperrorhandlerlogger.StackTrace;
import com.tmobile.services.schema.dataobject.servicecontext.ErrorDetails;
import com.tmobile.services.schema.dataobject.servicecontext.Key;
import com.tmobile.u2.exception.TMobileProcessException;
import com.tmobile.u2.service.ILogoutSOAPService;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.util.AuthenticateSOAPConnector;
import com.tmobile.u2.util.TibcoToJavaUtil;

import targetgsx.applications.gsx.endpoints.warranty.logoutsoap_start_input.LogoutSOAPStartInput;

/*************************************************************************
 * 
 * TMOBILE CONFIDENTIAL
 * _________________________________________________________________________________
 * 
 * TMOBILE is a trademark of TMOBILE Company.
 *
 * Copyright © 2021 TMOBILE. All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of
 * TMOBILE and its suppliers, if any. The intellectual and technical concepts
 * contained herein are proprietary to TMOBILE and its suppliers and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by
 * trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from TMOBILE.
 *
 *************************************************************************/
// Author : Generated by ATMA ®
// Revision History :  
@Service
public class LogoutSOAPServiceImpl implements ILogoutSOAPService {

	private static final Logger LOGGER = LoggerFactory.getLogger(LogoutSOAPServiceImpl.class);

	@Autowired
	private AuthenticateSOAPConnector sOAPConnector;

	public void setSOAPConnector(AuthenticateSOAPConnector sOAPConnector) {
		this.sOAPConnector = sOAPConnector;
	}

	@Autowired
	private ILogProcessDetailsService iLogProcessDetailsService;

	public void setLogProcessDetailsService(ILogProcessDetailsService iLogProcessDetailsService) {
		this.iLogProcessDetailsService = iLogProcessDetailsService;
	}

	/**
	 * Method logout
	 * 
	 * @param LogoutResponseWrapper logoutResponseMessage ,LogoutRequestWrapper
	 *                              logoutRequestMessage ,LogoutSOAPStartInput
	 *                              logoutSOAPStartInput
	 * @return void
	 * @throws TMobileProcessException
	 */
	@SuppressWarnings("unchecked")
	private void logout(LogoutResponseWrapper logoutResponseMessage, LogoutRequestWrapper logoutRequestMessage,
			LogoutSOAPStartInput logoutSOAPStartInput) throws TMobileProcessException {
		logoutRequestMessage.setLogoutRequest(new LogoutRequestType());
		logoutRequestMessage.getLogoutRequest().setUserSession(new GsxUserSessionType());
		logoutRequestMessage.getLogoutRequest().getUserSession()
				.setUserSessionId(logoutSOAPStartInput.getRoot().getSessionId());
		try {
			
			String soapURL = TibcoToJavaUtil.getProperty("TMO.Env.GSX.GSX_Authenticate_Host")
					+ ":" + TibcoToJavaUtil.getProperty("TMO.Env.GSX.GSX_Authenticate_Port")
					+ TibcoToJavaUtil.getProperty("TMO.Env.GSX.GSX_Authenticate_URI");
		
			JAXBElement<LogoutResponseWrapper> response = (JAXBElement<LogoutResponseWrapper>) sOAPConnector
					.callWebService(soapURL, logoutRequestMessage, "urn:logout");
			
			LogoutResponseWrapper soapResponseMessage = response.getValue();
			logoutResponseMessage.setLogoutResponse(soapResponseMessage.getLogoutResponse());
		} catch (Exception ex) {
			LOGGER.error("Error occurred in method: LogoutSOAP() and terminated due to ", ex);
			ErrorDetails errorDetails = new ErrorDetails();
			errorDetails.setCode(ex.getMessage());
			errorDetails.setMesssage(ex.getMessage());
			errorDetails.setStackTrace(ExceptionUtils.getStackTrace(ex));
			errorDetails.setProcessStack(Arrays.toString(ex.getStackTrace()));
			throw new TMobileProcessException(errorDetails);
		}
	}

	/**
	 * Method generateError
	 * 
	 * @param ErrorDetails errorDetails ,ErrorLinkHandlerFaultData
	 *                     errorLinkHandlerFaultData ,LogoutResponseWrapper
	 *                     logoutResponseMessage ,ErrorDetails errorDetails
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void generateError(ErrorDetails errorDetails) throws TMobileProcessException {
		ErrorShortMessage logoutErrorShortMessage = new ErrorShortMessage();
		DebugData debugData = new DebugData();
		debugData.setValue(TibcoToJavaUtil.renderXml(errorDetails));
		logoutErrorShortMessage.setDebugData(debugData);

		StackTrace stackTrace = new StackTrace();
		stackTrace.setValue(errorDetails.getStackTrace());
		logoutErrorShortMessage.setStackTrace(stackTrace);

		ProcessStack processStack = new ProcessStack();
		processStack.setValue(errorDetails.getProcessStack());
		logoutErrorShortMessage.setProcessStack(processStack);
		throw new TMobileProcessException(errorDetails);
	}

	/**
	 * Method logSuccess
	 * 
	 * @param LogoutResponseWrapper logoutResponseMessage ,LogoutSOAPStartInput
	 *                              logoutSOAPStartInput
	 *                              ,LogProcessDetailsStartInput
	 *                              logProcessDetailsStartInput
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void logSuccess(LogoutResponseWrapper logoutResponseMessage, LogoutSOAPStartInput logoutSOAPStartInput,
			LogProcessDetailsStartInput logProcessDetailsStartInput) throws TMobileProcessException {
		Root root = logProcessDetailsStartInput.getRoot();
		root.setIapName(logoutSOAPStartInput.getRoot().getIAPName());
		root.setMsgCode("DEBUG");
		root.setMessage("Response received from GSX OperationId: "
				+ logoutResponseMessage.getLogoutResponse().getOperationId());
		root.setLogType(TibcoToJavaUtil.getProperty("TMO_App_Global_ExceptionHandler_ExpHandler_Type_DEBUG"));
		root.setAction(TibcoToJavaUtil.getProperty("TMO_App_Global_ExceptionHandler_ExpHandler_Action_Notify"));
		root.setProcessName("LogoutSOAP");
		root.setTargetSystem("GSX");
		root.setTargetRequest(TibcoToJavaUtil.renderXml(logoutSOAPStartInput));
		root.setTargetResponse(TibcoToJavaUtil.renderXml(logoutResponseMessage));
		root.setRequestDatetime(LocalDateTime.now().toString());
		root.setRequestTimestamp(System.currentTimeMillis());
		Key key = new Key();
		key.setName("SessionID");
		root.getKey().add(key);
		key.setValue(logoutSOAPStartInput.getRoot().getSessionId());
		root.getKey().add(key);
		iLogProcessDetailsService.executeMethod(logProcessDetailsStartInput);
	}

	/**
	 * Method executeMethod
	 * 
	 * @param LogoutSOAPStartInput logoutSOAPStartInput
	 * @return void
	 * @throws TMobileProcessException
	 */
	public void executeMethod(LogoutSOAPStartInput logoutSOAPStartInput) throws TMobileProcessException {
		LogoutRequestWrapper logoutRequestMessage = new LogoutRequestWrapper();
		LogoutResponseWrapper logoutResponseMessage = new LogoutResponseWrapper();
		LogProcessDetailsStartInput logProcessDetailsStartInput = new LogProcessDetailsStartInput();
		ErrorDetails errorDetails = new ErrorDetails();
		try {
			logout(logoutResponseMessage, logoutRequestMessage, logoutSOAPStartInput);
			logSuccess(logoutResponseMessage, logoutSOAPStartInput, logProcessDetailsStartInput);
		} catch (TMobileProcessException ex) {
			LOGGER.error("Error occurred in method: LogoutSOAP() and terminated due to ", ex);
			if(null != ex.getErrorDetails()) {
				errorDetails = ex.getErrorDetails();
			}
			generateError(errorDetails);
		}

	}

}