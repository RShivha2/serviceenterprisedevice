package com.tmobile.u2.service.impl;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;

import javax.xml.bind.JAXB;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.xmlns.logvariable.LogVariables;
import com.example.xmlns.logvariable.Root;
import com.tibco.tns.bw.activity.jsonparser.xsd.output.GetAccessTokenOutput;
import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.services.schema.dataobject.sendhttprequest.requestactivityinput.RequestActivityInput;
import com.tmobile.services.schema.dataobject.sendhttprequest.requestactivityoutput.RequestActivityOutput;
import com.tmobile.services.schema.dataobject.sendhttprequest.requestactivityoutput.StatusLineType;
import com.tmobile.services.schema.dataobject.servicecontext.ErrorDetails;
import com.tmobile.u2.exception.TMobileProcessException;
import com.tmobile.u2.service.IGetAccessTokenService;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.util.TibcoToJavaUtil;

/*************************************************************************
 * 
 * TMOBILE CONFIDENTIAL
 * _________________________________________________________________________________
 * 
 * TMOBILE is a trademark of TMOBILE Company.
 *
 * Copyright © 2021 TMOBILE. All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of
 * TMOBILE and its suppliers, if any. The intellectual and technical concepts
 * contained herein are proprietary to TMOBILE and its suppliers and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by
 * trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from TMOBILE.
 *
 *************************************************************************/
// Author : Generated by ATMA ®
// Revision History :  
@Service
public class GetAccessTokenServiceImpl implements IGetAccessTokenService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GetAccessTokenServiceImpl.class);

	@Autowired
	private RestTemplate restTemplate;

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Autowired
	private ILogProcessDetailsService iLogProcessDetailsService;

	public void setLogProcessDetailsService(ILogProcessDetailsService iLogProcessDetailsService) {
		this.iLogProcessDetailsService = iLogProcessDetailsService;
	}

	/**
	 * Method assign
	 * 
	 * @param LogVariables logVariable
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void assign(LogVariables logVariable) throws TMobileProcessException {
		Root root = logVariable.getRoot();
		root.setRequestDateTime(LocalDateTime.now().toString());
		root.setRequestTimestamp(System.currentTimeMillis());
	}

	/**
	 * Method getAccessToken
	 * 
	 * @param RequestActivityOutput requestActivityOutput ,RequestActivityInput
	 *                              requestActivityInput
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void getAccessToken(RequestActivityOutput getAccessTokReqActivityOutput, RequestActivityInput getAccessTokReqActivityInput)
			throws TMobileProcessException {
		getAccessTokReqActivityInput
				.setHost(TibcoToJavaUtil.getProperty("TMO.U2.Env.Endpoints.API.REST.GSX_GetAccessToken_Host"));
		getAccessTokReqActivityInput.setPort(
				Integer.parseInt(TibcoToJavaUtil.getProperty("TMO.U2.Env.Endpoints.API.REST.GSX_GetAccessToken_Port")));
		getAccessTokReqActivityInput.setMethod("POST");
		getAccessTokReqActivityInput
				.setRequestURI(TibcoToJavaUtil.getProperty("TMO.U2.Env.Endpoints.API.REST.GSX_GetAccessToken_URI"));
		getAccessTokReqActivityInput
				.setTimeout(Integer.parseInt(TibcoToJavaUtil.getProperty("TMO.Timeouts.TimeoutMediumMilliSecs")));
		getAccessTokReqActivityInput.getHeaders().setAccept("application/json");
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.set("Authorization",
					"Basic " + TibcoToJavaUtil.getProperty("TMO.U2.Env.Endpoints.API.GSX.BasicAuthToken"));
			LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
			httpHeaders.setContentType(MediaType.valueOf(getAccessTokReqActivityInput.getHeaders().getContentType()));
			httpHeaders.setAccept(
					Collections.singletonList(MediaType.valueOf(getAccessTokReqActivityInput.getHeaders().getAccept())));
			HttpMethod httpMethod = "POST".equalsIgnoreCase(getAccessTokReqActivityInput.getMethod()) ? HttpMethod.POST
					: HttpMethod.GET;
			UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(getAccessTokReqActivityInput.getRequestURI())
					.queryParams(params).build();
			ResponseEntity<String> responseEntity = restTemplate.exchange(uriComponents.toUri(),
					httpMethod, new HttpEntity<>(getAccessTokReqActivityInput.getPostData(), httpHeaders),
					String.class);
			String response = responseEntity.getBody();
			if (responseEntity.getStatusCode().is4xxClientError()
					|| responseEntity.getStatusCode().is5xxServerError()) {

				String getAccessTokErrorMessage = responseEntity.getBody();
				ErrorDetails errorDetails = new ErrorDetails();
				errorDetails.setTargetSystem("getAccessToken");
				errorDetails.setCode(String.valueOf(responseEntity.getStatusCodeValue()));
				errorDetails.setMesssage(getAccessTokErrorMessage);
				errorDetails.setSourceMessage(TibcoToJavaUtil.renderXml(response));
				throw new TMobileProcessException(errorDetails);
			}
			if (response != null) {
				StatusLineType statusLine = new StatusLineType();
				statusLine.setStatusCode(responseEntity.getStatusCodeValue());
				statusLine.setReasonPhrase(responseEntity.getStatusCode().getReasonPhrase());
				statusLine.setReasonPhrase(responseEntity.getStatusCode().getReasonPhrase());
				getAccessTokReqActivityOutput.setStatusLine(statusLine);
				getAccessTokReqActivityOutput.setAsciiContent(response);
				getAccessTokReqActivityOutput.setBinaryContent(responseEntity.getBody().getBytes());
			}
		} catch (Exception ex) {
			LOGGER.error("Error occurred in method: GetWarrantyStatusHTTP() and terminated due to ", ex);
			ErrorDetails errorDetails = new ErrorDetails();
			errorDetails.setCode(ex.getMessage());
			errorDetails.setMesssage(ex.getMessage());
			errorDetails.setStackTrace(ExceptionUtils.getStackTrace(ex));
			errorDetails.setProcessStack(Arrays.toString(ex.getStackTrace()));
			throw new TMobileProcessException(errorDetails);
		}
	}

	/**
	 * Method sleep
	 * 
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void sleep() throws TMobileProcessException {
		try {
			Thread.sleep(Long.valueOf(TibcoToJavaUtil.getProperty("TMO.U2.Env.GSX.SleepIntervalInMilliSecs")));
		} catch (NumberFormatException | InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new TMobileProcessException(e);
		}
	}

	/**
	 * Method generateAuthError
	 * 
	 * @param ErrorDetails errorDetails
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void generateAuthError(ErrorDetails errorDetails)
			throws TMobileProcessException {
		errorDetails.setTargetSystem("API");
		throw new TMobileProcessException(errorDetails);
	}

	/**
	 * Method innerScope
	 * 
	 * @param GetAccessTokenOutput getAccessTokenOutput
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void innerScope(RequestActivityOutput requestActivityOutput) throws TMobileProcessException {
		RequestActivityInput requestActivityInput = new RequestActivityInput();
		try {
			getAccessToken(requestActivityOutput, requestActivityInput);
		} catch (TMobileProcessException ex) {
			LOGGER.error("Error occurred in method: GetAccessToken() and terminated due to ", ex);
			sleep();
			generateAuthError(ex.getErrorDetails());
		}

	}

	/**
	 * Method repeatOnError
	 * 
	 * @param GetAccessTokenOutput getAccessTokenOutput
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void repeatOnError(RequestActivityOutput requestActivityOutput) throws TMobileProcessException {
		boolean repeatOnErrorSuccessFlag = false;
		for (Integer index = 1; index <= 3; index++) {
			try {
				innerScope(requestActivityOutput);
			} catch (TMobileProcessException ex) {
				repeatOnErrorSuccessFlag = true;
				if(index > 3)
					generateAuthError(ex.getErrorDetails());
			}
			if (!repeatOnErrorSuccessFlag) {
				break;
			}
		}
	}

	/**
	 * Method parseJSON
	 * 
	 * @param ActivityInputClass activityInputClass ,RequestActivityOutput
	 *                           requestActivityOutput ,GetAccessTokenOutput
	 *                           getAccessTokenOutput
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void parseJSON(RequestActivityOutput requestActivityOutput, GetAccessTokenOutput getAccessTokenOutput)
			throws TMobileProcessException {
		GetAccessTokenOutput parsedOutput = JAXB.unmarshal(
				new ByteArrayInputStream(requestActivityOutput.getAsciiContent().getBytes(StandardCharsets.UTF_8)),
				GetAccessTokenOutput.class);
		getAccessTokenOutput.setAccessToken(parsedOutput.getAccessToken());
		getAccessTokenOutput.setExpiresIn(parsedOutput.getExpiresIn());
		getAccessTokenOutput.setRefreshToken(parsedOutput.getRefreshToken());
		getAccessTokenOutput.setScope(parsedOutput.getScope());
		getAccessTokenOutput.setTokenType(parsedOutput.getTokenType());
	}

	/**
	 * Method logProcess
	 * 
	 * @param ErrorDetails errorDetails ,ErrorLinkHandlerFaultData
	 *                     errorLinkHandlerFaultData ,LogVariables logVariable
	 *                     ,RequestActivityOutput requestActivityOutput
	 *                     ,LogProcessDetailsStartInput logProcessDetailsStartInput
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void logProcess(ErrorDetails errorDetails, LogVariables logVariable,
			RequestActivityOutput requestActivityOutput, LogProcessDetailsStartInput logProcessDetailsStartInput)
			throws TMobileProcessException {
		com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.Root root = logProcessDetailsStartInput
				.getRoot();
		if (errorDetails.getCode() != null) {
			root.setMsgCode(errorDetails.getCode());
			root.setMessage(errorDetails.getMesssage());
		}
		if (requestActivityOutput.getStatusLine().getStatusCode() != 200) {
			root.setStackTrace(TibcoToJavaUtil.renderXml(requestActivityOutput));
		} else {
			root.setStackTrace(TibcoToJavaUtil.renderXml(errorDetails));
		}
		if (errorDetails.getProcessStack() != null) {
			root.setProcessTack(errorDetails.getProcessStack());
		}
		root.setProcessName("getAccessToken");
		root.setTargetSystem("API");
		if (requestActivityOutput.getAsciiContent() != null) {
			root.setTargetResponse(requestActivityOutput.getAsciiContent());
		} else {
			root.setTargetResponse(TibcoToJavaUtil.renderXml(errorDetails));
		}
		if (logVariable.getRoot().getRequestDateTime() != null) {
			root.setRequestDatetime(logVariable.getRoot().getRequestDateTime());
		}
		if (logVariable.getRoot().getRequestTimestamp() != null) {
			root.setRequestTimestamp(logVariable.getRoot().getRequestTimestamp());
		}
		iLogProcessDetailsService.executeMethod(logProcessDetailsStartInput);
	}

	/**
	 * Method executeMethod
	 * 
	 * @param GetAccessTokenOutput getAccessTokenOutput
	 * @return void
	 * @throws TMobileProcessException
	 */
	public void executeMethod(GetAccessTokenOutput getAccessTokenOutput) throws TMobileProcessException {
		LogVariables logVariable = new LogVariables();
		RequestActivityOutput requestActivityOutput = new RequestActivityOutput();
		ErrorDetails errorDetails = new ErrorDetails();
		LogProcessDetailsStartInput logProcessDetailsStartInput = new LogProcessDetailsStartInput();
		assign(logVariable);
		try {
			repeatOnError(requestActivityOutput);
		} catch (TMobileProcessException ex) {
			LOGGER.error("Error occurred in method: GetAccessToken() and terminated due to ", ex);
			if(null != ex.getErrorDetails())
				errorDetails = ex.getErrorDetails();
		}

		parseJSON(requestActivityOutput, getAccessTokenOutput);
		logProcess(errorDetails, logVariable, requestActivityOutput, logProcessDetailsStartInput);
		if (errorDetails.getCode() != null || requestActivityOutput.getStatusLine().getStatusCode() != 200) {
			generateAuthError(errorDetails);
		}
	}

}