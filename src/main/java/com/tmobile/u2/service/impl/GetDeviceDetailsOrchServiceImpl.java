package com.tmobile.u2.service.impl;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.example.xmlns._1386753969593.logvariables.LogVariables;
import com.example.xmlns._1419836055689.dmwresponse.GetDeviceDetailsOrchDMWResponse;
import com.example.xmlns._1419836055689.warrantydetails.Root;
import com.example.xmlns._1419836055689.warrantydetails.WarrantyDetails;
import com.t_mobile.common.imeiwarranty.DateRangeType;
import com.t_mobile.common.imeiwarranty.ImeiWarranty;
import com.t_mobile.common.imeiwarranty.WarrantyDetailInfo;
import com.t_mobile.csi.base.CommonStatusType;
import com.t_mobile.csi.base.HeaderType;
import com.t_mobile.csi.device.getimei.CSIGetIMEIRequestType;
import com.t_mobile.csi.device.getimei.CSIGetIMEIResponseType;
import com.t_mobile.csi.device.getimei.CSIGetIMEIResponseType.IMEIInfo;
import com.t_mobile.csi.device.queryimeiwarranty.CSIQueryIMEIWarrantyRequestType;
import com.t_mobile.csi.device.queryimeiwarranty.CSIQueryIMEIWarrantyResponseType;
import com.t_mobile.csi.device.queryimeiwarranty.CSIQueryIMEIWarrantyResponseType.ImeiWarrantyDetails;
import com.t_mobile.schema.networkmanagement.union.resources.schemas.devicespecifications.DeviceSpecificationDetails;
import com.tibco.namespaces.tnt.plugins.generalactivities.mapper._1573730872785.MapperInputType;
import com.tmobile.services.base.Sender;
import com.tmobile.services.base.SpecificationValue;
import com.tmobile.services.co.warranty.WarrantyDetail;
import com.tmobile.services.csi.device.getdevicewarrantydetail.GetDeviceWarrantyDetailRequestType;
import com.tmobile.services.csi.device.getdevicewarrantydetail.GetDeviceWarrantyDetailRequestType.DeviceInfo;
import com.tmobile.services.csi.device.getshipusagedataforimei.GetShipUsageDataForIMEIRequestType;
import com.tmobile.services.csi.device.getshipusagedataforimei.GetShipUsageDataForIMEIResponseType;
import com.tmobile.services.productmanagement.deviceenterprise.v1.ActivityInstance;
import com.tmobile.services.productmanagement.deviceenterprise.v1.ActivityInstanceStatus;
import com.tmobile.services.productmanagement.deviceenterprise.v1.Attachment;
import com.tmobile.services.productmanagement.deviceenterprise.v1.Description;
import com.tmobile.services.productmanagement.deviceenterprise.v1.DescriptionByChannel;
import com.tmobile.services.productmanagement.deviceenterprise.v1.DeviceRooting;
import com.tmobile.services.productmanagement.deviceenterprise.v1.GetDeviceDetailsRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.GetDeviceDetailsResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.Indicator;
import com.tmobile.services.productmanagement.deviceenterprise.v1.Manufacturer;
import com.tmobile.services.productmanagement.deviceenterprise.v1.OfferingVariant;
import com.tmobile.services.productmanagement.deviceenterprise.v1.ProductOffering;
import com.tmobile.services.productmanagement.deviceenterprise.v1.ProductSpecificationBase;
import com.tmobile.services.productmanagement.deviceenterprise.v1.SerialNumberType;
import com.tmobile.services.productmanagement.deviceenterprise.v1.SpecificationGroup;
import com.tmobile.services.productmanagement.deviceenterprise.v1.TheftLock;
import com.tmobile.services.productmanagement.deviceenterprise.v1.TheftLockType;
import com.tmobile.services.productmanagement.deviceenterprise.v1.TimePeriod;
import com.tmobile.services.productmanagement.deviceenterprise.v1.Warranty;
import com.tmobile.services.schema.dataobject.guid.Guid;
import com.tmobile.services.schema.dataobject.logwarningdetailsstartinput.LogwarningdetailsStartInput;
import com.tmobile.services.schema.dataobject.responsestatus.ResponseStatus;
import com.tmobile.services.schema.dataobject.responsestatus.ResponseStatusDetail;
import com.tmobile.services.schema.dataobject.servicecontext.ErrorDetails;
import com.tmobile.services.schema.dataobject.servicecontext.SubStatus;
import com.tmobile.u2.exception.TMobileProcessException;
import com.tmobile.u2.service.IGetDeviceDetailsOrchService;
import com.tmobile.u2.service.IGetDeviceSpecificationsRESTService;
import com.tmobile.u2.service.IGetDeviceWarrantyDetailOrchService;
import com.tmobile.u2.service.IInvokeGetIMEIStatusService;
import com.tmobile.u2.service.IInvokeGetShipUsageDataService;
import com.tmobile.u2.service.IInvokeQueryIMEIWarrantyService;
import com.tmobile.u2.service.ILuhnCheckValidationService;
import com.tmobile.u2.shared.service.ILogWarningDetailsService;
import com.tmobile.u2.util.SharedServiceContext;
import com.tmobile.u2.util.TibcoToJavaUtil;

import serviceenterprisedevice.common.subprocesses.businessrules.luhncheckvalidation_end_output.LuhnCheckValidationEndOutput;
import serviceenterprisedevice.common.subprocesses.businessrules.luhncheckvalidation_start_input.LuhnCheckValidationStartInput;
import targetgsx.applications.gsx.orchestrations.warranty.getdevicewarrantydetailorch_end_output.GetDeviceDetailsOrchEndOutput;
import targetgsx.applications.gsx.orchestrations.warranty.getdevicewarrantydetailorch_start_input.GetDeviceDetailsOrchStartInput;
import targetunion.applications.union.endpoints.getdevicespecificationsrest_start_input.GetDeviceSpecificationsRequest;

/*************************************************************************
 * 
 * TMOBILE CONFIDENTIAL
 * _________________________________________________________________________________
 * 
 * TMOBILE is a trademark of TMOBILE Company.
 *
 * Copyright © 2021 TMOBILE. All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of
 * TMOBILE and its suppliers, if any. The intellectual and technical concepts
 * contained herein are proprietary to TMOBILE and its suppliers and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by
 * trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from TMOBILE.
 *
 *************************************************************************/
// Author : Generated by ATMA ®
// Revision History :  
@Service
public class GetDeviceDetailsOrchServiceImpl implements IGetDeviceDetailsOrchService {

	private static final String DD_MMM_YY = "dd-MMM-yy";

	private static final String YYYY_MM_DD = "yyyy-MM-dd";

	private static final String ANTITHEFT = "ANTITHEFT";

	private static final String ANDROID = "ANDROID";

	private static final String OTHER = "OTHER";

	private static final String ERROR_SOURCE = "ERROR_SOURCE: ";

	private static final String UNKNOWN_BACKEND_ERROR = "Unknown Backend Error ";

	private static final String GENERAL_0000 = "General-0000";

	private static final String TMO_APP_HANA_ENABLE_HANA_FLOW = "TMO.App.HANA.EnableHANAFlow";

	private static final String APPLE = "APPLE";

	private static final String IPHONE = "IPHONE";

	private static final Logger LOGGER = LoggerFactory.getLogger(GetDeviceDetailsOrchServiceImpl.class);

	@Autowired
	private IInvokeQueryIMEIWarrantyService iInvokeQueryIMEIWarrantyService;

	public void setInvokeQueryIMEIWarrantyService(IInvokeQueryIMEIWarrantyService iInvokeQueryIMEIWarrantyService) {
		this.iInvokeQueryIMEIWarrantyService = iInvokeQueryIMEIWarrantyService;
	}

	@Autowired
	private IInvokeGetIMEIStatusService iInvokeGetIMEIStatusService;

	public void setInvokeGetIMEIStatusService(IInvokeGetIMEIStatusService iInvokeGetIMEIStatusService) {
		this.iInvokeGetIMEIStatusService = iInvokeGetIMEIStatusService;
	}

	@Autowired
	private ILuhnCheckValidationService iLuhnCheckValidationService;

	public void setLuhnCheckValidationService(ILuhnCheckValidationService iLuhnCheckValidationService) {
		this.iLuhnCheckValidationService = iLuhnCheckValidationService;
	}

	@Autowired
	private ILogWarningDetailsService iLogWarningDetailsService;

	public void setLogWarningDetailsService(ILogWarningDetailsService iLogWarningDetailsService) {
		this.iLogWarningDetailsService = iLogWarningDetailsService;
	}

	@Autowired
	private IInvokeGetShipUsageDataService iInvokeGetShipUsageDataService;

	public void setInvokeGetShipUsageDataService(IInvokeGetShipUsageDataService iInvokeGetShipUsageDataService) {
		this.iInvokeGetShipUsageDataService = iInvokeGetShipUsageDataService;
	}

	@Autowired
	private IGetDeviceWarrantyDetailOrchService iGetDeviceWarrantyDetailOrchService;

	public void setGetDeviceWarrantyDetailOrchService(
			IGetDeviceWarrantyDetailOrchService iGetDeviceWarrantyDetailOrchService) {
		this.iGetDeviceWarrantyDetailOrchService = iGetDeviceWarrantyDetailOrchService;
	}

	@Autowired
	private IGetDeviceSpecificationsRESTService iGetDeviceSpecificationsRESTService;

	public void setGetDeviceSpecificationsRESTService(
			IGetDeviceSpecificationsRESTService iGetDeviceSpecificationsRESTService) {
		this.iGetDeviceSpecificationsRESTService = iGetDeviceSpecificationsRESTService;
	}

	@Autowired
	private SharedServiceContext sharedServiceContext;

	public void setSharedServiceContext(SharedServiceContext sharedServiceContext) {
		this.sharedServiceContext = sharedServiceContext;
	}

	/**
	 * Method logVariables
	 * 
	 * @param LogVariables logVariables
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void logVariables(LogVariables logVariables) throws TMobileProcessException {
		com.example.xmlns._1386753969593.logvariables.Root root = logVariables.getRoot();
		root.setRequestDate(LocalDateTime.now().toString());
		root.setRequestTimeStamp(LocalDateTime.now().toString());
	}

	/**
	 * Method invokeQueryImeiWarranty
	 * 
	 * @param GetDeviceDetailsRequest getDeviceDetailsRequest
	 *                                ,CSIQueryIMEIWarrantyResponseType
	 *                                queryIMEIWarrantyResponse
	 *                                ,QueryIMEIWarrantyRequest
	 *                                queryIMEIWarrantyRequest
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void invokeQueryImeiWarranty(GetDeviceDetailsRequest getDeviceDetailsRequest,
			CSIQueryIMEIWarrantyResponseType queryIMEIWarrantyResponse,
			CSIQueryIMEIWarrantyRequestType queryIMEIWarrantyRequest) throws TMobileProcessException {
		Sender headerSenderLocal = getDeviceDetailsRequest.getHeader().getSender();
		HeaderType requestHeader = new HeaderType();
		queryIMEIWarrantyRequest.setRequestHeader(requestHeader);
		if (null != getDeviceDetailsRequest.getDeviceBrandId()
				&& "Brightspot".equalsIgnoreCase(getDeviceDetailsRequest.getDeviceBrandId().value())) {
			requestHeader.setApplicationId("BSP");
		}
		if (null != getDeviceDetailsRequest.getDeviceBrandId()
				&& "MetroPCS".equalsIgnoreCase(getDeviceDetailsRequest.getDeviceBrandId().value())) {
			requestHeader.setApplicationId("2IP");
		}
		if (null != getDeviceDetailsRequest.getDeviceBrandId()
				&& "UVM".equalsIgnoreCase(getDeviceDetailsRequest.getDeviceBrandId().value())) {
			requestHeader.setApplicationId("UVM");
		}
		if (null != getDeviceDetailsRequest.getDeviceBrandId()
				&& "T-Mobile".equalsIgnoreCase(getDeviceDetailsRequest.getDeviceBrandId().value())) {
			requestHeader.setApplicationId(" ");
		}
		requestHeader.setChannelId(headerSenderLocal.getChannelId());
		if (headerSenderLocal.getApplicationUserId() != null) {
			requestHeader.setApplicationUserId(headerSenderLocal.getApplicationUserId());
		}
		if (headerSenderLocal.getStoreId() != null) {
			requestHeader.setStoreId(headerSenderLocal.getStoreId());
		}
		if (headerSenderLocal.getDealerCode() != null) {
			requestHeader.setDealerCode(headerSenderLocal.getDealerCode());
		}
		requestHeader.setSenderId(headerSenderLocal.getSenderId());
		if (headerSenderLocal.getSessionId() != null) {
			requestHeader.setSessionId(headerSenderLocal.getSessionId());
		}
		if (headerSenderLocal.getWorkflowId() != null) {
			requestHeader.setWorkflowId(headerSenderLocal.getWorkflowId());
		}
		if (headerSenderLocal.getActivityId() != null) {

			requestHeader.setActivityId(headerSenderLocal.getActivityId());
		}

		queryIMEIWarrantyRequest.getMsisdn().addAll(getDeviceDetailsRequest.getMSISDN());
		queryIMEIWarrantyRequest.setIncludeHistory(getDeviceDetailsRequest.isDeviceHistoryIndicator());
		queryIMEIWarrantyRequest.setReturnInsuranceDetails(true);
		iInvokeQueryIMEIWarrantyService.executeMethod(queryIMEIWarrantyRequest, queryIMEIWarrantyResponse);
	}

	/**
	 * Method initTransformedimeiNoMsisdnResponse
	 * 
	 * @param GetDeviceDetailsRequest getDeviceDetailsRequest ,WarrantyDetails
	 *                                warrantyDetails
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void initTransformedimeiNoMsisdnResponse(GetDeviceDetailsRequest getDeviceDetailsRequest,
			WarrantyDetails warrantyDetails) throws TMobileProcessException {
		Root root = warrantyDetails.getRoot();
		if (getDeviceDetailsRequest.getImei() != null) {
			ImeiWarranty imeiWarrenty = new ImeiWarranty();
			imeiWarrenty.setIMEI(getDeviceDetailsRequest.getImei());
			root.getImeiWarranty().add(imeiWarrenty);
		}
	}

	/**
	 * Method invokeGetimeistatusforml
	 * 
	 * @param GetDeviceDetailsRequest
	 *                              getDeviceDetailsRequest ,CSIGetIMEIResponseType
	 *                              csiGetIMEIResponse
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void invokeGetimeistatusforml(
			GetDeviceDetailsRequest getDeviceDetailsRequest, CSIGetIMEIResponseType csiGetIMEIResponse)
			throws TMobileProcessException {
		CSIGetIMEIRequestType invokeGetIMEIRequest = new CSIGetIMEIRequestType();
		GetDeviceDetailsRequest varRequest = getDeviceDetailsRequest;
		invokeGetIMEIRequest.setRequestHeader(new HeaderType());
		invokeGetIMEIRequest.getRequestHeader().setApplicationId(varRequest.getHeader().getSender().getApplicationId());
		invokeGetIMEIRequest.getRequestHeader().setChannelId(varRequest.getHeader().getSender().getChannelId());
		if (varRequest.getHeader().getSender().getTimestamp() != null) {
			invokeGetIMEIRequest.getRequestHeader()
					.setRequestStartTime(varRequest.getHeader().getSender().getTimestamp());
		}
		if (varRequest.getHeader().getSender().getApplicationUserId() != null) {
			invokeGetIMEIRequest.getRequestHeader()
					.setApplicationUserId(varRequest.getHeader().getSender().getApplicationUserId());
		}
		if (varRequest.getHeader().getSender().getStoreId() != null) {
			invokeGetIMEIRequest.getRequestHeader().setStoreId(varRequest.getHeader().getSender().getStoreId());
		}
		if (varRequest.getHeader().getSender().getDealerCode() != null) {
			invokeGetIMEIRequest.getRequestHeader().setDealerCode(varRequest.getHeader().getSender().getDealerCode());
		}
		invokeGetIMEIRequest.getRequestHeader().setSenderId(varRequest.getHeader().getSender().getSenderId());
		if (varRequest.getHeader().getSender().getSessionId() != null) {
			invokeGetIMEIRequest.getRequestHeader().setSessionId(varRequest.getHeader().getSender().getSessionId());
		}
		if (varRequest.getHeader().getSender().getWorkflowId() != null) {
			invokeGetIMEIRequest.getRequestHeader().setWorkflowId(varRequest.getHeader().getSender().getWorkflowId());
		}
		if (varRequest.getHeader().getSender().getActivityId() != null) {
			invokeGetIMEIRequest.getRequestHeader().setActivityId(varRequest.getHeader().getSender().getActivityId());
		}
		for (String msisdnLocal : varRequest.getMSISDN()) {
			invokeGetIMEIRequest.getMsisdn().add("1" + msisdnLocal);
		}
		iInvokeGetIMEIStatusService.executeMethod(invokeGetIMEIRequest, csiGetIMEIResponse);
	}

	/**
	 * Method assign
	 * 
	 * @param CSIQueryIMEIWarrantyResponseType queryIMEIWarrantyResponse
	 *                                         ,GetDeviceDetailsOrchDMWResponse
	 *                                         getDeviceDetailsOrchDMWResponse
	 *                                         ,CSIGetIMEIResponseType
	 *                                         csiGetIMEIResponse
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void assign(CSIQueryIMEIWarrantyResponseType queryIMEIWarrantyResponse,
			GetDeviceDetailsOrchDMWResponse getDeviceDetailsOrchDMWResponse, CSIGetIMEIResponseType csiGetIMEIResponse)
			throws TMobileProcessException {
		com.example.xmlns._1419836055689.dmwresponse.Root root = getDeviceDetailsOrchDMWResponse.getRoot();
		if (!CollectionUtils.isEmpty(queryIMEIWarrantyResponse.getImeiWarrantyDetails())) {
			for (ImeiWarrantyDetails warrantyDetails : queryIMEIWarrantyResponse.getImeiWarrantyDetails()) {
				root.getIMEI().add(warrantyDetails.getIMEI());
			}
		} else {
			for (IMEIInfo imeiInfo : csiGetIMEIResponse.getImeiInfo()) {
				root.getIMEI().add(imeiInfo.getImei());
			}
		}
		for (IMEIInfo imeiInfo : csiGetIMEIResponse.getImeiInfo()) {
			root.getMSISDN().add(imeiInfo.getSimNumber().substring(1));
		}
	}

	/**
	 * Method luhnCheckValidation
	 * 
	 * @param LuhnCheckValidationStartInput luhnCheckValidationStartInput
	 *                                      ,LuhnCheckValidationEndOutput
	 *                                      luhnCheckValidationEndOutput
	 *                                      ,ImeiIterationelem imeiIterationelem
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void luhnCheckValidation(MapperInputType lunhIMEI, String imeiIterationelem)
			throws TMobileProcessException {
		LuhnCheckValidationStartInput luhnCheckValidationStartInput = new LuhnCheckValidationStartInput();
		LuhnCheckValidationEndOutput luhnCheckValidationEndOutput = new LuhnCheckValidationEndOutput();
		ErrorDetails errorDetails = new ErrorDetails();
		try {
			serviceenterprisedevice.common.subprocesses.businessrules.luhncheckvalidation_start_input.Root root = luhnCheckValidationStartInput
					.getRoot();
			root.setIMEI(imeiIterationelem);
			iLuhnCheckValidationService.executeMethod(luhnCheckValidationStartInput, luhnCheckValidationEndOutput);
		} catch (TMobileProcessException ex) {
			if (ex.getErrorDetails() != null)
				errorDetails = ex.getErrorDetails();
			logError(errorDetails);
		}
		transformedIMEI(luhnCheckValidationEndOutput, lunhIMEI, imeiIterationelem, errorDetails);
	}

	/**
	 * Method logError
	 * 
	 * @param LogwarningdetailsStartInput logWarningDetailsStartInput
	 *                                    ,ErrorLinkHandlerFaultData
	 *                                    errorLinkHandlerFaultData
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void logError(ErrorDetails errorLinkHandlerFaultData) throws TMobileProcessException {
		LogwarningdetailsStartInput logWarningDetailsStartInput = new LogwarningdetailsStartInput();
		com.tmobile.services.schema.dataobject.logwarningdetailsstartinput.Root getDeviceOrchLogError = logWarningDetailsStartInput
				.getRoot();
		getDeviceOrchLogError.setTargetSystem("Luhn");
		if (errorLinkHandlerFaultData.getCode() != null) {
			getDeviceOrchLogError.setErrorCode(errorLinkHandlerFaultData.getCode());
		}
		if (errorLinkHandlerFaultData.getMesssage() != null) {
			getDeviceOrchLogError.setErrorMessage(errorLinkHandlerFaultData.getMesssage());
		}
		if (errorLinkHandlerFaultData.getStackTrace() != null) {
			getDeviceOrchLogError.setStackTrace(errorLinkHandlerFaultData.getStackTrace());
		}
		if (errorLinkHandlerFaultData.getProcessStack() != null) {
			getDeviceOrchLogError.setProcessStack(errorLinkHandlerFaultData.getProcessStack());
		}
		iLogWarningDetailsService.executeMethod(logWarningDetailsStartInput);
	}

	/**
	 * Method transformedIMEI
	 * 
	 * @param errorDetails
	 * 
	 * @param ErrorLinkHandlerFaultData errorLinkHandlerFaultData
	 *                                  ,LuhnCheckValidationEndOutput
	 *                                  luhnCheckValidationEndOutput ,1573730872785
	 *                                  1573730872785 ,ImeiIterationelem
	 *                                  imeiIterationelem
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void transformedIMEI(LuhnCheckValidationEndOutput luhnCheckValidationEndOutput, MapperInputType lunhIMEI,
			String imeiIterationelem, ErrorDetails errorDetails) throws TMobileProcessException {
		lunhIMEI.setIMEI(imeiIterationelem);
		if (luhnCheckValidationEndOutput.getRoot().getTransformedIMEI() != null) {
			lunhIMEI.setTransformedIMEI(luhnCheckValidationEndOutput.getRoot().getTransformedIMEI());
		}

		if (errorDetails.getMesssage() != null) {
			lunhIMEI.setErrorMsg(errorDetails.getMesssage());
		}
	}

	/**
	 * Method innerScope
	 * 
	 * @param warranty
	 * 
	 * @param
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void innerScope(String imeiIterationelem, MapperInputType lunhIMEI) throws TMobileProcessException {
		luhnCheckValidation(lunhIMEI, imeiIterationelem);
	}

	/**
	 * Method group
	 * 
	 * @param
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void group(GetDeviceDetailsOrchDMWResponse getDeviceDetailsOrchDMWResponse, MapperInputType lunhIMEI,
			StringBuilder warranty) throws TMobileProcessException {
		if (!CollectionUtils.isEmpty(getDeviceDetailsOrchDMWResponse.getRoot().getIMEI())) {
			innerScope(getDeviceDetailsOrchDMWResponse.getRoot().getIMEI().get(0), lunhIMEI);
			warranty.append(getDeviceDetailsOrchDMWResponse.getRoot().getIMEI().get(0));
		}

	}

	/**
	 * Method initTransformedimeiResponse
	 * 
	 * @param GetDeviceDetailsRequest getDeviceDetailsRequest ,WarrantyDetails
	 *                                warrantyDetails
	 *                                ,CSIQueryIMEIWarrantyResponseType
	 *                                queryIMEIWarrantyResponse ,MapperInputType
	 *                                accumulatedOutput ,CSIGetIMEIResponseType
	 *                                csiGetIMEIResponse
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void initTransformedimeiResponse(GetDeviceDetailsRequest getDeviceDetailsRequest,
			WarrantyDetails warrantyDetails, CSIQueryIMEIWarrantyResponseType queryIMEIWarrantyResponse,
			MapperInputType lunhIMEI, CSIGetIMEIResponseType csiGetIMEIResponse) throws TMobileProcessException {
		Root root = warrantyDetails.getRoot();
		List<ImeiWarrantyDetails> varWarrantyDetails = queryIMEIWarrantyResponse.getImeiWarrantyDetails();
		List<IMEIInfo> varIMEIStatus = csiGetIMEIResponse.getImeiInfo();
		String varIMEI = null != getDeviceDetailsRequest.getImei()
				&& getDeviceDetailsRequest.getImei().equalsIgnoreCase(lunhIMEI.getTransformedIMEI())
						? getDeviceDetailsRequest.getImei()
						: "";
		if (varIMEI.length() > 0 && !CollectionUtils.isEmpty(varWarrantyDetails)) {
			for (ImeiWarrantyDetails varImeiWarranty : varWarrantyDetails) {
				if (varIMEI.equalsIgnoreCase(varImeiWarranty.getIMEI())) {
					ImeiWarranty imeiWarrantyLocal = new ImeiWarranty();
					imeiWarrantyLocal.setIMEI(varImeiWarranty.getIMEI());
					imeiWarrantyLocal.setManufacturer(varImeiWarranty.getManufacturer());
					imeiWarrantyLocal.setModel(varImeiWarranty.getModel());
					imeiWarrantyLocal.setFirstUseDate(varImeiWarranty.getFirstUseDate());
					imeiWarrantyLocal.setLastUseDate(varImeiWarranty.getLastUseDate());
					imeiWarrantyLocal.setDeviceType(varImeiWarranty.getDeviceType());
					imeiWarrantyLocal.setDeviceWarrantyExpirationDate(varImeiWarranty.getDeviceWarrantyExpirationDate());
					imeiWarrantyLocal.setAccessoryWarrantyExpirationDate(varImeiWarranty.getAccessoryWarrantyExpirationDate());
					imeiWarrantyLocal.setMsisdn(varImeiWarranty.getMsisdn());
					imeiWarrantyLocal.setMyFaves(varImeiWarranty.isMyFaves());
					imeiWarrantyLocal.setFavesVersion(varImeiWarranty.getFavesVersion());
					imeiWarrantyLocal.setRepairCode(varImeiWarranty.getRepairCode());
					imeiWarrantyLocal.setShipmentType(varImeiWarranty.getShipmentType());

					imeiWarrantyLocal.setWarrantyDetailInfo(new WarrantyDetailInfo());
					setWarrantyDetailInfo(varImeiWarranty, imeiWarrantyLocal);
					imeiWarrantyLocal.setTmoMerchandise(varImeiWarranty.isTmoMerchandise());
					root.getImeiWarranty().add(imeiWarrantyLocal);
				}
			}
		}
		setImeiWarranty(root, varWarrantyDetails, varIMEIStatus, varIMEI);
	}

	/**
	 * @param root
	 * @param varWarrantyDetails
	 * @param varIMEIStatus
	 * @param varIMEI
	 */
	private void setImeiWarranty(Root root, List<ImeiWarrantyDetails> varWarrantyDetails, List<IMEIInfo> varIMEIStatus,
			String varIMEI) {
		if (varIMEI.length() > 0 && !CollectionUtils.isEmpty(varIMEIStatus)) {
			for (IMEIInfo imeiInfo : varIMEIStatus) {
				if (varIMEI.equalsIgnoreCase(imeiInfo.getImei())) {
					ImeiWarranty imeiWarranty = new ImeiWarranty();
					imeiWarranty.setIMEI(imeiInfo.getImei());
					imeiWarranty.setManufacturer(imeiInfo.getMake());
					imeiWarranty.setModel(imeiInfo.getModel());
					root.getImeiWarranty().add(imeiWarranty);
				}
			}
		} else {
			Integer var2 = Integer
					.valueOf(TibcoToJavaUtil.getProperty("TMO_App_ProductManagement_Device_lookupThresholdValue"));
			for (int position = 0; position < varWarrantyDetails.size(); position++) {
				ImeiWarrantyDetails warranty = varWarrantyDetails.get(position);

				if (position <= var2) {
					ImeiWarranty imeiWarranty = new ImeiWarranty();

					imeiWarranty.setIMEI(warranty.getIMEI());
					imeiWarranty.setManufacturer(warranty.getManufacturer());
					imeiWarranty.setModel(warranty.getModel());
					imeiWarranty.setFirstUseDate(warranty.getFirstUseDate());
					imeiWarranty.setLastUseDate(warranty.getLastUseDate());
					imeiWarranty.setDeviceType(warranty.getDeviceType());
					imeiWarranty.setDeviceWarrantyExpirationDate(warranty.getDeviceWarrantyExpirationDate());
					imeiWarranty.setAccessoryWarrantyExpirationDate(warranty.getAccessoryWarrantyExpirationDate());
					imeiWarranty.setMsisdn(warranty.getMsisdn());
					imeiWarranty.setMyFaves(warranty.isMyFaves());
					imeiWarranty.setFavesVersion(warranty.getFavesVersion());
					imeiWarranty.setRepairCode(warranty.getRepairCode());
					imeiWarranty.setShipmentType(warranty.getShipmentType());

					imeiWarranty.setWarrantyDetailInfo(new WarrantyDetailInfo());
					setWarrantyDetailInfo(warranty, imeiWarranty);
					imeiWarranty.setTmoMerchandise(warranty.isTmoMerchandise());
					root.getImeiWarranty().add(imeiWarranty);
				}
			}
		}
	}

	/**
	 * @param warranty
	 * @param imeiWarranty
	 */
	private void setWarrantyDetailInfo(ImeiWarrantyDetails warranty, ImeiWarranty imeiWarranty) {
		if (warranty.getWarrantyDetail() != null && null != warranty.getWarrantyDetail().getWarrantyDetailInfo()) {
			imeiWarranty.getWarrantyDetailInfo()
					.setWarrantyStatus(warranty.getWarrantyDetail().getWarrantyDetailInfo().getWarrantyStatus());
			if (null != warranty.getWarrantyDetail().getWarrantyDetailInfo().getExtendedCoveragePeriod()) {
				imeiWarranty.getWarrantyDetailInfo().setExtendedCoveragePeriod(new DateRangeType());
				imeiWarranty.getWarrantyDetailInfo().getExtendedCoveragePeriod().setStartDate(warranty
						.getWarrantyDetail().getWarrantyDetailInfo().getExtendedCoveragePeriod().getStartDate());
				imeiWarranty.getWarrantyDetailInfo().getExtendedCoveragePeriod().setEndDate(
						warranty.getWarrantyDetail().getWarrantyDetailInfo().getExtendedCoveragePeriod().getEndDate());
			}
			imeiWarranty.getWarrantyDetailInfo()
					.setDaysRemaining(warranty.getWarrantyDetail().getWarrantyDetailInfo().getDaysRemaining());
			imeiWarranty.getWarrantyDetailInfo().setEstimatedPurchaseDate(
					warranty.getWarrantyDetail().getWarrantyDetailInfo().getEstimatedPurchaseDate());
			imeiWarranty.getWarrantyDetailInfo()
					.setPurchaseCountry(warranty.getWarrantyDetail().getWarrantyDetailInfo().getPurchaseCountry());
			imeiWarranty.getWarrantyDetailInfo()
					.setRegistrationDate(warranty.getWarrantyDetail().getWarrantyDetailInfo().getRegistrationDate());
			if (warranty.getWarrantyDetail().getWarrantyDetailInfo().getManufacturerCoveragePeriod() != null) {
				imeiWarranty.getWarrantyDetailInfo().setManufacturerCoveragePeriod(new DateRangeType());
				imeiWarranty.getWarrantyDetailInfo().getManufacturerCoveragePeriod().setStartDate(warranty
						.getWarrantyDetail().getWarrantyDetailInfo().getManufacturerCoveragePeriod().getStartDate());
				imeiWarranty.getWarrantyDetailInfo().getManufacturerCoveragePeriod().setEndDate(warranty
						.getWarrantyDetail().getWarrantyDetailInfo().getManufacturerCoveragePeriod().getEndDate());
			}
			imeiWarranty.getWarrantyDetailInfo()
					.setPremiumCoverage(warranty.getWarrantyDetail().getWarrantyDetailInfo().isPremiumCoverage());
		}
	}

	/**
	 * Method invokeGetimeistatus
	 * 
	 * @param CSIGetIMEIRequestType csiGetIMEIRequest ,WarrantyDetails
	 *                              warrantyDetails ,GetDeviceDetailsRequest
	 *                              getDeviceDetailsRequest ,CSIGetIMEIResponseType
	 *                              csiGetIMEIResponse
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void invokeGetimeistatus(CSIGetIMEIRequestType csiGetIMEIRequest, WarrantyDetails warrantyDetails,
			GetDeviceDetailsRequest getDeviceDetailsRequest, CSIGetIMEIResponseType csiGetIMEIResponse)
			throws TMobileProcessException {
		GetDeviceDetailsRequest varRequest = getDeviceDetailsRequest;
		csiGetIMEIRequest.setRequestHeader(new HeaderType());
		csiGetIMEIRequest.getRequestHeader().setApplicationId(varRequest.getHeader().getSender().getApplicationId());
		csiGetIMEIRequest.getRequestHeader().setChannelId(varRequest.getHeader().getSender().getChannelId());
		if (varRequest.getHeader().getSender().getTimestamp() != null) {
			csiGetIMEIRequest.getRequestHeader().setRequestStartTime(varRequest.getHeader().getSender().getTimestamp());
		}
		if (varRequest.getHeader().getSender().getApplicationUserId() != null) {
			csiGetIMEIRequest.getRequestHeader()
					.setApplicationUserId(varRequest.getHeader().getSender().getApplicationUserId());
		}
		if (varRequest.getHeader().getSender().getStoreId() != null) {
			csiGetIMEIRequest.getRequestHeader().setStoreId(varRequest.getHeader().getSender().getStoreId());
		}
		if (varRequest.getHeader().getSender().getDealerCode() != null) {
			csiGetIMEIRequest.getRequestHeader().setDealerCode(varRequest.getHeader().getSender().getDealerCode());
		}
		csiGetIMEIRequest.getRequestHeader().setSenderId(varRequest.getHeader().getSender().getSenderId());
		if (varRequest.getHeader().getSender().getSessionId() != null) {
			csiGetIMEIRequest.getRequestHeader().setSessionId(varRequest.getHeader().getSender().getSessionId());
		}
		if (varRequest.getHeader().getSender().getWorkflowId() != null) {
			csiGetIMEIRequest.getRequestHeader().setWorkflowId(varRequest.getHeader().getSender().getWorkflowId());
		}
		if (varRequest.getHeader().getSender().getActivityId() != null) {
			csiGetIMEIRequest.getRequestHeader().setActivityId(varRequest.getHeader().getSender().getActivityId());
		}

		for (ImeiWarranty rootImeiWarrantyLocal : warrantyDetails.getRoot().getImeiWarranty()) {
			if (CollectionUtils.isEmpty(csiGetIMEIResponse.getImeiInfo())
					&& rootImeiWarrantyLocal.getMsisdn() != null) {
				csiGetIMEIRequest.getMsisdn().add("1" + rootImeiWarrantyLocal.getMsisdn());
			}
			if (rootImeiWarrantyLocal.getIMEI() != null) {
				csiGetIMEIRequest.getImei().add(rootImeiWarrantyLocal.getIMEI());
			}
		}
		iInvokeGetIMEIStatusService.executeMethod(csiGetIMEIRequest, csiGetIMEIResponse);
	}

	/**
	 * Method invokeGetdevicewarrantydetailorch
	 * 
	 * @param WarrantyDetails warrantyDetails ,GetDeviceDetailsRequest
	 *                        getDeviceDetailsRequest ,GetDeviceDetailsOrchEndOutput
	 *                        getDeviceWarrantyDetailOrchEndOutput
	 *                        ,GetDeviceDetailsOrchStartInput
	 *                        getDeviceWarrantyDetailOrchStartInput ,Guid guid
	 *                        ,MapperInputType accumulatedOutput ,ImeiIterationelem
	 *                        imeiIterationelem
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void invokeGetdevicewarrantydetailorch(WarrantyDetails warrantyDetails,
			GetDeviceDetailsRequest getDeviceDetailsRequest,
			GetDeviceDetailsOrchEndOutput getDeviceWarrantyDetailOrchEndOutput,
			GetDeviceDetailsOrchStartInput getDeviceWarrantyDetailOrchStartInput, Guid guid,
			MapperInputType accumulatedOutput, StringBuilder imeiIterationelem) throws TMobileProcessException {
		targetgsx.applications.gsx.orchestrations.warranty.getdevicewarrantydetailorch_start_input.Root root = getDeviceWarrantyDetailOrchStartInput
				.getRoot();
		GetDeviceDetailsRequest varReq = getDeviceDetailsRequest;
		String varAppleMSISDN = "";
		for (ImeiWarranty imeiWarranty : warrantyDetails.getRoot().getImeiWarranty()) {
			if ((imeiWarranty.getModel().toUpperCase().contains(IPHONE)
					|| imeiWarranty.getManufacturer().toUpperCase().contains(APPLE))
					&& null != imeiWarranty.getIMEI()) {
				varAppleMSISDN = imeiWarranty.getIMEI();
			}
		}

		root.setGetDeviceWarrantyDetailRequest(new GetDeviceWarrantyDetailRequestType());
		root.getGetDeviceWarrantyDetailRequest().setRequestHeader(new HeaderType());

		if (null != varReq.getHeader() && null != varReq.getHeader().getSender()) {

			root.getGetDeviceWarrantyDetailRequest().getRequestHeader()
					.setChannelId(varReq.getHeader().getSender().getChannelId());
			root.getGetDeviceWarrantyDetailRequest().getRequestHeader()
					.setRequestStartTime(varReq.getHeader().getSender().getTimestamp());
			root.getGetDeviceWarrantyDetailRequest().getRequestHeader()
					.setApplicationUserId(varReq.getHeader().getSender().getApplicationUserId());
			root.getGetDeviceWarrantyDetailRequest().getRequestHeader()
					.setStoreId(varReq.getHeader().getSender().getStoreId());
			root.getGetDeviceWarrantyDetailRequest().getRequestHeader()
					.setDealerCode(varReq.getHeader().getSender().getDealerCode());
			root.getGetDeviceWarrantyDetailRequest().getRequestHeader()
					.setSenderId(varReq.getHeader().getSender().getSenderId());
			root.getGetDeviceWarrantyDetailRequest().getRequestHeader()
					.setSessionId(varReq.getHeader().getSender().getSessionId());
			root.getGetDeviceWarrantyDetailRequest().getRequestHeader()
					.setWorkflowId(varReq.getHeader().getSender().getWorkflowId());
			root.getGetDeviceWarrantyDetailRequest().getRequestHeader()
					.setActivityId(varReq.getHeader().getSender().getActivityId());
		}
		if (varAppleMSISDN.equalsIgnoreCase(accumulatedOutput.getIMEI())) {
			setAccumulatedOutputIMEI(getDeviceDetailsRequest, accumulatedOutput, imeiIterationelem, root, varReq);
		} else {
			DeviceInfo deviceInfo = new DeviceInfo();
			if (!CollectionUtils.isEmpty(varReq.getMSISDN())) {
				deviceInfo.setMsisdn(varReq.getMSISDN().get(0));
			}
			deviceInfo.setImei(varReq.getImei());
			root.getGetDeviceWarrantyDetailRequest().getDeviceInfo().add(deviceInfo);
		}
		root.setIAPName("IphoneWarrentyDetailinfo");
		if (guid.getRoot().getGUID() != null) {
			root.setGUID(guid.getRoot().getGUID());
		}
		iGetDeviceWarrantyDetailOrchService.executeMethod(getDeviceWarrantyDetailOrchStartInput,
				getDeviceWarrantyDetailOrchEndOutput);
	}

	/**
	 * @param getDeviceDetailsRequest
	 * @param accumulatedOutput
	 * @param imeiIterationelem
	 * @param root
	 * @param varReq
	 */
	private void setAccumulatedOutputIMEI(GetDeviceDetailsRequest getDeviceDetailsRequest,
			MapperInputType accumulatedOutput, StringBuilder imeiIterationelem,
			targetgsx.applications.gsx.orchestrations.warranty.getdevicewarrantydetailorch_start_input.Root root,
			GetDeviceDetailsRequest varReq) {
		DeviceInfo deviceInfo = new DeviceInfo();
		deviceInfo.setMsisdn(varReq.getMSISDN().get(0));
		if (getDeviceDetailsRequest.getImei().equalsIgnoreCase(accumulatedOutput.getIMEI())
				&& null != accumulatedOutput.getTransformedIMEI()) {
			deviceInfo.setImei(accumulatedOutput.getTransformedIMEI());
		} else if (imeiIterationelem.toString().equalsIgnoreCase(accumulatedOutput.getIMEI())
				&& null != accumulatedOutput.getTransformedIMEI()) {
			deviceInfo.setImei(accumulatedOutput.getTransformedIMEI());
		} else {
			deviceInfo.setImei(varReq.getImei());
		}
		root.getGetDeviceWarrantyDetailRequest().getDeviceInfo().add(deviceInfo);
	}

	/**
	 * Method invokeGetDeviceSpecificationsREST
	 * 
	 * @param GetDeviceDetailsRequest getDeviceDetailsRequest
	 *                                ,GetDeviceSpecificationsRequest
	 *                                getDeviceSpecificationsRequest
	 *                                ,DeviceSpecificationDetails
	 *                                deviceSpecificationDetails ,MapperInputType
	 *                                accumulatedOutput ,ImeiIterationelem
	 *                                imeiIterationelem
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void invokeGetDeviceSpecificationsREST(GetDeviceDetailsRequest getDeviceDetailsRequest,
			GetDeviceSpecificationsRequest getDeviceSpecificationsRequest,
			DeviceSpecificationDetails deviceSpecificationDetails, MapperInputType accumulatedOutput,
			StringBuilder imeiIterationelem) throws TMobileProcessException {
		if (getDeviceDetailsRequest.getQuickCode() != null) {
			getDeviceSpecificationsRequest.setQuickCode(getDeviceDetailsRequest.getQuickCode());
		}
		if (accumulatedOutput.getIMEI() != null
				&& accumulatedOutput.getIMEI().equalsIgnoreCase(getDeviceDetailsRequest.getImei())
				&& null != accumulatedOutput.getTransformedIMEI()) {
			getDeviceSpecificationsRequest.setIMEI(accumulatedOutput.getIMEI());
		} else if (accumulatedOutput.getIMEI() != null
				&& accumulatedOutput.getIMEI().equalsIgnoreCase(imeiIterationelem.toString())
				&& null != accumulatedOutput.getTransformedIMEI()) {
			getDeviceSpecificationsRequest.setIMEI(accumulatedOutput.getTransformedIMEI());
		} else {
			getDeviceSpecificationsRequest.setIMEI(getDeviceDetailsRequest.getImei());
		}

		iGetDeviceSpecificationsRESTService.executeMethod(getDeviceSpecificationsRequest, deviceSpecificationDetails);
	}

	/**
	 * Method invokeGetimeishipusagedataimpl
	 * 
	 * @param GetDeviceDetailsRequest getDeviceDetailsRequest ,ImeiWarranty
	 *                                imeiWarranty ,GetShipUsageDataForIMEIResponse
	 *                                getShipUsageDataForIMEIResponse
	 *                                ,GetShipUsageDataForIMEIRequest
	 *                                getShipUsageDataForIMEIRequest
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void invokeGetimeishipusagedataimpl(GetDeviceDetailsRequest getDeviceDetailsRequest,
			ImeiWarranty imeiWarranty, GetShipUsageDataForIMEIResponseType getShipUsageDataForIMEIResponse,
			GetShipUsageDataForIMEIRequestType getShipUsageDataForIMEIRequest) throws TMobileProcessException {
		GetDeviceDetailsRequest varReq = getDeviceDetailsRequest;
		getShipUsageDataForIMEIRequest.setRequestHeader(new HeaderType());
		getShipUsageDataForIMEIRequest.getRequestHeader()
				.setApplicationId(varReq.getHeader().getSender().getApplicationId());
		getShipUsageDataForIMEIRequest.getRequestHeader().setChannelId(varReq.getHeader().getSender().getChannelId());
		if (varReq.getHeader().getSender().getTimestamp() != null) {
			getShipUsageDataForIMEIRequest.getRequestHeader()
					.setRequestStartTime(varReq.getHeader().getSender().getTimestamp());
		}
		if (varReq.getHeader().getSender().getApplicationUserId() != null) {
			getShipUsageDataForIMEIRequest.getRequestHeader()
					.setApplicationUserId(varReq.getHeader().getSender().getApplicationUserId());
		}
		if (varReq.getHeader().getSender().getStoreId() != null) {
			getShipUsageDataForIMEIRequest.getRequestHeader().setStoreId(varReq.getHeader().getSender().getStoreId());
		}
		if (varReq.getHeader().getSender().getDealerCode() != null) {
			getShipUsageDataForIMEIRequest.getRequestHeader()
					.setDealerCode(varReq.getHeader().getSender().getDealerCode());
		}
		getShipUsageDataForIMEIRequest.getRequestHeader().setSenderId(varReq.getHeader().getSender().getSenderId());
		if (varReq.getHeader().getSender().getSessionId() != null) {
			getShipUsageDataForIMEIRequest.getRequestHeader()
					.setSessionId(varReq.getHeader().getSender().getSessionId());
		}
		if (varReq.getHeader().getSender().getWorkflowId() != null) {
			getShipUsageDataForIMEIRequest.getRequestHeader()
					.setWorkflowId(varReq.getHeader().getSender().getWorkflowId());
		}
		if (varReq.getHeader().getSender().getActivityId() != null) {
			getShipUsageDataForIMEIRequest.getRequestHeader()
					.setActivityId(varReq.getHeader().getSender().getActivityId());
		}
		if (imeiWarranty.getIMEI() != null) {
			getShipUsageDataForIMEIRequest.setIMEI(imeiWarranty.getIMEI());
		}
		iInvokeGetShipUsageDataService.executeMethod(getShipUsageDataForIMEIRequest, getShipUsageDataForIMEIResponse);
	}

	/**
	 * Method shipData
	 * 
	 * @param ErrorLinkHandlerFaultData errorLinkHandlerFaultData ,ImeiWarranty
	 *                                  imeiWarranty
	 *                                  ,GetShipUsageDataForIMEIResponse
	 *                                  getShipUsageDataForIMEIResponse
	 *                                  ,1573734489550 1573734489550
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void shipData(ErrorDetails errorShortMessage, ImeiWarranty imeiWarranty,
			GetShipUsageDataForIMEIResponseType getShipUsageDataForIMEIResponse,
			com.tibco.namespaces.tnt.plugins.generalactivities.mapper._1573734489550.MapperInputType shipmentLastUsageData)
			throws TMobileProcessException {
		if (errorShortMessage != null && errorShortMessage.getCode() != null) {
			shipmentLastUsageData.setGetShipUsageDataForIMEIResponse(new GetShipUsageDataForIMEIResponseType());
			shipmentLastUsageData.getGetShipUsageDataForIMEIResponse().setStatus(new CommonStatusType());
			shipmentLastUsageData.getGetShipUsageDataForIMEIResponse().getStatus()
					.setStatusCode(errorShortMessage.getCode());
			shipmentLastUsageData.getGetShipUsageDataForIMEIResponse().getStatus()
					.setStatusMessage(errorShortMessage.getMesssage());
			shipmentLastUsageData.getGetShipUsageDataForIMEIResponse().getStatus()
					.setStackTrace(errorShortMessage.getStackTrace());
			if (errorShortMessage.getProcessStack() != null) {
				shipmentLastUsageData.getGetShipUsageDataForIMEIResponse().getStatus()
						.setProcessStack(errorShortMessage.getProcessStack());
			}
			for (SubStatus errorShortMessageSubStatusLocal : errorShortMessage.getSubstatus()) {
				com.t_mobile.csi.base.SubStatusType subStatus = new com.t_mobile.csi.base.SubStatusType();
				subStatus.setName(errorShortMessageSubStatusLocal.getName());
				subStatus.setCode(errorShortMessageSubStatusLocal.getCode());
				subStatus.setDescription(errorShortMessageSubStatusLocal.getDescription());
				subStatus.setExplanation(errorShortMessageSubStatusLocal.getExplanation());
				subStatus.setReferenceId(errorShortMessageSubStatusLocal.getReferenceId());
				shipmentLastUsageData.getGetShipUsageDataForIMEIResponse().getStatus().getSubStatus().add(subStatus);
			}
		} else {
			shipmentLastUsageData.setGetShipUsageDataForIMEIResponse(getShipUsageDataForIMEIResponse);
		}
		shipmentLastUsageData.setImei(imeiWarranty.getIMEI());
	}

	/**
	 * Method getGUID
	 * 
	 * @param GetdevicedetailsErrorinput getdevicedetailsErrorinput ,Guid guid
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void getGUID(Guid guid) throws TMobileProcessException {
		if(null != sharedServiceContext.getRequestValue("GUID")) { 
			Guid guidLocal = (Guid) sharedServiceContext.getRequestValue("GUID");
			guid.setRoot(guidLocal.getRoot());
		}
	}

	/**
	 * Method innerScope1
	 * 
	 * @param getDeviceDetailsRequest
	 * @param imeiWarranty
	 * 
	 * @param
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void innerScope1(GetDeviceDetailsRequest getDeviceDetailsRequest, ImeiWarranty imeiWarranty,
			com.tibco.namespaces.tnt.plugins.generalactivities.mapper._1573734489550.MapperInputType shipmentLastUsageData)
			throws TMobileProcessException {
		GetShipUsageDataForIMEIResponseType getShipUsageDataForIMEIResponse = new GetShipUsageDataForIMEIResponseType();
		GetShipUsageDataForIMEIRequestType getShipUsageDataForIMEIRequest = new GetShipUsageDataForIMEIRequestType();
		ErrorDetails errorShortMessage = new ErrorDetails();
		try {
			invokeGetimeishipusagedataimpl(getDeviceDetailsRequest, imeiWarranty, getShipUsageDataForIMEIResponse,
					getShipUsageDataForIMEIRequest);
		} catch (TMobileProcessException e) {
			LOGGER.error("innerScope1 : {} ", e.getMessage());
		}
		shipData(errorShortMessage, imeiWarranty, getShipUsageDataForIMEIResponse, shipmentLastUsageData);
	}

	/**
	 * Method shipDataLoop
	 * 
	 * @param getDeviceDetailsRequest
	 * @param warrantyDetails
	 * 
	 * @param
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void shipDataLoop(GetDeviceDetailsRequest getDeviceDetailsRequest, WarrantyDetails warrantyDetails,
			com.tibco.namespaces.tnt.plugins.generalactivities.mapper._1573734489550.MapperInputType shipmentLastUsageData)
			throws TMobileProcessException {
		for (ImeiWarranty imeiWarranty : warrantyDetails.getRoot().getImeiWarranty()) {
			innerScope1(getDeviceDetailsRequest, imeiWarranty, shipmentLastUsageData);
		}
	}

	/**
	 * Method mergeResponses
	 * 
	 * @param WarrantyDetails warrantyDetails ,GetDeviceDetailsRequest
	 *                        getDeviceDetailsRequest ,ErrorLinkHandlerFaultData
	 *                        errorLinkHandlerFaultData
	 *                        ,GetDeviceDetailsOrchDMWResponse
	 *                        getDeviceDetailsOrchDMWResponse
	 *                        ,GetDeviceDetailsOrchEndOutput
	 *                        getDeviceWarrantyDetailOrchEndOutput
	 *                        ,DeviceSpecificationDetails deviceSpecificationDetails
	 *                        ,GetDeviceDetailsResponse getDeviceDetailsResponse
	 *                        ,MapperInputType accumulatedOutput
	 *                        ,CSIGetIMEIResponseType csiGetIMEIResponse
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void mergeResponses(WarrantyDetails warrantyDetails, GetDeviceDetailsRequest getDeviceDetailsRequest,
			ErrorDetails errorLinkHandlerFaultData, GetDeviceDetailsOrchDMWResponse getDeviceDetailsOrchDMWResponse,
			GetDeviceDetailsOrchEndOutput getDeviceWarrantyDetailOrchEndOutput,
			DeviceSpecificationDetails deviceSpecificationDetails, GetDeviceDetailsResponse getDeviceDetailsResponse,
			MapperInputType lunhIMEI, CSIGetIMEIResponseType csiGetIMEIResponse,
			com.tibco.namespaces.tnt.plugins.generalactivities.mapper._1573734489550.MapperInputType shipmentLastUsageData)
			throws TMobileProcessException {
		String varTargetSystem = "true".equalsIgnoreCase(TibcoToJavaUtil.getProperty(TMO_APP_HANA_ENABLE_HANA_FLOW))
				? "HANA"
				: "DMW";
		List<String> tmoNonSupported = Arrays.asList(
				TibcoToJavaUtil.getProperty("TMO_App_ProductManagement_Device_tmoNonSupportedDevice").split(","));
		String requestIMEI = getDeviceDetailsRequest.getImei();
		List<String> requestMSISDN = getDeviceDetailsRequest.getMSISDN();
		List<String> dmwMISISDN = getDeviceDetailsOrchDMWResponse.getRoot().getMSISDN();

		getDeviceDetailsResponse.setServiceTransactionId(getDeviceDetailsRequest.getServiceTransactionId());
		getDeviceDetailsResponse.setVersion(getDeviceDetailsRequest.getVersion());
		getDeviceDetailsResponse.setHeader(getDeviceDetailsRequest.getHeader());

		if (null == getDeviceDetailsResponse.getResponseStatus())
			getDeviceDetailsResponse.setResponseStatus(new ResponseStatus());

		getDeviceDetailsResponse.getResponseStatus().setCode(BigInteger.valueOf(100));

		if (responseStatusErrorFlag(getDeviceDetailsRequest, errorLinkHandlerFaultData,
				getDeviceWarrantyDetailOrchEndOutput, lunhIMEI, csiGetIMEIResponse, shipmentLastUsageData)) {

			setResponseStatusError(errorLinkHandlerFaultData, getDeviceWarrantyDetailOrchEndOutput,
					getDeviceDetailsResponse, lunhIMEI, csiGetIMEIResponse, shipmentLastUsageData, varTargetSystem,
					requestIMEI, requestMSISDN, dmwMISISDN);
		}

		if (getDeviceDetailsRequest.getMSISDN().stream().filter(msisdn -> !StringUtils.isEmpty(msisdn)).count() == 1
				&& getDeviceDetailsRequest.isRetrieveDeviceWarrantyInfo()) {
			for (ImeiWarranty rootImeiWarrantyLocal : warrantyDetails.getRoot().getImeiWarranty()) {
				setImeiWarranty(getDeviceWarrantyDetailOrchEndOutput, deviceSpecificationDetails,
						getDeviceDetailsResponse, lunhIMEI, csiGetIMEIResponse, shipmentLastUsageData, tmoNonSupported,
						rootImeiWarrantyLocal);
			}
		} else if (getDeviceDetailsRequest.getMSISDN().stream().filter(msisdn -> !StringUtils.isEmpty(msisdn))
				.count() > 1
				|| (getDeviceDetailsRequest.getMSISDN().stream().filter(msisdn -> !StringUtils.isEmpty(msisdn))
						.count() == 1 && getDeviceDetailsRequest.isRetrieveDeviceWarrantyInfo())) {
			for (IMEIInfo imeiInfoLocal : csiGetIMEIResponse.getImeiInfo()) {
				setIMEIInfo(deviceSpecificationDetails, getDeviceDetailsResponse, lunhIMEI, shipmentLastUsageData,
						imeiInfoLocal);
			}
		} else {
			for (ImeiWarranty rootImeiWarrantyLocal : warrantyDetails.getRoot().getImeiWarranty()) {

				setImeiWarrantyOther(getDeviceWarrantyDetailOrchEndOutput, deviceSpecificationDetails,
						getDeviceDetailsResponse, csiGetIMEIResponse, shipmentLastUsageData, rootImeiWarrantyLocal);
			}

		}
	}

	/**
	 * @param getDeviceWarrantyDetailOrchEndOutput
	 * @param deviceSpecificationDetails
	 * @param getDeviceDetailsResponse
	 * @param csiGetIMEIResponse
	 * @param shipmentLastUsageData
	 * @param rootImeiWarrantyLocal
	 */
	private void setImeiWarrantyOther(GetDeviceDetailsOrchEndOutput getDeviceWarrantyDetailOrchEndOutput,
			DeviceSpecificationDetails deviceSpecificationDetails, GetDeviceDetailsResponse getDeviceDetailsResponse,
			CSIGetIMEIResponseType csiGetIMEIResponse,
			com.tibco.namespaces.tnt.plugins.generalactivities.mapper._1573734489550.MapperInputType shipmentLastUsageData,
			ImeiWarranty rootImeiWarrantyLocal) {
		com.tmobile.services.productmanagement.deviceenterprise.v1.DeviceInfo deviceInfoOther = new com.tmobile.services.productmanagement.deviceenterprise.v1.DeviceInfo();
		getDeviceDetailsResponse.getDeviceInfo().add(deviceInfoOther);

		String currentImeiNOMSISDN = rootImeiWarrantyLocal.getIMEI();
		String gsxInputIMEI = currentImeiNOMSISDN.substring(0, currentImeiNOMSISDN.length() - 2) + "0";

		Optional<WarrantyDetail> findFirstWarrantyDetail = null != getDeviceWarrantyDetailOrchEndOutput.getRoot()
				.getGetDeviceWarrantyDetailResponse() ? getDeviceWarrantyDetailOrchEndOutput.getRoot()
						.getGetDeviceWarrantyDetailResponse().getWarrantyDetail().stream()
						.filter(warrantyDetail -> gsxInputIMEI.equalsIgnoreCase(warrantyDetail.getImei())).findFirst()
						: null;
		WarrantyDetail appleWarrantyNOMSISDN = null != findFirstWarrantyDetail && findFirstWarrantyDetail.isPresent()
				? findFirstWarrantyDetail.get()
				: null;

		GetShipUsageDataForIMEIResponseType shipmentDataNOMSISDN = null != shipmentLastUsageData.getImei()
				&& shipmentLastUsageData.getImei().equalsIgnoreCase(currentImeiNOMSISDN)
						? shipmentLastUsageData.getGetShipUsageDataForIMEIResponse()
						: null;

		Optional<IMEIInfo> imeiInfoOptional = csiGetIMEIResponse.getImeiInfo().stream()
				.filter(imeiInfo -> currentImeiNOMSISDN.equalsIgnoreCase(imeiInfo.getImei())).findFirst();
		IMEIInfo imeiStatusNOMSISDN = imeiInfoOptional.isPresent() ? imeiInfoOptional.get() : null;

		DeviceSpecificationDetails varHardwareInfoNOMSISDN = null != deviceSpecificationDetails.getIMEI()
				&& deviceSpecificationDetails.getIMEI().equalsIgnoreCase(currentImeiNOMSISDN)
						? deviceSpecificationDetails
						: null;

		ProductOffering productOffering = new ProductOffering();
		deviceInfoOther.setProductOffering(productOffering);

		OfferingVariant offeringVarient = new OfferingVariant();
		deviceInfoOther.getProductOffering().getOfferingVariant().add(offeringVarient);

		ProductSpecificationBase productSpecification = new ProductSpecificationBase();
		deviceInfoOther.getProductOffering().getProductSpecification().add(productSpecification);

		ActivityInstance processingHistory = new ActivityInstance();
		deviceInfoOther.getProcessingHistory().add(processingHistory);

		Warranty warranty = new Warranty();
		deviceInfoOther.setWarranty(warranty);

		Description description = new Description();
		deviceInfoOther.getWarranty().getConfigurationDescription().add(description);

		if (appleWarrantyNOMSISDN != null) {
			setAppleWarrantyNOMSISDNDetails(deviceInfoOther, appleWarrantyNOMSISDN, description);
		}

		TheftLock ftLock = new TheftLock();
		ftLock.setTheftLockType(TheftLockType.valueOf(OTHER));
		deviceInfoOther.setTheftLock(ftLock);

		if (varHardwareInfoNOMSISDN != null) {
			setVarHardwareInfoNOMSISDNDetails(deviceInfoOther, appleWarrantyNOMSISDN, varHardwareInfoNOMSISDN,
					offeringVarient, processingHistory, ftLock);
		}
		if (shipmentDataNOMSISDN != null) {
			setShipmentDataNOMSISDNDetails(deviceInfoOther, shipmentDataNOMSISDN, productSpecification);
		}
		if (rootImeiWarrantyLocal.getLastUseDate() != null) {
			deviceInfoOther.setLastUseDate(TibcoToJavaUtil
					.convertDateStringToXMLGregorianCalendar(rootImeiWarrantyLocal.getLastUseDate(), YYYY_MM_DD));
		}
		if (rootImeiWarrantyLocal.getFirstUseDate() != null) {
			deviceInfoOther.setFirstUseDate(TibcoToJavaUtil
					.convertDateStringToXMLGregorianCalendar(rootImeiWarrantyLocal.getFirstUseDate(), YYYY_MM_DD));
		}
		if (rootImeiWarrantyLocal.getDeviceType() != null) {
			deviceInfoOther.setDeviceType(rootImeiWarrantyLocal.getDeviceType());
		}
		if (currentImeiNOMSISDN != null) {
			deviceInfoOther.setImei(currentImeiNOMSISDN);
		}
		if (imeiStatusNOMSISDN != null) {
			setImeiStatusNOMSISDNDetails(deviceInfoOther, imeiStatusNOMSISDN);
		}
	}

	/**
	 * @param deviceInfo
	 * @param imeiStatusNOMSISDN
	 */
	private void setImeiStatusNOMSISDNDetails(
			com.tmobile.services.productmanagement.deviceenterprise.v1.DeviceInfo deviceInfo,
			IMEIInfo imeiStatusNOMSISDN) {
		if (imeiStatusNOMSISDN.getMake() != null) {
			deviceInfo.setDeviceManufacturer(new Manufacturer());
			deviceInfo.getDeviceManufacturer().setManufacturerName(imeiStatusNOMSISDN.getMake());
		}
		if (imeiStatusNOMSISDN.getModel() != null) {
			deviceInfo.setDeviceModel(imeiStatusNOMSISDN.getModel());
		}
		if (imeiStatusNOMSISDN.getFirstUseDate() != null) {
			deviceInfo.setFirstUseDate(TibcoToJavaUtil
					.convertDateStringToXMLGregorianCalendar(imeiStatusNOMSISDN.getFirstUseDate(), YYYY_MM_DD));
		}
		if (imeiStatusNOMSISDN.getSimNumber() != null) {
			deviceInfo.setMSISDN(imeiStatusNOMSISDN.getSimNumber().substring(1));
		}
		if (imeiStatusNOMSISDN.getSupported() != null) {
			deviceInfo.setTmoApproved(imeiStatusNOMSISDN.getSupported());
		}
		if (imeiStatusNOMSISDN.getFaveCapable() != null) {
			deviceInfo.setMyNetworkComp(imeiStatusNOMSISDN.getFaveCapable());
		}
		if (imeiStatusNOMSISDN.getUmaCapable() != null) {
			deviceInfo.setUmaCapable(imeiStatusNOMSISDN.getUmaCapable());
		}
		if (imeiStatusNOMSISDN.getRj11RouterInd() != null) {
			deviceInfo.setRouteIndicator(imeiStatusNOMSISDN.getRj11RouterInd());
		}
		if (imeiStatusNOMSISDN.getPdaInd() != null) {
			deviceInfo.setPdaIndicator(imeiStatusNOMSISDN.getPdaInd());
		}
		if (imeiStatusNOMSISDN.getNfcCapable() != null) {
			deviceInfo.setNfcIndicator(imeiStatusNOMSISDN.getNfcCapable());
		}
		if (imeiStatusNOMSISDN.getIsisCertifiedInd() != null) {
			deviceInfo.setIsisCertifiedIndicator(imeiStatusNOMSISDN.getIsisCertifiedInd());
		}
	}

	/**
	 * @param deviceInfo
	 * @param shipmentDataNOMSISDN
	 * @param productSpecification
	 */
	private void setShipmentDataNOMSISDNDetails(
			com.tmobile.services.productmanagement.deviceenterprise.v1.DeviceInfo deviceInfo,
			GetShipUsageDataForIMEIResponseType shipmentDataNOMSISDN, ProductSpecificationBase productSpecification) {
		if (shipmentDataNOMSISDN.getSku() != null) {
			productSpecification.setSku(shipmentDataNOMSISDN.getSku());
		}
		if (StringUtils.isNotEmpty(shipmentDataNOMSISDN.getShipDate())) {
				deviceInfo.setShipmentDate(TibcoToJavaUtil
						.convertDateStringToXMLGregorianCalendar(shipmentDataNOMSISDN.getShipDate(), DD_MMM_YY));
		}
		if (shipmentDataNOMSISDN.getShipDealerCode() != null) {
			deviceInfo.setShipmentDealerCode(shipmentDataNOMSISDN.getShipDealerCode());
		}
	}

	/**
	 * @param deviceInfo
	 * @param appleWarrantyNOMSISDN
	 * @param varHardwareInfoNOMSISDN
	 * @param offeringVarient
	 * @param processingHistory
	 * @param ftLock
	 */
	private void setVarHardwareInfoNOMSISDNDetails(
			com.tmobile.services.productmanagement.deviceenterprise.v1.DeviceInfo deviceInfo,
			WarrantyDetail appleWarrantyNOMSISDN, DeviceSpecificationDetails varHardwareInfoNOMSISDN,
			OfferingVariant offeringVarient, ActivityInstance processingHistory, TheftLock ftLock) {
		if (varHardwareInfoNOMSISDN.getDeviceSpecificationDetail().getHardwareInfo().getMaxInternalStorage() != null) {
			offeringVarient.setMemory(
					varHardwareInfoNOMSISDN.getDeviceSpecificationDetail().getHardwareInfo().getMaxInternalStorage());
		}

		if (appleWarrantyNOMSISDN != null && StringUtils.isNotEmpty(appleWarrantyNOMSISDN.getSerialNumber())) {
			deviceInfo.setSerialNumber(appleWarrantyNOMSISDN.getSerialNumber());
		} else {
			deviceInfo.setSerialNumber(varHardwareInfoNOMSISDN.getIMEI());
		}
		if (StringUtils.isNotEmpty(varHardwareInfoNOMSISDN.getIMEIType())) {
			deviceInfo
					.setSerialNumberType(SerialNumberType.valueOf(varHardwareInfoNOMSISDN.getIMEIType().toUpperCase()));
		}
		if (varHardwareInfoNOMSISDN.getDeviceSpecificationDetail().getHardwareInfo().getHardwareTestRunDate() != null) {
			TimePeriod activityPeriod = new TimePeriod();
			processingHistory.setActivityPeriod(activityPeriod);
			activityPeriod.setEndTime(
					varHardwareInfoNOMSISDN.getDeviceSpecificationDetail().getHardwareInfo().getHardwareTestRunDate());
		}
		if (varHardwareInfoNOMSISDN.getDeviceSpecificationDetail().getHardwareInfo().getHardwareTestStatus() != null) {
			ActivityInstanceStatus activityStatus = new ActivityInstanceStatus();
			processingHistory.getActivityInstanceStatus();
			activityStatus.setStatusCode(varHardwareInfoNOMSISDN.getDeviceSpecificationDetail().getHardwareInfo()
					.getHardwareTestStatus().value());
			processingHistory.getActivityInstanceStatus().add(activityStatus);
		}
		if (varHardwareInfoNOMSISDN.getDeviceSpecificationDetail().getHardwareInfo().getCarrierName() != null) {
			deviceInfo.setOldServiceProvider(
					varHardwareInfoNOMSISDN.getDeviceSpecificationDetail().getHardwareInfo().getCarrierName());
		}
		deviceInfo.setMobileOS(varHardwareInfoNOMSISDN.getDeviceSpecificationDetail().getHardwareInfo().getMobileOS());
		deviceInfo.setFirmware(varHardwareInfoNOMSISDN.getDeviceSpecificationDetail().getHardwareInfo().getFirmware());

		setDeviceSpecificationDetailRootInfo(varHardwareInfoNOMSISDN, deviceInfo);

		if ("IOS".equalsIgnoreCase(
				varHardwareInfoNOMSISDN.getDeviceSpecificationDetail().getTheftLockInfo().getType().name())) {
			ftLock.setTheftLockType(TheftLockType.valueOf("FMIP"));
		} else if (ANDROID.equalsIgnoreCase(
				varHardwareInfoNOMSISDN.getDeviceSpecificationDetail().getTheftLockInfo().getType().name())) {
			ftLock.setTheftLockType(TheftLockType.valueOf(ANTITHEFT));
		} else {
			ftLock.setTheftLockType(TheftLockType.valueOf(OTHER));
		}
		deviceInfo.setTheftLock(ftLock);

		if (varHardwareInfoNOMSISDN.getDeviceSpecificationDetail().getTheftLockInfo().getLastRetrivalDate() != null) {
			deviceInfo.getTheftLock().setLastRetrievalDate(
					varHardwareInfoNOMSISDN.getDeviceSpecificationDetail().getTheftLockInfo().getLastRetrivalDate());
		}
		if (varHardwareInfoNOMSISDN.getDeviceSpecificationDetail().getTheftLockInfo().getLastRetrivalDate() != null) {
			deviceInfo.getTheftLock().setLastRetrievalDate(
					varHardwareInfoNOMSISDN.getDeviceSpecificationDetail().getTheftLockInfo().getLastRetrivalDate());
		}
		if (varHardwareInfoNOMSISDN.getDeviceSpecificationDetail().getTheftLockInfo().getStatus() != null) {
			deviceInfo.getTheftLock().getStatus().setName(varHardwareInfoNOMSISDN.getDeviceSpecificationDetail()
					.getTheftLockInfo().getStatus().getDeclaringClass().getName());
		}
		if (varHardwareInfoNOMSISDN.getDeviceSpecificationDetail().getTmoMerchandizeInfo() != null) {
			deviceInfo.setLastTMODiagnosticDate(varHardwareInfoNOMSISDN.getDeviceSpecificationDetail()
					.getTmoMerchandizeInfo().getLastTmoDiagnosticDate());
		}
	}

	/**
	 * @param deviceInfo
	 * @param appleWarrantyNOMSISDN
	 * @param description
	 */
	private void setAppleWarrantyNOMSISDNDetails(
			com.tmobile.services.productmanagement.deviceenterprise.v1.DeviceInfo deviceInfo,
			WarrantyDetail appleWarrantyNOMSISDN, Description description) {
		deviceInfo.setSerialNumber(appleWarrantyNOMSISDN.getSerialNumber());
		if (appleWarrantyNOMSISDN.getProductDescription() != null) {
			DescriptionByChannel descriptionChannel = new DescriptionByChannel();
			descriptionChannel.setValue(appleWarrantyNOMSISDN.getProductDescription());
		}
		deviceInfo.getWarranty().setMerchandiseProvider(appleWarrantyNOMSISDN.getMerchandizeCarrier());
		description.setValue(appleWarrantyNOMSISDN.getConfigDescription());
		if (appleWarrantyNOMSISDN.getWarrantyDetailInfo().getManufacturerCoveragePeriod() != null) {
			deviceInfo.getWarranty().getManufacturerCoveragePeriod().setStartTime(
					appleWarrantyNOMSISDN.getWarrantyDetailInfo().getManufacturerCoveragePeriod().getStartDate());
			deviceInfo.getWarranty().getManufacturerCoveragePeriod().setEndTime(
					appleWarrantyNOMSISDN.getWarrantyDetailInfo().getManufacturerCoveragePeriod().getEndDate());
		}
		if (appleWarrantyNOMSISDN.getWarrantyDetailInfo().getExtendedCoveragePeriod() != null) {
			deviceInfo.getWarranty().getExtendedCoveragePeriod().setStartTime(
					appleWarrantyNOMSISDN.getWarrantyDetailInfo().getExtendedCoveragePeriod().getStartDate());
			deviceInfo.getWarranty().getExtendedCoveragePeriod()
					.setEndTime(appleWarrantyNOMSISDN.getWarrantyDetailInfo().getExtendedCoveragePeriod().getEndDate());
		}
		if (appleWarrantyNOMSISDN.getWarrantyDetailInfo().getDaysRemaining() != null) {
			deviceInfo.getWarranty().setDaysRemaining(
					Integer.valueOf(appleWarrantyNOMSISDN.getWarrantyDetailInfo().getDaysRemaining()));
		}
		deviceInfo.getWarranty()
				.setEstimatedPurchaseDate(appleWarrantyNOMSISDN.getWarrantyDetailInfo().getEstimatedPurchaseDate());
		if ("United States".equalsIgnoreCase(appleWarrantyNOMSISDN.getWarrantyDetailInfo().getPurchaseCountry())) {
			deviceInfo.getWarranty().setPurchaseCountry("USA");
		}
		if (appleWarrantyNOMSISDN.getWarrantyDetailInfo().getRegistrationDate() != null) {
			deviceInfo.getWarranty()
					.setRegistrationDate(appleWarrantyNOMSISDN.getWarrantyDetailInfo().getRegistrationDate());
		}
		if (appleWarrantyNOMSISDN.getWarrantyDetailInfo().isPremiumCoverage() != null) {
			Indicator indicator = new Indicator();
			indicator.setValue(appleWarrantyNOMSISDN.getWarrantyDetailInfo().isPremiumCoverage().toString());
			deviceInfo.getWarranty().setPremiumCoverageVendor(indicator);
		}
		if (appleWarrantyNOMSISDN.getImageURL() != null) {
			Attachment attachment = new Attachment();
			attachment.getName().add("imageURL");
			attachment.setURI(appleWarrantyNOMSISDN.getImageURL());
			deviceInfo.getWarranty().getWarrantyAttachment().add(attachment);
		}
		if (StringUtils.isNotEmpty(appleWarrantyNOMSISDN.getExplodedViewURL())) {
			Attachment attachment = new Attachment();
			attachment.getName().add("explodedViewURL");
			attachment.setURI(appleWarrantyNOMSISDN.getExplodedViewURL());
			deviceInfo.getWarranty().getWarrantyAttachment().add(attachment);
		}
		if (StringUtils.isNotEmpty(appleWarrantyNOMSISDN.getManualURL())) {
			Attachment attachment = new Attachment();
			attachment.getName().add("manualURL");
			attachment.setURI(appleWarrantyNOMSISDN.getManualURL());
			deviceInfo.getWarranty().getWarrantyAttachment().add(attachment);
		}
		if (appleWarrantyNOMSISDN.isPowerTrainFlag() != null) {
			SpecificationGroup specification = new SpecificationGroup();
			SpecificationValue value = new SpecificationValue();
			value.setName("powerTrainFlag");
			value.setValue(appleWarrantyNOMSISDN.isPowerTrainFlag().toString());
			specification.getSpecificationValue().add(value);
			deviceInfo.getWarranty().getWarrantySpecification().add(specification);
		}
		if (appleWarrantyNOMSISDN.isIsPersonalized() != null) {
			SpecificationGroup specification = new SpecificationGroup();
			SpecificationValue value = new SpecificationValue();
			value.setName("isPersonalized");
			value.setValue(appleWarrantyNOMSISDN.isIsPersonalized().toString());
			specification.getSpecificationValue().add(value);
			deviceInfo.getWarranty().getWarrantySpecification().add(specification);
		}
	}

	/**
	 * @param deviceSpecificationDetails
	 * @param getDeviceDetailsResponse
	 * @param lunhIMEI
	 * @param shipmentLastUsageData
	 * @param imeiInfoLocal
	 */
	private void setIMEIInfo(DeviceSpecificationDetails deviceSpecificationDetails,
			GetDeviceDetailsResponse getDeviceDetailsResponse, MapperInputType lunhIMEI,
			com.tibco.namespaces.tnt.plugins.generalactivities.mapper._1573734489550.MapperInputType shipmentLastUsageData,
			IMEIInfo imeiInfoLocal) {
		String currentIMEI = imeiInfoLocal.getImei();
		String currentTransImei = lunhIMEI.getIMEI().equalsIgnoreCase(currentIMEI) ? lunhIMEI.getTransformedIMEI() : "";
		DeviceSpecificationDetails varHardwareInfo = currentTransImei
				.equalsIgnoreCase(deviceSpecificationDetails.getIMEI()) ? deviceSpecificationDetails : null;
		GetShipUsageDataForIMEIResponseType shipmentData = currentIMEI.equalsIgnoreCase(shipmentLastUsageData.getImei())
				? shipmentLastUsageData.getGetShipUsageDataForIMEIResponse()
				: null;

		com.tmobile.services.productmanagement.deviceenterprise.v1.DeviceInfo deviceInfo = new com.tmobile.services.productmanagement.deviceenterprise.v1.DeviceInfo();
		getDeviceDetailsResponse.getDeviceInfo().add(deviceInfo);

		ProductOffering productOffering = new ProductOffering();
		deviceInfo.setProductOffering(productOffering);

		OfferingVariant offeringVarient = new OfferingVariant();
		deviceInfo.getProductOffering().getOfferingVariant().add(offeringVarient);

		ProductSpecificationBase productSpecification = new ProductSpecificationBase();
		deviceInfo.getProductOffering().getProductSpecification().add(productSpecification);

		ActivityInstance processingHistory = new ActivityInstance();
		deviceInfo.getProcessingHistory().add(processingHistory);

		Warranty warranty = new Warranty();
		deviceInfo.setWarranty(warranty);

		Description description = new Description();
		deviceInfo.getWarranty().getConfigurationDescription().add(description);

		TheftLock ftLock = new TheftLock();
		ftLock.setTheftLockType(TheftLockType.valueOf(OTHER));
		deviceInfo.setTheftLock(ftLock);

		if (varHardwareInfo != null) {
			setVarHardwareInfo(varHardwareInfo, deviceInfo, offeringVarient, processingHistory, ftLock);
		}
		if (imeiInfoLocal.getMake() != null) {
			deviceInfo.setDeviceManufacturer(new Manufacturer());
			deviceInfo.getDeviceManufacturer().setManufacturerName(imeiInfoLocal.getMake());
		}
		if (imeiInfoLocal.getModel() != null) {
			deviceInfo.setDeviceModel(imeiInfoLocal.getModel());
		}

		if (StringUtils.isNotEmpty(imeiInfoLocal.getFirstUseDate())) {
			deviceInfo.setFirstUseDate(TibcoToJavaUtil
					.convertDateStringToXMLGregorianCalendar(imeiInfoLocal.getFirstUseDate(), "yyyy-MM-dd HH:mm:ss"));
		}
		if (StringUtils.isNotEmpty(imeiInfoLocal.getSimNumber())) {
			deviceInfo.setMSISDN(imeiInfoLocal.getSimNumber().substring(1));
		}
		if (StringUtils.isNotEmpty(currentTransImei)) {
			deviceInfo.setImei(currentTransImei);
		}
		if (shipmentData != null) {
			productSpecification.setSku(shipmentData.getSku());
			setShipmentDataDetails(shipmentData, deviceInfo);
		}
		deviceInfo.setTmoApproved(imeiInfoLocal.getSupported());
		deviceInfo.setMyNetworkComp(imeiInfoLocal.getFaveCapable());
		deviceInfo.setUmaCapable(imeiInfoLocal.getUmaCapable());
		deviceInfo.setRouteIndicator(imeiInfoLocal.getRj11RouterInd());
		deviceInfo.setPdaIndicator(imeiInfoLocal.getPdaInd());
		deviceInfo.setNfcIndicator(imeiInfoLocal.getNfcCapable());
		deviceInfo.setIsisCertifiedIndicator(imeiInfoLocal.getIsisCertifiedInd());
	}

	/**
	 * @param shipmentData
	 * @param deviceInfo
	 */
	private void setShipmentDataDetails(GetShipUsageDataForIMEIResponseType shipmentData,
			com.tmobile.services.productmanagement.deviceenterprise.v1.DeviceInfo deviceInfo) {
		if (StringUtils.isNotEmpty(shipmentData.getShipDate())) {
				deviceInfo.setShipmentDate(
						TibcoToJavaUtil.convertDateStringToXMLGregorianCalendar(shipmentData.getShipDate(), DD_MMM_YY));
		}
		deviceInfo.setShipmentDealerCode(shipmentData.getShipDealerCode());
	}

	/**
	 * @param varHardwareInfo
	 * @param shipmentData
	 * @param deviceInfo
	 * @param offeringVarient
	 * @param productSpecification
	 * @param processingHistory
	 * @param ftLock
	 */
	private void setVarHardwareInfo(DeviceSpecificationDetails varHardwareInfo,
			com.tmobile.services.productmanagement.deviceenterprise.v1.DeviceInfo deviceInfo,
			OfferingVariant offeringVarient, ActivityInstance processingHistory, TheftLock ftLock) {
		if (varHardwareInfo.getDeviceSpecificationDetail() != null) {
			if (varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo() != null) {
				setDeviceSpecificationDetailHardwareInfo(varHardwareInfo, deviceInfo, offeringVarient,
						processingHistory);
			}
			if (varHardwareInfo.getDeviceSpecificationDetail().getRootInfo() != null) {
				setDeviceSpecificationDetailRootInfo(varHardwareInfo, deviceInfo);
			}

			if (varHardwareInfo.getDeviceSpecificationDetail().getTheftLockInfo().getLastRetrivalDate() != null) {
				deviceInfo.getTheftLock().setLastRetrievalDate(
						varHardwareInfo.getDeviceSpecificationDetail().getTheftLockInfo().getLastRetrivalDate());
			}
			if (varHardwareInfo.getDeviceSpecificationDetail().getTheftLockInfo().getStatus() != null) {
				deviceInfo.getTheftLock().getStatus().setName(varHardwareInfo.getDeviceSpecificationDetail()
						.getTheftLockInfo().getStatus().getDeclaringClass().getName());
			}

			if (varHardwareInfo.getDeviceSpecificationDetail().getTmoMerchandizeInfo() != null) {
				deviceInfo.setLastTMODiagnosticDate(varHardwareInfo.getDeviceSpecificationDetail()
						.getTmoMerchandizeInfo().getLastTmoDiagnosticDate());
			}
		}
		deviceInfo.setSerialNumber(varHardwareInfo.getIMEI());
		if (StringUtils.isNotEmpty(varHardwareInfo.getIMEIType())) {
			deviceInfo.setSerialNumberType(SerialNumberType.valueOf(varHardwareInfo.getIMEIType().toUpperCase()));
		}

		if ("IOS"
				.equalsIgnoreCase(varHardwareInfo.getDeviceSpecificationDetail().getTheftLockInfo().getType().name())) {
			ftLock.setTheftLockType(TheftLockType.valueOf("FMIP"));
		} else if (ANDROID
				.equalsIgnoreCase(varHardwareInfo.getDeviceSpecificationDetail().getTheftLockInfo().getType().name())) {
			ftLock.setTheftLockType(TheftLockType.valueOf(ANTITHEFT));
		} else {
			ftLock.setTheftLockType(TheftLockType.valueOf(OTHER));
		}
		deviceInfo.setTheftLock(ftLock);
	}

	/**
	 * @param varHardwareInfo
	 * @param deviceEnterpriseInfo
	 */
	private void setDeviceSpecificationDetailRootInfo(DeviceSpecificationDetails varHardwareInfo,
			com.tmobile.services.productmanagement.deviceenterprise.v1.DeviceInfo deviceEnterpriseInfo) {
		deviceEnterpriseInfo.setDeviceRooting(new DeviceRooting());
		if (varHardwareInfo.getDeviceSpecificationDetail().getRootInfo().isCurrentlyRooted() != null) {
			Indicator indicator = new Indicator();
			indicator.setValue(
					varHardwareInfo.getDeviceSpecificationDetail().getRootInfo().isCurrentlyRooted().toString());
			deviceEnterpriseInfo.getDeviceRooting().setIsCurrentlyRooted(indicator);
		}
		if (varHardwareInfo.getDeviceSpecificationDetail().getRootInfo().getCurrentRootDate() != null) {
			deviceEnterpriseInfo.getDeviceRooting().setCurrentRootDate(
					varHardwareInfo.getDeviceSpecificationDetail().getRootInfo().getCurrentRootDate());
		}
		if (varHardwareInfo.getDeviceSpecificationDetail().getRootInfo().isEverRooted() != null) {
			Indicator indicator = new Indicator();
			indicator.setValue(varHardwareInfo.getDeviceSpecificationDetail().getRootInfo().isEverRooted().toString());
			deviceEnterpriseInfo.getDeviceRooting().setEverRooted(indicator);
		}
		if (varHardwareInfo.getDeviceSpecificationDetail().getRootInfo().getLastRootDate() != null) {
			deviceEnterpriseInfo.getDeviceRooting()
					.setLastRootingDate(varHardwareInfo.getDeviceSpecificationDetail().getRootInfo().getLastRootDate());
		}
	}

	/**
	 * @param varHardwareInfo
	 * @param deviceInfo
	 * @param offeringVarient
	 * @param processingHistory
	 */
	private void setDeviceSpecificationDetailHardwareInfo(DeviceSpecificationDetails varHardwareInfo,
			com.tmobile.services.productmanagement.deviceenterprise.v1.DeviceInfo deviceInfo,
			OfferingVariant offeringVarient, ActivityInstance processingHistory) {
		if (varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo().getMaxInternalStorage() != null) {
			offeringVarient.setMemory(
					varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo().getMaxInternalStorage());
		}
		if (varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo().getCarrierName() != null) {
			deviceInfo.setOldServiceProvider(
					varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo().getCarrierName());
		}
		if (varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo().getMobileOS() != null) {
			deviceInfo.setMobileOS(varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo().getMobileOS());
		}
		if (varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo().getFirmware() != null) {
			deviceInfo.setFirmware(varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo().getFirmware());
		}
		if (varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo().getHardwareTestRunDate() != null) {
			TimePeriod activityPeriod = new TimePeriod();
			processingHistory.setActivityPeriod(activityPeriod);
			activityPeriod.setEndTime(
					varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo().getHardwareTestRunDate());
		}
		if (varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo().getHardwareTestStatus() != null) {
			ActivityInstanceStatus activityStatus = new ActivityInstanceStatus();
			processingHistory.getActivityInstanceStatus();
			activityStatus.setStatusCode(
					varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo().getHardwareTestStatus().value());
			processingHistory.getActivityInstanceStatus().add(activityStatus);
		}
	}

	/**
	 * @param getDeviceWarrantyDetailOrchEndOutput
	 * @param deviceSpecificationDetails
	 * @param getDeviceDetailsResponse
	 * @param lunhIMEI
	 * @param csiGetIMEIResponse
	 * @param shipmentLastUsageData
	 * @param tmoNonSupported
	 * @param rootImeiWarrantyLocal
	 */
	private void setImeiWarranty(GetDeviceDetailsOrchEndOutput getDeviceWarrantyDetailOrchEndOutput,
			DeviceSpecificationDetails deviceSpecificationDetails, GetDeviceDetailsResponse getDeviceDetailsResponse,
			MapperInputType lunhIMEI, CSIGetIMEIResponseType csiGetIMEIResponse,
			com.tibco.namespaces.tnt.plugins.generalactivities.mapper._1573734489550.MapperInputType shipmentLastUsageData,
			List<String> tmoNonSupported, ImeiWarranty rootImeiWarrantyLocal) {
		com.tmobile.services.productmanagement.deviceenterprise.v1.DeviceInfo deviceInfoImeiWarranty = new com.tmobile.services.productmanagement.deviceenterprise.v1.DeviceInfo();
		getDeviceDetailsResponse.getDeviceInfo().add(deviceInfoImeiWarranty);
		String currentMsisidn = rootImeiWarrantyLocal.getMsisdn().substring(1);
		String currentIMEI = rootImeiWarrantyLocal.getIMEI();
		String currentTransImei = currentIMEI.equalsIgnoreCase(rootImeiWarrantyLocal.getIMEI())
				? lunhIMEI.getTransformedIMEI()
				: "";
		String currentDmwIMEI = currentIMEI.equalsIgnoreCase(rootImeiWarrantyLocal.getIMEI())
				? rootImeiWarrantyLocal.getIMEI()
				: "";
		GetShipUsageDataForIMEIResponseType shipmentData = shipmentLastUsageData.getGetShipUsageDataForIMEIResponse();
		DeviceSpecificationDetails varHardwareInfo = deviceSpecificationDetails.getIMEI()
				.equalsIgnoreCase(currentTransImei) ? deviceSpecificationDetails : null;
		
		Optional<WarrantyDetail> appleWarrantyOpt = getDeviceWarrantyDetailOrchEndOutput.getRoot()
				.getGetDeviceWarrantyDetailResponse().getWarrantyDetail().stream()
				.filter(warrantyDetail -> currentDmwIMEI.equalsIgnoreCase(warrantyDetail.getImei())
						&& currentMsisidn.equalsIgnoreCase(warrantyDetail.getMsisdn()))
				.findFirst();
		WarrantyDetail appleWarranty = appleWarrantyOpt.isPresent() ? appleWarrantyOpt.get() : new WarrantyDetail();

		ProductOffering productOffering = new ProductOffering();
		deviceInfoImeiWarranty.setProductOffering(productOffering);

		OfferingVariant offeringVarient = new OfferingVariant();
		deviceInfoImeiWarranty.getProductOffering().getOfferingVariant().add(offeringVarient);

		ProductSpecificationBase productSpecification = new ProductSpecificationBase();
		deviceInfoImeiWarranty.getProductOffering().getProductSpecification().add(productSpecification);

		ActivityInstance processingHistory = new ActivityInstance();
		deviceInfoImeiWarranty.getProcessingHistory().add(processingHistory);

		Warranty warranty = new Warranty();
		deviceInfoImeiWarranty.setWarranty(warranty);

		Description description = new Description();
		deviceInfoImeiWarranty.getWarranty().getConfigurationDescription().add(description);

		TheftLock ftLock = new TheftLock();
		ftLock.setTheftLockType(TheftLockType.valueOf(OTHER));
		deviceInfoImeiWarranty.setTheftLock(ftLock);

		if (null != varHardwareInfo) {
			setVarHardwareInfo(deviceInfoImeiWarranty, varHardwareInfo, offeringVarient, processingHistory, ftLock);
		}
		if (appleWarranty.getProductDescription() != null) {
			DescriptionByChannel descriptionChannel = new DescriptionByChannel();
			descriptionChannel.setValue(appleWarranty.getProductDescription());
			productSpecification.getDescription().add(descriptionChannel);
		}
		productSpecification.setSku(shipmentData.getSku());
		deviceInfoImeiWarranty.getStatus().setName(appleWarranty.getWarrantyDetailInfo().getWarrantyStatus());
		deviceInfoImeiWarranty.setSerialNumber(appleWarranty.getSerialNumber());
		
		setDeviceModel(rootImeiWarrantyLocal, deviceInfoImeiWarranty, appleWarranty);
		
		deviceInfoImeiWarranty.getWarranty().setMerchandiseProvider(appleWarranty.getMerchandizeCarrier());
		description.setValue(appleWarranty.getConfigDescription());
		if (appleWarranty.getWarrantyDetailInfo().getManufacturerCoveragePeriod() != null) {
			deviceInfoImeiWarranty.getWarranty().getManufacturerCoveragePeriod()
					.setStartTime(appleWarranty.getWarrantyDetailInfo().getManufacturerCoveragePeriod().getStartDate());
			deviceInfoImeiWarranty.getWarranty().getManufacturerCoveragePeriod()
					.setEndTime(appleWarranty.getWarrantyDetailInfo().getManufacturerCoveragePeriod().getEndDate());
		}
		if (appleWarranty.getWarrantyDetailInfo().getExtendedCoveragePeriod() != null) {
			deviceInfoImeiWarranty.getWarranty().getExtendedCoveragePeriod()
					.setStartTime(appleWarranty.getWarrantyDetailInfo().getExtendedCoveragePeriod().getStartDate());
			deviceInfoImeiWarranty.getWarranty().getExtendedCoveragePeriod()
					.setEndTime(appleWarranty.getWarrantyDetailInfo().getExtendedCoveragePeriod().getEndDate());
		}
		if (rootImeiWarrantyLocal.getDeviceWarrantyExpirationDate() != null) {
			deviceInfoImeiWarranty.getWarranty().setWarrantyExpirationDate(TibcoToJavaUtil.convertDateStringToXMLGregorianCalendar(
					rootImeiWarrantyLocal.getDeviceWarrantyExpirationDate(), YYYY_MM_DD));
		}
		if (appleWarranty.getWarrantyDetailInfo().getDaysRemaining() != null) {
			deviceInfoImeiWarranty.getWarranty()
					.setDaysRemaining(Integer.valueOf(appleWarranty.getWarrantyDetailInfo().getDaysRemaining()));
		}
		deviceInfoImeiWarranty.getWarranty()
				.setEstimatedPurchaseDate(appleWarranty.getWarrantyDetailInfo().getEstimatedPurchaseDate());
		if ("United States".equalsIgnoreCase(appleWarranty.getWarrantyDetailInfo().getPurchaseCountry())) {
			deviceInfoImeiWarranty.getWarranty().setPurchaseCountry("USA");
		}
		deviceInfoImeiWarranty.getWarranty().setRegistrationDate(appleWarranty.getWarrantyDetailInfo().getRegistrationDate());
		if (appleWarranty.getWarrantyDetailInfo().isPremiumCoverage() != null) {
			Indicator indicator = new Indicator();
			indicator.setValue(appleWarranty.getWarrantyDetailInfo().isPremiumCoverage().toString());
			deviceInfoImeiWarranty.getWarranty().setPremiumCoverageVendor(indicator);
		}
		setImeiWarrantyDeviceInfo(tmoNonSupported, rootImeiWarrantyLocal, deviceInfoImeiWarranty, appleWarranty);
		if(null != varHardwareInfo)
			setImeiWarrantyShipmentData(csiGetIMEIResponse, rootImeiWarrantyLocal, deviceInfoImeiWarranty, currentTransImei,
					shipmentData, varHardwareInfo);
	}

	/**
	 * @param rootImeiWarrantyLocal
	 * @param deviceInfo
	 * @param appleWarranty
	 */
	private void setDeviceModel(ImeiWarranty rootImeiWarrantyLocal,
			com.tmobile.services.productmanagement.deviceenterprise.v1.DeviceInfo deviceInfo,
			WarrantyDetail appleWarranty) {
		if (rootImeiWarrantyLocal.getManufacturer() != null) {
			deviceInfo.setDeviceManufacturer(new Manufacturer());
			deviceInfo.getDeviceManufacturer().setManufacturerName(rootImeiWarrantyLocal.getManufacturer());
		}
		if (StringUtils.isNotEmpty(appleWarranty.getConfigDescription())) {
			deviceInfo.setDeviceModel(appleWarranty.getConfigDescription());
		} else {
			deviceInfo.setDeviceModel(rootImeiWarrantyLocal.getModel());
		}
	}

	/**
	 * @param csiGetIMEIResponse
	 * @param rootImeiWarrantyLocal
	 * @param deviceInfo
	 * @param currentTransImei
	 * @param shipmentData
	 * @param varHardwareInfo
	 */
	private void setImeiWarrantyShipmentData(CSIGetIMEIResponseType csiGetIMEIResponse,
			ImeiWarranty rootImeiWarrantyLocal,
			com.tmobile.services.productmanagement.deviceenterprise.v1.DeviceInfo deviceInfo, String currentTransImei,
			GetShipUsageDataForIMEIResponseType shipmentData, DeviceSpecificationDetails varHardwareInfo) {
		if (varHardwareInfo.getDeviceSpecificationDetail().getTmoMerchandizeInfo() != null) {
			deviceInfo.setLastTMODiagnosticDate(
					varHardwareInfo.getDeviceSpecificationDetail().getTmoMerchandizeInfo().getLastTmoDiagnosticDate());
		}
		if (rootImeiWarrantyLocal.getMsisdn() != null) {
			deviceInfo.setMSISDN(rootImeiWarrantyLocal.getMsisdn().substring(1));
		}
		if (currentTransImei != null) {
			deviceInfo.setImei(currentTransImei);
		}
		if (rootImeiWarrantyLocal.getDeviceType() != null) {
			deviceInfo.setDeviceType(rootImeiWarrantyLocal.getDeviceType());
		}
		if (shipmentData.getShipDate() != null) {
				deviceInfo.setShipmentDate(
						TibcoToJavaUtil.convertDateStringToXMLGregorianCalendar(shipmentData.getShipDate(), DD_MMM_YY));
		}
		if (shipmentData.getShipDealerCode() != null) {
			deviceInfo.setShipmentDealerCode(shipmentData.getShipDealerCode());
		}
		if (!CollectionUtils.isEmpty(csiGetIMEIResponse.getImeiInfo())) {
			setCsiGetIMEIResponseImeiInfo(csiGetIMEIResponse, deviceInfo);
		}
	}

	/**
	 * @param csiGetIMEIResponse
	 * @param deviceInfo
	 */
	private void setCsiGetIMEIResponseImeiInfo(CSIGetIMEIResponseType csiGetIMEIResponse,
			com.tmobile.services.productmanagement.deviceenterprise.v1.DeviceInfo deviceInfo) {
		if (csiGetIMEIResponse.getImeiInfo().get(0).getSupported() != null) {
			deviceInfo.setTmoApproved(csiGetIMEIResponse.getImeiInfo().get(0).getSupported());
		}
		if (csiGetIMEIResponse.getImeiInfo().get(0).getFaveCapable() != null) {
			deviceInfo.setMyNetworkComp(csiGetIMEIResponse.getImeiInfo().get(0).getFaveCapable());
		}
		if (csiGetIMEIResponse.getImeiInfo().get(0).getUmaCapable() != null) {
			deviceInfo.setUmaCapable(csiGetIMEIResponse.getImeiInfo().get(0).getUmaCapable());
		}
		if (csiGetIMEIResponse.getImeiInfo().get(0).getRj11RouterInd() != null) {
			deviceInfo.setRouteIndicator(csiGetIMEIResponse.getImeiInfo().get(0).getRj11RouterInd());
		}
		if (csiGetIMEIResponse.getImeiInfo().get(0).getPdaInd() != null) {
			deviceInfo.setPdaIndicator(csiGetIMEIResponse.getImeiInfo().get(0).getPdaInd());
		}
		if (csiGetIMEIResponse.getImeiInfo().get(0).getNfcCapable() != null) {
			deviceInfo.setNfcIndicator(csiGetIMEIResponse.getImeiInfo().get(0).getNfcCapable());
		}
		if (csiGetIMEIResponse.getImeiInfo().get(0).getIsisCertifiedInd() != null) {
			deviceInfo.setIsisCertifiedIndicator(csiGetIMEIResponse.getImeiInfo().get(0).getIsisCertifiedInd());
		}
	}

	/**
	 * @param tmoNonSupported
	 * @param rootImeiWarrantyLocal
	 * @param deviceInfo
	 * @param appleWarranty
	 */
	private void setImeiWarrantyDeviceInfo(List<String> tmoNonSupported, ImeiWarranty rootImeiWarrantyLocal,
			com.tmobile.services.productmanagement.deviceenterprise.v1.DeviceInfo deviceInfo,
			WarrantyDetail appleWarranty) {
		if (rootImeiWarrantyLocal.getDeviceType() != null
				&& tmoNonSupported.contains(rootImeiWarrantyLocal.getDeviceType())) {
			Indicator indicator = new Indicator();
			indicator.setValue("false");
			deviceInfo.getWarranty().setTmoMerchandiseIndicator(indicator);
		} else {
			Indicator indicator = new Indicator();
			indicator.setValue("true");
			deviceInfo.getWarranty().setTmoMerchandiseIndicator(indicator);
		}
		if (appleWarranty.getImageURL() != null) {

			Attachment attachment = new Attachment();
			attachment.getName().add("imageURL");
			attachment.setURI(appleWarranty.getImageURL());
			deviceInfo.getWarranty().getWarrantyAttachment().add(attachment);
		}
		if (StringUtils.isNotEmpty(appleWarranty.getExplodedViewURL())) {
			Attachment attachment = new Attachment();
			attachment.getName().add("explodedViewURL");
			attachment.setURI(appleWarranty.getExplodedViewURL());
			deviceInfo.getWarranty().getWarrantyAttachment().add(attachment);
		}
		if (StringUtils.isNotEmpty(appleWarranty.getManualURL())) {
			Attachment attachment = new Attachment();
			attachment.getName().add("manualURL");
			attachment.setURI(appleWarranty.getManualURL());
			deviceInfo.getWarranty().getWarrantyAttachment().add(attachment);
		}
		if (rootImeiWarrantyLocal.getShipmentType() != null) {
			SpecificationGroup specification = new SpecificationGroup();
			SpecificationValue value = new SpecificationValue();
			value.setName("INS_PROVIDER_CD");
			value.setValue(rootImeiWarrantyLocal.getShipmentType());
			specification.getSpecificationValue().add(value);
			deviceInfo.getWarranty().getWarrantySpecification().add(specification);
		}
		if (rootImeiWarrantyLocal.getFavesVersion() != null) {
			SpecificationGroup specification = new SpecificationGroup();
			SpecificationValue value = new SpecificationValue();
			value.setName("FAVES_VER");
			value.setValue(rootImeiWarrantyLocal.getFavesVersion());
			specification.getSpecificationValue().add(value);
			deviceInfo.getWarranty().getWarrantySpecification().add(specification);
		}
		if (rootImeiWarrantyLocal.getRepairCode() != null) {
			SpecificationGroup specification = new SpecificationGroup();
			SpecificationValue value = new SpecificationValue();
			value.setName("REPAIR_TYPE_CD");
			value.setValue(rootImeiWarrantyLocal.getRepairCode());
			specification.getSpecificationValue().add(value);
			deviceInfo.getWarranty().getWarrantySpecification().add(specification);
		}
		if (rootImeiWarrantyLocal.getAccessoryWarrantyExpirationDate() != null) {
			SpecificationGroup specification = new SpecificationGroup();
			SpecificationValue value = new SpecificationValue();
			value.setName("AccessoryWarrantyExpirationDate");
			value.setValue(rootImeiWarrantyLocal.getAccessoryWarrantyExpirationDate());
			specification.getSpecificationValue().add(value);
			deviceInfo.getWarranty().getWarrantySpecification().add(specification);
		}
		if (appleWarranty.isPowerTrainFlag() != null) {
			SpecificationGroup specification = new SpecificationGroup();
			SpecificationValue value = new SpecificationValue();
			value.setName("powerTrainFlag");
			value.setValue(appleWarranty.isPowerTrainFlag().toString());
			specification.getSpecificationValue().add(value);
			deviceInfo.getWarranty().getWarrantySpecification().add(specification);
		}
		if (appleWarranty.isIsPersonalized() != null) {
			SpecificationGroup specification = new SpecificationGroup();
			SpecificationValue value = new SpecificationValue();
			value.setName("isPersonalized");
			value.setValue(appleWarranty.isIsPersonalized().toString());
			specification.getSpecificationValue().add(value);
			deviceInfo.getWarranty().getWarrantySpecification().add(specification);
		}
		if (rootImeiWarrantyLocal.getLastUseDate() != null) {
			deviceInfo.setLastUseDate(TibcoToJavaUtil
					.convertDateStringToXMLGregorianCalendar(rootImeiWarrantyLocal.getLastUseDate(), YYYY_MM_DD));
		}
		if (rootImeiWarrantyLocal.getFirstUseDate() != null) {
			deviceInfo.setFirstUseDate(TibcoToJavaUtil
					.convertDateStringToXMLGregorianCalendar(rootImeiWarrantyLocal.getFirstUseDate(), YYYY_MM_DD));
		}
	}

	/**
	 * @param deviceInfoVar
	 * @param varHardwareInfo
	 * @param offeringVarient
	 * @param processingHistory
	 * @param ftLock
	 */
	private void setVarHardwareInfo(com.tmobile.services.productmanagement.deviceenterprise.v1.DeviceInfo deviceInfoVar,
			DeviceSpecificationDetails varHardwareInfo, OfferingVariant offeringVarient,
			ActivityInstance processingHistory, TheftLock ftLock) {
		if (varHardwareInfo.getDeviceSpecificationDetail() != null
				&& varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo() != null) {
			offeringVarient.setMemory(
					varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo().getMaxInternalStorage());
		}
		deviceInfoVar.setSerialNumber(varHardwareInfo.getIMEI());
		if (StringUtils.isNotEmpty(varHardwareInfo.getIMEIType())) {
			deviceInfoVar.setSerialNumberType(SerialNumberType.valueOf(varHardwareInfo.getIMEIType().toUpperCase()));
		}
		if (varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo().getHardwareTestRunDate() != null) {
			TimePeriod activityPeriod = new TimePeriod();
			processingHistory.setActivityPeriod(activityPeriod);
			activityPeriod.setEndTime(
					varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo().getHardwareTestRunDate());
		}
		if (varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo().getHardwareTestStatus() != null) {
			ActivityInstanceStatus activityStatus = new ActivityInstanceStatus();
			processingHistory.getActivityInstanceStatus();
			activityStatus.setStatusCode(
					varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo().getHardwareTestStatus().value());
			processingHistory.getActivityInstanceStatus().add(activityStatus);
		}
		deviceInfoVar.setOldServiceProvider(
				varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo().getCarrierName());
		deviceInfoVar.setMobileOS(varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo().getMobileOS());
		deviceInfoVar.setFirmware(varHardwareInfo.getDeviceSpecificationDetail().getHardwareInfo().getFirmware());
		deviceInfoVar.setDeviceRooting(new DeviceRooting());
		if (varHardwareInfo.getDeviceSpecificationDetail().getRootInfo().isCurrentlyRooted() != null) {
			Indicator indicator = new Indicator();
			indicator.setValue(
					varHardwareInfo.getDeviceSpecificationDetail().getRootInfo().isCurrentlyRooted().toString());
			deviceInfoVar.getDeviceRooting().setIsCurrentlyRooted(indicator);
		}
		if (varHardwareInfo.getDeviceSpecificationDetail().getRootInfo().getCurrentRootDate() != null) {
			deviceInfoVar.getDeviceRooting().setCurrentRootDate(
					varHardwareInfo.getDeviceSpecificationDetail().getRootInfo().getCurrentRootDate());
		}
		if (varHardwareInfo.getDeviceSpecificationDetail().getRootInfo().isEverRooted() != null) {
			Indicator indicator = new Indicator();
			indicator.setValue(varHardwareInfo.getDeviceSpecificationDetail().getRootInfo().isEverRooted().toString());
			deviceInfoVar.getDeviceRooting().setEverRooted(indicator);
		}
		deviceInfoVar.getDeviceRooting()
				.setLastRootingDate(varHardwareInfo.getDeviceSpecificationDetail().getRootInfo().getLastRootDate());
		if ("IOS"
				.equalsIgnoreCase(varHardwareInfo.getDeviceSpecificationDetail().getTheftLockInfo().getType().name())) {
			ftLock.setTheftLockType(TheftLockType.valueOf("FMIP"));
		} else if (ANDROID
				.equalsIgnoreCase(varHardwareInfo.getDeviceSpecificationDetail().getTheftLockInfo().getType().name())) {
			ftLock.setTheftLockType(TheftLockType.valueOf(ANTITHEFT));
		} else {
			ftLock.setTheftLockType(TheftLockType.valueOf(OTHER));
		}
		deviceInfoVar.setTheftLock(ftLock);
		if (varHardwareInfo.getDeviceSpecificationDetail().getTheftLockInfo().getLastRetrivalDate() != null) {
			deviceInfoVar.getTheftLock().setLastRetrievalDate(
					varHardwareInfo.getDeviceSpecificationDetail().getTheftLockInfo().getLastRetrivalDate());
		}
		if (varHardwareInfo.getDeviceSpecificationDetail().getTheftLockInfo().getStatus() != null) {
			deviceInfoVar.getTheftLock().getStatus().setName(varHardwareInfo.getDeviceSpecificationDetail()
					.getTheftLockInfo().getStatus().getDeclaringClass().getName());
		}
	}

	/**
	 * @param errorLinkHandlerFaultData
	 * @param getDeviceWarrantyDetailOrchEndOutput
	 * @param getDeviceDetailsResponse
	 * @param lunhIMEI
	 * @param csiGetIMEIResponse
	 * @param shipmentLastUsageData
	 * @param varTargetSystem
	 * @param requestIMEI
	 * @param requestMSISDN
	 * @param dmwMISISDN
	 */
	private void setResponseStatusError(ErrorDetails errorLinkHandlerFaultData,
			GetDeviceDetailsOrchEndOutput getDeviceWarrantyDetailOrchEndOutput,
			GetDeviceDetailsResponse getDeviceDetailsResponse, MapperInputType lunhIMEI,
			CSIGetIMEIResponseType csiGetIMEIResponse,
			com.tibco.namespaces.tnt.plugins.generalactivities.mapper._1573734489550.MapperInputType shipmentLastUsageData,
			String varTargetSystem, String requestIMEI, List<String> requestMSISDN, List<String> dmwMISISDN) {
		getDeviceDetailsResponse.getResponseStatus().setCode(BigInteger.valueOf(101));
		ResponseStatusDetail detail = new ResponseStatusDetail();
		getDeviceDetailsResponse.getResponseStatus().getDetail().add(detail);
		if (lunhIMEI.getErrorMsg() != null) {
			detail.setSubStatusCode(GENERAL_0000);
			detail.setDefinition("ERROR_SOURCE: ESP; ERROR_CODE: ; LUNH Validation failed for one or more IMEI's");
			detail.setExplanation(UNKNOWN_BACKEND_ERROR);
		}
		if (errorLinkHandlerFaultData.getCode() != null || csiGetIMEIResponse.getImeiInfo().size() > 0) {

			if (csiGetIMEIResponse.getImeiInfo().stream().filter(imeiInfo -> StringUtils.isEmpty(imeiInfo.getImei()))
					.count() > 0) {
				detail.setSubStatusCode(GENERAL_0000);
				detail.setDefinition(ERROR_SOURCE + varTargetSystem + "; ERROR_CODE: Requested IMEI '" + requestIMEI
						+ "' Not Found");
				detail.setExplanation("No Data Found");
			} else {
				detail.setSubStatusCode(GENERAL_0000);
				detail.setDefinition(
						ERROR_SOURCE + varTargetSystem + "; ERROR_CODE: ;+ Failed to retrieve all device details");
				detail.setExplanation(UNKNOWN_BACKEND_ERROR);
			}
			if (isGSXError(errorLinkHandlerFaultData, getDeviceWarrantyDetailOrchEndOutput)) {
				detail.setSubStatusCode(GENERAL_0000);
				detail.setDefinition("ERROR_SOURCE: GSX; ERROR_CODE: ; Failed to retrieve all device details");
				detail.setExplanation(UNKNOWN_BACKEND_ERROR);
			}
		}
		if (errorLinkHandlerFaultData.getCode() != null) {
			detail.setSubStatusCode(GENERAL_0000);
			detail.setDefinition("ERROR_SOURCE: UNION; ERROR_CODE: ; Failed to retrieve all device details");
			detail.setExplanation(UNKNOWN_BACKEND_ERROR);
		}
		if (isShipmentLastUsageDataStatusCode(shipmentLastUsageData)) {
			detail.setSubStatusCode(GENERAL_0000);
			detail.setDefinition(
					ERROR_SOURCE + varTargetSystem + "; ERROR_CODE: ;, Failed to retrieve all device shipping details");
			detail.setExplanation(UNKNOWN_BACKEND_ERROR);
		}
		setMissingMSISDNErrorDetail(varTargetSystem, requestMSISDN, dmwMISISDN, detail);
	}

	/**
	 * @param varTargetSystem
	 * @param requestMSISDN
	 * @param dmwMISISDN
	 * @param detail
	 */
	private void setMissingMSISDNErrorDetail(String varTargetSystem, List<String> requestMSISDN,
			List<String> dmwMISISDN, ResponseStatusDetail detail) {
		if (dmwMISISDN.size() != requestMSISDN.size()) {
			List<String> varMisisdn = new ArrayList<String>();
			for (String var : requestMSISDN) {
				if (!dmwMISISDN.contains(var)) {
					varMisisdn.add(var);
				}
			}

			String missingMSISDN = TibcoToJavaUtil.concateSequenceFormat(varMisisdn, ",");
			detail.setSubStatusCode("PRODUCT-0150");
			detail.setDefinition(ERROR_SOURCE + varTargetSystem + "; ERROR_CODE: MSISDN has no association to IMEI");
			detail.setExplanation("MSISDN " + missingMSISDN + " has no association to IMEI in the source of record");
		}
	}

	/**
	 * @param shipmentLastUsageData
	 * @return
	 */
	private boolean isShipmentLastUsageDataStatusCode(
			com.tibco.namespaces.tnt.plugins.generalactivities.mapper._1573734489550.MapperInputType shipmentLastUsageData) {
		return shipmentLastUsageData.getGetShipUsageDataForIMEIResponse() != null
				&& shipmentLastUsageData.getGetShipUsageDataForIMEIResponse().getStatus() != null
				&& shipmentLastUsageData.getGetShipUsageDataForIMEIResponse().getStatus().getStatusCode() != null;
	}

	/**
	 * @param errorLinkHandlerFaultData
	 * @param getDeviceWarrantyDetailOrchEndOutput
	 * @return
	 */
	private boolean isGSXError(ErrorDetails errorLinkHandlerFaultData,
			GetDeviceDetailsOrchEndOutput getDeviceWarrantyDetailOrchEndOutput) {
		return errorLinkHandlerFaultData.getCode() != null
				|| (getDeviceWarrantyDetailOrchEndOutput.getRoot().getGetDeviceWarrantyDetailResponse() != null
						&& getDeviceWarrantyDetailOrchEndOutput.getRoot().getGetDeviceWarrantyDetailResponse()
								.getStatus() != null
						&& ("101"
								.equalsIgnoreCase(getDeviceWarrantyDetailOrchEndOutput.getRoot()
										.getGetDeviceWarrantyDetailResponse().getStatus().getStatusCode())
								|| "102".equalsIgnoreCase(getDeviceWarrantyDetailOrchEndOutput.getRoot()
										.getGetDeviceWarrantyDetailResponse().getStatus().getStatusCode())));
	}

	/**
	 * @param getDeviceDetailsRequest
	 * @param errorLinkHandlerFaultData
	 * @param getDeviceWarrantyDetailOrchEndOutput
	 * @param lunhIMEI
	 * @param csiGetIMEIResponse
	 * @param shipmentLastUsageData
	 * @return
	 */
	private boolean responseStatusErrorFlag(GetDeviceDetailsRequest getDeviceDetailsRequest,
			ErrorDetails errorLinkHandlerFaultData, GetDeviceDetailsOrchEndOutput getDeviceWarrantyDetailOrchEndOutput,
			MapperInputType lunhIMEI, CSIGetIMEIResponseType csiGetIMEIResponse,
			com.tibco.namespaces.tnt.plugins.generalactivities.mapper._1573734489550.MapperInputType shipmentLastUsageData) {
		return lunhIMEI.getErrorMsg() != null || errorLinkHandlerFaultData.getCode() != null
				|| ((getDeviceWarrantyDetailOrchEndOutput.getRoot().getGetDeviceWarrantyDetailResponse() != null
						&& getDeviceWarrantyDetailOrchEndOutput.getRoot().getGetDeviceWarrantyDetailResponse()
								.getStatus() != null)
						&& ("101"
								.equalsIgnoreCase(getDeviceWarrantyDetailOrchEndOutput.getRoot()
										.getGetDeviceWarrantyDetailResponse().getStatus().getStatusCode())
								|| "102".equalsIgnoreCase(getDeviceWarrantyDetailOrchEndOutput.getRoot()
										.getGetDeviceWarrantyDetailResponse().getStatus().getStatusCode())))
				|| csiGetIMEIResponse.getImeiInfo().stream().filter(imeiInfo -> StringUtils.isEmpty(imeiInfo.getImei()))
						.count() > 0
				|| (null != shipmentLastUsageData.getGetShipUsageDataForIMEIResponse()
						&& null != shipmentLastUsageData.getGetShipUsageDataForIMEIResponse().getStatus()
						&& StringUtils.isNotEmpty(
								shipmentLastUsageData.getGetShipUsageDataForIMEIResponse().getStatus().getStatusCode())
						&& !"0".equals(
								shipmentLastUsageData.getGetShipUsageDataForIMEIResponse().getStatus().getStatusCode()))
				|| (csiGetIMEIResponse.getStatus() != null
						&& getDeviceDetailsRequest.getMSISDN().size() != csiGetIMEIResponse.getImeiInfo().stream()
								.filter(imeiInfo -> StringUtils.isNotEmpty(imeiInfo.getSimNumber())).count());
	}

	/**
	 * Method logGetWarrantyError
	 * 
	 * @param LogwarningdetailsStartInput logWarningDetailsStartInput
	 *                                    ,ErrorLinkHandlerFaultData
	 *                                    errorLinkHandlerFaultData
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void logGetWarrantyError(LogwarningdetailsStartInput logWarningDetailsStartInput,
			ErrorDetails errorLinkHandlerFaultData) throws TMobileProcessException {
		com.tmobile.services.schema.dataobject.logwarningdetailsstartinput.Root logGetWarrantyErrRoot = logWarningDetailsStartInput
				.getRoot();
		logGetWarrantyErrRoot.setTargetSystem("GSX");
		if (errorLinkHandlerFaultData.getCode() != null) {
			logGetWarrantyErrRoot.setErrorCode(errorLinkHandlerFaultData.getCode());
		}
		if (errorLinkHandlerFaultData.getMesssage() != null) {
			logGetWarrantyErrRoot.setErrorMessage(errorLinkHandlerFaultData.getMesssage());
		}
		if (errorLinkHandlerFaultData.getStackTrace() != null) {
			logGetWarrantyErrRoot.setStackTrace(errorLinkHandlerFaultData.getStackTrace());
		}
		if (errorLinkHandlerFaultData.getProcessStack() != null) {
			logGetWarrantyErrRoot.setProcessStack(errorLinkHandlerFaultData.getProcessStack());
		}
		iLogWarningDetailsService.executeMethod(logWarningDetailsStartInput);
	}

	/**
	 * Method getdevicedetailsError
	 * 
	 * @param ErrorDetails errorDetails ,GetDeviceDetailsRequest
	 *                     getDeviceDetailsRequest ,GetdevicedetailsErrorinput
	 *                     getdevicedetailsErrorinput ,ErrorLinkHandlerFaultData
	 *                     errorLinkHandlerFaultData ,GetDeviceDetailsResponse
	 *                     getDeviceDetailsResponse ,MapperInputType
	 *                     accumulatedOutput
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void getdevicedetailsError(ErrorDetails errorDetails, GetDeviceDetailsRequest getDeviceDetailsRequest,
			MapperInputType accumulatedOutput) throws TMobileProcessException {
		if (errorDetails.getCode() != null || !StringUtils.isEmpty(accumulatedOutput.getIMEI())) {
			if ("true".equalsIgnoreCase(TibcoToJavaUtil.getProperty(TMO_APP_HANA_ENABLE_HANA_FLOW))) {
				errorDetails.setTargetSystem("HANA");
			} else {
				errorDetails.setTargetSystem("DMW");
			}

			if (errorDetails.getCode() == null) {
				errorDetails.setCode("B02");
			}
			if (errorDetails.getMesssage() != null) {
				errorDetails.setMesssage("Input IMEI is not valid");
			}
		} else {
			errorDetails.setTargetSystem("ESP");

			if (errorDetails.getCode() == null) {
				errorDetails.setCode("B02");
			}
			if (errorDetails.getMesssage() != null) {
				errorDetails.setMesssage("Input IMEI is not valid");
			}
			errorDetails.setSourceMessage(TibcoToJavaUtil.renderXml(getDeviceDetailsRequest));
		}
		throw new TMobileProcessException(errorDetails);
	}

	/**
	 * Method getimeistatusformlError
	 * 
	 * @param ErrorDetails errorDetails ,GetDeviceDetailsRequest
	 *                     getDeviceDetailsRequest ,ErrorLinkHandlerFaultData
	 *                     errorLinkHandlerFaultData ,GetDeviceDetailsResponse
	 *                     getDeviceDetailsResponse ,CSIGetIMEIResponseType
	 *                     csiGetIMEIResponse ,GetimeistatusformlErrorinput
	 *                     getimeistatusformlErrorinput
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void getimeistatusformlError(ErrorDetails errorDetails, GetDeviceDetailsRequest getDeviceDetailsRequest,
			CSIGetIMEIResponseType csiGetIMEIResponse) throws TMobileProcessException {
		if (errorDetails.getCode() != null) {
			if ("true".equalsIgnoreCase(TibcoToJavaUtil.getProperty(TMO_APP_HANA_ENABLE_HANA_FLOW))) {
				errorDetails.setTargetSystem("HANA");
			} else {
				errorDetails.setTargetSystem("DMW");
			}
		}
		if (0 == csiGetIMEIResponse.getImeiInfo().size()) {
			if ("true".equalsIgnoreCase(TibcoToJavaUtil.getProperty(TMO_APP_HANA_ENABLE_HANA_FLOW))) {
				errorDetails.setTargetSystem("HANA");
			} else {
				errorDetails.setTargetSystem("DMW");
			}
			errorDetails.setCode("102");
			errorDetails.setMesssage("No Data Found");
		} else {
			errorDetails.setTargetSystem("ESP");
			errorDetails.setSourceMessage(TibcoToJavaUtil.renderXml(getDeviceDetailsRequest));
		}
		throw new TMobileProcessException(errorDetails);
	}

	/**
	 * Method logGetDeviceSpecificationsError
	 * 
	 * @param LogwarningdetailsStartInput logWarningDetailsStartInput
	 *                                    ,ErrorLinkHandlerFaultData
	 *                                    errorLinkHandlerFaultData
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void logGetDeviceSpecificationsError(LogwarningdetailsStartInput logWarningDetailsStartInput,
			ErrorDetails errorLinkHandlerFaultData) throws TMobileProcessException {
		com.tmobile.services.schema.dataobject.logwarningdetailsstartinput.Root logGetDeviceSpecErrRoot = logWarningDetailsStartInput
				.getRoot();
		logGetDeviceSpecErrRoot.setTargetSystem("UNION");
		if (errorLinkHandlerFaultData.getCode() != null) {
			logGetDeviceSpecErrRoot.setErrorCode(errorLinkHandlerFaultData.getCode());
		}
		if (errorLinkHandlerFaultData.getMesssage() != null) {
			logGetDeviceSpecErrRoot.setErrorMessage(errorLinkHandlerFaultData.getMesssage());
		}
		if (errorLinkHandlerFaultData.getStackTrace() != null) {
			logGetDeviceSpecErrRoot.setStackTrace(errorLinkHandlerFaultData.getStackTrace());
		}
		if (errorLinkHandlerFaultData.getProcessStack() != null) {
			logGetDeviceSpecErrRoot.setProcessStack(errorLinkHandlerFaultData.getProcessStack());
		}
		iLogWarningDetailsService.executeMethod(logWarningDetailsStartInput);
	}

	/**
	 * Method logGetimeistatusformlError
	 * 
	 * @param LogwarningdetailsStartInput logWarningDetailsStartInput
	 *                                    ,ErrorLinkHandlerFaultData
	 *                                    errorLinkHandlerFaultData
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void logGetimeistatusformlError(LogwarningdetailsStartInput logWarningDetailsStartInput,
			ErrorDetails errorLinkHandlerFaultData) throws TMobileProcessException {
		com.tmobile.services.schema.dataobject.logwarningdetailsstartinput.Root root = logWarningDetailsStartInput
				.getRoot();
		if ("true".equalsIgnoreCase(TibcoToJavaUtil.getProperty(TMO_APP_HANA_ENABLE_HANA_FLOW))) {
			root.setTargetSystem("HANA");
		} else {
			root.setTargetSystem("DMW");
		}
		if (errorLinkHandlerFaultData.getCode() != null) {
			root.setErrorCode(errorLinkHandlerFaultData.getCode());
		}
		if (errorLinkHandlerFaultData.getMesssage() != null) {
			root.setErrorMessage(errorLinkHandlerFaultData.getMesssage());
		}
		if (errorLinkHandlerFaultData.getStackTrace() != null) {
			root.setStackTrace(errorLinkHandlerFaultData.getStackTrace());
		}
		if (errorLinkHandlerFaultData.getProcessStack() != null) {
			root.setProcessStack(errorLinkHandlerFaultData.getProcessStack());
		}
		if (errorLinkHandlerFaultData.getSourceMessage() != null) {
			root.setSourceMessage(errorLinkHandlerFaultData.getSourceMessage());
		}
		iLogWarningDetailsService.executeMethod(logWarningDetailsStartInput);
	}

	/**
	 * Method executeMethod
	 * 
	 * @param
	 * @return void
	 * @throws TMobileProcessException
	 */
	public void executeMethod(GetDeviceDetailsRequest getDeviceDetailsRequest,
			GetDeviceDetailsResponse getDeviceDetailsResponse) throws TMobileProcessException {
		WarrantyDetails warrantyDetails = new WarrantyDetails();
		LogwarningdetailsStartInput logWarningDetailsStartInput = new LogwarningdetailsStartInput();
		ErrorDetails errorDetails = new ErrorDetails();
		GetDeviceDetailsOrchEndOutput getDeviceWarrantyDetailOrchEndOutput = new GetDeviceDetailsOrchEndOutput();
		com.tibco.namespaces.tnt.plugins.generalactivities.mapper._1573734489550.MapperInputType shipmentLastUsageData = new com.tibco.namespaces.tnt.plugins.generalactivities.mapper._1573734489550.MapperInputType();
		MapperInputType lunhIMEI = new MapperInputType();
		CSIGetIMEIRequestType csiGetIMEIRequest = new CSIGetIMEIRequestType();
		DeviceSpecificationDetails deviceSpecificationDetails = new DeviceSpecificationDetails();
		Guid guid = new Guid();
		GetDeviceDetailsOrchDMWResponse getDeviceDetailsOrchDMWResponse = new GetDeviceDetailsOrchDMWResponse();
		GetDeviceSpecificationsRequest getDeviceSpecificationsRequest = new GetDeviceSpecificationsRequest();
		LogVariables logVariables = new LogVariables();
		CSIGetIMEIResponseType csiGetIMEIResponse = new CSIGetIMEIResponseType();
		CSIQueryIMEIWarrantyResponseType queryIMEIWarrantyResponse = new CSIQueryIMEIWarrantyResponseType();
		StringBuilder imeiIterationelem = new StringBuilder();
		CSIQueryIMEIWarrantyRequestType queryIMEIWarrantyRequest = new CSIQueryIMEIWarrantyRequestType();
		GetDeviceDetailsOrchStartInput getDeviceWarrantyDetailOrchStartInput = new GetDeviceDetailsOrchStartInput();

		long startMsisdnCount = getDeviceDetailsRequest.getMSISDN().stream()
				.filter(msisdn -> !StringUtils.isEmpty(msisdn)).count();

		logVariables(logVariables);
		if (getDeviceDetailsRequest.getMSISDN().size() == 0) {
			initTransformedimeiNoMsisdnResponse(getDeviceDetailsRequest, warrantyDetails);
		} else {
			initTransformedImeiMsisdnResponse(getDeviceDetailsRequest, warrantyDetails,
					logWarningDetailsStartInput, errorDetails, lunhIMEI, getDeviceDetailsOrchDMWResponse, csiGetIMEIResponse, queryIMEIWarrantyResponse, imeiIterationelem,
					queryIMEIWarrantyRequest);
		}

		try {
			if (isRetrieveDeviceHardwareInfo(getDeviceDetailsRequest, startMsisdnCount)) {
				invokeGetimeistatus(csiGetIMEIRequest, warrantyDetails, getDeviceDetailsRequest, csiGetIMEIResponse);
			}
		} catch (TMobileProcessException ex) {
			LOGGER.error("Error occurred in method: invokeGetimeistatus() and terminated due to ", ex);
			if (null != ex.getErrorDetails())
				errorDetails = ex.getErrorDetails();
			logGetimeistatusformlError(logWarningDetailsStartInput, errorDetails);
			getimeistatusformlError(errorDetails, getDeviceDetailsRequest, csiGetIMEIResponse);
		}

		long imeiWarrantyCount = warrantyDetails.getRoot().getImeiWarranty().stream()
				.filter(imeiWarranty -> ((!StringUtils.isEmpty(imeiWarranty.getModel())
						&& imeiWarranty.getModel().toUpperCase().contains(IPHONE))
						|| (!StringUtils.isEmpty(imeiWarranty.getManufacturer())
								&& imeiWarranty.getManufacturer().toUpperCase().contains(APPLE)))
						&& !StringUtils.isEmpty(imeiWarranty.getIMEI()))
				.count();

		long imeiInfoCount = getImeiInfoCount(csiGetIMEIResponse);

		if (isRetrieveDeviceWarrantyInfo(getDeviceDetailsRequest, startMsisdnCount, imeiWarrantyCount)) {
			getGUID(guid);
		}
		try {
			invokeGetdevicewarrantydetailorch(getDeviceDetailsRequest, warrantyDetails,
					getDeviceWarrantyDetailOrchEndOutput, lunhIMEI, guid, imeiIterationelem,
					getDeviceWarrantyDetailOrchStartInput, startMsisdnCount, imeiWarrantyCount, imeiInfoCount);
		} catch (TMobileProcessException ex) {
			LOGGER.error("Error occurred in method: invokeGetdevicewarrantydetailorch() and terminated due to ", ex);
			if (null != ex.getErrorDetails())
				errorDetails = ex.getErrorDetails();
			logGetWarrantyError(logWarningDetailsStartInput, errorDetails);
		}

		isRetrieveShipDataOrDeviceHardwareInfo(getDeviceDetailsRequest, warrantyDetails, logWarningDetailsStartInput, errorDetails, shipmentLastUsageData, lunhIMEI,
				deviceSpecificationDetails, getDeviceSpecificationsRequest, csiGetIMEIResponse, imeiIterationelem,
				startMsisdnCount);
		mergeResponses(warrantyDetails, getDeviceDetailsRequest, errorDetails, getDeviceDetailsOrchDMWResponse,
				getDeviceWarrantyDetailOrchEndOutput, deviceSpecificationDetails, getDeviceDetailsResponse, lunhIMEI,
				csiGetIMEIResponse, shipmentLastUsageData);

	}

	/**
	 * @param csiGetIMEIResponse
	 * @return
	 */
	private long getImeiInfoCount(CSIGetIMEIResponseType csiGetIMEIResponse) {
		return csiGetIMEIResponse.getImeiInfo().stream().filter(imeiInfo -> (!StringUtils.isEmpty(imeiInfo.getModel())
				&& imeiInfo.getModel().toUpperCase().contains(IPHONE))
				|| (!StringUtils.isEmpty(imeiInfo.getMake()) && imeiInfo.getMake().toUpperCase().contains(APPLE)))
				.count();
	}

	/**
	 * @param getDeviceDetailsRequest
	 * @param startMsisdnCount
	 * @return
	 */
	private boolean isRetrieveDeviceHardwareInfo(GetDeviceDetailsRequest getDeviceDetailsRequest,
			long startMsisdnCount) {
		return getDeviceDetailsRequest.getMSISDN().size() == 0
				|| (startMsisdnCount == 1 && getDeviceDetailsRequest.isRetrieveDeviceHardwareInfo());
	}

	/**
	 * @param getDeviceDetailsRequest
	 * @param startMsisdnCount
	 * @param imeiWarrantyCount
	 * @return
	 */
	private boolean isRetrieveDeviceWarrantyInfo(GetDeviceDetailsRequest getDeviceDetailsRequest, long startMsisdnCount,
			long imeiWarrantyCount) {
		return imeiWarrantyCount > 0 && startMsisdnCount == 1 && getDeviceDetailsRequest.isRetrieveDeviceWarrantyInfo();
	}

	/**
	 * @param getDeviceDetailsRequest
	 * @param getDeviceDetailsResponse
	 * @param warrantyDetails
	 * @param logWarningDetailsStartInput
	 * @param errorDetails
	 * @param shipmentLastUsageData
	 * @param lunhIMEI
	 * @param deviceSpecificationDetails
	 * @param getDeviceSpecificationsRequest
	 * @param csiGetIMEIResponse
	 * @param imeiIterationelem
	 * @param startMsisdnCount
	 * @return
	 * @throws TMobileProcessException
	 */
	private void isRetrieveShipDataOrDeviceHardwareInfo(GetDeviceDetailsRequest getDeviceDetailsRequest,
			WarrantyDetails warrantyDetails,
			LogwarningdetailsStartInput logWarningDetailsStartInput, ErrorDetails errorDetails,
			com.tibco.namespaces.tnt.plugins.generalactivities.mapper._1573734489550.MapperInputType shipmentLastUsageData,
			MapperInputType lunhIMEI, DeviceSpecificationDetails deviceSpecificationDetails,
			GetDeviceSpecificationsRequest getDeviceSpecificationsRequest, CSIGetIMEIResponseType csiGetIMEIResponse,
			StringBuilder imeiIterationelem, long startMsisdnCount) throws TMobileProcessException {
		if (getDeviceDetailsRequest.isRetrieveShipData() != null && getDeviceDetailsRequest.isRetrieveShipData()
				&& !(startMsisdnCount > 1)) {
			shipDataLoop(getDeviceDetailsRequest, warrantyDetails, shipmentLastUsageData);
		}
		if (getDeviceDetailsRequest.isRetrieveDeviceHardwareInfo() != null
				&& getDeviceDetailsRequest.isRetrieveDeviceHardwareInfo() && !(startMsisdnCount > 1)) {
			try {
				invokeGetDeviceSpecificationsREST(getDeviceDetailsRequest, getDeviceSpecificationsRequest,
						deviceSpecificationDetails, lunhIMEI, imeiIterationelem);
			} catch (TMobileProcessException ex) {
				LOGGER.error("Error occurred in method: invokeGetDeviceSpecificationsREST() and terminated due to ",
						ex);
				if (null != ex.getErrorDetails()) {
					ErrorDetails errorDetailsLocal = ex.getErrorDetails();
					errorDetails.setCode(errorDetailsLocal.getCode());
					errorDetails.setJobId(errorDetailsLocal.getJobId());
					errorDetails.setMesssage(errorDetailsLocal.getMesssage());
					errorDetails.setProcessStack(errorDetailsLocal.getProcessStack());
					errorDetails.setSourceMessage(errorDetailsLocal.getSourceMessage());
					errorDetails.setStackTrace(errorDetailsLocal.getStackTrace());
					errorDetails.setTargetSystem(errorDetailsLocal.getTargetSystem());
					errorDetails.getSubstatus().addAll(errorDetailsLocal.getSubstatus());
				}
				logGetDeviceSpecificationsError(logWarningDetailsStartInput, errorDetails);
				getimeistatusformlError(errorDetails, getDeviceDetailsRequest, csiGetIMEIResponse);
			}
		}
	}

	/**
	 * @param getDeviceDetailsRequest
	 * @param getDeviceDetailsResponse
	 * @param warrantyDetails
	 * @param logWarningDetailsStartInput
	 * @param errorDetails
	 * @param lunhIMEI
	 * @param csiGetIMEIRequest
	 * @param getDeviceDetailsOrchDMWResponse
	 * @param csiGetIMEIResponse
	 * @param queryIMEIWarrantyResponse
	 * @param imeiIterationelem
	 * @param queryIMEIWarrantyRequest
	 * @throws TMobileProcessException
	 */
	private void initTransformedImeiMsisdnResponse(GetDeviceDetailsRequest getDeviceDetailsRequest,
			WarrantyDetails warrantyDetails,
			LogwarningdetailsStartInput logWarningDetailsStartInput, ErrorDetails errorDetails,
			MapperInputType lunhIMEI, 
			GetDeviceDetailsOrchDMWResponse getDeviceDetailsOrchDMWResponse, CSIGetIMEIResponseType csiGetIMEIResponse,
			CSIQueryIMEIWarrantyResponseType queryIMEIWarrantyResponse, StringBuilder imeiIterationelem,
			CSIQueryIMEIWarrantyRequestType queryIMEIWarrantyRequest) throws TMobileProcessException {
		if (getDeviceDetailsRequest.getMSISDN().size() == 1 && getDeviceDetailsRequest.isRetrieveDeviceWarrantyInfo()) {
			try {
				invokeQueryImeiWarranty(getDeviceDetailsRequest, queryIMEIWarrantyResponse, queryIMEIWarrantyRequest);
			} catch (TMobileProcessException ex) {
				LOGGER.error("Error occurred in method: invokeQueryImeiWarranty() and terminated due to ", ex);
				if (ex.getErrorDetails() != null)
					errorDetails = ex.getErrorDetails();
				getdevicedetailsError(errorDetails, getDeviceDetailsRequest, lunhIMEI);
			}
		} else {
			try {
				invokeGetimeistatusforml(getDeviceDetailsRequest, csiGetIMEIResponse);
				if (csiGetIMEIResponse.getImeiInfo().size() == 0) {
					getimeistatusformlError(errorDetails, getDeviceDetailsRequest, csiGetIMEIResponse);
				}
			} catch (TMobileProcessException ex) {
				LOGGER.error("Error occurred in method: invokeGetimeistatusforml() and terminated due to ", ex);
				if (null != ex.getErrorDetails())
					errorDetails = ex.getErrorDetails();
				logGetimeistatusformlError(logWarningDetailsStartInput, errorDetails);
				getimeistatusformlError(errorDetails, getDeviceDetailsRequest, csiGetIMEIResponse);
			}
		}

		assign(queryIMEIWarrantyResponse, getDeviceDetailsOrchDMWResponse, csiGetIMEIResponse);
		group(getDeviceDetailsOrchDMWResponse, lunhIMEI, imeiIterationelem);

		initTransformedimeiResponse(getDeviceDetailsRequest, warrantyDetails, errorDetails,
				lunhIMEI, csiGetIMEIResponse, queryIMEIWarrantyResponse);
	}

	/**
	 * @param getDeviceDetailsRequest
	 * @param getDeviceDetailsResponse
	 * @param warrantyDetails
	 * @param errorDetails
	 * @param lunhIMEI
	 * @param csiGetIMEIResponse
	 * @param queryIMEIWarrantyResponse
	 * @throws TMobileProcessException
	 */
	private void initTransformedimeiResponse(GetDeviceDetailsRequest getDeviceDetailsRequest,
			WarrantyDetails warrantyDetails,
			ErrorDetails errorDetails, MapperInputType lunhIMEI, CSIGetIMEIResponseType csiGetIMEIResponse,
			CSIQueryIMEIWarrantyResponseType queryIMEIWarrantyResponse) throws TMobileProcessException {
		if (StringUtils.isNotEmpty(getDeviceDetailsRequest.getImei())
				&& getDeviceDetailsRequest.getImei().equalsIgnoreCase(lunhIMEI.getTransformedIMEI())
				&& StringUtils.isEmpty(lunhIMEI.getTransformedIMEI())) {
			getdevicedetailsError(errorDetails, getDeviceDetailsRequest, lunhIMEI);
		} else {
			initTransformedimeiResponse(getDeviceDetailsRequest, warrantyDetails, queryIMEIWarrantyResponse, lunhIMEI,
					csiGetIMEIResponse);
		}
	}

	/**
	 * @param getDeviceDetailsRequest
	 * @param warrantyDetails
	 * @param getDeviceWarrantyDetailOrchEndOutput
	 * @param lunhIMEI
	 * @param guid
	 * @param imeiIterationelem
	 * @param getDeviceWarrantyDetailOrchStartInput
	 * @param startMsisdnCount
	 * @param imeiWarrantyCount
	 * @param imeiInfoCount
	 * @throws TMobileProcessException
	 */
	private void invokeGetdevicewarrantydetailorch(GetDeviceDetailsRequest getDeviceDetailsRequest,
			WarrantyDetails warrantyDetails, GetDeviceDetailsOrchEndOutput getDeviceWarrantyDetailOrchEndOutput,
			MapperInputType lunhIMEI, Guid guid, StringBuilder imeiIterationelem,
			GetDeviceDetailsOrchStartInput getDeviceWarrantyDetailOrchStartInput, long startMsisdnCount,
			long imeiWarrantyCount, long imeiInfoCount) throws TMobileProcessException {
		if (isRetrieveDeviceWarrantyInfo(getDeviceDetailsRequest, startMsisdnCount, imeiWarrantyCount)
				|| (startMsisdnCount == 0 && imeiInfoCount > 0
						&& getDeviceDetailsRequest.isRetrieveDeviceWarrantyInfo())) {
			invokeGetdevicewarrantydetailorch(warrantyDetails, getDeviceDetailsRequest,
					getDeviceWarrantyDetailOrchEndOutput, getDeviceWarrantyDetailOrchStartInput, guid, lunhIMEI,
					imeiIterationelem);
		}
	}

}