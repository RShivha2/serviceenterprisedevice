package com.tmobile.u2.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmobile.services.base.ApplicationObjectKey;
import com.tmobile.services.base.Header;
import com.tmobile.services.productmanagement.deviceenterprise.v1.SendDeviceUnlockCodeRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.SendDeviceUnlockCodeResponse;
import com.tmobile.services.schema.dataobject.createservicecontextendoutput.CreateServiceContextOutput;
import com.tmobile.services.schema.dataobject.createservicecontextstartinput.CreateServiceContextInput;
import com.tmobile.services.schema.dataobject.errorlookupendoutput.ErrorLookupEndOutput;
import com.tmobile.services.schema.dataobject.guid.Guid;
import com.tmobile.services.schema.dataobject.legacyesoaerrorlookupstartinput.LegacyESOAErrorLookupStartInput;
import com.tmobile.services.schema.dataobject.legacyesoaerrorlookupstartinput.Root;
import com.tmobile.services.schema.dataobject.logservicecontextstartinput.LogServiceContextStartInput;
import com.tmobile.services.schema.dataobject.responsestatus.ResponseStatus;
import com.tmobile.services.schema.dataobject.responsestatus.ResponseStatusDetail;
import com.tmobile.services.schema.dataobject.servicecontext.ErrorDetails;
import com.tmobile.services.schema.dataobject.servicecontext.Key;
import com.tmobile.u2.exception.TMobileProcessException;
import com.tmobile.u2.service.IInvokesendDeviceUnlockCodeService;
import com.tmobile.u2.service.ISendDeviceUnlockCodeImplService;
import com.tmobile.u2.shared.service.ICreateServiceContextService;
import com.tmobile.u2.shared.service.IErrorLookupService;
import com.tmobile.u2.shared.service.ILogServiceContextService;
import com.tmobile.u2.util.SharedServiceContext;
import com.tmobile.u2.util.TibcoToJavaUtil;

/*************************************************************************
 * 
 * TMOBILE CONFIDENTIAL
 * _________________________________________________________________________________
 * 
 * TMOBILE is a trademark of TMOBILE Company.
 *
 * Copyright © 2021 TMOBILE. All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of
 * TMOBILE and its suppliers, if any. The intellectual and technical concepts
 * contained herein are proprietary to TMOBILE and its suppliers and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by
 * trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from TMOBILE.
 *
 *************************************************************************/
// Author : Generated by ATMA ®
// Revision History :  
@Service
public class SendDeviceUnlockCodeImplServiceImpl implements ISendDeviceUnlockCodeImplService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SendDeviceUnlockCodeImplServiceImpl.class);

	@Autowired
	private IErrorLookupService iErrorLookupService;

	public void setErrorLookupService(IErrorLookupService iErrorLookupService) {
		this.iErrorLookupService = iErrorLookupService;
	}

	@Autowired
	private ICreateServiceContextService iCreateServiceContextService;

	public void setCreateServiceContextService(ICreateServiceContextService iCreateServiceContextService) {
		this.iCreateServiceContextService = iCreateServiceContextService;
	}

	@Autowired
	private ILogServiceContextService iLogServiceContextService;

	public void setLogServiceContextService(ILogServiceContextService iLogServiceContextService) {
		this.iLogServiceContextService = iLogServiceContextService;
	}

	@Autowired
	private SharedServiceContext sharedServiceContext;

	public void setSharedServiceContext(SharedServiceContext sharedServiceContext) {
		this.sharedServiceContext = sharedServiceContext;
	}

	@Autowired
	private IInvokesendDeviceUnlockCodeService iInvokesendDeviceUnlockCodeService;

	public void setInvokesendDeviceUnlockCodeService(
			IInvokesendDeviceUnlockCodeService iInvokesendDeviceUnlockCodeService) {
		this.iInvokesendDeviceUnlockCodeService = iInvokesendDeviceUnlockCodeService;
	}

	/**
	 * Method lookupError
	 * 
	 * @param LegacyESOAErrorLookupStartInput legacyESOAErrorLookupStartInput
	 *                                        ,ErrorLookupEndOutput
	 *                                        errorLookupEndOutput ,ErrorDetails
	 *                                        errorDetails
	 *                                        ,SendDeviceUnlockCodeRequest
	 *                                        sendDeviceUnlockCodeRequest
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void lookupError(LegacyESOAErrorLookupStartInput legacyESOAErrorLookupStartInput,
			ErrorLookupEndOutput errorLookupEndOutput, ErrorDetails errorDetails,
			SendDeviceUnlockCodeRequest sendDeviceUnlockCodeRequest) throws TMobileProcessException {
		Root sendDeviceUnlockErrRoot = legacyESOAErrorLookupStartInput.getRoot();
		if (errorLookupEndOutput.getRoot().getErrorDetails() != null) {
			sendDeviceUnlockErrRoot.setErrorDetails(errorLookupEndOutput.getRoot().getErrorDetails());
		} else {
			sendDeviceUnlockErrRoot.setErrorDetails(new ErrorDetails());
			sendDeviceUnlockErrRoot.getErrorDetails().setTargetSystem(null != errorDetails.getTargetSystem() ? errorDetails.getTargetSystem() : "ESP");
			sendDeviceUnlockErrRoot.getErrorDetails().setCode(errorDetails.getCode());
			sendDeviceUnlockErrRoot.getErrorDetails().setMesssage(errorDetails.getMesssage());
			sendDeviceUnlockErrRoot.getErrorDetails().setStackTrace(errorDetails.getStackTrace());
			sendDeviceUnlockErrRoot.getErrorDetails().setProcessStack(errorDetails.getProcessStack());
			sendDeviceUnlockErrRoot.getErrorDetails().setSourceMessage(TibcoToJavaUtil.renderXml(sendDeviceUnlockCodeRequest));
		}
		iErrorLookupService.executeMethod(legacyESOAErrorLookupStartInput, errorLookupEndOutput);
	}

	/**
	 * Method createServicecontext
	 * 
	 * @param Guid guid ,CreateServiceContextOutput createServiceContextEndOutput
	 *             ,CreateServiceContextInput createServiceContextStartInput
	 *             ,SendDeviceUnlockCodeRequest sendDeviceUnlockCodeRequest
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void createServicecontextErr(Guid guid, CreateServiceContextOutput createServiceContextEndOutput,
			CreateServiceContextInput createServiceContextStartInput,
			SendDeviceUnlockCodeRequest sendDeviceUnlockCodeRequest) throws TMobileProcessException {
		SendDeviceUnlockCodeRequest varSendDeviceUnlockCodeContextErr = sendDeviceUnlockCodeRequest;
		com.tmobile.services.schema.dataobject.createservicecontextstartinput.Root sendDeviceUnlockCodeContextErrRoot = createServiceContextStartInput
				.getRoot();
		sendDeviceUnlockCodeContextErrRoot.setDomainName("ProductManagement");
		sendDeviceUnlockCodeContextErrRoot.setServiceName("DeviceEnterprise");
		sendDeviceUnlockCodeContextErrRoot.setOperationName("sendDeviceUnlockCode");
		sendDeviceUnlockCodeContextErrRoot.setEngineName(TibcoToJavaUtil.getProperty("engineName"));
		sendDeviceUnlockCodeContextErrRoot.setRequest(TibcoToJavaUtil.renderXml(sendDeviceUnlockCodeRequest));
		if (varSendDeviceUnlockCodeContextErr.getHeader().getSender().getSessionId() != null) {
			sendDeviceUnlockCodeContextErrRoot.setJobId(varSendDeviceUnlockCodeContextErr.getHeader().getSender().getSessionId());
		}
		sendDeviceUnlockCodeContextErrRoot.setSenderId(varSendDeviceUnlockCodeContextErr.getHeader().getSender().getSenderId());
		sendDeviceUnlockCodeContextErrRoot.setChannel(varSendDeviceUnlockCodeContextErr.getHeader().getSender().getChannelId());
		sendDeviceUnlockCodeContextErrRoot.setApplicationId(varSendDeviceUnlockCodeContextErr.getHeader().getSender().getApplicationId());
		if (varSendDeviceUnlockCodeContextErr.getHeader().getSender().getApplicationUserId() != null) {
			sendDeviceUnlockCodeContextErrRoot.setUserId(varSendDeviceUnlockCodeContextErr.getHeader().getSender().getApplicationUserId());
		}
		if (guid.getRoot().getGUID() != null) {
			Key key = new Key();
			key.setName("GUID");
			key.setValue(guid.getRoot().getGUID());
			sendDeviceUnlockCodeContextErrRoot.getKey().add(key);
		}
		if (StringUtils.isNotEmpty(sendDeviceUnlockCodeRequest.getHeader().getSender().getInteractionId())) {
			Key key = new Key();
			key.setName("InteractionId");
			if (sendDeviceUnlockCodeRequest.getHeader().getSender().getInteractionId() != null) {
				key.setValue(sendDeviceUnlockCodeRequest.getHeader().getSender().getInteractionId());
			}
			sendDeviceUnlockCodeContextErrRoot.getKey().add(key);
		}
		if (sendDeviceUnlockCodeRequest.getImei() != null) {
			Key key = new Key();
			key.setName("IMEI");
			key.setValue(sendDeviceUnlockCodeRequest.getImei());
			sendDeviceUnlockCodeContextErrRoot.getKey().add(key);
		}
		iCreateServiceContextService.executeMethod(createServiceContextStartInput, createServiceContextEndOutput);
	}

	/**
	 * Method mapErrorResponse
	 * 
	 * @param Guid guid ,ErrorLookupEndOutput errorLookupEndOutput
	 *             ,SendDeviceUnlockCodeResponse sendDeviceUnlockCodeResponse
	 *             ,SendDeviceUnlockCodeRequest sendDeviceUnlockCodeRequest
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void mapErrorResponse(Guid guid, ErrorLookupEndOutput errorLookupEndOutput,
			SendDeviceUnlockCodeResponse sendDeviceUnlockCodeResponse,
			SendDeviceUnlockCodeRequest sendDeviceUnlockCodeRequest) throws TMobileProcessException {
		if (sendDeviceUnlockCodeRequest.getServiceTransactionId() != null) {
			sendDeviceUnlockCodeResponse.setServiceTransactionId(sendDeviceUnlockCodeRequest.getServiceTransactionId());
		}
		if (sendDeviceUnlockCodeRequest.getVersion() != null) {
			sendDeviceUnlockCodeResponse.setVersion(sendDeviceUnlockCodeRequest.getVersion());
		}
		sendDeviceUnlockCodeResponse.setHeader(new Header());
		if (sendDeviceUnlockCodeRequest.getHeader() != null) {
			sendDeviceUnlockCodeResponse.getHeader().setSender(sendDeviceUnlockCodeRequest.getHeader().getSender());
			sendDeviceUnlockCodeResponse.getHeader().setTarget(sendDeviceUnlockCodeRequest.getHeader().getTarget());
			sendDeviceUnlockCodeResponse.getHeader().getProviderId()
					.addAll(sendDeviceUnlockCodeRequest.getHeader().getProviderId());
			if (guid.getRoot().getGUID() != null) {
				ApplicationObjectKey providerId = new ApplicationObjectKey();
				providerId.setId(guid.getRoot().getGUID());
				sendDeviceUnlockCodeResponse.getHeader().getProviderId().add(providerId);
			}
		} else {
			sendDeviceUnlockCodeResponse.getHeader().getSender().setSenderId("");
			sendDeviceUnlockCodeResponse.getHeader().getSender().setChannelId("");
			sendDeviceUnlockCodeResponse.getHeader().getSender().setApplicationId("");
			if (guid.getRoot().getGUID() != null) {
				ApplicationObjectKey providerId = new ApplicationObjectKey();
				providerId.setId(guid.getRoot().getGUID());
				sendDeviceUnlockCodeResponse.getHeader().getProviderId().add(providerId);
			}
		}

		if (errorLookupEndOutput.getRoot().getResponseStatus() != null) {
			setResponseStatus(errorLookupEndOutput, sendDeviceUnlockCodeResponse);
		}
	}

	/**
	 * @param errorLookupEndOutput
	 * @param sendDeviceUnlockCodeResponse
	 */
	private void setResponseStatus(ErrorLookupEndOutput errorLookupEndOutput,
			SendDeviceUnlockCodeResponse sendDeviceUnlockCodeResponse) {
		ResponseStatus sendDeviceUnlockRespStatus = new ResponseStatus();
		ResponseStatus rootResponseStatusLocal = errorLookupEndOutput.getRoot().getResponseStatus();
		if (rootResponseStatusLocal.getCode() != null) {
			sendDeviceUnlockRespStatus.setCode(rootResponseStatusLocal.getCode());
		}
		for (ResponseStatusDetail responseStatusLocalDetailLocal : rootResponseStatusLocal.getDetail()) {
			ResponseStatusDetail responseStatusDetail = new ResponseStatusDetail();
			if (responseStatusLocalDetailLocal.getSubStatusCode() != null) {
				responseStatusDetail.setSubStatusCode(responseStatusLocalDetailLocal.getSubStatusCode());
			}
			if (responseStatusLocalDetailLocal.getDefinition() != null) {
				responseStatusDetail.setDefinition(responseStatusLocalDetailLocal.getDefinition());
			}
			if (responseStatusLocalDetailLocal.getExplanation() != null) {
				responseStatusDetail.setExplanation(responseStatusLocalDetailLocal.getExplanation());
			}
			sendDeviceUnlockRespStatus.getDetail().add(responseStatusDetail);
		}
		sendDeviceUnlockCodeResponse.setResponseStatus(sendDeviceUnlockRespStatus);
	}

	/**
	 * Method logError
	 * 
	 * @param ErrorLookupEndOutput errorLookupEndOutput
	 *                             ,SendDeviceUnlockCodeResponse
	 *                             sendDeviceUnlockCodeResponse
	 *                             ,LogServiceContextStartInput
	 *                             logServiceContextStartInput
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void logError(ErrorLookupEndOutput errorLookupEndOutput,
			SendDeviceUnlockCodeResponse sendDeviceUnlockCodeResponse,
			LogServiceContextStartInput logServiceContextStartInput) throws TMobileProcessException {
		com.tmobile.services.schema.dataobject.logservicecontextstartinput.Root root = logServiceContextStartInput.getRoot();
		root.setResponse(TibcoToJavaUtil.renderXml(sendDeviceUnlockCodeResponse));
		root.getErrorDetails().add(errorLookupEndOutput.getRoot().getErrorDetails());
		iLogServiceContextService.executeMethod(logServiceContextStartInput);
	}

	/**
	 * Method getGUIDErr
	 * 
	 * @param Guid guid ,SendDeviceUnlockCodeResponse sendDeviceUnlockCodeResponse
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void getGUIDErr(Guid guid) throws TMobileProcessException {
		if(null != sharedServiceContext.getRequestValue("GUID")) { 
			Guid guidLocal = (Guid) sharedServiceContext.getRequestValue("GUID");
			guid.setRoot(guidLocal.getRoot());
		}
	}

	/**
	 * Method createServiceContext
	 * 
	 * @param Guid guid ,CreateServiceContextOutput createServiceContextEndOutput
	 *             ,CreateServiceContextInput createServiceContextStartInput
	 *             ,SendDeviceUnlockCodeRequest sendDeviceUnlockCodeRequest
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void createServiceContext(Guid guid, CreateServiceContextOutput createServiceContextEndOutput,
			CreateServiceContextInput createServiceContextStartInput,
			SendDeviceUnlockCodeRequest sendDeviceUnlockCodeRequest) throws TMobileProcessException {
		com.tmobile.services.schema.dataobject.createservicecontextstartinput.Root root = createServiceContextStartInput.getRoot();
		root.setDomainName("ProductManagement");
		root.setServiceName("DeviceEnterprise");
		root.setOperationName("sendDeviceUnlockCode");
		root.setEngineName(TibcoToJavaUtil.getProperty("engineName"));
		root.setRequest(TibcoToJavaUtil.renderXml(sendDeviceUnlockCodeRequest));
		if (sendDeviceUnlockCodeRequest.getHeader().getSender().getSessionId() != null) {
			root.setJobId(sendDeviceUnlockCodeRequest.getHeader().getSender().getSessionId());
		}
		root.setSenderId(sendDeviceUnlockCodeRequest.getHeader().getSender().getSenderId());
		root.setChannel(sendDeviceUnlockCodeRequest.getHeader().getSender().getChannelId());
		root.setApplicationId(sendDeviceUnlockCodeRequest.getHeader().getSender().getApplicationId());
		if (sendDeviceUnlockCodeRequest.getHeader().getSender().getApplicationUserId() != null) {
			root.setUserId(sendDeviceUnlockCodeRequest.getHeader().getSender().getApplicationUserId());
		}
		if (guid.getRoot().getGUID() != null) {
			Key key = new Key();
			key.setName("GUID");
			key.setValue(guid.getRoot().getGUID());
			root.getKey().add(key);
		}
		if (StringUtils.isNotEmpty(sendDeviceUnlockCodeRequest.getHeader().getSender().getInteractionId())) {
			Key key = new Key();
			key.setName("InteractionId");
			if (sendDeviceUnlockCodeRequest.getHeader().getSender().getInteractionId() != null) {
				key.setValue(sendDeviceUnlockCodeRequest.getHeader().getSender().getInteractionId());
			}
			root.getKey().add(key);
		}
		if (sendDeviceUnlockCodeRequest.getImei() != null) {
			Key key = new Key();
			key.setName("IMEI");
			key.setValue(sendDeviceUnlockCodeRequest.getImei());
			root.getKey().add(key);
		}
		iCreateServiceContextService.executeMethod(createServiceContextStartInput, createServiceContextEndOutput);
	}

	/**
	 * Method invokesendDeviceUnlockCode
	 * 
	 * @param SendDeviceUnlockCodeResponse sendDeviceUnlockCodeResponse
	 *                                     ,SendDeviceUnlockCodeRequest
	 *                                     sendDeviceUnlockCodeRequest
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void invokesendDeviceUnlockCode(SendDeviceUnlockCodeResponse sendDeviceUnlockCodeResponse,
			SendDeviceUnlockCodeRequest sendDeviceUnlockCodeRequest) throws TMobileProcessException {
		iInvokesendDeviceUnlockCodeService.executeMethod(sendDeviceUnlockCodeRequest, sendDeviceUnlockCodeResponse);
	}

	/**
	 * Method logSuccess
	 * 
	 * @param SendDeviceUnlockCodeResponse sendDeviceUnlockCodeResponse
	 *                                     ,LogServiceContextStartInput
	 *                                     logServiceContextStartInput
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void logSuccess(SendDeviceUnlockCodeResponse sendDeviceUnlockCodeResponse,
			LogServiceContextStartInput logServiceContextStartInput) throws TMobileProcessException {
		com.tmobile.services.schema.dataobject.logservicecontextstartinput.Root root = logServiceContextStartInput.getRoot();
		root.setResponse(TibcoToJavaUtil.renderXml(sendDeviceUnlockCodeResponse));
		iLogServiceContextService.executeMethod(logServiceContextStartInput);
	}

	/**
	 * Method getGUID
	 * 
	 * @param Guid guid ,SendDeviceUnlockCodeResponse sendDeviceUnlockCodeResponse
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void getGUID(Guid guid) throws TMobileProcessException {
		if(null != sharedServiceContext.getRequestValue("GUID")) { 
			Guid guidLocal = (Guid) sharedServiceContext.getRequestValue("GUID");
			guid.setRoot(guidLocal.getRoot());
		}
	}

	/**
	 * Method executeMethod
	 * 
	 * @param
	 * @return void
	 * @throws TMobileProcessException
	 */
	public void executeMethod(SendDeviceUnlockCodeRequest sendDeviceUnlockCodeRequest, SendDeviceUnlockCodeResponse sendDeviceUnlockCodeResponse) throws TMobileProcessException {
		Guid guid = new Guid();
		LegacyESOAErrorLookupStartInput legacyESOAErrorLookupStartInput = new LegacyESOAErrorLookupStartInput();
		CreateServiceContextInput createServiceContextStartInput = new CreateServiceContextInput();
		ErrorLookupEndOutput errorLookupEndOutput = new ErrorLookupEndOutput();
		CreateServiceContextOutput createServiceContextEndOutput = new CreateServiceContextOutput();
		LogServiceContextStartInput logServiceContextStartInput = new LogServiceContextStartInput();
		ErrorDetails errorDetails = new ErrorDetails();
		try {
			getGUID(guid);
			createServiceContext(guid, createServiceContextEndOutput, createServiceContextStartInput,
					sendDeviceUnlockCodeRequest);
			invokesendDeviceUnlockCode(sendDeviceUnlockCodeResponse, sendDeviceUnlockCodeRequest);
			logSuccess(sendDeviceUnlockCodeResponse, logServiceContextStartInput);
		} catch (TMobileProcessException ex) {
			LOGGER.error("Error occurred in method: SendDeviceUnlockCodeImpl() and terminated due to ", ex);
			if(null != ex.getErrorDetails()) {
				errorDetails = ex.getErrorDetails();
			}
			if (!createServiceContextEndOutput.getRoot().isComplete()) {
				getGUIDErr(guid);
				createServicecontextErr(guid, createServiceContextEndOutput, createServiceContextStartInput,
						sendDeviceUnlockCodeRequest);
			}
			lookupError(legacyESOAErrorLookupStartInput, errorLookupEndOutput, errorDetails,
					sendDeviceUnlockCodeRequest);
			mapErrorResponse(guid, errorLookupEndOutput, sendDeviceUnlockCodeResponse, sendDeviceUnlockCodeRequest);
			logError(errorLookupEndOutput, sendDeviceUnlockCodeResponse, logServiceContextStartInput);
		}

	}

}