package com.tmobile.u2.service.impl;

import java.math.BigInteger;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.namespaces.tns._1588947392231.QueryAllSOAPStartInput;
import com.example.namespaces.tns._1588947392231.Root.QueryAll;
import com.example.namespaces.tns._1588947392232.QueryAllSOAPEndOutput;
import com.example.namespaces.tns._1588947392232.Root.QueryAllResponse;
import com.tmobile.eus.unlock.DeviceBrandEnum;
import com.tmobile.eus.unlock.EUSUnlockRequest;
import com.tmobile.eus.unlock.EUSUnlockResponse;
import com.tmobile.eus.unlock.UnlockTypeEnum;
import com.tmobile.services.base.ApplicationObjectKey;
import com.tmobile.services.base.Header;
import com.tmobile.services.productmanagement.deviceenterprise.v1.DeviceUnlockDetails;
import com.tmobile.services.productmanagement.deviceenterprise.v1.SendDeviceUnlockCodeRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.SendDeviceUnlockCodeResponse;
import com.tmobile.services.schema.dataobject.guid.Guid;
import com.tmobile.services.schema.dataobject.responsestatus.ResponseStatus;
import com.tmobile.services.schema.dataobject.servicecontext.ErrorDetails;
import com.tmobile.u2.exception.TMobileProcessException;
import com.tmobile.u2.service.IEUSUnlockServiceSOAPService;
import com.tmobile.u2.service.IInvokesendDeviceUnlockCodeService;
import com.tmobile.u2.service.IQueryAllSOAPService;
import com.tmobile.u2.shared.service.IHeaderValidationService;
import com.tmobile.u2.util.SharedServiceContext;
import com.tmobile.u2.util.TibcoToJavaUtil;

import sharedservicecontext.sharedprocesses.utilityprocesses.headervalidation_start_input.HeaderValidationStartInput;
import sharedservicecontext.sharedprocesses.utilityprocesses.headervalidation_start_input.Root;
import subprocesses.deviceenterprise.invokegetdevicedetails_process.validate_data.ValidateData;

/*************************************************************************
 * 
 * TMOBILE CONFIDENTIAL
 * _________________________________________________________________________________
 * 
 * TMOBILE is a trademark of TMOBILE Company.
 *
 * Copyright © 2021 TMOBILE. All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of
 * TMOBILE and its suppliers, if any. The intellectual and technical concepts
 * contained herein are proprietary to TMOBILE and its suppliers and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by
 * trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from TMOBILE.
 *
 *************************************************************************/
// Author : Generated by ATMA ®
// Revision History :  
@Service
public class InvokesendDeviceUnlockCodeServiceImpl implements IInvokesendDeviceUnlockCodeService {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(InvokesendDeviceUnlockCodeServiceImpl.class);

	@Autowired
	private IHeaderValidationService iHeaderValidationService;

	public void setHeaderValidationService(IHeaderValidationService iHeaderValidationService) {
		this.iHeaderValidationService = iHeaderValidationService;
	}

	@Autowired
	private IEUSUnlockServiceSOAPService iEUSUnlockServiceSOAPService;

	public void setEUSUnlockServiceSOAPService(IEUSUnlockServiceSOAPService iEUSUnlockServiceSOAPService) {
		this.iEUSUnlockServiceSOAPService = iEUSUnlockServiceSOAPService;
	}

	@Autowired
	private IQueryAllSOAPService iQueryAllSOAPService;

	public void setQueryAllSOAPService(IQueryAllSOAPService iQueryAllSOAPService) {
		this.iQueryAllSOAPService = iQueryAllSOAPService;
	}

	@Autowired
	private SharedServiceContext sharedServiceContext;

	public void setSharedServiceContext(SharedServiceContext sharedServiceContext) {
		this.sharedServiceContext = sharedServiceContext;
	}

	/**
	 * Method validateHeaders
	 * 
	 * @param HeaderValidationStartInput headerValidationStartInput
	 *                                   ,SendDeviceUnlockCodeRequest
	 *                                   sendDeviceUnlockCodeRequest
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void validateHeaders(HeaderValidationStartInput headerValidationStartInput,
			SendDeviceUnlockCodeRequest sendDeviceUnlockCodeRequest) throws TMobileProcessException {
		Root root = headerValidationStartInput.getRoot();
		root.setSenderId(sendDeviceUnlockCodeRequest.getHeader().getSender().getSenderId());
		root.setChannelId(sendDeviceUnlockCodeRequest.getHeader().getSender().getChannelId());
		root.setApplicationId(sendDeviceUnlockCodeRequest.getHeader().getSender().getApplicationId());
		iHeaderValidationService.executeMethod(headerValidationStartInput);
	}

	/**
	 * Method validateData
	 * 
	 * @param ValidateData validateData ,SendDeviceUnlockCodeRequest
	 *                     sendDeviceUnlockCodeRequest
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void validateData(ValidateData validateData, SendDeviceUnlockCodeRequest sendDeviceUnlockCodeRequest)
			throws TMobileProcessException {
		subprocesses.deviceenterprise.invokegetdevicedetails_process.validate_data.Root root = validateData.getRoot();
		if (sendDeviceUnlockCodeRequest.isUnlockableRemotely() == false) {
			if (StringUtils.isEmpty(sendDeviceUnlockCodeRequest.getManufacturerName())) {
				root.getMessageSet().getText().add("manufacturerName is missing in Request");
			}
			if (StringUtils.isEmpty(sendDeviceUnlockCodeRequest.getDeviceModel())) {
				root.getMessageSet().getText().add("deviceModel is missing in Request");
			}
			if (sendDeviceUnlockCodeRequest.isNotifybyEmail()
					&& sendDeviceUnlockCodeRequest.getEmailAddress().size() == 0) {
				root.getMessageSet().getText().add("emailAddress is missing in Request");
			}
		}
		if (sendDeviceUnlockCodeRequest.isUnlockableRemotely()) {
			if ("Solaveil".equalsIgnoreCase(sendDeviceUnlockCodeRequest.getDeviceBrandId().value())) {
				root.getMessageSet().getText().add("Unsupported Brand Id for EUS");
			}
			if (StringUtils.isEmpty(sendDeviceUnlockCodeRequest.getUnlockType())
					|| (!"TEMPORARY".equalsIgnoreCase(sendDeviceUnlockCodeRequest.getUnlockType())
							&& !"PERMANENT".equalsIgnoreCase(sendDeviceUnlockCodeRequest.getUnlockType())
							&& !"PARTIAL".equalsIgnoreCase(sendDeviceUnlockCodeRequest.getUnlockType()))) {
				root.getMessageSet().getText().add("Invalid unlockType");
			}
		}
	}

	/**
	 * Method validationError
	 * 
	 * @param ValidateData validateData ,ErrorDetails errorDetails
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void validationError(ValidateData validateData, ErrorDetails errorDetails) throws TMobileProcessException {
		errorDetails.setCode("GENERAL-0001");
		errorDetails.setMesssage(TibcoToJavaUtil.concateSequence(validateData.getRoot().getMessageSet().getText()));
		throw new TMobileProcessException(errorDetails);
	}

	/**
	 * Method invokeEUSUnlockServiceSoap
	 * 
	 * @param EUSUnlockRequest eUSUnlockRequest ,EUSUnlockResponse eUSUnlockResponse
	 *                         ,SendDeviceUnlockCodeRequest
	 *                         sendDeviceUnlockCodeRequest
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void invokeEUSUnlockServiceSoap(EUSUnlockRequest eUSUnlockRequest, EUSUnlockResponse eUSUnlockResponse,
			SendDeviceUnlockCodeRequest sendDeviceUnlockCodeRequest) throws TMobileProcessException {
		eUSUnlockRequest.setVersion(1);
		eUSUnlockRequest.setCorrelationId(sendDeviceUnlockCodeRequest.getServiceTransactionId());
		eUSUnlockRequest.setUnlockType(UnlockTypeEnum.valueOf(sendDeviceUnlockCodeRequest.getUnlockType()));
		eUSUnlockRequest.setImei(sendDeviceUnlockCodeRequest.getImei());
		eUSUnlockRequest.setMsisdn(sendDeviceUnlockCodeRequest.getMSISDN());
		if (sendDeviceUnlockCodeRequest.getDeviceBrandId().value() == "T-Mobile") {
			eUSUnlockRequest.setDeviceBrandId(DeviceBrandEnum.valueOf("TMOBILE"));
		} else if (sendDeviceUnlockCodeRequest.getDeviceBrandId().value() == "MetroPCS") {
			eUSUnlockRequest.setDeviceBrandId(DeviceBrandEnum.valueOf("METROPCS"));
		} else if (sendDeviceUnlockCodeRequest.getDeviceBrandId().value() == "Brightspot") {
			eUSUnlockRequest.setDeviceBrandId(DeviceBrandEnum.valueOf("BRIGHTSPOT"));
		} else if (sendDeviceUnlockCodeRequest.getDeviceBrandId().value() == "UVM") {
			eUSUnlockRequest.setDeviceBrandId(DeviceBrandEnum.valueOf("UNIVISION"));
		} else if (sendDeviceUnlockCodeRequest.getDeviceBrandId().value() == "GoSmart") {
			eUSUnlockRequest.setDeviceBrandId(DeviceBrandEnum.valueOf("GOSMART"));
		} else {
			eUSUnlockRequest.setDeviceBrandId(DeviceBrandEnum.valueOf("WALMART"));
		}
		iEUSUnlockServiceSOAPService.executeMethod(eUSUnlockRequest, eUSUnlockResponse);
	}

	/**
	 * Method invokeRemedySoap
	 * 
	 * @param QueryAllResponse queryAllResponse ,1588947392231 1588947392231
	 *                         ,SendDeviceUnlockCodeRequest
	 *                         sendDeviceUnlockCodeRequest
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void invokeRemedySoap(QueryAllSOAPStartInput queryAllSOAPStartInput,
			QueryAllSOAPEndOutput queryAllSOAPEndOutput, SendDeviceUnlockCodeRequest sendDeviceUnlockCodeRequest)
			throws TMobileProcessException {
		com.example.namespaces.tns._1588947392231.Root root = queryAllSOAPStartInput.getRoot();
		root.setQueryAll(new QueryAll());
		root.getQueryAll().setFieldInput01(sendDeviceUnlockCodeRequest.getMSISDN());
		root.getQueryAll().setFieldInput02(sendDeviceUnlockCodeRequest.getImei());
		if (sendDeviceUnlockCodeRequest.getManufacturerName() != null) {
			root.getQueryAll().setFieldInput03(sendDeviceUnlockCodeRequest.getManufacturerName());
		}
		if (sendDeviceUnlockCodeRequest.getDeviceModel() != null) {
			root.getQueryAll().setFieldInput04(sendDeviceUnlockCodeRequest.getDeviceModel());
		}
		root.getQueryAll()
				.setFieldInput05(TibcoToJavaUtil.concateSequence(sendDeviceUnlockCodeRequest.getEmailAddress()));
		if (sendDeviceUnlockCodeRequest.getAccountType() != null) {
			root.getQueryAll().setFieldInput06(sendDeviceUnlockCodeRequest.getAccountType().getValue());
		}
		if ("PREPAID".equalsIgnoreCase(sendDeviceUnlockCodeRequest.getAccountType().getValue())) {
			root.getQueryAll().setFieldInput07(sendDeviceUnlockCodeRequest.getAccountNumber());
		}
		if ("POSTPAID".equalsIgnoreCase(sendDeviceUnlockCodeRequest.getAccountType().getValue()))

		{
			root.getQueryAll().setFieldInput08(sendDeviceUnlockCodeRequest.getAccountNumber());
		}
		if (sendDeviceUnlockCodeRequest.isNotifybyEmail() != null)

		{
			root.getQueryAll().setFieldInput09(sendDeviceUnlockCodeRequest.isNotifybyEmail().toString());
		}
		if (sendDeviceUnlockCodeRequest.getLanguage() != null) {
			root.getQueryAll().setFieldInput10(sendDeviceUnlockCodeRequest.getLanguage());
		}
		if ("T-Mobile".equalsIgnoreCase(sendDeviceUnlockCodeRequest.getDeviceBrandId().value())) {
			root.getQueryAll().setFieldInput11("TMO");
		} else if ("MetroPCS".equalsIgnoreCase(sendDeviceUnlockCodeRequest.getDeviceBrandId().value())) {
			root.getQueryAll().setFieldInput11("MPCS");
		} else if ("Brightspot".equalsIgnoreCase(sendDeviceUnlockCodeRequest.getDeviceBrandId().value())) {
			root.getQueryAll().setFieldInput11("BSP");
		} else if ("UVM".equalsIgnoreCase(sendDeviceUnlockCodeRequest.getDeviceBrandId().value())) {
			root.getQueryAll().setFieldInput11("UVM");
		} else if ("GoSmart".equalsIgnoreCase(sendDeviceUnlockCodeRequest.getDeviceBrandId().value())) {
			root.getQueryAll().setFieldInput11("2IP");
		} else {
			root.getQueryAll().setFieldInput11("WFM");
		}
		iQueryAllSOAPService.executeMethod(queryAllSOAPStartInput, queryAllSOAPEndOutput);
	}

	/**
	 * Method transformPayload
	 * 
	 * @param Guid guid ,QueryAllResponse queryAllResponse
	 *             ,SendDeviceUnlockCodeResponse sendDeviceUnlockCodeResponse
	 *             ,SendDeviceUnlockCodeRequest sendDeviceUnlockCodeRequest
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void transformPayload(Guid guid, QueryAllSOAPEndOutput queryAllSOAPEndOutput,
			SendDeviceUnlockCodeResponse sendDeviceUnlockCodeResponse,
			SendDeviceUnlockCodeRequest sendDeviceUnlockCodeRequest) throws TMobileProcessException {
		sendDeviceUnlockCodeResponse.setServiceTransactionId(sendDeviceUnlockCodeRequest.getServiceTransactionId());
		sendDeviceUnlockCodeResponse.setVersion(sendDeviceUnlockCodeRequest.getVersion());
		sendDeviceUnlockCodeResponse.setHeader(new Header());
		sendDeviceUnlockCodeResponse.getHeader().setSender(sendDeviceUnlockCodeRequest.getHeader().getSender());
		sendDeviceUnlockCodeResponse.getHeader().setTarget(sendDeviceUnlockCodeRequest.getHeader().getTarget());
		sendDeviceUnlockCodeResponse.getHeader().getProviderId()
				.addAll(sendDeviceUnlockCodeRequest.getHeader().getProviderId());
		if (guid.getRoot().getGUID() != null) {
			ApplicationObjectKey providerId = new ApplicationObjectKey();
			providerId.setId(guid.getRoot().getGUID());
			sendDeviceUnlockCodeResponse.getHeader().getProviderId().add(providerId);
		}
		sendDeviceUnlockCodeResponse.setResponseStatus(new ResponseStatus());
		sendDeviceUnlockCodeResponse.getResponseStatus().setCode(BigInteger.valueOf(100));
		sendDeviceUnlockCodeResponse.setDeviceUnlockDetails(new DeviceUnlockDetails());
		if (queryAllSOAPEndOutput != null && queryAllSOAPEndOutput.getRoot().getQueryAllResponse() != null) {
			QueryAllResponse queryAllResponse = queryAllSOAPEndOutput.getRoot().getQueryAllResponse();
			sendDeviceUnlockCodeResponse.getDeviceUnlockDetails()
					.setUnlockableRemotely(sendDeviceUnlockCodeRequest.isUnlockableRemotely());
			sendDeviceUnlockCodeResponse.getDeviceUnlockDetails().setMSISDN(queryAllResponse.getFieldInput01());
			sendDeviceUnlockCodeResponse.getDeviceUnlockDetails().setImei(queryAllResponse.getFieldInput02());
			sendDeviceUnlockCodeResponse.getDeviceUnlockDetails()
					.setManufacturerName(queryAllResponse.getFieldInput03());
			sendDeviceUnlockCodeResponse.getDeviceUnlockDetails().setDeviceModel(queryAllResponse.getFieldInput04());
			sendDeviceUnlockCodeResponse.getDeviceUnlockDetails().getEmailAddress()
					.add(queryAllResponse.getFieldInput05());
			if (StringUtils.isNotEmpty(queryAllResponse.getFieldInputA())) {
				sendDeviceUnlockCodeResponse.getDeviceUnlockDetails().setUnlockCode(queryAllResponse.getFieldInputA());
			} else {
				sendDeviceUnlockCodeResponse.getDeviceUnlockDetails().setUnlockCode("NO UNLOCK CODE FOUND");
			}
			if (StringUtils.isNotEmpty(queryAllResponse.getFieldInputN())) {
				sendDeviceUnlockCodeResponse.getDeviceUnlockDetails()
						.setUnlockInstruction(queryAllResponse.getFieldInputN());
			} else {
				sendDeviceUnlockCodeResponse.getDeviceUnlockDetails().setUnlockInstruction("NO INSTRUCTIONS FOUND");
			}
			sendDeviceUnlockCodeResponse.getDeviceUnlockDetails().setLanguage(queryAllResponse.getFieldInput10());
		} else {
			sendDeviceUnlockCodeResponse.getDeviceUnlockDetails()
					.setUnlockableRemotely(sendDeviceUnlockCodeRequest.isUnlockableRemotely());
			sendDeviceUnlockCodeResponse.getDeviceUnlockDetails().setMSISDN(sendDeviceUnlockCodeRequest.getMSISDN());
			sendDeviceUnlockCodeResponse.getDeviceUnlockDetails().setImei(sendDeviceUnlockCodeRequest.getImei());
			sendDeviceUnlockCodeResponse.getDeviceUnlockDetails()
					.setManufacturerName(sendDeviceUnlockCodeRequest.getManufacturerName());
			sendDeviceUnlockCodeResponse.getDeviceUnlockDetails()
					.setDeviceModel(sendDeviceUnlockCodeRequest.getDeviceModel());
			sendDeviceUnlockCodeRequest.getEmailAddress().addAll(sendDeviceUnlockCodeRequest.getEmailAddress());
			sendDeviceUnlockCodeResponse.getDeviceUnlockDetails()
					.setLanguage(sendDeviceUnlockCodeRequest.getLanguage());
		}
	}

	/**
	 * Method getGUID
	 * 
	 * @param Guid guid
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void getGUID(Guid guid) throws TMobileProcessException {
		if(null != sharedServiceContext.getRequestValue("GUID")) { 
			Guid guidLocal = (Guid) sharedServiceContext.getRequestValue("GUID");
			guid.setRoot(guidLocal.getRoot());
		}
	}

	/**
	 * Method executeMethod
	 * 
	 * @param
	 * @return void
	 * @throws TMobileProcessException
	 */
	public void executeMethod(SendDeviceUnlockCodeRequest sendDeviceUnlockCodeRequest,
			SendDeviceUnlockCodeResponse sendDeviceUnlockCodeResponse) throws TMobileProcessException {
		HeaderValidationStartInput headerValidationStartInput = new HeaderValidationStartInput();
		QueryAllSOAPStartInput queryAllSOAPStartInput = new QueryAllSOAPStartInput();
		QueryAllSOAPEndOutput queryAllSOAPEndOutput = new QueryAllSOAPEndOutput();
		Guid guid = new Guid();
		ValidateData validateData = new ValidateData();
		EUSUnlockRequest eUSUnlockRequest = new EUSUnlockRequest();
		EUSUnlockResponse eUSUnlockResponse = new EUSUnlockResponse();
		ErrorDetails errorDetails = new ErrorDetails();
		validateHeaders(headerValidationStartInput, sendDeviceUnlockCodeRequest);
		validateData(validateData, sendDeviceUnlockCodeRequest);
		if (validateData.getRoot().getMessageSet() != null
				&& validateData.getRoot().getMessageSet().getText().size() > 0) {
			validationError(validateData, errorDetails);
		}
		if (sendDeviceUnlockCodeRequest.isUnlockableRemotely()) {
			invokeEUSUnlockServiceSoap(eUSUnlockRequest, eUSUnlockResponse, sendDeviceUnlockCodeRequest);
		} else {
			invokeRemedySoap(queryAllSOAPStartInput, queryAllSOAPEndOutput, sendDeviceUnlockCodeRequest);
		}
		getGUID(guid);
		transformPayload(guid, queryAllSOAPEndOutput, sendDeviceUnlockCodeResponse, sendDeviceUnlockCodeRequest);

	}

}