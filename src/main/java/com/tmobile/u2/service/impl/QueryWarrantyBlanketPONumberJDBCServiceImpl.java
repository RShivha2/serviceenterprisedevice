package com.tmobile.u2.service.impl;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.example.xmlns.logvariable.LogVariables;
import com.example.xmlns.logvariable.Root;
import com.tibco.namespaces.tnt.plugins.jdbc_a13cb515_6e41_4894_84ca_d54b87b05824_input.JdbcQueryActivityInput;
import com.tibco.namespaces.tnt.plugins.jdbc_a13cb515_6e41_4894_84ca_d54b87b05824_output.QueryWarrantyBlanketPONumberRecordList;
import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.services.schema.dataobject.servicecontext.ErrorDetails;
import com.tmobile.u2.exception.TMobileDataAccessException;
import com.tmobile.u2.exception.TMobileProcessException;
import com.tmobile.u2.repositories.IQueryWarrantyBlanketPONumberJDBCRepository;
import com.tmobile.u2.service.IQueryWarrantyBlanketPONumberJDBCService;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.util.TibcoToJavaUtil;

import targetreef.applications.reef.endpoints.devicewarranty.querywarrantyblanketponumberjdbc_end_output.QueryWarrantyBlanketPONumberJDBCEndOutput;

/*************************************************************************
 * 
 * TMOBILE CONFIDENTIAL
 * _________________________________________________________________________________
 * 
 * TMOBILE is a trademark of TMOBILE Company.
 *
 * Copyright © 2021 TMOBILE. All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of
 * TMOBILE and its suppliers, if any. The intellectual and technical concepts
 * contained herein are proprietary to TMOBILE and its suppliers and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by
 * trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from TMOBILE.
 *
 *************************************************************************/
// Author : Generated by ATMA ®
// Revision History :  
@Service
public class QueryWarrantyBlanketPONumberJDBCServiceImpl implements IQueryWarrantyBlanketPONumberJDBCService {

	private static final Logger LOGGER = LoggerFactory.getLogger(QueryWarrantyBlanketPONumberJDBCServiceImpl.class);

	@Autowired
	private IQueryWarrantyBlanketPONumberJDBCRepository queryWarrantyBlanketPONumberJDBCRepository;

	public void setIQueryWarrantyBlanketPONumberJDBCRepository(
			IQueryWarrantyBlanketPONumberJDBCRepository queryWarrantyBlanketPONumberJDBCRepository) {
		this.queryWarrantyBlanketPONumberJDBCRepository = queryWarrantyBlanketPONumberJDBCRepository;
	}

	@Autowired
	private ILogProcessDetailsService iLogProcessDetailsService;

	public void setLogProcessDetailsService(ILogProcessDetailsService iLogProcessDetailsService) {
		this.iLogProcessDetailsService = iLogProcessDetailsService;
	}

	/**
	 * Method queryWarrantyBlanketPONumber
	 * 
	 * @param ResultSet resultSet ,JdbcQueryActivityInput jdbcQueryActivityInput
	 * @return void
	 * @throws TMobileProcessException
	 * @throws TMobileDataAccessException
	 */
	private void queryWarrantyBlanketPONumber(
			QueryWarrantyBlanketPONumberRecordList queryWarrantyBlanketPONumberRecordList,
			JdbcQueryActivityInput jdbcQueryActivityInput) throws TMobileProcessException {

		QueryWarrantyBlanketPONumberRecordList queryWarrantyBlanketPONumberRecordListLocal = queryWarrantyBlanketPONumberJDBCRepository
				.queryWarrantyBlanketPONumber(jdbcQueryActivityInput);
		queryWarrantyBlanketPONumberRecordList.setQueryWarrantyBlanketPONumberRecord(
				queryWarrantyBlanketPONumberRecordListLocal.getQueryWarrantyBlanketPONumberRecord());
	}

	/**
	 * Method logSuccess
	 * 
	 * @param ErrorDetails errorDetails ,LogVariables logVariable
	 *                     ,LogProcessDetailsStartInput logProcessDetailsStartInput
	 *                     ,QueryWarrantyBlanketPONumberRecordList
	 *                     queryWarrantyBlanketPONumberRecordList
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void logSuccess(ErrorDetails errorDetails, LogVariables logVariable,
			LogProcessDetailsStartInput logProcessDetailsStartInput,
			QueryWarrantyBlanketPONumberRecordList queryWarrantyBlanketPONumberRecordList)
			throws TMobileProcessException {
		com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.Root root = logProcessDetailsStartInput
				.getRoot();
		if (errorDetails.getCode() != null) {
			root.setMsgCode(errorDetails.getCode());
			if (errorDetails.getMesssage() != null) {
				root.setMessage(errorDetails.getMesssage());
			}
		}
		root.setProcessName("QueryWarrantyBlanketPONUmberJDBC");
		root.setTargetSystem("REEF");
		if (errorDetails.getCode() != null) {
			root.setTargetResponse(TibcoToJavaUtil.renderXml(errorDetails));
		} else if (!CollectionUtils
				.isEmpty(queryWarrantyBlanketPONumberRecordList.getQueryWarrantyBlanketPONumberRecord())) {
			root.setTargetResponse(TibcoToJavaUtil.renderXml(queryWarrantyBlanketPONumberRecordList));
		} else {
			root.setTargetResponse(TibcoToJavaUtil.renderXml(errorDetails));
		}
		if (logVariable.getRoot().getRequestDateTime() != null) {
			root.setRequestDatetime(logVariable.getRoot().getRequestDateTime());
		}
		if (logVariable.getRoot().getRequestTimestamp() != null) {
			root.setRequestTimestamp(logVariable.getRoot().getRequestTimestamp());
		}
		iLogProcessDetailsService.executeMethod(logProcessDetailsStartInput);
	}

	/**
	 * Method end
	 * 
	 * @param QueryWarrantyBlanketPONumberJDBCEndOutput queryWarrantyBlanketPONumberJDBCEndOutput
	 *                                                  ,QueryWarrantyBlanketPONumberRecordList
	 *                                                  queryWarrantyBlanketPONumberRecordList
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void end(QueryWarrantyBlanketPONumberJDBCEndOutput queryWarrantyBlanketPONumberJDBCEndOutput,
			QueryWarrantyBlanketPONumberRecordList queryWarrantyBlanketPONumberRecordList)
			throws TMobileProcessException {
		targetreef.applications.reef.endpoints.devicewarranty.querywarrantyblanketponumberjdbc_end_output.Root root = queryWarrantyBlanketPONumberJDBCEndOutput
				.getRoot();
		if (!CollectionUtils.isEmpty(queryWarrantyBlanketPONumberRecordList.getQueryWarrantyBlanketPONumberRecord()) 
				&& queryWarrantyBlanketPONumberRecordList.getQueryWarrantyBlanketPONumberRecord().get(0)
				.getPonumber() != null) {
			root.setPONUMBER(queryWarrantyBlanketPONumberRecordList.getQueryWarrantyBlanketPONumberRecord().get(0)
					.getPonumber());
		}
	}

	/**
	 * Method assign
	 * 
	 * @param LogVariables logVariable
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void assign(LogVariables logVariable) throws TMobileProcessException {
		Root root = logVariable.getRoot();
		root.setRequestDateTime(LocalDateTime.now().toString());
		root.setRequestTimestamp(System.currentTimeMillis());
	}

	/**
	 * Method error
	 * 
	 * @param ErrorDetails errorDetails ,LogVariables logVariable ,ErrorInput
	 *                     errorInput
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void error(ErrorDetails errorDetails) throws TMobileProcessException {
		errorDetails.setTargetSystem("REEF");
		throw new TMobileProcessException(errorDetails);
	}

	/**
	 * Method executeMethod
	 * 
	 * @param QueryWarrantyBlanketPONumberJDBCEndOutput queryWarrantyBlanketPONumberJDBCEndOutput
	 * @return void
	 * @throws TMobileProcessException
	 */
	public void executeMethod(QueryWarrantyBlanketPONumberJDBCEndOutput queryWarrantyBlanketPONumberJDBCEndOutput)
			throws TMobileProcessException {
		JdbcQueryActivityInput jdbcQueryActivityInput = new JdbcQueryActivityInput();
		QueryWarrantyBlanketPONumberRecordList queryWarrantyBlanketPONumberRecordList = new QueryWarrantyBlanketPONumberRecordList();
		LogVariables logVariable = new LogVariables();
		ErrorDetails errorDetails = new ErrorDetails();
		LogProcessDetailsStartInput logProcessDetailsStartInput = new LogProcessDetailsStartInput();
		assign(logVariable);
		try {
			queryWarrantyBlanketPONumber(queryWarrantyBlanketPONumberRecordList, jdbcQueryActivityInput);
			logSuccess(errorDetails, logVariable, logProcessDetailsStartInput, queryWarrantyBlanketPONumberRecordList);
			end(queryWarrantyBlanketPONumberJDBCEndOutput, queryWarrantyBlanketPONumberRecordList);
		} catch (TMobileProcessException ex) {
			LOGGER.error("Error occurred in method: QueryWarrantyBlanketPONumberJDBC() and terminated due to ", ex);
			if(null != ex.getErrorDetails()) {
				errorDetails = ex.getErrorDetails();
			}
			logSuccess(errorDetails, logVariable, logProcessDetailsStartInput, queryWarrantyBlanketPONumberRecordList);
			error(errorDetails);
		}

	}

}