package com.tmobile.u2.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmobile.services.base.ApplicationObjectKey;
import com.tmobile.services.base.Header;
import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckBlackListRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckBlackListResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.NetworkResource;
import com.tmobile.services.schema.dataobject.createservicecontextendoutput.CreateServiceContextOutput;
import com.tmobile.services.schema.dataobject.createservicecontextstartinput.CreateServiceContextInput;
import com.tmobile.services.schema.dataobject.errorlookupendoutput.ErrorLookupEndOutput;
import com.tmobile.services.schema.dataobject.guid.Guid;
import com.tmobile.services.schema.dataobject.legacyesoaerrorlookupstartinput.LegacyESOAErrorLookupStartInput;
import com.tmobile.services.schema.dataobject.legacyesoaerrorlookupstartinput.Root;
import com.tmobile.services.schema.dataobject.logservicecontextstartinput.LogServiceContextStartInput;
import com.tmobile.services.schema.dataobject.responsestatus.ResponseStatus;
import com.tmobile.services.schema.dataobject.responsestatus.ResponseStatusDetail;
import com.tmobile.services.schema.dataobject.servicecontext.ErrorDetails;
import com.tmobile.services.schema.dataobject.servicecontext.Key;
import com.tmobile.u2.exception.TMobileProcessException;
import com.tmobile.u2.service.ICheckBlackListImplService;
import com.tmobile.u2.service.IInvokeCheckBlackListService;
import com.tmobile.u2.shared.service.ICreateServiceContextService;
import com.tmobile.u2.shared.service.IErrorLookupService;
import com.tmobile.u2.shared.service.ILogServiceContextService;
import com.tmobile.u2.util.SharedServiceContext;
import com.tmobile.u2.util.TibcoToJavaUtil;

/*************************************************************************
 * 
 * TMOBILE CONFIDENTIAL
 * _________________________________________________________________________________
 * 
 * TMOBILE is a trademark of TMOBILE Company.
 *
 * Copyright © 2021 TMOBILE. All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of
 * TMOBILE and its suppliers, if any. The intellectual and technical concepts
 * contained herein are proprietary to TMOBILE and its suppliers and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by
 * trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from TMOBILE.
 *
 *************************************************************************/
// Author : Generated by ATMA ®
// Revision History :  
@Service
public class CheckBlackListImplServiceImpl implements ICheckBlackListImplService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CheckBlackListImplServiceImpl.class);

	@Autowired
	private IErrorLookupService iErrorLookupService;

	public void setErrorLookupService(IErrorLookupService iErrorLookupService) {
		this.iErrorLookupService = iErrorLookupService;
	}

	@Autowired
	private SharedServiceContext sharedServiceContext;

	public void setSharedServiceContext(SharedServiceContext sharedServiceContext) {
		this.sharedServiceContext = sharedServiceContext;
	}

	@Autowired
	private ICreateServiceContextService iCreateServiceContextService;

	public void setCreateServiceContextService(ICreateServiceContextService iCreateServiceContextService) {
		this.iCreateServiceContextService = iCreateServiceContextService;
	}

	@Autowired
	private ILogServiceContextService iLogServiceContextService;

	public void setLogServiceContextService(ILogServiceContextService iLogServiceContextService) {
		this.iLogServiceContextService = iLogServiceContextService;
	}

	@Autowired
	private IInvokeCheckBlackListService iInvokeCheckBlackListService;

	public void setInvokeCheckBlackListService(IInvokeCheckBlackListService iInvokeCheckBlackListService) {
		this.iInvokeCheckBlackListService = iInvokeCheckBlackListService;
	}

	/**
	 * Method lookupError
	 * 
	 * @param LegacyESOAErrorLookupStartInput legacyESOAErrorLookupStartInput
	 *                                        ,CheckBlackListRequest
	 *                                        checkBlackListRequest
	 *                                        ,ErrorLookupEndOutput
	 *                                        errorLookupEndOutput ,ErrorDetails
	 *                                        errorDetails
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void lookupError(LegacyESOAErrorLookupStartInput legacyESOAErrorLookupStartInput,
			CheckBlackListRequest checkBlackListRequest, ErrorLookupEndOutput errorLookupEndOutput,
			ErrorDetails errorDetails) throws TMobileProcessException {
		Root root = legacyESOAErrorLookupStartInput.getRoot();
		if (errorLookupEndOutput.getRoot().getErrorDetails() != null) {
			root.setErrorDetails(errorLookupEndOutput.getRoot().getErrorDetails());
		} else {
			root.setErrorDetails(new ErrorDetails());
			root.getErrorDetails().setTargetSystem("ESP");
			root.getErrorDetails().setCode(errorDetails.getCode());
			root.getErrorDetails().setMesssage(errorDetails.getMesssage());
			root.getErrorDetails().setStackTrace(errorDetails.getStackTrace());
			root.getErrorDetails().setProcessStack(errorDetails.getProcessStack());
			root.getErrorDetails().setSourceMessage(TibcoToJavaUtil.renderXml(checkBlackListRequest));
		}
		iErrorLookupService.executeMethod(legacyESOAErrorLookupStartInput, errorLookupEndOutput);
	}

	/**
	 * Method createServicecontext
	 * 
	 * @param CheckBlackListRequest checkBlackListRequest ,Guid guid
	 *                              ,CreateServiceContextOutput
	 *                              createServiceContextEndOutput
	 *                              ,CreateServiceContextInput
	 *                              createServiceContextStartInput
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void createServicecontextErr(CheckBlackListRequest checkBlackListRequestErr, Guid guid,
			CreateServiceContextOutput createServiceContextEndOutput,
			CreateServiceContextInput createServiceContextStartInput) throws TMobileProcessException {
		CheckBlackListRequest var = checkBlackListRequestErr;
		com.tmobile.services.schema.dataobject.createservicecontextstartinput.Root root = createServiceContextStartInput
				.getRoot();
		root.setDomainName("ProductManagement");
		root.setServiceName("DeviceEnterprise");
		root.setOperationName("checkBlackList");
		root.setEngineName(TibcoToJavaUtil.getProperty("engineName"));
		root.setRequest(TibcoToJavaUtil.renderXml(var));
		if (var.getHeader().getSender().getSessionId() != null) {
			root.setJobId(var.getHeader().getSender().getSessionId());
		}
		root.setSenderId(var.getHeader().getSender().getSenderId());
		root.setChannel(var.getHeader().getSender().getChannelId());
		root.setApplicationId(var.getHeader().getSender().getApplicationId());
		if (var.getHeader().getSender().getApplicationUserId() != null) {
			root.setUserId(var.getHeader().getSender().getApplicationUserId());
		}
		if (StringUtils.isNotEmpty(checkBlackListRequestErr.getHeader().getSender().getInteractionId())) {
			Key key = new Key();
			key.setName("InteractionId");
			if (checkBlackListRequestErr.getHeader().getSender().getInteractionId() != null) {
				key.setValue(checkBlackListRequestErr.getHeader().getSender().getInteractionId());
			}
			root.getKey().add(key);
		}
		if (guid.getRoot().getGUID() != null) {
			Key key = new Key();
			key.setName("GUID");
			key.setValue(guid.getRoot().getGUID());
			root.getKey().add(key);
		}

		for (NetworkResource checkBlackNetworkResource : checkBlackListRequestErr.getNetworkResource()) {
			if (checkBlackNetworkResource.getMobileNumber() != null && checkBlackNetworkResource.getMobileNumber().getMsisdn() != null) {
				Key key = new Key();
				key.setName("MSISDN");
				key.setValue(checkBlackNetworkResource.getMobileNumber().getMsisdn());
				root.getKey().add(key);
			}
			if (checkBlackNetworkResource.getImei() != null) {
				Key key = new Key();
				key.setName("IMEI");
				key.setValue(checkBlackNetworkResource.getImei());
				root.getKey().add(key);
			}
		}
		iCreateServiceContextService.executeMethod(createServiceContextStartInput, createServiceContextEndOutput);
	}

	/**
	 * Method mapErrorResponse
	 * 
	 * @param CheckBlackListResponse checkBlackListResponse ,ErrorLookupEndOutput
	 *                               errorLookupEndOutput ,CheckBlackListRequest
	 *                               checkBlackListRequest ,Guid guid
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void mapErrorResponse(CheckBlackListResponse checkBlackListResponse,
			ErrorLookupEndOutput errorLookupEndOutput, CheckBlackListRequest checkBlackListRequest, Guid guid)
			throws TMobileProcessException {
		checkBlackListResponse.setServiceTransactionId(checkBlackListRequest.getServiceTransactionId());
		checkBlackListResponse.setVersion(checkBlackListRequest.getVersion());
		checkBlackListResponse.setHeader(new Header());
		if (checkBlackListRequest.getHeader() != null) {
			checkBlackListResponse.getHeader().setSender(checkBlackListRequest.getHeader().getSender());
			checkBlackListResponse.getHeader().setTarget(checkBlackListRequest.getHeader().getTarget());
			checkBlackListResponse.getHeader().getProviderId()
					.addAll(checkBlackListRequest.getHeader().getProviderId());
		} else {
			checkBlackListResponse.getHeader().getSender().setSenderId("");
			checkBlackListResponse.getHeader().getSender().setChannelId("");
			checkBlackListResponse.getHeader().getSender().setApplicationId("");
		}
		if (guid.getRoot().getGUID() != null) {
			ApplicationObjectKey providerId = new ApplicationObjectKey();
			providerId.setId(guid.getRoot().getGUID());
			checkBlackListResponse.getHeader().getProviderId().add(providerId);
		}
		if (errorLookupEndOutput.getRoot().getResponseStatus() != null) {
			ResponseStatus checkBlackResponseStatus = new ResponseStatus();
			ResponseStatus rootResponseStatusLocal = errorLookupEndOutput.getRoot().getResponseStatus();
			if (rootResponseStatusLocal.getCode() != null) {
				checkBlackResponseStatus.setCode(rootResponseStatusLocal.getCode());
			}
			for (ResponseStatusDetail responseStatusLocalDetailLocal : rootResponseStatusLocal.getDetail()) {
				ResponseStatusDetail responseStatusDetail = new ResponseStatusDetail();
				responseStatusDetail.setSubStatusCode(responseStatusLocalDetailLocal.getSubStatusCode());
				responseStatusDetail.setDefinition(responseStatusLocalDetailLocal.getDefinition());
				responseStatusDetail.setExplanation(responseStatusLocalDetailLocal.getExplanation());
				checkBlackResponseStatus.getDetail().add(responseStatusDetail);
			}
			checkBlackListResponse.setResponseStatus(checkBlackResponseStatus);
		}
	}

	/**
	 * Method logError
	 * 
	 * @param CheckBlackListResponse checkBlackListResponse ,ErrorLookupEndOutput
	 *                               errorLookupEndOutput
	 *                               ,LogServiceContextStartInput
	 *                               logServiceContextStartInput
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void logError(CheckBlackListResponse checkBlackListResponse, ErrorLookupEndOutput errorLookupEndOutput,
			LogServiceContextStartInput logServiceContextStartInput) throws TMobileProcessException {
		com.tmobile.services.schema.dataobject.logservicecontextstartinput.Root root = logServiceContextStartInput
				.getRoot();
		root.setResponse(TibcoToJavaUtil.renderXml(checkBlackListResponse));
		root.getErrorDetails().add(errorLookupEndOutput.getRoot().getErrorDetails());
		iLogServiceContextService.executeMethod(logServiceContextStartInput);
	}

	/**
	 * Method getGUIDEr
	 * 
	 * @param CheckBlackListResponse checkBlackListResponse ,Guid guid
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void getGUIDEr(Guid guid) throws TMobileProcessException {
		if(null != sharedServiceContext.getRequestValue("GUID")) { 
			Guid guidLocal = (Guid) sharedServiceContext.getRequestValue("GUID");
			guid.setRoot(guidLocal.getRoot());
		}
	}

	/**
	 * Method createServiceContext
	 * 
	 * @param CheckBlackListRequest checkBlackListRequest ,Guid guid
	 *                              ,CreateServiceContextOutput
	 *                              createServiceContextEndOutput
	 *                              ,CreateServiceContextInput
	 *                              createServiceContextStartInput
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void createServiceContext(CheckBlackListRequest checkBlackListRequest, Guid guid,
			CreateServiceContextOutput createServiceContextEndOutput,
			CreateServiceContextInput createServiceContextStartInput) throws TMobileProcessException {
		com.tmobile.services.schema.dataobject.createservicecontextstartinput.Root root = createServiceContextStartInput
				.getRoot();
		root.setDomainName("ProductManagement");
		root.setServiceName("DeviceEnterprise");
		root.setOperationName("checkBlackList");
		root.setEngineName(TibcoToJavaUtil.getProperty("engineName"));
		root.setRequest(TibcoToJavaUtil.renderXml(checkBlackListRequest));
		if (checkBlackListRequest.getHeader().getSender().getSessionId() != null) {
			root.setJobId(checkBlackListRequest.getHeader().getSender().getSessionId());
		}
		root.setSenderId(checkBlackListRequest.getHeader().getSender().getSenderId());
		root.setChannel(checkBlackListRequest.getHeader().getSender().getChannelId());
		root.setApplicationId(checkBlackListRequest.getHeader().getSender().getApplicationId());
		if (checkBlackListRequest.getHeader().getSender().getApplicationUserId() != null) {
			root.setUserId(checkBlackListRequest.getHeader().getSender().getApplicationUserId());
		}
		if (StringUtils.isNotEmpty(checkBlackListRequest.getHeader().getSender().getInteractionId())) {
			Key key = new Key();
			key.setName("InteractionId");
			if (checkBlackListRequest.getHeader().getSender().getInteractionId() != null) {
				key.setValue(checkBlackListRequest.getHeader().getSender().getInteractionId());
			}
			root.getKey().add(key);
		}
		if (guid.getRoot().getGUID() != null) {
			Key key = new Key();
			key.setName("GUID");
			key.setValue(guid.getRoot().getGUID());
			root.getKey().add(key);
		}

		for (NetworkResource networkResource : checkBlackListRequest.getNetworkResource()) {
			if (networkResource.getMobileNumber() != null && networkResource.getMobileNumber().getMsisdn() != null) {
				Key key = new Key();
				key.setName("MSISDN");
				key.setValue(networkResource.getMobileNumber().getMsisdn());
				root.getKey().add(key);
			}
			if (networkResource.getImei() != null) {
				Key imeiKkey = new Key();
				imeiKkey.setName("IMEI");
				imeiKkey.setValue(networkResource.getImei());
				root.getKey().add(imeiKkey);
			}
		}
		iCreateServiceContextService.executeMethod(createServiceContextStartInput, createServiceContextEndOutput);
	}

	/**
	 * Method invokeSappiValidateIMEISoap
	 * 
	 * @param CheckBlackListResponse checkBlackListResponse ,CheckBlackListRequest
	 *                               checkBlackListRequest
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void invokeSappiValidateIMEISoap(CheckBlackListResponse checkBlackListResponse,
			CheckBlackListRequest checkBlackListRequest) throws TMobileProcessException {
		iInvokeCheckBlackListService.executeMethod(checkBlackListRequest, checkBlackListResponse);
	}

	/**
	 * Method logSuccess
	 * 
	 * @param CheckBlackListResponse checkBlackListResponse
	 *                               ,LogServiceContextStartInput
	 *                               logServiceContextStartInput
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void logSuccess(CheckBlackListResponse checkBlackListResponse,
			LogServiceContextStartInput logServiceContextStartInput) throws TMobileProcessException {
		com.tmobile.services.schema.dataobject.logservicecontextstartinput.Root root = logServiceContextStartInput
				.getRoot();
		root.setResponse(TibcoToJavaUtil.renderXml(checkBlackListResponse));
		iLogServiceContextService.executeMethod(logServiceContextStartInput);
	}

	/**
	 * Method getGUID
	 * 
	 * @param Guid guid
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void getGUID(Guid guid) throws TMobileProcessException {
		if(null != sharedServiceContext.getRequestValue("GUID")) { 
			Guid guidLocal = (Guid) sharedServiceContext.getRequestValue("GUID");
			guid.setRoot(guidLocal.getRoot());
		}
	}

	/**
	 * Method executeMethod
	 * 
	 * @param
	 * @return void
	 * @throws TMobileProcessException
	 */
	public void executeMethod(CheckBlackListRequest checkBlackListRequest,
			CheckBlackListResponse checkBlackListResponse) throws TMobileProcessException {
		CreateServiceContextInput createServiceContextStartInput = new CreateServiceContextInput();
		ErrorLookupEndOutput errorLookupEndOutput = new ErrorLookupEndOutput();
		CreateServiceContextOutput createServiceContextEndOutput = new CreateServiceContextOutput();
		Guid guid = new Guid();
		LegacyESOAErrorLookupStartInput legacyESOAErrorLookupStartInput = new LegacyESOAErrorLookupStartInput();
		LogServiceContextStartInput logServiceContextStartInput = new LogServiceContextStartInput();
		ErrorDetails errorDetails = new ErrorDetails();
		try {
			getGUID(guid);
			createServiceContext(checkBlackListRequest, guid, createServiceContextEndOutput,
					createServiceContextStartInput);
			invokeSappiValidateIMEISoap(checkBlackListResponse, checkBlackListRequest);
			logSuccess(checkBlackListResponse, logServiceContextStartInput);
		} catch (TMobileProcessException ex) {
			LOGGER.error("Error occurred in method: CheckBlackListImpl() and terminated due to ", ex);
			if (null != ex.getErrorDetails()) {
				errorDetails = ex.getErrorDetails();
			}
			if (!createServiceContextEndOutput.getRoot().isComplete()) {
				getGUIDEr(guid);
				createServicecontextErr(checkBlackListRequest, guid, createServiceContextEndOutput,
						createServiceContextStartInput);
			}
			lookupError(legacyESOAErrorLookupStartInput, checkBlackListRequest, errorLookupEndOutput, errorDetails);
			mapErrorResponse(checkBlackListResponse, errorLookupEndOutput, checkBlackListRequest, guid);
			logError(checkBlackListResponse, errorLookupEndOutput, logServiceContextStartInput);
		}

	}

}