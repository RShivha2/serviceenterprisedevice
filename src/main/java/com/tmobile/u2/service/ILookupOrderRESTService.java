package com.tmobile.u2.service;

import com.t_mobile.schema.apple._360lookup.OrderLookupRequesttype;
import com.t_mobile.schema.apple._360lookup.OrderLookupRestype;
import com.tmobile.u2.exception.TMobileProcessException;

public interface ILookupOrderRESTService {

	void executeMethod(OrderLookupRequesttype orderLookupReq, OrderLookupRestype orderLookupRes) throws TMobileProcessException;

}
