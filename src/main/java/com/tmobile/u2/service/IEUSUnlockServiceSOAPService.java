package com.tmobile.u2.service;

import com.tmobile.eus.unlock.EUSUnlockRequest;
import com.tmobile.eus.unlock.EUSUnlockResponse;
import com.tmobile.u2.exception.TMobileProcessException;

public interface IEUSUnlockServiceSOAPService {

	void executeMethod(EUSUnlockRequest eUSUnlockRequest, EUSUnlockResponse eUSUnlockResponse) throws TMobileProcessException;

}
