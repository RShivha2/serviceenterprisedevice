package com.tmobile.u2.service;

import com.tmobile.services.csi.device.getshipusagedataforimei.GetShipUsageDataForIMEIRequestType;
import com.tmobile.services.csi.device.getshipusagedataforimei.GetShipUsageDataForIMEIResponseType;
import com.tmobile.u2.exception.TMobileProcessException;

public interface IGetShipUsageDataForImeiSqlService {

	void executeMethod(GetShipUsageDataForIMEIRequestType getShipUsageDataForIMEIRequest,
			GetShipUsageDataForIMEIResponseType getShipUsageDataForIMEIResponse) throws TMobileProcessException;

}
