package com.tmobile.u2.service;

import com.t_mobile.schema.apple.createorder.CreateOrderRequesttype;
import com.t_mobile.schema.apple.createorder.CreateOrderResponsetype;
import com.tmobile.u2.exception.TMobileProcessException;

public interface ICreateOrderRESTService {

	void executeMethod(CreateOrderRequesttype createOrderRequest, CreateOrderResponsetype createOrderResponse) throws TMobileProcessException;

}
