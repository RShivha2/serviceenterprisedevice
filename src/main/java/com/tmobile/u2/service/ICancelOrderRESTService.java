package com.tmobile.u2.service;

import com.t_mobile.schema.apple.cancelorder.CancelOrderReqtype;
import com.t_mobile.schema.apple.cancelorder.CancelOrderRestype;
import com.tmobile.u2.exception.TMobileProcessException;

public interface ICancelOrderRESTService {

	void executeMethod(CancelOrderReqtype cancelOrderReq, CancelOrderRestype cancelOrderRes) throws TMobileProcessException;

}
