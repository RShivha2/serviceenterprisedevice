package com.tmobile.u2.service;

import com.tmobile.u2.exception.TMobileProcessException;

import targetcaasclient.applications.caas.endpoints.spaceops.executespaceophttps_end_output.Response;
import targetcaasclient.applications.caas.endpoints.spaceops.executespaceophttps_start_input.SpaceOps;

public interface IExecuteSpaceOpHttpsService {

	void executeMethod(SpaceOps spaceOps, Response response) throws TMobileProcessException;

}
