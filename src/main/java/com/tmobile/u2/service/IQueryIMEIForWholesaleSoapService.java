package com.tmobile.u2.service;

import com.t_mobile.erp.services.logistics.MtQueryIMEIResponse;
import com.tmobile.u2.exception.TMobileProcessException;

import targetsappi.applications.sappi.endpoints.device.queryimeiforwholesalesoap_start_input.QueryIMEIForWholesaleSoapStartInput;

public interface IQueryIMEIForWholesaleSoapService {

	void executeMethod(QueryIMEIForWholesaleSoapStartInput queryIMEIForWholesaleSoapStartInput,
			MtQueryIMEIResponse dtQueryimeiresponse) throws TMobileProcessException;

}
