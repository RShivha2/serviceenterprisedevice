package com.tmobile.u2.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckAppleCareCompatabilityRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckAppleCareCompatabilityResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckBlackListRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckBlackListResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckDeviceCommissionEligiblityRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckDeviceCommissionEligiblityResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.GetAppleCareEnrollmentRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.GetAppleCareEnrollmentResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.GetDeviceDetailsRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.GetDeviceDetailsResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.ManageAppleCareEnrollmentRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.ManageAppleCareEnrollmentResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.ManageBlackListRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.ManageBlackListResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.PostDeviceAssesmentResultsRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.PostDeviceAssesmentResultsResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.RegisterDeviceForFirstUseRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.RegisterDeviceForFirstUseResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.SendDeviceUnlockCodeRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.SendDeviceUnlockCodeResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.UpdateDeviceOrderRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.UpdateDeviceOrderResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.UpdateUnlockEligibilityRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.UpdateUnlockEligibilityResponse;
import com.tmobile.u2.constants.U2Constants;
import com.tmobile.u2.exception.TMobileProcessException;
import com.tmobile.u2.service.IDeviceEnterpriseService;

@Endpoint
public class DeviceEnterpriseEndPoint {

	private static final Logger LOGGER = LoggerFactory.getLogger(DeviceEnterpriseEndPoint.class);

	@Autowired
	private IDeviceEnterpriseService deviceEnterpriseService;

	/**
	 * @param sendDeviceUnlockCodeRequest
	 * @return
	 * @throws TMobileProcessException
	 */
	@PayloadRoot(namespace = "http://services.tmobile.com/ProductManagement/DeviceEnterprise/V1", localPart = "sendDeviceUnlockCodeRequest")
	@ResponsePayload
	public SendDeviceUnlockCodeResponse sendDeviceUnlockCode(
			@RequestPayload SendDeviceUnlockCodeRequest sendDeviceUnlockCodeRequest) throws TMobileProcessException {
		LOGGER.debug(U2Constants.REQUEST, sendDeviceUnlockCodeRequest);
		SendDeviceUnlockCodeResponse sendDeviceUnlockCodeResponse = new SendDeviceUnlockCodeResponse();
		try {
			deviceEnterpriseService.sendDeviceUnlockCode(sendDeviceUnlockCodeRequest, sendDeviceUnlockCodeResponse);
		} catch (TMobileProcessException dataEx) {
			LOGGER.error(U2Constants.ERROR, dataEx);
			throw new TMobileProcessException(dataEx);
		} catch (Exception ex) {
			LOGGER.error(U2Constants.ERROR, ex);
			throw new TMobileProcessException(ex);
		}
		LOGGER.debug(U2Constants.RESPONSE, sendDeviceUnlockCodeResponse);
		return sendDeviceUnlockCodeResponse;
	}

	/**
	 * @param updateDeviceOrderRequest
	 * @return
	 * @throws TMobileProcessException
	 */
	@PayloadRoot(namespace = "http://services.tmobile.com/ProductManagement/DeviceEnterprise/V1", localPart = "updateDeviceOrderRequest")
	@ResponsePayload
	public UpdateDeviceOrderResponse updateDeviceOrder(
			@RequestPayload UpdateDeviceOrderRequest updateDeviceOrderRequest) throws TMobileProcessException {
		LOGGER.debug(U2Constants.REQUEST, updateDeviceOrderRequest);
		UpdateDeviceOrderResponse updateDeviceOrderResponse = new UpdateDeviceOrderResponse();
		try {
			deviceEnterpriseService.updateDeviceOrder(updateDeviceOrderRequest, updateDeviceOrderResponse);
		} catch (TMobileProcessException dataEx) {
			LOGGER.error(U2Constants.ERROR, dataEx);
			throw new TMobileProcessException(dataEx);
		} catch (Exception ex) {
			LOGGER.error(U2Constants.ERROR, ex);
			throw new TMobileProcessException(ex);
		}
		LOGGER.debug(U2Constants.RESPONSE, updateDeviceOrderResponse);
		return updateDeviceOrderResponse;
	}

	/**
	 * @param getAppleCareEnrollmentRequest
	 * @return
	 * @throws TMobileProcessException
	 */
	@PayloadRoot(namespace = "http://services.tmobile.com/ProductManagement/DeviceEnterprise/V1", localPart = "getAppleCareEnrollmentRequest")
	@ResponsePayload
	public GetAppleCareEnrollmentResponse getAppleCareEnrollment(
			@RequestPayload GetAppleCareEnrollmentRequest getAppleCareEnrollmentRequest)
			throws TMobileProcessException {
		LOGGER.debug(U2Constants.REQUEST, getAppleCareEnrollmentRequest);
		GetAppleCareEnrollmentResponse getAppleCareEnrollmentResponse = new GetAppleCareEnrollmentResponse();
		try {
			deviceEnterpriseService.getAppleCareEnrollment(getAppleCareEnrollmentRequest,
					getAppleCareEnrollmentResponse);
		} catch (TMobileProcessException dataEx) {
			LOGGER.error(U2Constants.ERROR, dataEx);
			throw new TMobileProcessException(dataEx);
		} catch (Exception ex) {
			LOGGER.error(U2Constants.ERROR, ex);
			throw new TMobileProcessException(ex);
		}
		LOGGER.debug(U2Constants.RESPONSE, getAppleCareEnrollmentResponse);
		return getAppleCareEnrollmentResponse;
	}

	/**
	 * @param manageBlackListRequest
	 * @return
	 * @throws TMobileProcessException
	 */
	@PayloadRoot(namespace = "http://services.tmobile.com/ProductManagement/DeviceEnterprise/V1", localPart = "manageBlackListRequest")
	@ResponsePayload
	public ManageBlackListResponse manageBlackList(@RequestPayload ManageBlackListRequest manageBlackListRequest)
			throws TMobileProcessException {
		LOGGER.debug(U2Constants.REQUEST, manageBlackListRequest);
		ManageBlackListResponse manageBlackListResponse = new ManageBlackListResponse();
		try {
			deviceEnterpriseService.manageBlackList(manageBlackListRequest, manageBlackListResponse);
		} catch (TMobileProcessException dataEx) {
			LOGGER.error(U2Constants.ERROR, dataEx);
			throw new TMobileProcessException(dataEx);
		} catch (Exception ex) {
			LOGGER.error(U2Constants.ERROR, ex);
			throw new TMobileProcessException(ex);
		}
		LOGGER.debug(U2Constants.RESPONSE, manageBlackListResponse);
		return manageBlackListResponse;
	}

	/**
	 * @param checkDeviceCommissionEligiblityRequest
	 * @return
	 * @throws TMobileProcessException
	 */
	@PayloadRoot(namespace = "http://services.tmobile.com/ProductManagement/DeviceEnterprise/V1", localPart = "checkDeviceCommissionEligiblityRequest")
	@ResponsePayload
	public CheckDeviceCommissionEligiblityResponse checkDeviceCommissionEligiblity(
			@RequestPayload CheckDeviceCommissionEligiblityRequest checkDeviceCommissionEligiblityRequest)
			throws TMobileProcessException {
		LOGGER.debug(U2Constants.REQUEST, checkDeviceCommissionEligiblityRequest);
		CheckDeviceCommissionEligiblityResponse checkDeviceCommissionEligiblityResponse = new CheckDeviceCommissionEligiblityResponse();
		try {
			deviceEnterpriseService.checkDeviceCommissionEligiblity(checkDeviceCommissionEligiblityRequest,
					checkDeviceCommissionEligiblityResponse);
		} catch (TMobileProcessException dataEx) {
			LOGGER.error(U2Constants.ERROR, dataEx);
			throw new TMobileProcessException(dataEx);
		} catch (Exception ex) {
			LOGGER.error(U2Constants.ERROR, ex);
			throw new TMobileProcessException(ex);
		}
		LOGGER.debug(U2Constants.RESPONSE, checkDeviceCommissionEligiblityResponse);
		return checkDeviceCommissionEligiblityResponse;
	}

	/**
	 * @param manageAppleCareEnrollmentRequest
	 * @return
	 * @throws TMobileProcessException
	 */
	@PayloadRoot(namespace = "http://services.tmobile.com/ProductManagement/DeviceEnterprise/V1", localPart = "manageAppleCareEnrollmentRequest")
	@ResponsePayload
	public ManageAppleCareEnrollmentResponse manageAppleCareEnrollment(
			@RequestPayload ManageAppleCareEnrollmentRequest manageAppleCareEnrollmentRequest)
			throws TMobileProcessException {
		LOGGER.debug(U2Constants.REQUEST, manageAppleCareEnrollmentRequest);
		ManageAppleCareEnrollmentResponse manageAppleCareEnrollmentResponse = new ManageAppleCareEnrollmentResponse();
		try {
			deviceEnterpriseService.manageAppleCareEnrollment(manageAppleCareEnrollmentRequest,
					manageAppleCareEnrollmentResponse);
		} catch (TMobileProcessException dataEx) {
			LOGGER.error(U2Constants.ERROR, dataEx);
			throw new TMobileProcessException(dataEx);
		} catch (Exception ex) {
			LOGGER.error(U2Constants.ERROR, ex);
			throw new TMobileProcessException(ex);
		}
		LOGGER.debug(U2Constants.RESPONSE, manageAppleCareEnrollmentResponse);
		return manageAppleCareEnrollmentResponse;
	}

	/**
	 * @param updateUnlockEligibilityRequest
	 * @return
	 * @throws TMobileProcessException
	 */
	@PayloadRoot(namespace = "http://services.tmobile.com/ProductManagement/DeviceEnterprise/V1", localPart = "updateUnlockEligibilityRequest")
	@ResponsePayload
	public UpdateUnlockEligibilityResponse updateUnlockEligibility(
			@RequestPayload UpdateUnlockEligibilityRequest updateUnlockEligibilityRequest)
			throws TMobileProcessException {
		LOGGER.debug(U2Constants.REQUEST, updateUnlockEligibilityRequest);
		UpdateUnlockEligibilityResponse updateUnlockEligibilityResponse = new UpdateUnlockEligibilityResponse();
		try {
			deviceEnterpriseService.updateUnlockEligibility(updateUnlockEligibilityRequest,
					updateUnlockEligibilityResponse);
		} catch (TMobileProcessException dataEx) {
			LOGGER.error(U2Constants.ERROR, dataEx);
			throw new TMobileProcessException(dataEx);
		} catch (Exception ex) {
			LOGGER.error(U2Constants.ERROR, ex);
			throw new TMobileProcessException(ex);
		}
		LOGGER.debug(U2Constants.RESPONSE, updateUnlockEligibilityResponse);
		return updateUnlockEligibilityResponse;
	}

	/**
	 * @param checkBlackListRequest
	 * @return
	 * @throws TMobileProcessException
	 */
	@PayloadRoot(namespace = "http://services.tmobile.com/ProductManagement/DeviceEnterprise/V1", localPart = "checkBlackListRequest")
	@ResponsePayload
	public CheckBlackListResponse checkBlackList(@RequestPayload CheckBlackListRequest checkBlackListRequest)
			throws TMobileProcessException {
		LOGGER.debug(U2Constants.REQUEST, checkBlackListRequest);
		CheckBlackListResponse checkBlackListResponse = new CheckBlackListResponse();
		try {
			deviceEnterpriseService.checkBlackList(checkBlackListRequest, checkBlackListResponse);
		} catch (TMobileProcessException dataEx) {
			LOGGER.error(U2Constants.ERROR, dataEx);
			throw new TMobileProcessException(dataEx);
		} catch (Exception ex) {
			LOGGER.error(U2Constants.ERROR, ex);
			throw new TMobileProcessException(ex);
		}
		LOGGER.debug(U2Constants.RESPONSE, checkBlackListResponse);
		return checkBlackListResponse;
	}

	/**
	 * @param postDeviceAssesmentResultsRequest
	 * @return
	 * @throws TMobileProcessException
	 */
	@PayloadRoot(namespace = "http://services.tmobile.com/ProductManagement/DeviceEnterprise/V1", localPart = "postDeviceAssesmentResultsRequest")
	@ResponsePayload
	public PostDeviceAssesmentResultsResponse postDeviceAssesmentResults(
			@RequestPayload PostDeviceAssesmentResultsRequest postDeviceAssesmentResultsRequest)
			throws TMobileProcessException {
		LOGGER.debug(U2Constants.REQUEST, postDeviceAssesmentResultsRequest);
		PostDeviceAssesmentResultsResponse postDeviceAssesmentResultsResponse = new PostDeviceAssesmentResultsResponse();
		try {
			deviceEnterpriseService.postDeviceAssesmentResults(postDeviceAssesmentResultsRequest,
					postDeviceAssesmentResultsResponse);
		} catch (TMobileProcessException dataEx) {
			LOGGER.error(U2Constants.ERROR, dataEx);
			throw new TMobileProcessException(dataEx);
		} catch (Exception ex) {
			LOGGER.error(U2Constants.ERROR, ex);
			throw new TMobileProcessException(ex);
		}
		LOGGER.debug(U2Constants.RESPONSE, postDeviceAssesmentResultsResponse);
		return postDeviceAssesmentResultsResponse;
	}

	/**
	 * @param getDeviceDetailsRequest
	 * @return
	 * @throws TMobileProcessException
	 */
	@PayloadRoot(namespace = "http://services.tmobile.com/ProductManagement/DeviceEnterprise/V1", localPart = "getDeviceDetailsRequest")
	@ResponsePayload
	public GetDeviceDetailsResponse getDeviceDetails(@RequestPayload GetDeviceDetailsRequest getDeviceDetailsRequest)
			throws TMobileProcessException {
		LOGGER.debug(U2Constants.REQUEST, getDeviceDetailsRequest);
		GetDeviceDetailsResponse getDeviceDetailsResponse = new GetDeviceDetailsResponse();
		try {
			deviceEnterpriseService.getDeviceDetails(getDeviceDetailsRequest, getDeviceDetailsResponse);
		} catch (TMobileProcessException dataEx) {
			LOGGER.error(U2Constants.ERROR, dataEx);
			throw new TMobileProcessException(dataEx);
		} catch (Exception ex) {
			LOGGER.error(U2Constants.ERROR, ex);
			throw new TMobileProcessException(ex);
		}
		LOGGER.debug(U2Constants.RESPONSE, getDeviceDetailsResponse);
		return getDeviceDetailsResponse;
	}

	/**
	 * @param registerDeviceForFirstUseRequest
	 * @return
	 * @throws TMobileProcessException
	 */
	@PayloadRoot(namespace = "http://services.tmobile.com/ProductManagement/DeviceEnterprise/V1", localPart = "registerDeviceForFirstUseRequest")
	@ResponsePayload
	public RegisterDeviceForFirstUseResponse registerDeviceForFirstUse(
			@RequestPayload RegisterDeviceForFirstUseRequest registerDeviceForFirstUseRequest)
			throws TMobileProcessException {
		LOGGER.debug(U2Constants.REQUEST, registerDeviceForFirstUseRequest);
		RegisterDeviceForFirstUseResponse registerDeviceForFirstUseResponse = new RegisterDeviceForFirstUseResponse();
		try {
			deviceEnterpriseService.registerDeviceForFirstUse(registerDeviceForFirstUseRequest,
					registerDeviceForFirstUseResponse);
		} catch (TMobileProcessException dataEx) {
			LOGGER.error(U2Constants.ERROR, dataEx);
			throw new TMobileProcessException(dataEx);
		} catch (Exception ex) {
			LOGGER.error(U2Constants.ERROR, ex);
			throw new TMobileProcessException(ex);
		}
		LOGGER.debug(U2Constants.RESPONSE, registerDeviceForFirstUseResponse);
		return registerDeviceForFirstUseResponse;
	}

	/*
	 * Method executeMethod
	 * 
	 * @param
	 * 
	 * @return voidWrapper
	 * 
	 * @throws U2RestException
	 */
	@PayloadRoot(namespace = "http://services.tmobile.com/ProductManagement/DeviceEnterprise/V1", localPart = "checkAppleCareCompatabilityRequest")
	@ResponsePayload
	public CheckAppleCareCompatabilityResponse checkAppleCareCompatability(
			@RequestPayload CheckAppleCareCompatabilityRequest request) throws TMobileProcessException {
		LOGGER.debug(U2Constants.REQUEST, request);
		CheckAppleCareCompatabilityResponse response = new CheckAppleCareCompatabilityResponse();
		try {
			deviceEnterpriseService.checkAppleCareCompatability(request, response);
		} catch (TMobileProcessException dataEx) {
			LOGGER.error(U2Constants.ERROR, dataEx);
			throw new TMobileProcessException(dataEx);
		} catch (Exception ex) {
			LOGGER.error(U2Constants.ERROR, ex);
			throw new TMobileProcessException(ex);
		}
		LOGGER.debug(U2Constants.RESPONSE, response);
		return response;
	}
}
