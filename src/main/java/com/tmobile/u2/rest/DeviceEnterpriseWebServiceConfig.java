package com.tmobile.u2.rest;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class DeviceEnterpriseWebServiceConfig {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Bean(name = "deviceEnterpriseMessageDispatcherServlet")
	public ServletRegistrationBean messageDispatcherServlet(ApplicationContext context) {
		MessageDispatcherServlet messageDispatcherServlet = new MessageDispatcherServlet();
		messageDispatcherServlet.setApplicationContext(context);
		messageDispatcherServlet.setTransformWsdlLocations(true);
		return new ServletRegistrationBean(messageDispatcherServlet,
				"/service/soap/ProductManagement/DeviceEnterprise/V1");
	}

	@Bean(name = "deviceEnterprise")
	public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema lineOfServiceDetailsEnterpriseSchema) {
		DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
		definition.setPortTypeName("DeviceEnterprisePort");
		definition.setTargetNamespace("http://services.tmobile.com/ProductManagement/DeviceEnterprise/V1");
		definition.setLocationUri("/service/soap/ProductManagement/DeviceEnterprise/V1");
		definition.setSchema(lineOfServiceDetailsEnterpriseSchema);
		return definition;
	}

	@Bean
	public XsdSchema lineOfServiceDetailsEnterpriseSchema() {
		return new SimpleXsdSchema(new ClassPathResource("/xsd/deviceRequest.xsd"));
	}
}
