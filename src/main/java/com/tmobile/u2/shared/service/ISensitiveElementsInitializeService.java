package com.tmobile.u2.shared.service;

import com.tmobile.u2.exception.TMobileProcessException;

public interface ISensitiveElementsInitializeService {

	void executeMethod() throws TMobileProcessException;

}
