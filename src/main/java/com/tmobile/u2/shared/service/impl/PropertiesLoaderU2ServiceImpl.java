package com.tmobile.u2.shared.service.impl;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmobile.services.schema.dataobject.createservicecontextendoutput.CreateServiceContextOutput;
import com.tmobile.services.schema.dataobject.createservicecontextstartinput.CreateServiceContextInput;
import com.tmobile.services.schema.dataobject.localvar.Root;
import com.tmobile.services.schema.dataobject.logservicecontextstartinput.LogServiceContextStartInput;
import com.tmobile.services.schema.dataobject.servicecontext.ErrorDetails;
import com.tmobile.services.schema.dataobject.servicecontext.Key;
import com.tmobile.u2.exception.TMobileProcessException;
import com.tmobile.u2.shared.service.ICreateServiceContextService;
import com.tmobile.u2.shared.service.IErrorCodesLoaderOnStartupU2Service;
import com.tmobile.u2.shared.service.ILogServiceContextService;
import com.tmobile.u2.shared.service.IPropertiesLoaderU2Service;
import com.tmobile.u2.util.TibcoToJavaUtil;

/*************************************************************************
 *
 * TMOBILE CONFIDENTIAL
 * _________________________________________________________________________________
 *
 * TMOBILE is a trademark of TMOBILE Company.
 *
 * Copyright � 2020 TMOBILE. All rights reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of
 * TMOBILE and its suppliers, if any. The intellectual and technical concepts
 * contained herein are proprietary to TMOBILE and its suppliers and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by
 * trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from TMOBILE.
 *
 *************************************************************************/
// Author : Generated by ATMA �
// Revision History :
@Service
public class PropertiesLoaderU2ServiceImpl implements IPropertiesLoaderU2Service {

	private static final String ON_STARTUP = "OnStartup";
	
	@Autowired
	private ICreateServiceContextService iCreateServiceContextService;
	
	@Autowired
	private IErrorCodesLoaderOnStartupU2Service iErrorCodesLoaderOnStartupU2Service;
	
	@Autowired
	private ILogServiceContextService iLogServiceContextService;

	public void setLogServiceContextService(ILogServiceContextService iLogServiceContextService) {
		this.iLogServiceContextService = iLogServiceContextService;
	}

	public void setCreateServiceContextService(ICreateServiceContextService iCreateServiceContextService) {
		this.iCreateServiceContextService = iCreateServiceContextService;
	}

	/**
	 * Method logError
	 * 
	 * @param
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void logError(ErrorDetails faultDetail) throws TMobileProcessException {
		LogServiceContextStartInput logservicecontextStartInput = new LogServiceContextStartInput();
		logservicecontextStartInput.getRoot().setResponse(TibcoToJavaUtil.renderXml(faultDetail));
		logservicecontextStartInput.getRoot().getErrorDetails().add(faultDetail);
		iLogServiceContextService.executeMethod(logservicecontextStartInput);
	}

	/**
	 * Method assign
	 * 
	 * @param localVar
	 * @param
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void assign(Root localVar) throws TMobileProcessException {

		localVar.setIapName("PropertiesLoaderU2");
		localVar.setProcessName("PropertiesLoaderU2");

	}

	/**
	 * Method createContext
	 * 
	 * @param localVar
	 * @param
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void createContext(Root localVar) throws TMobileProcessException {
		CreateServiceContextInput createservicecontextStartInput = new CreateServiceContextInput();
		CreateServiceContextOutput createservicecontextEndOutput = new CreateServiceContextOutput();
		createservicecontextStartInput.getRoot().setDomainName(ON_STARTUP);
		createservicecontextStartInput.getRoot().setServiceName(ON_STARTUP);
		createservicecontextStartInput.getRoot().setOperationName(localVar.getIapName());
		createservicecontextStartInput.getRoot().setRequest(LocalDateTime.now().toString());
		createservicecontextStartInput.getRoot().setSenderId(ON_STARTUP);
		createservicecontextStartInput.getRoot().setApplicationId(localVar.getProcessName());

		Key msisdn = new Key();
		msisdn.setName("msisdn");
		msisdn.setValue(String.valueOf(System.currentTimeMillis()));
		createservicecontextStartInput.getRoot().getKey().add(msisdn);

		Key timestamp = new Key();
		timestamp.setName("timestamp");
		timestamp.setValue(String.valueOf(System.currentTimeMillis()));
		createservicecontextStartInput.getRoot().getKey().add(timestamp);

		iCreateServiceContextService.executeMethod(createservicecontextStartInput, createservicecontextEndOutput);
	}

	/**
	 * Method log
	 * 
	 * @param
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void log() throws TMobileProcessException {
		LogServiceContextStartInput logservicecontextStartInput = new LogServiceContextStartInput();
		logservicecontextStartInput.getRoot().setResponse("Success");

		iLogServiceContextService.executeMethod(logservicecontextStartInput);
	}

	/**
	 * Method callProcess
	 * 
	 * @param
	 * @return void
	 * @throws TMobileProcessException
	 */
	private void callProcess() throws TMobileProcessException {
		iErrorCodesLoaderOnStartupU2Service.executeMethod();
	}

	/**
	 * Method executeMethod
	 * 
	 * @param
	 * @return void
	 * @throws TMobileProcessException
	 */
	public void executeMethod() throws TMobileProcessException {
		ErrorDetails faultDetails = new ErrorDetails();
		Root localVar = new Root();
		try {
			assign(localVar);
			createContext(localVar);
			callProcess();
			log();
		} catch (TMobileProcessException e) {
			faultDetails = e.getErrorDetails();

			// 5. LogError
			logError(faultDetails);
		}
	}

}