package com.tmobile.u2.shared.service.impl;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import javax.xml.bind.JAXB;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.tmobile.services.schema.dataobject.servicecontext.ErrorDetails;
import com.tmobile.services.schema.dataobject.servicecontext.SubStatus;
import com.tmobile.services.schema.dataobject.validatetoken.ValidateTokenRequest;
import com.tmobile.services.schema.dataobject.validatetoken.ValidateTokenResponse;
import com.tmobile.u2.exception.TMobileProcessException;
import com.tmobile.u2.shared.service.IJWTValidationService;
import com.tmobile.u2.util.TibcoToJavaUtil;

/*************************************************************************
 *
 * TMOBILE CONFIDENTIAL
 * _________________________________________________________________________________
 *
 * TMOBILE is a trademark of TMOBILE Company.
 *
 * Copyright © 2020 TMOBILE. All rights reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of
 * TMOBILE and its suppliers, if any. The intellectual and technical concepts
 * contained herein are proprietary to TMOBILE and its suppliers and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by
 * trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from TMOBILE.
 *
 *************************************************************************/
// Author : Generated by ATMA ®
// Revision History :
@Service
public class JWTValidationServiceImpl implements IJWTValidationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(JWTValidationServiceImpl.class);

	/**
	 * Method throw
	 * 
	 * @param ValidateTokenRequest validateTokenRequest ,ValidateTokenResponse
	 *                             validateTokenResponse
	 * @return void
	 * @throws App1BusinessException
	 */
	private void throwAct(ValidateTokenRequest validateTokenRequest)
			throws TMobileProcessException {
		String error_JavaInvoke = "";
		String error = "";
		ErrorDetails errorDetails = new ErrorDetails();
		if (error_JavaInvoke.contains("Access token has expired") || error.contains("Access token has expired")) {
			errorDetails.setTargetSystem("Internal");
			errorDetails.setCode("SECURITY-401");
			errorDetails.setMesssage("Invalid_token-Access token has expired .");
			SubStatus subStatus = new SubStatus();
			subStatus.setDescription("UNAUTHORIZED");
			errorDetails.getSubstatus().add(subStatus);
		} else {
			errorDetails.setTargetSystem("Internal");
			errorDetails.setCode("SECURITY-401");

			errorDetails.setMesssage("Invalid_token");
			SubStatus subStatus = new SubStatus();
			subStatus.setDescription("UNAUTHORIZED");
			errorDetails.getSubstatus().add(subStatus);
		}
		errorDetails.setSourceMessage(TibcoToJavaUtil.renderXml(validateTokenRequest));
		throw new TMobileProcessException(errorDetails);
	}

	/**
	 * Method javaInvoke
	 * 
	 * @param ValidateTokenRequest validateTokenRequest ,ValidateTokenResponse
	 *                             validateTokenResponse
	 * @return void
	 * @throws Exception
	 * @throws App1BusinessException
	 */
	private void javaInvoke(ValidateTokenRequest validateTokenRequest)
			throws Exception {
		String authorization = null;
		String xAuthOriginator;
		String xAuthorization;
		String trustedTaapIssuerEnvironment;
		String operationName;
		String[] values = null;

		if (StringUtils.isNotEmpty(validateTokenRequest.getAccessToken())) {
			authorization = validateTokenRequest.getAccessToken();
		} else {
			authorization = "NA";
		}

		if (StringUtils.isNotEmpty(validateTokenRequest.getIdToken())) {
			xAuthOriginator = validateTokenRequest.getIdToken();
		} else {
			xAuthOriginator = "NA";
		}
		if (StringUtils.isNotEmpty(validateTokenRequest.getPopToken())) {
			xAuthorization = validateTokenRequest.getPopToken();
		} else {
			xAuthorization = "NA";
		}

		trustedTaapIssuerEnvironment = validateTokenRequest.getTrustedTappIssuerEnvironment();
		operationName = validateTokenRequest.getOperationName();
		int i = 0;
		values = new String[validateTokenRequest.getDataValidationField().size()];
		for (String dataValidationField : validateTokenRequest.getDataValidationField()) {
			values[i] = dataValidationField;
		}
		com.tmo.security.jwt.validate.ValidateJWTToken validateJWTToken = new com.tmo.security.jwt.validate.ValidateJWTToken();
		validateJWTToken.validateToken(authorization, xAuthOriginator, xAuthorization,
				trustedTaapIssuerEnvironment, operationName, values);
	}

	/**
	 * Method parsejwtHeader
	 * 
	 * @param ValidateTokenResponse
	 *                             validateTokenResponse
	 * @return void
	 * @throws App1BusinessException
	 */
	private void parsejwtHeader(ValidateTokenResponse validateTokenResponse,
			String outputValue) throws TMobileProcessException {
		ValidateTokenResponse validateTokenResponseLocal = JAXB.unmarshal(new ByteArrayInputStream(outputValue.getBytes(StandardCharsets.UTF_8)),
				ValidateTokenResponse.class);
		validateTokenResponse.setApplicationId(validateTokenResponseLocal.getApplicationId());
		validateTokenResponse.setChannelId(validateTokenResponseLocal.getChannelId());
		validateTokenResponse.setSenderId(validateTokenResponseLocal.getSenderId());
		validateTokenResponse.setStatus(validateTokenResponseLocal.getStatus());
	}

	/**
	 * Method executeMethod
	 * 
	 * @param ValidateTokenRequest validateTokenRequest ,ValidateTokenResponse
	 *                             validateTokenResponse
	 * @return void
	 * @throws App1BusinessException
	 */
	@Override
	public void executeMethod(ValidateTokenRequest validateTokenRequest, ValidateTokenResponse validateTokenResponse)
			throws TMobileProcessException {
		String outputValue = null;
		try {
			javaInvoke(validateTokenRequest);
		} catch (Exception ex) {
			LOGGER.error("Error occurred in method: JWTValidation() and terminated due to ", ex);
			throwAct(validateTokenRequest);
		}
		try {
			parsejwtHeader(validateTokenResponse, outputValue);
		} catch (Exception ex) {
			LOGGER.error("Error occurred in method: JWTValidation() and terminated due to ", ex);
		}

	}

}