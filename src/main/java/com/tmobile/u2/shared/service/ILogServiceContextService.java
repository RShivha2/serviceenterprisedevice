package com.tmobile.u2.shared.service;

import com.tmobile.services.schema.dataobject.logservicecontextstartinput.LogServiceContextStartInput;
import com.tmobile.u2.exception.TMobileProcessException;

public interface ILogServiceContextService {

	void executeMethod(LogServiceContextStartInput logServiceContextStartInput) throws TMobileProcessException;

}
