package com.tmobile.u2.shared.service;

import com.tmobile.services.schema.dataobject.islogenabledendinput.IsLogEnabledEndOutput;
import com.tmobile.services.schema.dataobject.islogenabledstartinput.IsLogEnabledStartInput;
import com.tmobile.u2.exception.TMobileProcessException;

public interface IIsLogEnabledService {

	public void executeMethod(IsLogEnabledStartInput input,IsLogEnabledEndOutput output) throws TMobileProcessException;
}
