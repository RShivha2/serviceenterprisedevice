package com.tmobile.u2.shared.service;

import com.tmobile.u2.exception.TMobileProcessException;

import sharedservicecontext.sharedprocesses.utilityprocesses.headervalidation_start_input.HeaderValidationStartInput;

public interface IHeaderValidationService {

	/**
	 * Method executeMethod
	 * 
	 * @param HeaderValidationStartInput HeaderValidationStartInput
	 * @return void
	 * @throws TMobileProcessException
	 */
	void executeMethod(HeaderValidationStartInput headerValidationStartInput) throws TMobileProcessException;

}
