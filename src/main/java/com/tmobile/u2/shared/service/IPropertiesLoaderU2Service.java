package com.tmobile.u2.shared.service;

import com.tmobile.u2.exception.TMobileProcessException;

public interface IPropertiesLoaderU2Service {

	void executeMethod() throws TMobileProcessException;

}
