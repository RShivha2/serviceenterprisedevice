package com.tmobile.u2.shared.service;

import com.tmobile.u2.exception.TMobileProcessException;

public interface ISensitiveElementsXSLTInitializeService {

	void executeMethod() throws TMobileProcessException;

}
