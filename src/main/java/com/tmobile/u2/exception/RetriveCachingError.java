package com.tmobile.u2.exception;

import com.tmobile.services.schema.dataobject.responsestatus.gsperrorhandlerlogger.ErrorShortMessage;

public class RetriveCachingError extends Exception {

	private static final long serialVersionUID = 1L;
	
	ErrorShortMessage errorShortMessage;

	public RetriveCachingError(ErrorShortMessage errorShortMessage) {
		super();
		this.errorShortMessage = errorShortMessage;
	}
}
