package com.tmobile.u2.exception;

public class ConfigInstantiationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ConfigInstantiationException() {
		super();
	}

	public ConfigInstantiationException(Throwable cause) {
		super(cause);
	}

	public ConfigInstantiationException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConfigInstantiationException(String message) {
		super(message);
	}

}
