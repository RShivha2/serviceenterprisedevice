package com.tmobile.u2.exception;

import com.tmobile.services.schema.dataobject.servicecontext.ErrorDetails;

public class RaiseErrorException extends Exception{

	private static final long serialVersionUID = 1L;
	
	ErrorDetails errorDetails = new ErrorDetails();

	public RaiseErrorException(ErrorDetails errorDetails) {
		super();
		this.errorDetails = errorDetails;
	}	
}
