package com.tmobile.u2.exception;

/*************************************************************************
 *
 * DB CONFIDENTIAL
 * _________________________________________________________________________________
 *
 * DB is a trademark of DB Company.
 *
 * Copyright © 2019 DB. All rights reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of DB
 * and its suppliers, if any. The intellectual and technical concepts contained
 * herein are proprietary to DB and its suppliers and may be covered by U.S.
 * and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this
 * material is strictly forbidden unless prior written permission is obtained
 * from DB.
 *
 *************************************************************************/
// Author : Generated by ATMA ®
// Revision History :
public  class TMobileDataAccessException extends Exception {

    private static final long serialVersionUID = 2287096558361439469L;

    private final String errorCode;

    private int SQLErrorCode;

    public  TMobileDataAccessException(Throwable ex) {
        super(ex);
        this.errorCode = "";
    }

    public  TMobileDataAccessException(Throwable ex, int SQLErrorCode) {
        super(ex);
        this.errorCode = "";
        this.SQLErrorCode = SQLErrorCode;
    }

    public  TMobileDataAccessException(String message, Throwable cause) {
        super(message, cause);
        this.errorCode = "";
    }

    public  TMobileDataAccessException(String message) {
        super(message);
        this.errorCode = "";
    }

    public  TMobileDataAccessException(Throwable ex, String errorCode) {
        super(ex);
        this.errorCode = errorCode;
    }

    public  TMobileDataAccessException(String message, Throwable cause, String errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public  TMobileDataAccessException(String message, String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public int getSQLErrorCode() {
        return SQLErrorCode;
    }

}