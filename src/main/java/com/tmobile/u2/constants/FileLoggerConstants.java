package com.tmobile.u2.constants;

public class FileLoggerConstants {

	private FileLoggerConstants() {

	}

	public static final String OPERATION = "||OPERATION=";

	public static final String APPLICATIONID = "||APPLICATIONID=";

	public static final String CHANNELID = "||CHANNELID=";

	public static final String ZONE = "|ZONE=";

	public static final String SENDERID = "||SENDERID=";

	public static final String ENGINENAME = "||ENGINENAME=";

	public static final String USER = "||USER=";

	public static final String KEY = "||KEY=";
	
	public static final String PIPE_KEY = "|KEY=";

	public static final String SERVICE = "|SERVICE=";

	public static final String SERVICEID = "||SERVICE=";

	public static final String EVENTID = "||EVENT_ID=";

	public static final String COMPONENT = "||COMPONENT=";
	public static final String EVENT_TYPE = "||EVENT_TYPE=";
	public static final String ENVIRONMENT = "||ENVIRONMENT=";
	public static final String TYPE = "||TYPE=";
	public static final String REQUEST_TIMESTAMP = "||REQUEST_TIMESTAMP=";
	public static final String RESPONSE_TIMESTAMP = "||RESPONSE_TIMESTAMP=";
	public static final String TARGET_ELAPSEDTIME = "||TARGET_ELAPSEDTIME=";
	public static final String CODE = "||CODE=";
	public static final String ATTACHMENT_FORMAT = "||ATTACHMENT_FORMAT=";
	public static final String ATTACHMENT = "||ATTACHMENT=";
	public static final String MESSAGE = "||MESSAGE=";
	public static final String TARGET_SYSTEM = "||TARGET_SYSTEM=";
	public static final String EVENT_TIME = "EVENT_TIME=";
	
	public static final String PIPE_ENGINENAME = "|ENGINENAME=";
	
	public static final String PIPE_CODE = "|CODE=";
	
	public static final String PIPE_MESSAGE = "|MESSAGE=";
	
	public static final String PIPE_MSGTIMESTAMP = "|MSGTIMESTAMP=";
	
	public static final String PIPE_APPLICATIONID = "|APPLICATIONID=";
	
	public static final String PIPE_TYPE = "|TYPE=";
	
	public static final String PIPE_OPERATION = "|OPERATION=";
	
	public static final String PIPE_USER = "|USER=";
	
	public static final String PIPE_SENDERID = "|SENDERID=";
	
}
