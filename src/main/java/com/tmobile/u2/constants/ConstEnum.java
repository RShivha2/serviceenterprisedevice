package com.tmobile.u2.constants;


public enum ConstEnum {
	
	SERVICE_REQUEST						("service_request"),
	SERVICE_RESPONSE					("service_response"),
	TARGET_REQUEST						("target_request"),
	TARGET_RESPONSE						("target_response"),
	CONTEXT_AUDIT						("context_audit"),
	PROXY_CONTEXT_AUDIT					("proxy_context_audit"),
	
	APPLICATIONID						("applicationId"),
	SENDERID							("senderId"),
	CHANNELID							("channelId"),
	USERID								("userId"),
	JOBID								("jobId"),
	APP_NAME							("appname"),
	RESOURCE_URI						("resourceUri"),
	HOST_ENDPOINT						("hostEndpoint"),
	EVENT_TIME							("event_time"),
	DOMAIN								("domain"),
	EVENT_ID							("event_id"),
	OPERATION_NAME						("operation_name"),
	ENV_NAME							("env_name"),
	ENGINE_NAME							("engine_name"),
	LOG_LEVEL							("log_level"),
	START_TIME							("start_time"),
	COMPONENT							("component"),
	EVENT_TYPE							("event_type"),
	ATTACHMENT_FORMAT					("attachment_format"),
	ATTACHMENT							("attachment"),
	JSON								("JSON"),
	XML									("XML"),
	END_TIME							("end_time"),
	ELAPSED_TIME_MS						("elapsed_time_ms"),
	RESOURCE_NAME						("resource_name"),
	TARGET_SYSTEM						("target_system"),
	SERVICE_NAME						("service_name"),
	ORDER_ID							("orderId"),
	STATUS_CODE							("status_code"),
	TRANSACTION_STATUS					("transaction_status"),
	QUERY_PARAMS						("queryparams"),
	TARGET_RESOURCE_URI					("target_resourceUri"),
	TARGET_HOST_ENDPOINT				("target_host_endpoint"),
	TARGET_QUERY_PARAMS					("target_query_params"),
	TARGET_OPERATION_NAME				("target_operation_name"),
	ERROR_CODE							("error_code"),
	ERROR_DESC							("error_desc"),
	ERROR_MSG							("error_message"),
	ERROR_STACKTRACE					("error_stack_trace"),
	ERROR_PROCESSTACK					("error_process_stack"),
	ERROR_TARGETRESPONSE				("error_target_response"),
	RETRY_COUNT							("retrycount"),
	TRANSACTION_TYPE					("transactionType"),
	RETURN_ORDER_ID						("returnOrderId"),
	PROCESS_NAME						("returnOrderId"),
	
	RABBITMQ_ACKNOWLEDGE_MODE_AUTO		("AUTO"),
	RABBITMQ_ACKNOWLEDGE_MODE_MANUAL	("MANUAL"),
	RABBITMQ_ACKNOWLEDGE_MODE_NONE		("NONE"),
	
	EVENTTYPE							("eventType"),
	ACCOUNT_TYPE						("accountType"),
	ACCOUNT_SUBTYPE						("accountSubType"),
	BAN									("ban"),
	CHANNEL								("channel"),
	SENDER								("sender"),
	APPLICATION							("application"),
	BAN_STATUS							("banStatus"),
	LINE_TYPE							("lineType"),
	IDENTIFICATION_ID					("identificationId")
	;
	
	private String value;
	
	ConstEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
