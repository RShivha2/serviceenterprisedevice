/**
 * 
 */
package com.tmobile.u2.util;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.transport.http.HttpUrlConnectionMessageSender;

/**
 * @author shivharer
 *
 */
public class WebServiceMessageSenderWithAuth extends HttpUrlConnectionMessageSender {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(WebServiceMessageSenderWithAuth.class);

	private String userName;
	
	private String password;
	
	public WebServiceMessageSenderWithAuth(String userName, String password) {
		this.userName = userName;
		this.password = password;
	}
	
	 @Override
     protected void prepareConnection(HttpURLConnection connection) throws IOException { 
        String userpassword = userName+":"+password;
        byte[] encodedAuthorization = Base64.getEncoder().encode(userpassword.getBytes());
        LOGGER.debug("Basic {}", new String(encodedAuthorization));
        connection.setRequestProperty("Authorization", "Basic " +  new String(encodedAuthorization)); 
        super.prepareConnection(connection); 
    }
}
