package com.tmobile.u2.util;

import java.io.IOException;

import javax.xml.soap.MimeHeaders;
import javax.xml.transform.TransformerException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.saaj.SaajSoapMessage;
import org.springframework.ws.transport.TransportConstants;

public class UpdateOrderSOAPConnector extends WebServiceGatewaySupport {
	
	private static final Logger LOG = LoggerFactory.getLogger(UpdateOrderSOAPConnector.class);

	public Object callWebService(String url, Object request) {

		WebServiceTemplate webServiceTemplate = getWebServiceTemplate();
		LOG.debug("url :: {}", url);
		LOG.debug("request :{}",  TibcoToJavaUtil.renderXml(request));
		if(!url.startsWith("http"))
			url = "https://" + url;
		LOG.debug("url :: {}", url);
		Object object = webServiceTemplate.marshalSendAndReceive(url, request, new WebServiceMessageCallback() {
			public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
				SaajSoapMessage soapMessage = (SaajSoapMessage) message;
				MimeHeaders headers = soapMessage.getSaajMessage().getMimeHeaders();
				headers.addHeader(TransportConstants.HEADER_CONTENT_TYPE, "text/xml;charset=utf-8");
				headers.addHeader(TransportConstants.HEADER_SOAP_ACTION, "\"document/http://xmlns.oracle.com/apps/mdm/customer:updateOrder\"");
			}
		});
		return object;
	}
}