package com.tmobile.u2.util;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.tmobile.u2.constants.U2Constants;

@Configuration
public class JpaConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(JpaConfig.class);
	
	@SuppressWarnings("rawtypes")
	@Bean(name = "DMWDataSource")
	public DataSource dmwDataSource() {
		DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
		try {
			dataSourceBuilder.driverClassName(TibcoToJavaUtil.getProperty("TMO_U2_driverClass"));
			dataSourceBuilder.url(TibcoToJavaUtil.getProperty("TMO.U2.Env.Endpoints.DMW.DBConnections.DMW_DB_URL"));
			dataSourceBuilder.username(TibcoToJavaUtil.getProperty("TMO.U2.Env.Endpoints.DMW.DBConnections.DMW_DB_Username"));
			dataSourceBuilder.password(TibcoToJavaUtil.getProperty("TMO.U2.Env.Endpoints.DMW.DBConnections.DMW_DB_Password"));
			dataSourceBuilder.build();
		} catch (Exception ex) {
			LOGGER.error(U2Constants.ERROR, ex);
		}
		return dataSourceBuilder.build();
	}

	@SuppressWarnings("rawtypes")
	@Bean(name = "REEFDataSource")
	@Primary
	public DataSource reefDataSource() {
		DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
		try {
			dataSourceBuilder.driverClassName(TibcoToJavaUtil.getProperty("TMO_U2_driverClass"));
			dataSourceBuilder.url(TibcoToJavaUtil.getProperty("TMO.U2.Env.Endpoints.REEF.JDBC.Database_URL"));
			dataSourceBuilder.username(TibcoToJavaUtil.getProperty("TMO.U2.Env.Endpoints.REEF.JDBC.UserName"));
			dataSourceBuilder.password(TibcoToJavaUtil.getProperty("TMO.U2.Env.Endpoints.REEF.JDBC.Password"));
			dataSourceBuilder.build();
		} catch (Exception ex) {
			LOGGER.error(U2Constants.ERROR, ex);
		}
		return dataSourceBuilder.build();
	}
}
