package com.tmobile.u2.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.lang.NonNull;
import org.springframework.util.ResourceUtils;

import com.tmobile.u2.exception.TMobileProcessException;

public class SpringConfigServer {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringConfigServer.class);

	private SpringConfigServer() {
	}

	private static long endTime;
	private static long startTime;
	private static String xmlcontent;

	private static final String CREDENTIALS = "credentials";

	private static final String AUTHORIZATION = "Authorization";

	public static String getXml(String xmlFileName) throws TMobileProcessException {
		startTime = System.currentTimeMillis();
		String disableSsl = System.getenv("DISABLE_SSL_VERIFICATION");

		if (Boolean.parseBoolean(disableSsl)) {
			disableSSLVerification();

		}

		xmlcontent = getXmlFromConfigServer(xmlFileName);

		endTime = System.currentTimeMillis();
		LOGGER.info("Time Taken to process request : {}", (startTime - endTime));
		return xmlcontent;
	}

	@SuppressWarnings("unchecked")
	public static String getXmlFromConfigServer(String xmlFileName) throws TMobileProcessException {
		String vcap_services = System.getenv("VCAP_SERVICES");
		if (StringUtils.isEmpty(vcap_services)) {
			LOGGER.info("VCAP_SERVICES environment variable is missing .Reading from local files. ");
			return readLocalXMLFile(xmlFileName);
		}
		try {
			LOGGER.info("Reading file from Spring config server . ");
			JSONObject vcapObject = new JSONObject(vcap_services);
			Iterator<String> serviceItr = vcapObject.keys();
			while (serviceItr.hasNext()) {
				String servicekey = serviceItr.next();
				JSONArray services = vcapObject.getJSONArray(servicekey);
				for (int j = 0; j < services.length(); j++) {
					JSONObject serviceConfig = services.getJSONObject(j);
					if (isSpringCloudConfigurationService(serviceConfig)) {
						// This is Spring Cloud config Service
						return getXmlFromSpringConfigServer(xmlFileName, serviceConfig);
					}
				}
			}
		} catch (JSONException | IOException t) {
			LOGGER.error("Error in getXmlFromConfigServer due to {}", ExceptionUtils.getStackTrace(t));
			throw new TMobileProcessException("Unable to retrieve xml from Spring-cloud Config service using URL [{}].",
					t);
		}
		return null;
	}

	/**
	 * @param xmlFileName
	 * @param serviceConfig
	 * @return
	 * @throws JSONException
	 * @throws TMobileProcessException
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 */
	private static String getXmlFromSpringConfigServer(String xmlFileName, JSONObject serviceConfig)
			throws JSONException, TMobileProcessException, MalformedURLException, IOException,
			UnsupportedEncodingException {
		String propUrl = serviceConfig.getJSONObject(CREDENTIALS).has("url")
				? serviceConfig.getJSONObject(CREDENTIALS).getString("url")
				: (serviceConfig.getJSONObject(CREDENTIALS).has("uri")
						? serviceConfig.getJSONObject(CREDENTIALS).getString("uri")
						: null);
		if (StringUtils.isEmpty(propUrl)) {
			throw new TMobileProcessException("Spring cloud config service must specify uri.");
		}

		String url = constructSpringCloudConfigURI(propUrl, xmlFileName);

		LOGGER.info("Loading xml from [{}]", url);

		URL springURL = new URL(url);

		URLConnection connection = springURL.openConnection();
		connection.setReadTimeout(30000);
		connection.setConnectTimeout(30000);

		JSONObject credentialsObject = serviceConfig.getJSONObject(CREDENTIALS);

		if (credentialsObject.has("access_token_uri")) {
			// OAuth 2.0
			connection.setRequestProperty(AUTHORIZATION, getAuthorization(credentialsObject));
		} else if (url.contains("@")) {
			// Basic Auth
			connection.setRequestProperty(AUTHORIZATION, getBasicAuthentication(url));
		}
		StringBuilder result = new StringBuilder();
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
		try {
			String line;
			while ((line = in.readLine()) != null) {
				result.append(line);
			}
		} finally {
			in.close();
		}
		return result.toString();
	}

	public static String getAuthorization(JSONObject credentials) throws JSONException, IOException {
		// For OAUTH 2.0
		String client_id = credentials.getString("client_id");
		String client_secret = credentials.getString("client_secret");
		String accessTokenUri = credentials.getString("access_token_uri");

		String authString = client_id + ":" + client_secret;
		String authEncodedString = DatatypeConverter.printBase64Binary(authString.getBytes());
		String body = "grant_type=client_credentials";

		URL accessTokenUrl = new URL(accessTokenUri);
		URLConnection urlConnection = accessTokenUrl.openConnection();
		urlConnection.setRequestProperty(AUTHORIZATION, "Basic " + authEncodedString);
		urlConnection.setDoOutput(true);
		OutputStream output = urlConnection.getOutputStream();
		output.write(body.getBytes());
		output.close();

		StringBuilder result = new StringBuilder();
		BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
		try {
			String line;
			while ((line = in.readLine()) != null) {
				result.append(line);
			}
		} finally {
			in.close();
		}

		JSONObject accessTokenConfig = new JSONObject(result.toString());
		String accessToken = accessTokenConfig.getString("access_token");
		String tokenType = accessTokenConfig.getString("token_type");
		return tokenType + " " + accessToken;
	}

	public static String getBasicAuthentication(String serverUri) throws TMobileProcessException {
		return "Basic " + DatatypeConverter.printBase64Binary(serverUri.split("@")[0].split("://")[1].getBytes());
	}

	public static boolean isSpringCloudConfigurationService(JSONObject serviceConfig) {
		boolean result = false;
		try {
			if (serviceConfig.has("tags")) {
				// Managed Service
				List<String> list = new ArrayList<String>();
				JSONArray tagsArray = serviceConfig.getJSONArray("tags");
				if (tagsArray != null && tagsArray.length() > 0) {
					for (int k = 0; k < tagsArray.length(); k++) {
						String tagName = tagsArray.getString(k);
						list.add(tagName);
					}
				}
				result = list.contains("spring-cloud") && list.contains("configuration");
			}

			if (Boolean.FALSE == result) {
				// CUPS
				result = serviceConfig.getString("name").toLowerCase().contains("spring-cloud-config");
			}
		} catch (JSONException e) {
			LOGGER.error("Error in isSpringCloudConfigurationService due to ", e);
		}
		return result;
	}

	public static String constructSpringCloudConfigURI(@NonNull String propUrl, String xmlFileName)
			throws JSONException {
		StringBuilder sb = new StringBuilder();
		sb.append(propUrl);
		if (!propUrl.endsWith("/")) {
			sb.append("/");
		}
		String vcap_application = System.getenv("VCAP_APPLICATION");
		JSONObject vcapObject = new JSONObject(vcap_application);
		String profileName = System.getenv("APP_CONFIG_PROFILE");
		if (profileName == null) {
			profileName = "default";
		}
		String appName = vcapObject.getString("application_name");
		sb.append(appName).append("/").append(profileName).append("/").append("master").append("/").append(xmlFileName);
		return sb.toString();
	}

	public static void disableSSLVerification() {

		try {
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				@Override
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					LOGGER.info("getAcceptedIssuers");
					return null;
				}

				@Override
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
					LOGGER.info("checkClientTrusted");
				}

				@Override
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
					LOGGER.info("checkServerTrusted");
				}
			} };

			// Install the all-trusting trust manager SSL - TLSv1.2
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = (String hostname, SSLSession session) -> {
				return StringUtils.isEmpty(hostname) || !StringUtils.isEmpty(hostname) ? true : false;
			};

			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

		} catch (NoSuchAlgorithmException | KeyManagementException e) {
			LOGGER.error("Error in disable_ssl_verification due to {} ", ExceptionUtils.getStackTrace(e));
		}
	}

	public static String readLocalXMLFile(String fileName) {
		StringBuilder result = new StringBuilder();
		try {
			InputStream resource = new ClassPathResource("xml/" + fileName).getInputStream();
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource))) {
				String line;
				while ((line = reader.readLine()) != null) {
					result.append(line + System.lineSeparator());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error in readLocalXMLFile due to {} " , ExceptionUtils.getStackTrace(e));
		}
		return result.toString();
	}

	public static String readLocalXMLFile1(String fileName) {
		try (BufferedReader bufReader = new BufferedReader(
				new FileReader(ResourceUtils.getFile("classpath:xml/" + fileName)))) {
			StringBuilder result = new StringBuilder();
			String line;
			while ((line = bufReader.readLine()) != null) {
				result.append(line);
			}
			return result.toString();
		} catch (IOException e) {
			// LOGGER.error("Error in readLocalXMLFile due to {} " ,
			// ExceptionUtils.getStackTrace(e));
			// throw new TMobileProcessException(e);
		}
		return "";
	}
}
