package com.tmobile.u2.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tmobile.u2.exception.ConfigInstantiationException;
import com.tmobile.u2.exception.KeyNotFoundException;


/**
 * @author umamaheswararao.k
 */

public class GlobalProperties {

	private static Logger log = LoggerFactory.getLogger(GlobalProperties.class);

	private Properties properties;
	InputStream input = null;
	private static GlobalProperties manager = null;
	
	private static final String EXCEPTION_MESSAGE="Exception in GlobalProperties: constructor";
	
	private static final String PROBLEM_MESSAGE="Problem in loading properties to GlobalProperties";
	
	private static final String EXCEPTION_MESSAGE_GET="Exception in GlobalProperties: get:{}";

	private GlobalProperties() throws ConfigInstantiationException {
		properties = new Properties();
		try {
			input = new FileLoaderUtil().getInputStream("application.properties");
			properties.load(input);
		} catch (IOException e) {
			log.error(EXCEPTION_MESSAGE, e);
			throw new ConfigInstantiationException(PROBLEM_MESSAGE, e);
		} catch (Exception e) {
			log.error(EXCEPTION_MESSAGE, e);
			throw new ConfigInstantiationException(PROBLEM_MESSAGE, e);
		}
	}

	public String get(String key) {
		log.trace("Retrieve GlobalProperties with the Key:{}" , key);
		String value = null;
		try {
			if (key != null) {
				if (properties != null) {
					if (properties.containsKey(key)) {
						value = properties.getProperty(key);
					} else {
						log.trace(EXCEPTION_MESSAGE_GET , key);
					}
				} else {
					throw new ConfigInstantiationException("Configuration not initialized");
				}
			}
		} catch (ConfigInstantiationException e) {
			log.error(EXCEPTION_MESSAGE_GET + key, e);

		}
		log.trace("For the key: " + key + "supplied returned the value " + value + " from config manager");
		return value;
	}

	public void put(String key, String value) throws KeyNotFoundException {
		log.trace("Going to add GlobalProperties with the key:" + key + " and value : " + value);
		try {
			if (key != null && value != null) {
				properties.setProperty(key, value);
			} else {
				throw new KeyNotFoundException("Key/Value is not present in properties for the Key : " + key + " with the value :" + value);
			}
		} catch (KeyNotFoundException e) {
			log.error(EXCEPTION_MESSAGE_GET, e);
			throw e;
		}
		log.trace("GlobalProperties added with the key:" + key + " and value : " + value);

	}

	public void load(String prop) throws ConfigInstantiationException {
		try {
			input = new FileLoaderUtil().getInputStream(prop);
			properties.load(input);
		} catch (IOException e) {
			log.error(EXCEPTION_MESSAGE, e);
			throw new ConfigInstantiationException(PROBLEM_MESSAGE, e);
		} catch (Exception e) {
			log.error(EXCEPTION_MESSAGE, e);
			throw new ConfigInstantiationException(PROBLEM_MESSAGE, e);
		}
	}

	public static GlobalProperties getInstance() {
		if (manager == null) {
			try {
				manager = new GlobalProperties();
			} catch (ConfigInstantiationException e) {
				log.error("Exception in instantiating GlobalProperties", e);
			}
		}
		log.trace("GlobalProperties Instantiated and Ready to use");
		return manager;
	}

}
