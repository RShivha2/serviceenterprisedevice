package com.tmobile.u2.util;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.transport.http.HttpsUrlConnectionMessageSender;

@Configuration
public class ClientAppConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(ClientAppConfig.class);

	@Bean
	public AuthenticateSOAPConnector authenticateSOAPConnector() throws NoSuchAlgorithmException, CertificateException,
			IOException, KeyStoreException, UnrecoverableKeyException {
		AuthenticateSOAPConnector uc = new AuthenticateSOAPConnector();
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setPackagesToScan("com.apple.gsxws.elements.global");
		marshaller.setContextPaths("com.apple.gsxws.elements.global");
		uc.setMarshaller(marshaller);
		uc.setUnmarshaller(marshaller);
		return uc;
	}

	@Bean
	public BlockServiceSOAPConnector blockServiceSOAPConnector() throws NoSuchAlgorithmException, CertificateException,
			IOException, KeyStoreException, UnrecoverableKeyException {
		BlockServiceSOAPConnector uc = new BlockServiceSOAPConnector();
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setPackagesToScan("com.sap.document.sap.rfc.functions");
		marshaller.setContextPaths("com.sap.document.sap.rfc.functions");
		uc.setMarshaller(marshaller);
		uc.setUnmarshaller(marshaller);
		return uc;
	}

	@Bean
	public BlockingStatusUpdateSOAPConnector blockingStatusUpdateSOAPConnector() throws NoSuchAlgorithmException,
			CertificateException, IOException, KeyStoreException, UnrecoverableKeyException {
		BlockingStatusUpdateSOAPConnector uc = new BlockingStatusUpdateSOAPConnector();
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setPackagesToScan("com.t_mobile.oer.services.logistics");
		marshaller.setContextPaths("com.t_mobile.oer.services.logistics");
		uc.setMarshaller(marshaller);
		uc.setUnmarshaller(marshaller);
		uc.setMessageSender(new WebServiceMessageSenderWithAuth(
				TibcoToJavaUtil.getProperty("TMO.U2.Env.Endpoints.SAPPI.SOAP.Username_UpdateStatus"),
				TibcoToJavaUtil.getProperty("TMO.U2.Env.Endpoints.SAPPI.SOAP.Password_UpdateStatus")));
		return uc;
	}

	@Bean
	public EUSUnlockServiceSOAPConnector eusUnlockServiceSOAPConnector() throws NoSuchAlgorithmException,
			CertificateException, IOException, KeyStoreException, UnrecoverableKeyException {
		EUSUnlockServiceSOAPConnector uc = new EUSUnlockServiceSOAPConnector();
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setPackagesToScan("com.tmobile.eus.unlock");
		marshaller.setContextPaths("com.tmobile.eus.unlock");
		uc.setMarshaller(marshaller);
		uc.setUnmarshaller(marshaller);
		return uc;
	}

	@Bean
	public QueryAllSOAPConnector queryAllSOAPConnector() throws NoSuchAlgorithmException,
			CertificateException, IOException, KeyStoreException, UnrecoverableKeyException {
		QueryAllSOAPConnector uc = new QueryAllSOAPConnector();
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setPackagesToScan("ism_transaction");
		marshaller.setContextPaths("ism_transaction");
		uc.setMarshaller(marshaller);
		uc.setUnmarshaller(marshaller);
		return uc;
	}

	@Bean
	public ServiceEnterpriseSOAPConnector subscriberRestServiceSOAPConnector() throws NoSuchAlgorithmException,
			CertificateException, IOException, KeyStoreException, UnrecoverableKeyException {
		ServiceEnterpriseSOAPConnector uc = new ServiceEnterpriseSOAPConnector();
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setPackagesToScan("com.t_mobile.erp.services.logistics");
		marshaller.setContextPaths("com.t_mobile.erp.services.logistics");
		uc.setMarshaller(marshaller);
		uc.setUnmarshaller(marshaller);
		uc.setMessageSender(new WebServiceMessageSenderWithAuth(
				TibcoToJavaUtil.getProperty("TMO.Env.SAPPI.SAPPI_Username_BlockingStatusCheck"),
				TibcoToJavaUtil.getProperty("TMO.Env.SAPPI.SAPPI_Password_BlockingStatusCheck")));
		return uc;
	}

	@Bean
	public UpdateOrderSOAPConnector updateOrderSOAPConnector() throws NoSuchAlgorithmException, CertificateException,
			IOException, KeyStoreException, UnrecoverableKeyException {
		UpdateOrderSOAPConnector uc = new UpdateOrderSOAPConnector();
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setPackagesToScan("com.oracle.xmlns.apps.mdm.customer");
		marshaller.setContextPaths("com.oracle.xmlns.apps.mdm.customer");
		uc.setMarshaller(marshaller);
		uc.setUnmarshaller(marshaller);

		try {
			try (InputStream fis = new ClassPathResource("TrustStore.jks").getInputStream()) {
				KeyStore ks = KeyStore.getInstance("JKS");
				ks.load(fis, "changeit".toCharArray());
				KeyManagerFactory keyManagerFactory = KeyManagerFactory
						.getInstance(KeyManagerFactory.getDefaultAlgorithm());
				keyManagerFactory.init(ks, "changeit".toCharArray());
				TrustManagerFactory trustManagerFactory = TrustManagerFactory
						.getInstance(TrustManagerFactory.getDefaultAlgorithm());
				trustManagerFactory.init(ks);
				HttpsUrlConnectionMessageSender messageSender = new HttpsUrlConnectionMessageSender();
				messageSender.setKeyManagers(keyManagerFactory.getKeyManagers());

				messageSender.setTrustManagers(trustManagerFactory.getTrustManagers());
				uc.setMessageSender(messageSender);

			} catch (Exception e) {
				LOGGER.error("Error in subscriberRestServiceSOAPConnector due to ",e);
			}
		} catch (Exception e) {
			LOGGER.error("Error in subscriberRestServiceSOAPConnector due to ",e);
		}
		return uc;
	}
}