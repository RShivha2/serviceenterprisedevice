package com.tmobile.u2.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

import com.tmobile.services.schema.dataobject.commoncache.CacheElement;
import com.tmobile.u2.cache.CacheService;

import net.sf.ehcache.Element;

@Component
public class CacheUtil {

	@Qualifier("ehCacheCacheManager")
	@Autowired(required = false)
	private CacheManager ehCacheCacheManager;

	@Autowired
	CacheService cacheService;

	public void saveCacheElementbyType(CacheElement cacheElement) {
		if (cacheElement.getType().equals("EC")) {
			cacheService.saveErrorCodesCache(cacheElement);
		} else if (cacheElement.getType().equals("ED")) {
			cacheService.saveErrorCodesDescription(cacheElement);
		}
	}

	public List<Element> getAllCacheElements(String cacheName) {
		Cache cc = ehCacheCacheManager.getCache(cacheName);

		List<Element> elements = new ArrayList<Element>();

		net.sf.ehcache.Cache pv = (net.sf.ehcache.Cache) cc.getNativeCache();
		for (Object key : pv.getKeys()) {
			Element element = pv.get(key);
			elements.add(element);
		}

		return elements;

	}

	public CacheElement getCacheElement(String cacheName, String key) {

		Cache cc = ehCacheCacheManager.getCache(cacheName);
		net.sf.ehcache.Cache pv = (net.sf.ehcache.Cache) cc.getNativeCache();
		if (pv.get(key) != null) {
			return (CacheElement) pv.get(key).getObjectValue();
		}

		// if No element found in Cache then return default values
		CacheElement cacheElement = new CacheElement();
		cacheElement.setKey(key);
		cacheElement.setType(cacheName);
		// cacheElement.setValue("Key is not found in Cache..");
		return cacheElement;

	}

}
