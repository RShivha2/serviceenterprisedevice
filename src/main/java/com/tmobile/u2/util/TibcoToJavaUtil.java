package com.tmobile.u2.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import com.tmobile.services.schema.dataobject.guid.Guid;

public class TibcoToJavaUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(TibcoToJavaUtil.class);
	
	public static final DateFormat DATEFORMAT1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

	public static final DateFormat DATEFORMAT2 = new SimpleDateFormat("MMddyyyy:HHmmss");

	public static final DateFormat DATEFORMAT3 = new SimpleDateFormat("yyyy-MM-dd");

	static ArrayList<String> names = new ArrayList<String>(Arrays.asList("alex", "brian", "charles"));

	public static long getTimeStamp() {
		return System.currentTimeMillis();
	}

	public static String renderXml(Object object) {
		StringWriter sw = new StringWriter();
		JAXB.marshal(object, sw);
		String xmlResponseStr = sw.toString();
		return xmlResponseStr;
		// return "";
	}

	@SuppressWarnings("rawtypes")
	public static Object parseXml(String value, Class outputClass) throws IOException, SOAPException, JAXBException {

		SOAPMessage message = MessageFactory.newInstance().createMessage(null,
				new ClassPathResource(value).getInputStream());
		JAXBContext jaxbContext = JAXBContext.newInstance(outputClass);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		return jaxbUnmarshaller.unmarshal(message.getSOAPBody().extractContentAsDocument());

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Object parseXml(InputStream fileInputStream, Class outputClass) throws IOException, JAXBException {
		return JAXB.unmarshal(fileInputStream, outputClass);
	}

	public static String addToDateTime(String dateTime, int years, int months, int days, int hours, int minutes,
			int seconds) {
		LocalDateTime localDateTime;
		if (dateTime != null) {
			localDateTime = LocalDateTime.parse("1980-08-05T08:24:55");
		} else {
			localDateTime = LocalDateTime.now();
		}
		// Add years, months, weeks, days, hours, minutes, seconds to LocalDateTime
		LocalDateTime newDateTime = localDateTime.plusYears(years).plusMonths(months).plusDays(days).plusHours(hours)
				.plusMinutes(minutes).plusSeconds(seconds);

		return newDateTime.toString();

	}

	public static Guid createUUID() {
		Guid guid = new Guid();
		guid.getRoot().setGUID(UUID.randomUUID().toString());
		LOGGER.info("New UUID Generated ::  {} ", guid.getRoot().getGUID());
		return guid;

	}

	public static String substringAfterLast(String value, String delimiter) {
		return value.substring(value.lastIndexOf(delimiter) + 1);

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static String concateSequence(List values) {
		return String.join("", values);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static String concateSequenceFormat(List values, String delimeter) {
		return String.join(delimeter, values);

	}

	public static String left(String value, int noOfCharacters) {
		if (noOfCharacters > value.length()) {
			return value;
		} else if (noOfCharacters < 0) {
			return "";
		} else {
			return value.substring(0, noOfCharacters);
		}
	}

	public static String base64ToString(String value) {

		byte[] decodedBytes = Base64.getDecoder().decode(value);
		return new String(decodedBytes);
	}

	public static String stringToBase64(String value) {

		byte[] decodedBytes = Base64.getEncoder().encode(value.getBytes());
		return new String(decodedBytes);
	}

	public static String getProperty(String key) {
		return GlobalProperties.getInstance().get(key);
	}

	public static String base64ToString(byte[] binaryContent) {
		byte[] decodedBytes = Base64.getDecoder().decode(binaryContent);
		return new String(decodedBytes);
	}

	public static Calendar convertStringToCalendar(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault());
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(sdf.parse(date));
		} catch (ParseException e) {
		}
		return cal;
	}

	public static XMLGregorianCalendar convertDateStringToXMLGregorianCalendar(String dateString, String datePattern) {
		try {
			GregorianCalendar gc = new GregorianCalendar();
			gc.setTime(new SimpleDateFormat(datePattern).parse(dateString));
			XMLGregorianCalendar xmlCalendarDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
			xmlCalendarDate.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
			return xmlCalendarDate;
		} catch (ParseException e) {
		} catch (DatatypeConfigurationException e) {
		}
		return null;
	}

	public static String getFormattedStringFromDate(Date date, String datePattern) {
		return new SimpleDateFormat(datePattern).format(date);
	}

	public static String getFormattedStringFromXMLGregorianCalendar(XMLGregorianCalendar date, String datePattern) {
		DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(datePattern);
		ZonedDateTime zdt = date.toGregorianCalendar().toZonedDateTime();
		return DATE_TIME_FORMATTER.format(zdt);
	}

	public static void padFront(String str, int length, char padChar) {
		String value = "";
		if (str.length() < length) {
			int len = length - str.length();
			for (int i = 0; i < len; i++) {
				value = value + padChar;
			}
		}
		str = value + str;
	}

	public static void pad(String str, int length, char padChar) {
		String value = "";
		if (str.length() < length) {
			int len = length - str.length();
			for (int i = 0; i < len; i++) {
				value = value + padChar;
			}
		}
		str = str + value;
	}

	public static XMLGregorianCalendar formatDateTime(String date, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
		GregorianCalendar gc = new GregorianCalendar();
		XMLGregorianCalendar xmlCalendarDate = null;
		try {
			gc.setTime(sdf.parse(date));
			xmlCalendarDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
			return xmlCalendarDate;
		} catch (ParseException | DatatypeConfigurationException e) {
			LOGGER.error("Error in convertStringToCalendar due to ", e);
		}
		return xmlCalendarDate;
	}

	public static XMLGregorianCalendar parseDateTime(String format, String date) {
		SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
		GregorianCalendar gc = new GregorianCalendar();
		XMLGregorianCalendar xmlCalendarDate = null;
		try {
			gc.setTime(sdf.parse(date));
			xmlCalendarDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
			return xmlCalendarDate;
		} catch (ParseException | DatatypeConfigurationException e) {
			LOGGER.error("Error in convertStringToCalendar due to ", e);
		}
		return xmlCalendarDate;
	}

	public static Date parseDateString(DateFormat format, String dateString) throws Exception {
		Date date = format.parse(dateString);
		return date;
	}

}
