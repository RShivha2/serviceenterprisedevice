package com.tmobile.u2.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.tmobile.u2.constants.U2Constants;
import com.tmobile.u2.exception.TMobileProcessException;
import com.tmobile.u2.service.IActivatorService;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationStartup.class);

	/**
	 * This Class is used to run the activator service to initialize the cache and
	 * properties when the application is ready to service requests.
	 */
	@Autowired
	IActivatorService iActivatorService;

	@Override
	public void onApplicationEvent(final ApplicationReadyEvent event) {
		LOGGER.info("------ApplicationStartup : Activator service is started---------- ");
		try {
			iActivatorService.executeMethod();
		} catch (TMobileProcessException e) {
			LOGGER.error(U2Constants.ERROR, e);
		} finally {
			LOGGER.info("------ Activator service is successfully initialised all data and cache---------- ");
		}
	}

} 