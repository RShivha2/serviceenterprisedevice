package com.tmobile.u2.util;

import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.transform.TransformerException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.saaj.SaajSoapMessage;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;
import org.springframework.ws.transport.TransportConstants;

import com.tmobile.u2.constants.U2Constants;

import ism_transaction.AuthenticationInfo;

public class QueryAllSOAPConnector extends WebServiceGatewaySupport {
	
	private static final Logger LOG = LoggerFactory.getLogger(QueryAllSOAPConnector.class);

	public Object callWebService(String url, Object request, String soapAction, AuthenticationInfo authInfo) {

		MessageFactory msgFactory = null;
		try {
			msgFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
		} catch (SOAPException e) {
			LOG.error(U2Constants.ERROR, e);
		}
		final SaajSoapMessageFactory newSoapMessageFactory = new SaajSoapMessageFactory(msgFactory);
		WebServiceTemplate webServiceTemplate = getWebServiceTemplate();
		webServiceTemplate.setMessageFactory(newSoapMessageFactory);

		LOG.debug("url ={}", url);
		LOG.debug("request :{}", TibcoToJavaUtil.renderXml(request));
		if (!url.startsWith("http")) {
			url = "https://" + url;
		}
		LOG.debug("url ={}", url);
		
		JAXBElement<AuthenticationInfo> authInfoHeaders = new ism_transaction.ObjectFactory().createAuthenticationInfo(authInfo);
		
		Object object = webServiceTemplate.marshalSendAndReceive(url, request, new WebServiceMessageCallback() {
			public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
				SaajSoapMessage soapMessage = (SaajSoapMessage) message;
				
				MimeHeaders headers = soapMessage.getSaajMessage().getMimeHeaders();
				headers.addHeader(TransportConstants.HEADER_CONTENT_TYPE, "text/xml;charset=utf-8");
				if(null != soapAction)
					headers.addHeader(TransportConstants.HEADER_SOAP_ACTION, soapAction);
				SoapHeader soapHeader = soapMessage.getSoapHeader();
				
				try {
					// create a marshaller
					JAXBContext context = JAXBContext.newInstance(AuthenticationInfo.class);
					Marshaller marshaller = context.createMarshaller();
					
					// marshal the headers into the specified result
					marshaller.marshal(authInfoHeaders, soapHeader.getResult());
				} catch (JAXBException e) {
					LOG.error("error during marshalling of the SOAP headers", e);
				}
			}
		});
		LOG.debug("response :{}", object);
		return object;
	}
}