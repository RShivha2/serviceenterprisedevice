package com.tmobile.u2.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
@Scope(value = WebApplicationContext.SCOPE_APPLICATION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ModuleServiceContext {

	private Map<String, Object> objectMap = new HashMap<String, Object>();

	public ModuleServiceContext() {
		super();
	}

	public void putRequestValue(String key, Object value) {
		objectMap.put(key, value);
	}

	public Object getRequestValue(String key) {
		return objectMap.get(key);
	}
}