package com.tmobile.u2.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class FileLoaderUtil {

	public InputStream getInputStream(String path) throws FileNotFoundException {
		InputStream input = getClass().getClassLoader().getResourceAsStream(path);
		if (input != null) {
			return input;
		}
		File initialFile = new File("resources/" + path);
		input = new FileInputStream(initialFile);
		return input;
	}
	
	
	public  String getResourceFileAsString(String fileName) throws FileNotFoundException {
	    InputStream is = getInputStream(fileName);
	    if (is != null) {
	        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	        return (String)reader.lines().collect(Collectors.joining(System.lineSeparator()));
	    } else {
	        throw new FileNotFoundException("resource not found");
	    }
	}
	
}
