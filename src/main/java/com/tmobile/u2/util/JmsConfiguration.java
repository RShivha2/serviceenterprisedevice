package com.tmobile.u2.util;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

import com.tibco.tibjms.TibjmsConnectionFactory;

@EnableJms
@Configuration
public class JmsConfiguration {

	@Bean
	public ConnectionFactory connectionFactory() {
		// For SSL connectivity Configuration with Tibco EMS Server
		TibjmsConnectionFactory connectionFactory = new TibjmsConnectionFactory(TibcoToJavaUtil.getProperty("TMO_ENV_NodeCommon_JMS_providerUrl.tcp"));

		connectionFactory.setUserName(TibcoToJavaUtil.getProperty("TMO_ENV_NodeCommon_JMS_username"));
		connectionFactory.setUserPassword(TibcoToJavaUtil.getProperty("TMO_ENV_NodeCommon_JMS_password"));
		connectionFactory.setSSLEnableVerifyHost(false);
		connectionFactory.setSSLEnableVerifyHostName(false);
		// connectionFactory.setSSLTrace(true);
		// connectionFactory.setSSLDebugTrace(true);

		connectionFactory.setSSLTrustedCertificate(TibcoToJavaUtil.getProperty("BW_GLOBAL_TRUSTED_CA_STORE"));

		// For TCP connectivity with EMS Server
		/*
		 * TibjmsConnectionFactory connectionFactory = new
		 * TibjmsConnectionFactory(TibcoToJavaUtil.getProperty(
		 * "TMO_ENV_NodeCommon_JMS_providerUrl.tcp"));
		 * connectionFactory.setUserName(TibcoToJavaUtil.getProperty(
		 * "TMO_ENV_NodeCommon_JMS_username"));
		 * connectionFactory.setUserPassword(TibcoToJavaUtil.getProperty(
		 * "TMO_ENV_NodeCommon_JMS_password"));
		 */
		return connectionFactory;
	}

	@Bean
	public JmsTemplate jmsTemplate(ConnectionFactory connectionFactory) throws JMSException {
		JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);
		jmsTemplate.setSessionAcknowledgeMode(2);
		return jmsTemplate;
		// return new JmsTemplate(connectionFactory);
	}

	@Bean
	public DefaultJmsListenerContainerFactory jmsListenerContainerFactory(ConnectionFactory connectionFactory,
			DefaultJmsListenerContainerFactoryConfigurer configurer) {
		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
		factory.setConnectionFactory(connectionFactory);
		factory.setConcurrency("1");
		factory.setSessionTransacted(true);
		factory.setSessionAcknowledgeMode(javax.jms.Session.CLIENT_ACKNOWLEDGE);
		configurer.configure(factory, connectionFactory);
		return factory;
	}
}