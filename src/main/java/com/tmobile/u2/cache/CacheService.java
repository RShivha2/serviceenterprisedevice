package com.tmobile.u2.cache;

import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.tmobile.services.schema.dataobject.commoncache.CacheElement;

@Service
public class CacheService {

	
	// @Cacheable annotation adds the caching behaviour.
	// If multiple requests are received, then the method won't be repeatedly
	// executed, instead, the results are shared from cached storage.
	@Cacheable(value = "EC", key = "#p0.key")
	public Object saveErrorCodesCache(CacheElement object) {
		return object;
	}

	@Cacheable(value = "ED", key = "#p0.key")
	public Object saveErrorCodesDescription(CacheElement object) {
		return object;
	}

	

	/**
	 * Method to update product on the basis of key
	 * 
	 * @param product
	 * @param productName
	 * @return
	 */
	 //@CachePut annotation updates the cached value.
	  @CachePut(value = "EC",key = "#p0.key") 
	  public Object updateEC(CacheElement cacheElement) {
		  return cacheElement;
	  }
	 

	/**
	 * Method to delete product on the basis of key
	 * 
	 * @param ticketId
	 */
	// @CacheEvict annotation removes one or all entries from cached storage.
	// <code>allEntries=true</code> attribute allows developers to purge all entries
	// from the cache.
	
	
}