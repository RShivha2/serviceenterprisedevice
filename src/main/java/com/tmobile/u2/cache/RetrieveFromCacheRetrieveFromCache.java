package com.tmobile.u2.cache;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.tmobile.util.cache.CacheList;
import com.tmobile.util.cache.CacheManager;
import com.tmobile.util.cache.CacheManagerJCS;
import com.tmobile.util.cache.CacheObject;

public class RetrieveFromCacheRetrieveFromCache {
	/****** START SET/GET METHOD, DO NOT MODIFY *****/
	protected String[] iTypes = null;
	protected String[] iKeys = null;
	protected String[] oTypes = null;
	protected String[] oKeys = null;
	protected String[] oValues = null;

	public String[] getiTypes() {
		return iTypes;
	}

	public void setiTypes(String[] val) {
		iTypes = val;
	}

	public String[] getiKeys() {
		return iKeys;
	}

	public void setiKeys(String[] val) {
		iKeys = val;
	}

	public String[] getoTypes() {
		return oTypes;
	}

	public void setoTypes(String[] val) {
		oTypes = val;
	}

	public String[] getoKeys() {
		return oKeys;
	}

	public void setoKeys(String[] val) {
		oKeys = val;
	}

	public String[] getoValues() {
		return oValues;
	}

	public void setoValues(String[] val) {
		oValues = val;
	}

	/****** END SET/GET METHOD, DO NOT MODIFY *****/
	public RetrieveFromCacheRetrieveFromCache() {
	}

	public void invoke() throws Exception {
		/*
		 * Available Variables: DO NOT MODIFY In : String[] iTypes In : String[] iKeys
		 * Out : String[] oTypes Out : String[] oKeys Out : String[] oValues Available
		 * Variables: DO NOT MODIFY
		 *****/

		// build the cache list
		List lst = new ArrayList(iTypes.length);
		for (int i = 0; i < iTypes.length; i++) {
			String key = null;
			String type = null;
			String value = null;
			Date expiration = null;
			if (i < iTypes.length)
				type = iTypes[i];
			if (i < iKeys.length)
				key = iKeys[i];

			CacheObject o = new CacheObject(type, key, value, expiration);
			lst.add(o);
		}

		CacheList clst = new CacheList();
		clst.setCacheObjects((CacheObject[]) (lst.toArray(new CacheObject[0])));

		// query the cache
		CacheManager mgr = CacheManagerJCS.getInstance();
		CacheList cache = mgr.getCacheObject(clst);

		// now, lets get the data back into a format we can use..
		CacheObject[] ocache = cache.getCacheObjects();

		oTypes = new String[ocache.length];
		oKeys = new String[ocache.length];
		oValues = new String[ocache.length];
		// oExpirations = new Date[ocache.length];

		for (int j = 0; j < ocache.length; j++) {
			oTypes[j] = ocache[j].getType();
			oKeys[j] = ocache[j].getKey();
			oValues[j] = ocache[j].getValue();
			// oExpirations[j] = ocache[j].getExpiration();
		}

		// done.
	}
}
