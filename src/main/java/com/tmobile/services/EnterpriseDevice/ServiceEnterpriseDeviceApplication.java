package com.tmobile.services.EnterpriseDevice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@EnableCaching
@ComponentScan(basePackages = {"com.tmobile.*"})
public class ServiceEnterpriseDeviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceEnterpriseDeviceApplication.class, args);
	}

}
