package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.u2.repositories.IInsertWarrantyBlanketPONumberJDBCRepository;
import com.tmobile.u2.service.impl.InsertWarrantyBlanketPONumberJDBCServiceImpl;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.shared.service.impl.LogProcessDetailsServiceImpl;

import targetreef.applications.reef.endpoints.devicewarranty.querywarrantyblanketponumberjdbc_end_output.QueryWarrantyBlanketPONumberJDBCEndOutput;

@ExtendWith(MockitoExtension.class)
public class InsertWarrantyBlanketPONumberJDBCServiceImplTest {

	@InjectMocks
	private InsertWarrantyBlanketPONumberJDBCServiceImpl insertWarrantyBlanketPONumberJDBCServiceImpl;

	@Mock
	private IInsertWarrantyBlanketPONumberJDBCRepository iInsertWarrantyBlanketPONumberJDBCRepository;

	@Mock
	private ILogProcessDetailsService iLogProcessDetailsService;

	@Before
	public void setup() throws Exception {
		Mockito.when(iInsertWarrantyBlanketPONumberJDBCRepository.insertWarrantyBlanketPONumber(Mockito.anyString()))
				.thenReturn(Mockito.any());
		Mockito.doNothing().when(iLogProcessDetailsService)
				.executeMethod(Mockito.any(LogProcessDetailsStartInput.class));
	}

	@Test
	public void testExecuteMethod() {
		QueryWarrantyBlanketPONumberJDBCEndOutput queryWarrantyBlanketPONumberJDBCEndOutput = new QueryWarrantyBlanketPONumberJDBCEndOutput();
		Exception ex = null;
		try {
			insertWarrantyBlanketPONumberJDBCServiceImpl.executeMethod(queryWarrantyBlanketPONumberJDBCEndOutput);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetInsertWarrantyBlanketPONumberJDBCRepository() {
		Exception ex = null;
		try {
			insertWarrantyBlanketPONumberJDBCServiceImpl
					.setInsertWarrantyBlanketPONumberJDBCRepository(iInsertWarrantyBlanketPONumberJDBCRepository);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetLogProcessDetailsService() {
		ILogProcessDetailsService iLogProcessDetailsService;
		Exception ex = null;
		try {
			insertWarrantyBlanketPONumberJDBCServiceImpl
					.setLogProcessDetailsService(new LogProcessDetailsServiceImpl());
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@AfterClass
	public void tearDownClass() throws Exception {
	}
}
