package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.t_mobile.csi.device.getimei.CSIGetIMEIRequestType;
import com.t_mobile.csi.device.getimei.CSIGetIMEIResponseType;
import com.t_mobile.csi.device.queryimeiwarranty.CSIQueryIMEIWarrantyRequestType;
import com.t_mobile.csi.device.queryimeiwarranty.CSIQueryIMEIWarrantyResponseType;
import com.t_mobile.schema.networkmanagement.union.resources.schemas.devicespecifications.DeviceSpecificationDetails;
import com.tmobile.services.csi.device.getshipusagedataforimei.GetShipUsageDataForIMEIRequestType;
import com.tmobile.services.csi.device.getshipusagedataforimei.GetShipUsageDataForIMEIResponseType;
import com.tmobile.services.productmanagement.deviceenterprise.v1.GetDeviceDetailsRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.GetDeviceDetailsResponse;
import com.tmobile.services.schema.dataobject.logwarningdetailsstartinput.LogwarningdetailsStartInput;
import com.tmobile.u2.service.IGetDeviceSpecificationsRESTService;
import com.tmobile.u2.service.IGetDeviceWarrantyDetailOrchService;
import com.tmobile.u2.service.IInvokeGetIMEIStatusService;
import com.tmobile.u2.service.IInvokeGetShipUsageDataService;
import com.tmobile.u2.service.IInvokeQueryIMEIWarrantyService;
import com.tmobile.u2.service.ILuhnCheckValidationService;
import com.tmobile.u2.service.impl.GetDeviceDetailsOrchServiceImpl;
import com.tmobile.u2.shared.service.ILogWarningDetailsService;
import com.tmobile.u2.util.SharedServiceContext;

import serviceenterprisedevice.common.subprocesses.businessrules.luhncheckvalidation_end_output.LuhnCheckValidationEndOutput;
import serviceenterprisedevice.common.subprocesses.businessrules.luhncheckvalidation_start_input.LuhnCheckValidationStartInput;
import targetgsx.applications.gsx.orchestrations.warranty.getdevicewarrantydetailorch_end_output.GetDeviceDetailsOrchEndOutput;
import targetgsx.applications.gsx.orchestrations.warranty.getdevicewarrantydetailorch_start_input.GetDeviceDetailsOrchStartInput;
import targetunion.applications.union.endpoints.getdevicespecificationsrest_start_input.GetDeviceSpecificationsRequest;

@ExtendWith(MockitoExtension.class)
public class GetDeviceDetailsOrchServiceImplTest {

	@InjectMocks
	private GetDeviceDetailsOrchServiceImpl deviceDetailsOrchServiceImpl;

	@Mock
	private IInvokeQueryIMEIWarrantyService iInvokeQueryIMEIWarrantyService;

	@Mock
	private IInvokeGetIMEIStatusService iInvokeGetIMEIStatusService;

	@Mock
	private ILuhnCheckValidationService iLuhnCheckValidationService;

	@Mock
	private ILogWarningDetailsService iLogWarningDetailsService;

	@Mock
	private IInvokeGetShipUsageDataService iInvokeGetShipUsageDataService;

	@Mock
	private IGetDeviceWarrantyDetailOrchService iGetDeviceWarrantyDetailOrchService;

	@Mock
	private IGetDeviceSpecificationsRESTService iGetDeviceSpecificationsRESTService;

	@Mock
	private SharedServiceContext sharedServiceContext;

	@Before
	public void setup() throws Exception {
		Mockito.when(sharedServiceContext.getRequestValue(Mockito.anyString())).thenReturn(Mockito.any());
		Mockito.doNothing().when(iLogWarningDetailsService)
				.executeMethod(Mockito.any(LogwarningdetailsStartInput.class));
		Mockito.doNothing().when(iInvokeQueryIMEIWarrantyService).executeMethod(
				Mockito.any(CSIQueryIMEIWarrantyRequestType.class),
				Mockito.any(CSIQueryIMEIWarrantyResponseType.class));
		Mockito.doNothing().when(iInvokeGetIMEIStatusService).executeMethod(Mockito.any(CSIGetIMEIRequestType.class),
				Mockito.any(CSIGetIMEIResponseType.class));
		Mockito.doNothing().when(iLuhnCheckValidationService).executeMethod(
				Mockito.any(LuhnCheckValidationStartInput.class), Mockito.any(LuhnCheckValidationEndOutput.class));
		Mockito.doNothing().when(iInvokeGetShipUsageDataService).executeMethod(
				Mockito.any(GetShipUsageDataForIMEIRequestType.class),
				Mockito.any(GetShipUsageDataForIMEIResponseType.class));
		Mockito.doNothing().when(iGetDeviceWarrantyDetailOrchService).executeMethod(
				Mockito.any(GetDeviceDetailsOrchStartInput.class), Mockito.any(GetDeviceDetailsOrchEndOutput.class));
		Mockito.doNothing().when(iGetDeviceSpecificationsRESTService).executeMethod(
				Mockito.any(GetDeviceSpecificationsRequest.class), Mockito.any(DeviceSpecificationDetails.class));

	}

	@Test
	public void testExecuteMethod() {
		GetDeviceDetailsRequest getDeviceDetailsRequest = new GetDeviceDetailsRequest();
		GetDeviceDetailsResponse getDeviceDetailsResponse = new GetDeviceDetailsResponse();
		Exception ex = null;
		try {
			deviceDetailsOrchServiceImpl.executeMethod(getDeviceDetailsRequest, getDeviceDetailsResponse);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetLogWarningDetailsService() {
		Exception ex = null;
		try {
			deviceDetailsOrchServiceImpl.setLogWarningDetailsService(iLogWarningDetailsService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetInvokeQueryIMEIWarrantyService() {
		Exception ex = null;
		try {
			deviceDetailsOrchServiceImpl.setInvokeQueryIMEIWarrantyService(iInvokeQueryIMEIWarrantyService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetGetDeviceSpecificationsRESTService() {
		Exception ex = null;
		try {
			deviceDetailsOrchServiceImpl.setGetDeviceSpecificationsRESTService(iGetDeviceSpecificationsRESTService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetGetDeviceWarrantyDetailOrchService() {
		Exception ex = null;
		try {
			deviceDetailsOrchServiceImpl.setGetDeviceWarrantyDetailOrchService(iGetDeviceWarrantyDetailOrchService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetLuhnCheckValidationService() {
		Exception ex = null;
		try {
			deviceDetailsOrchServiceImpl.setLuhnCheckValidationService(iLuhnCheckValidationService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetInvokeGetShipUsageDataService() {
		Exception ex = null;
		try {
			deviceDetailsOrchServiceImpl.setInvokeGetShipUsageDataService(iInvokeGetShipUsageDataService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetSharedServiceContext() {
		SharedServiceContext SharedServiceContext = new SharedServiceContext();
		Exception ex = null;
		try {
			deviceDetailsOrchServiceImpl.setSharedServiceContext(SharedServiceContext);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetInvokeGetIMEIStatusService() {
		Exception ex = null;
		try {
			deviceDetailsOrchServiceImpl.setInvokeGetIMEIStatusService(iInvokeGetIMEIStatusService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@AfterClass
	public void tearDownClass() throws Exception {
	}
}
