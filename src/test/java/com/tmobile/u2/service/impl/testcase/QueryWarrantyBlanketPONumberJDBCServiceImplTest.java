package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tibco.namespaces.tnt.plugins.jdbc_a13cb515_6e41_4894_84ca_d54b87b05824_input.JdbcQueryActivityInput;
import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.u2.repositories.IQueryWarrantyBlanketPONumberJDBCRepository;
import com.tmobile.u2.service.impl.QueryWarrantyBlanketPONumberJDBCServiceImpl;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.shared.service.impl.LogProcessDetailsServiceImpl;

import targetreef.applications.reef.endpoints.devicewarranty.querywarrantyblanketponumberjdbc_end_output.QueryWarrantyBlanketPONumberJDBCEndOutput;

@ExtendWith(MockitoExtension.class)
public class QueryWarrantyBlanketPONumberJDBCServiceImplTest {

    @InjectMocks
    private QueryWarrantyBlanketPONumberJDBCServiceImpl queryWarrantyBlanketPONumberJDBCServiceImpl;

    @Mock
    private IQueryWarrantyBlanketPONumberJDBCRepository iQueryWarrantyBlanketPONumberJDBCRepository;

    @Mock
    private ILogProcessDetailsService iLogProcessDetailsService;

    @Before
    public void setup() throws Exception {
    	Mockito.doNothing().when(iLogProcessDetailsService).executeMethod(Mockito.any(LogProcessDetailsStartInput.class));
        Mockito.when(iQueryWarrantyBlanketPONumberJDBCRepository.queryWarrantyBlanketPONumber(Mockito.any(JdbcQueryActivityInput.class))).thenReturn(Mockito.any());
    }

    @Test
    public void testExecuteMethod() {
        QueryWarrantyBlanketPONumberJDBCEndOutput queryWarrantyBlanketPONumberJDBCEndOutput = new QueryWarrantyBlanketPONumberJDBCEndOutput();
        Exception ex = null;
        try {
            queryWarrantyBlanketPONumberJDBCServiceImpl.executeMethod(queryWarrantyBlanketPONumberJDBCEndOutput);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetIQueryWarrantyBlanketPONumberJDBCRepository() {
        Exception ex = null;
        try {
            queryWarrantyBlanketPONumberJDBCServiceImpl.setIQueryWarrantyBlanketPONumberJDBCRepository(iQueryWarrantyBlanketPONumberJDBCRepository);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetLogProcessDetailsService() {
        Exception ex = null;
        try {
            queryWarrantyBlanketPONumberJDBCServiceImpl.setLogProcessDetailsService(new LogProcessDetailsServiceImpl());
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
