package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.t_mobile.schema.apple.verifyorder.VerifyOrderRequesttype;
import com.t_mobile.schema.apple.verifyorder.VerifyOrderResponsetype;
import com.t_mobile.schema.productmanagement.devicewarranty.verifywarranty.VerifyWarrantyRequest;
import com.tmobile.u2.service.IBlanketPOSyncSoapService;
import com.tmobile.u2.service.IExecuteSpaceOpHttpsService;
import com.tmobile.u2.service.IInsertWarrantyBlanketPONumberJDBCService;
import com.tmobile.u2.service.IQueryWarrantyBlanketPONumberJDBCService;
import com.tmobile.u2.service.IVerifyOrderRESTService;
import com.tmobile.u2.service.impl.VerifyWarrantyOrchServiceImpl;

import serviceenterprisedevice.orchestrations.deviceenterprise.verifywarrantyorch_end_output.VerifyWarrantyOrchEndOutput;
import targetcaasclient.applications.caas.endpoints.spaceops.executespaceophttps_end_output.Response;
import targetcaasclient.applications.caas.endpoints.spaceops.executespaceophttps_start_input.SpaceOps;
import targetreef.applications.reef.endpoints.devicewarranty.querywarrantyblanketponumberjdbc_end_output.QueryWarrantyBlanketPONumberJDBCEndOutput;
import targetsappi.applications.sappi.endpoints.order.blanketposyncsoap_end_output.BlanketPOSyncSoapEndOutput;
import targetsappi.applications.sappi.endpoints.order.blanketposyncsoap_start_input.BlanketPOSyncSoapStartInput;

@ExtendWith(MockitoExtension.class)
public class VerifyWarrantyOrchServiceImplTest {

    @InjectMocks
    private VerifyWarrantyOrchServiceImpl verifyWarrantyOrchServiceImpl;

    @Mock
    private IExecuteSpaceOpHttpsService iExecuteSpaceOpHttpsService;

    @Mock
    private IQueryWarrantyBlanketPONumberJDBCService iQueryWarrantyBlanketPONumberJDBCService;

    @Mock
    private IBlanketPOSyncSoapService iBlanketPOSyncSoapService;

    @Mock
    private IInsertWarrantyBlanketPONumberJDBCService iInsertWarrantyBlanketPONumberJDBCService;

    @Mock
    private IVerifyOrderRESTService iVerifyOrderRESTService;

    @Before
    public void setup() throws Exception {
        Mockito.doNothing().when(iExecuteSpaceOpHttpsService).executeMethod(Mockito.any(SpaceOps.class), Mockito.any(Response.class));
        Mockito.doNothing().when(iBlanketPOSyncSoapService).executeMethod(Mockito.any(BlanketPOSyncSoapStartInput.class), Mockito.any(BlanketPOSyncSoapEndOutput.class));
        Mockito.doNothing().when(iVerifyOrderRESTService).executeMethod(Mockito.any(VerifyOrderRequesttype.class), Mockito.any(VerifyOrderResponsetype.class));
        Mockito.doNothing().when(iQueryWarrantyBlanketPONumberJDBCService).executeMethod(Mockito.any(QueryWarrantyBlanketPONumberJDBCEndOutput.class));
        Mockito.doNothing().when(iInsertWarrantyBlanketPONumberJDBCService).executeMethod(Mockito.any(QueryWarrantyBlanketPONumberJDBCEndOutput.class));
    }

    @Test
    public void testExecuteMethod() {
        VerifyWarrantyRequest verifyWarrantyRequest = new VerifyWarrantyRequest();
        VerifyWarrantyOrchEndOutput verifyWarrantyOrchEndOutput = new VerifyWarrantyOrchEndOutput();
        Exception ex = null;
        try {
            verifyWarrantyOrchServiceImpl.executeMethod(verifyWarrantyRequest, verifyWarrantyOrchEndOutput);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetInsertWarrantyBlanketPONumberJDBCService() {
        Exception ex = null;
        try {
            verifyWarrantyOrchServiceImpl.setInsertWarrantyBlanketPONumberJDBCService(iInsertWarrantyBlanketPONumberJDBCService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetQueryWarrantyBlanketPONumberJDBCService() {
        Exception ex = null;
        try {
            verifyWarrantyOrchServiceImpl.setQueryWarrantyBlanketPONumberJDBCService(iQueryWarrantyBlanketPONumberJDBCService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetVerifyOrderRESTService() {
        Exception ex = null;
        try {
            verifyWarrantyOrchServiceImpl.setVerifyOrderRESTService(iVerifyOrderRESTService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetBlanketPOSyncSoapService() {
        Exception ex = null;
        try {
            verifyWarrantyOrchServiceImpl.setBlanketPOSyncSoapService(iBlanketPOSyncSoapService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetExecuteSpaceOpHttpsService() {
        Exception ex = null;
        try {
            verifyWarrantyOrchServiceImpl.setExecuteSpaceOpHttpsService(iExecuteSpaceOpHttpsService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
