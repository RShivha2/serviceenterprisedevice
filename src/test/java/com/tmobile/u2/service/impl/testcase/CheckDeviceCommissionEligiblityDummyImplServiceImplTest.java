package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckDeviceCommissionEligiblityRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckDeviceCommissionEligiblityResponse;
import com.tmobile.u2.service.impl.CheckDeviceCommissionEligiblityDummyImplServiceImpl;

@ExtendWith(MockitoExtension.class)
public class CheckDeviceCommissionEligiblityDummyImplServiceImplTest {

	@InjectMocks
	private CheckDeviceCommissionEligiblityDummyImplServiceImpl checkDeviceCommissionEligiblityDummyImplServiceImpl;

	@Before
	public void setup() throws Exception {
	}

	@Test
	public void testExecuteMethod() {
		CheckDeviceCommissionEligiblityRequest checkDeviceCommissionEligiblityRequest = new CheckDeviceCommissionEligiblityRequest();
		CheckDeviceCommissionEligiblityResponse checkDeviceCommissionEligiblityResponse = new CheckDeviceCommissionEligiblityResponse();
		Exception ex = null;
		try {
			checkDeviceCommissionEligiblityDummyImplServiceImpl.executeMethod(checkDeviceCommissionEligiblityRequest,
					checkDeviceCommissionEligiblityResponse);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@AfterClass
	public void tearDownClass() throws Exception {
	}
}
