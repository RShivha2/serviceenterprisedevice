package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.namespaces.tns._1588947392231.QueryAllSOAPStartInput;
import com.example.namespaces.tns._1588947392232.QueryAllSOAPEndOutput;
import com.tmobile.eus.unlock.EUSUnlockRequest;
import com.tmobile.eus.unlock.EUSUnlockResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.SendDeviceUnlockCodeRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.SendDeviceUnlockCodeResponse;
import com.tmobile.u2.service.IEUSUnlockServiceSOAPService;
import com.tmobile.u2.service.IQueryAllSOAPService;
import com.tmobile.u2.service.impl.InvokesendDeviceUnlockCodeServiceImpl;
import com.tmobile.u2.shared.service.IHeaderValidationService;
import com.tmobile.u2.util.SharedServiceContext;

import sharedservicecontext.sharedprocesses.utilityprocesses.headervalidation_start_input.HeaderValidationStartInput;

@ExtendWith(MockitoExtension.class)
public class InvokesendDeviceUnlockCodeServiceImplTest {

    @InjectMocks
    private InvokesendDeviceUnlockCodeServiceImpl invokesendDeviceUnlockCodeServiceImpl;

    @Mock
    private IHeaderValidationService iHeaderValidationService;

    @Mock
    private IEUSUnlockServiceSOAPService iEUSUnlockServiceSOAPService;

    @Mock
    private IQueryAllSOAPService iQueryAllSOAPService;

    @Mock
    private SharedServiceContext sharedServiceContext;

    @Before
    public void setup() throws Exception {
    	Mockito.when(sharedServiceContext.getRequestValue(Mockito.anyString())).thenReturn(Mockito.any());
        Mockito.doNothing().when(iHeaderValidationService).executeMethod(Mockito.any(HeaderValidationStartInput.class));;
        Mockito.doNothing().when(iEUSUnlockServiceSOAPService).executeMethod(Mockito.any(EUSUnlockRequest.class), Mockito.any(EUSUnlockResponse.class));
        Mockito.doNothing().when(iQueryAllSOAPService).executeMethod(Mockito.any(QueryAllSOAPStartInput.class), Mockito.any(QueryAllSOAPEndOutput.class));
    }

    @Test
    public void testExecuteMethod() {
        SendDeviceUnlockCodeRequest sendDeviceUnlockCodeRequest = new SendDeviceUnlockCodeRequest();
        SendDeviceUnlockCodeResponse sendDeviceUnlockCodeResponse = new SendDeviceUnlockCodeResponse();
        Exception ex = null;
        try {
            invokesendDeviceUnlockCodeServiceImpl.executeMethod(sendDeviceUnlockCodeRequest, sendDeviceUnlockCodeResponse);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetHeaderValidationService() {
        Exception ex = null;
        try {
            invokesendDeviceUnlockCodeServiceImpl.setHeaderValidationService(iHeaderValidationService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetSharedServiceContext() {
        Exception ex = null;
        try {
            invokesendDeviceUnlockCodeServiceImpl.setSharedServiceContext(sharedServiceContext);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetQueryAllSOAPService() {
        Exception ex = null;
        try {
            invokesendDeviceUnlockCodeServiceImpl.setQueryAllSOAPService(iQueryAllSOAPService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetEUSUnlockServiceSOAPService() {
        Exception ex = null;
        try {
            invokesendDeviceUnlockCodeServiceImpl.setEUSUnlockServiceSOAPService(iEUSUnlockServiceSOAPService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
