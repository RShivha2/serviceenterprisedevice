package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.namespaces.tns._1593722054053.Element;
import com.tibco.tns.bw.activity.jsonparser.xsd.output.GetAccessTokenOutput;
import com.tmobile.u2.service.IGetAccessTokenService;
import com.tmobile.u2.service.impl.RefreshAccessTokensServiceImpl;
import com.tmobile.u2.util.ModuleServiceContext;

@ExtendWith(MockitoExtension.class)
public class RefreshAccessTokensServiceImplTest {

    @InjectMocks
    private RefreshAccessTokensServiceImpl refreshAccessTokensServiceImpl;

    @Mock
    private ModuleServiceContext moduleServiceContext;

    @Mock
    private IGetAccessTokenService iGetAccessTokenService;

    @Before
    public void setup() throws Exception {
        Mockito.when(moduleServiceContext.getRequestValue(Mockito.anyString())).thenReturn(Mockito.any());
        Mockito.doNothing().when(iGetAccessTokenService).executeMethod(Mockito.any(GetAccessTokenOutput.class));
    }

    @Test
    public void testSetModuleServiceContext() {
        Exception ex = null;
        try {
            refreshAccessTokensServiceImpl.setModuleServiceContext(moduleServiceContext);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testExecuteMethod() {
        Element element = new Element();
        Exception ex = null;
        try {
            refreshAccessTokensServiceImpl.executeMethod(element);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetGetAccessTokenService() {
        Exception ex = null;
        try {
            refreshAccessTokensServiceImpl.setGetAccessTokenService(iGetAccessTokenService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
