package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tmobile.services.productmanagement.deviceenterprise.v1.UpdateUnlockEligibilityRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.UpdateUnlockEligibilityResponse;
import com.tmobile.u2.service.IUpdateUnlockEligibilitySoapService;
import com.tmobile.u2.service.impl.InvokeUpdateUnlockEligibilityServiceImpl;
import com.tmobile.u2.shared.service.IHeaderValidationService;
import com.tmobile.u2.util.SharedServiceContext;

import sharedservicecontext.sharedprocesses.utilityprocesses.headervalidation_start_input.HeaderValidationStartInput;
import targetsappi.applications.sappi.endpoints.device.updateunlockeligibilitysoap_end_output.UpdateUnlockEligibilitySoapEndOutput;
import targetsappi.applications.sappi.endpoints.device.updateunlockeligibilitysoap_start_input.UpdateUnlockEligibilitySoapStartInput;

@ExtendWith(MockitoExtension.class)
public class InvokeUpdateUnlockEligibilityServiceImplTest {

    @InjectMocks
    private InvokeUpdateUnlockEligibilityServiceImpl invokeUpdateUnlockEligibilityServiceImpl;

    @Mock
    private IHeaderValidationService iHeaderValidationService;

    @Mock
    private IUpdateUnlockEligibilitySoapService iUpdateUnlockEligibilitySoapService;

    @Mock
    private SharedServiceContext sharedServiceContext;

    @Before
    public void setup() throws Exception {
    	Mockito.when(sharedServiceContext.getRequestValue(Mockito.anyString())).thenReturn(Mockito.any());
        Mockito.doNothing().when(iHeaderValidationService).executeMethod(Mockito.any(HeaderValidationStartInput.class));;
        Mockito.doNothing().when(iUpdateUnlockEligibilitySoapService).executeMethod(Mockito.any(UpdateUnlockEligibilitySoapStartInput.class), Mockito.any(UpdateUnlockEligibilitySoapEndOutput.class));
    }

    @Test
    public void testExecuteMethod() {
        UpdateUnlockEligibilityRequest updateUnlockEligibilityRequest = new UpdateUnlockEligibilityRequest();
        UpdateUnlockEligibilityResponse updateUnlockEligibilityResponse = new UpdateUnlockEligibilityResponse();
        Exception ex = null;
        try {
            invokeUpdateUnlockEligibilityServiceImpl.executeMethod(updateUnlockEligibilityRequest, updateUnlockEligibilityResponse);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetHeaderValidationService() {
        Exception ex = null;
        try {
            invokeUpdateUnlockEligibilityServiceImpl.setHeaderValidationService(iHeaderValidationService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetUpdateUnlockEligibilitySoapService() {
        Exception ex = null;
        try {
            invokeUpdateUnlockEligibilityServiceImpl.setUpdateUnlockEligibilitySoapService(iUpdateUnlockEligibilitySoapService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetSharedServiceContext() {
        Exception ex = null;
        try {
            invokeUpdateUnlockEligibilityServiceImpl.setSharedServiceContext(sharedServiceContext);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
