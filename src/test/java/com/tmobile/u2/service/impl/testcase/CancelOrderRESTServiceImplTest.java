package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import com.t_mobile.schema.apple.cancelorder.CancelOrderReqtype;
import com.t_mobile.schema.apple.cancelorder.CancelOrderRestype;
import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.u2.exception.TMobileProcessException;
import com.tmobile.u2.service.impl.CancelOrderRESTServiceImpl;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;

@ExtendWith(MockitoExtension.class)
public class CancelOrderRESTServiceImplTest {

	@InjectMocks
	private CancelOrderRESTServiceImpl cancelOrderRESTServiceImpl;

	@Mock
	private RestTemplate restTemplate;

	@Mock
	private ILogProcessDetailsService iLogProcessDetailsService;

	@Before
	public void setup() throws Exception {
	}

	@Test
	public void testExecuteMethod() throws TMobileProcessException {
		// TODO replace MethodName with actual method and return type as well if
		// required;
		////Mockito.when(restTemplate.MethodName(Mockito.any(clazz.class))).thenReturn(Mockito.any());
		Mockito.doNothing().when(iLogProcessDetailsService).executeMethod(Mockito.any(LogProcessDetailsStartInput.class));
		CancelOrderReqtype cancelOrderReq = new CancelOrderReqtype();
		CancelOrderRestype cancelOrderRes = new CancelOrderRestype();
		Exception ex = null;
		try {
			cancelOrderRESTServiceImpl.executeMethod(cancelOrderReq, cancelOrderRes);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@AfterClass
	public void tearDownClass() throws Exception {
	}
}
