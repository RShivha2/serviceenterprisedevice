package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tmobile.services.csi.device.getshipusagedataforimei.GetShipUsageDataForIMEIRequestType;
import com.tmobile.services.csi.device.getshipusagedataforimei.GetShipUsageDataForIMEIResponseType;
import com.tmobile.u2.service.IGetIMEIShipUsageDataSQLService;
import com.tmobile.u2.service.IGetShipUsageDataForImeiSqlService;
import com.tmobile.u2.service.impl.InvokeGetShipUsageDataServiceImpl;

@ExtendWith(MockitoExtension.class)
public class InvokeGetShipUsageDataServiceImplTest {

    @InjectMocks
    private InvokeGetShipUsageDataServiceImpl invokeGetShipUsageDataServiceImpl;

    @Mock
    private IGetShipUsageDataForImeiSqlService iGetShipUsageDataForImeiSqlService;

    @Mock
    private IGetIMEIShipUsageDataSQLService iGetIMEIShipUsageDataSQLService;

    @Before
    public void setup() throws Exception {
        // TODO replace MethodName with actual method and return type as well if required;
        Mockito.doNothing().when(iGetShipUsageDataForImeiSqlService).executeMethod(Mockito.any(GetShipUsageDataForIMEIRequestType.class), Mockito.any(GetShipUsageDataForIMEIResponseType.class));
        Mockito.doNothing().when(iGetIMEIShipUsageDataSQLService).executeMethod(Mockito.any(GetShipUsageDataForIMEIRequestType.class), Mockito.any(GetShipUsageDataForIMEIResponseType.class));
    }

    @Test
    public void testExecuteMethod() {
        GetShipUsageDataForIMEIRequestType getShipUsageDataForIMEIRequest = new GetShipUsageDataForIMEIRequestType();
        GetShipUsageDataForIMEIResponseType getShipUsageDataForIMEIResponse = new GetShipUsageDataForIMEIResponseType();
        Exception ex = null;
        try {
            invokeGetShipUsageDataServiceImpl.executeMethod(getShipUsageDataForIMEIRequest, getShipUsageDataForIMEIResponse);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetGetIMEIShipUsageDataSQLService() {
        Exception ex = null;
        try {
            invokeGetShipUsageDataServiceImpl.setGetIMEIShipUsageDataSQLService(iGetIMEIShipUsageDataSQLService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetGetShipUsageDataForImeiSqlService() {
        Exception ex = null;
        try {
            invokeGetShipUsageDataServiceImpl.setGetShipUsageDataForImeiSqlService(iGetShipUsageDataForImeiSqlService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
