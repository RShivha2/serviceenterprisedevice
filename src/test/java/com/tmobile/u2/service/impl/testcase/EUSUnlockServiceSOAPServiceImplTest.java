package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tmobile.eus.unlock.EUSUnlockRequest;
import com.tmobile.eus.unlock.EUSUnlockResponse;
import com.tmobile.u2.exception.TMobileProcessException;
import com.tmobile.u2.service.impl.EUSUnlockServiceSOAPServiceImpl;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.shared.service.impl.LogProcessDetailsServiceImpl;
import com.tmobile.u2.util.EUSUnlockServiceSOAPConnector;

@ExtendWith(MockitoExtension.class)
public class EUSUnlockServiceSOAPServiceImplTest {

    @InjectMocks
    private EUSUnlockServiceSOAPServiceImpl eUSUnlockServiceSOAPServiceImpl;

    @Mock
    private ILogProcessDetailsService iLogProcessDetailsService;

    @Mock
    private EUSUnlockServiceSOAPConnector eUSUnlockServiceSOAPConnector;

    @Before
    public void setup() throws Exception {
        //Mockito.doNothing().when(iLogProcessDetailsService).executeMethod(Mockito.any(LogProcessDetailsStartInput.class));
        //Mockito.when(eUSUnlockServiceSOAPConnector.MethodName(Mockito.any(clazz.class))).thenReturn(Mockito.any());
    }

    @Test
    public void testExecuteMethod() {
        EUSUnlockRequest eUSUnlockRequest = new EUSUnlockRequest();
        EUSUnlockResponse eUSUnlockResponse = new EUSUnlockResponse();
        Exception ex = null;
        try {
            eUSUnlockServiceSOAPServiceImpl.executeMethod(eUSUnlockRequest, eUSUnlockResponse);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetLogProcessDetailsService() throws TMobileProcessException {
        Exception ex = null;
        try {
            eUSUnlockServiceSOAPServiceImpl.setLogProcessDetailsService(new LogProcessDetailsServiceImpl());
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetSOAPConnector() throws TMobileProcessException {
        Exception ex = null;
        try {
            eUSUnlockServiceSOAPServiceImpl.setSOAPConnector(eUSUnlockServiceSOAPConnector);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
