package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.oracle.xmlns.apps.mdm.customer.UpdateOrderOutput;
import com.t_mobile.u2.xml.tmolineordersio.ListOfTMOLineOrdersIO;
import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.u2.service.impl.UpdateOrderSoapServiceImpl;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.shared.service.impl.LogProcessDetailsServiceImpl;
import com.tmobile.u2.util.UpdateOrderSOAPConnector;

@ExtendWith(MockitoExtension.class)
public class UpdateOrderSoapServiceImplTest {

    @InjectMocks
    private UpdateOrderSoapServiceImpl updateOrderSoapServiceImpl;

    @Mock
    private ILogProcessDetailsService iLogProcessDetailsService;

    @Mock
    private UpdateOrderSOAPConnector updateOrderSOAPConnector;

    @Before
    public void setup() throws Exception {
        // TODO replace MethodName with actual method and return type as well if required;
        Mockito.doNothing().when(iLogProcessDetailsService).executeMethod(Mockito.any(LogProcessDetailsStartInput.class));
        //Mockito.when(updateOrderSOAPConnector.callWebService(Mockito.any(clazz.class))).thenReturn(Mockito.any());
    }

    @Test
    public void testExecuteMethod() {
        ListOfTMOLineOrdersIO listOfTMOLineOrdersIO = new ListOfTMOLineOrdersIO();
        UpdateOrderOutput updateOrderOutput = new UpdateOrderOutput();
        Exception ex = null;
        try {
            updateOrderSoapServiceImpl.executeMethod(listOfTMOLineOrdersIO, updateOrderOutput);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetLogProcessDetailsService() {
        Exception ex = null;
        try {
            updateOrderSoapServiceImpl.setLogProcessDetailsService(new LogProcessDetailsServiceImpl());
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetSOAPConnector() {
        Exception ex = null;
        try {
            updateOrderSoapServiceImpl.setSOAPConnector(updateOrderSOAPConnector);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
