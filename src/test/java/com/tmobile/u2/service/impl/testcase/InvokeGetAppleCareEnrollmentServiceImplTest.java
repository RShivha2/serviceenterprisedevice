package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.t_mobile.schema.apple._360lookup.OrderLookupRequesttype;
import com.t_mobile.schema.apple._360lookup.OrderLookupRestype;
import com.tmobile.services.productmanagement.deviceenterprise.v1.GetAppleCareEnrollmentRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.GetAppleCareEnrollmentResponse;
import com.tmobile.u2.service.ILookupOrderRESTService;
import com.tmobile.u2.service.impl.InvokeGetAppleCareEnrollmentServiceImpl;
import com.tmobile.u2.shared.service.IHeaderValidationService;
import com.tmobile.u2.util.SharedServiceContext;

import sharedservicecontext.sharedprocesses.utilityprocesses.headervalidation_start_input.HeaderValidationStartInput;

@ExtendWith(MockitoExtension.class)
public class InvokeGetAppleCareEnrollmentServiceImplTest {

	@InjectMocks
	private InvokeGetAppleCareEnrollmentServiceImpl invokeGetAppleCareEnrollmentServiceImpl;

	@Mock
	private IHeaderValidationService iHeaderValidationService;

	@Mock
	private ILookupOrderRESTService iLookupOrderRESTService;

	@Mock
	private SharedServiceContext sharedServiceContext;

	@Before
	public void setup() throws Exception {
		// TODO replace MethodName with actual method and return type as well if
		// required;
		Mockito.doNothing().when(iHeaderValidationService).executeMethod(Mockito.any(HeaderValidationStartInput.class));
		;
		Mockito.doNothing().when(iLookupOrderRESTService).executeMethod(Mockito.any(OrderLookupRequesttype.class),
				Mockito.any(OrderLookupRestype.class));
		Mockito.when(sharedServiceContext.getRequestValue(Mockito.anyString())).thenReturn(Mockito.any());
	}

	@Test
	public void testExecuteMethod() {
		GetAppleCareEnrollmentRequest getAppleCareEnrollmentRequest = new GetAppleCareEnrollmentRequest();
		GetAppleCareEnrollmentResponse getAppleCareEnrollmentResponse = new GetAppleCareEnrollmentResponse();
		Exception ex = null;
		try {
			invokeGetAppleCareEnrollmentServiceImpl.executeMethod(getAppleCareEnrollmentRequest,
					getAppleCareEnrollmentResponse);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetHeaderValidationService() {
		Exception ex = null;
		try {
			invokeGetAppleCareEnrollmentServiceImpl.setHeaderValidationService(iHeaderValidationService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetLookupOrderRESTService() {
		Exception ex = null;
		try {
			invokeGetAppleCareEnrollmentServiceImpl.setLookupOrderRESTService(iLookupOrderRESTService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetSharedServiceContext() {
		Exception ex = null;
		try {
			invokeGetAppleCareEnrollmentServiceImpl.setSharedServiceContext(sharedServiceContext);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@AfterClass
	public void tearDownClass() throws Exception {
	}
}
