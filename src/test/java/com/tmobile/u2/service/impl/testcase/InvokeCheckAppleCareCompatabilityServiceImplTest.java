package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.t_mobile.schema.productmanagement.devicewarranty.verifywarranty.VerifyWarrantyRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckAppleCareCompatabilityRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckAppleCareCompatabilityResponse;
import com.tmobile.u2.service.IVerifyWarrantyOrchService;
import com.tmobile.u2.service.impl.InvokeCheckAppleCareCompatabilityServiceImpl;
import com.tmobile.u2.shared.service.IHeaderValidationService;
import com.tmobile.u2.util.SharedServiceContext;

import serviceenterprisedevice.orchestrations.deviceenterprise.verifywarrantyorch_end_output.VerifyWarrantyOrchEndOutput;
import sharedservicecontext.sharedprocesses.utilityprocesses.headervalidation_start_input.HeaderValidationStartInput;

@ExtendWith(MockitoExtension.class)
public class InvokeCheckAppleCareCompatabilityServiceImplTest {

    @InjectMocks
    private InvokeCheckAppleCareCompatabilityServiceImpl invokeCheckAppleCareCompatabilityServiceImpl;

    @Mock
    private IHeaderValidationService iHeaderValidationService;

    @Mock
    private IVerifyWarrantyOrchService iVerifyWarrantyOrchService;

    @Mock
    private SharedServiceContext sharedServiceContext;

    @Before
    public void setup() throws Exception {
    	Mockito.when(sharedServiceContext.getRequestValue(Mockito.anyString())).thenReturn(Mockito.any());
    	Mockito.doNothing().when(iHeaderValidationService).executeMethod(Mockito.any(HeaderValidationStartInput.class));
        Mockito.doNothing().when(iVerifyWarrantyOrchService).executeMethod(Mockito.any(VerifyWarrantyRequest.class), Mockito.any(VerifyWarrantyOrchEndOutput.class));
    }

    @Test
    public void testExecuteMethod() {
        CheckAppleCareCompatabilityRequest checkAppleCareCompatabilityRequest = new CheckAppleCareCompatabilityRequest();
        CheckAppleCareCompatabilityResponse checkAppleCareCompatabilityResponse = new CheckAppleCareCompatabilityResponse();
        Exception ex = null;
        try {
            invokeCheckAppleCareCompatabilityServiceImpl.executeMethod(checkAppleCareCompatabilityRequest, checkAppleCareCompatabilityResponse);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetHeaderValidationService() {
        Exception ex = null;
        try {
            invokeCheckAppleCareCompatabilityServiceImpl.setHeaderValidationService(iHeaderValidationService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetVerifyWarrantyOrchService() {
        Exception ex = null;
        try {
            invokeCheckAppleCareCompatabilityServiceImpl.setVerifyWarrantyOrchService(iVerifyWarrantyOrchService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetSharedServiceContext() {
        Exception ex = null;
        try {
            invokeCheckAppleCareCompatabilityServiceImpl.setSharedServiceContext(sharedServiceContext);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
