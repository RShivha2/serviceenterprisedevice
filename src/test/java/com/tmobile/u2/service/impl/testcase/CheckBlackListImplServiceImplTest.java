package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckBlackListRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckBlackListResponse;
import com.tmobile.services.schema.dataobject.createservicecontextendoutput.CreateServiceContextOutput;
import com.tmobile.services.schema.dataobject.createservicecontextstartinput.CreateServiceContextInput;
import com.tmobile.services.schema.dataobject.errorlookupendoutput.ErrorLookupEndOutput;
import com.tmobile.services.schema.dataobject.legacyesoaerrorlookupstartinput.LegacyESOAErrorLookupStartInput;
import com.tmobile.services.schema.dataobject.logservicecontextstartinput.LogServiceContextStartInput;
import com.tmobile.u2.exception.TMobileProcessException;
import com.tmobile.u2.service.IInvokeCheckBlackListService;
import com.tmobile.u2.service.impl.CheckBlackListImplServiceImpl;
import com.tmobile.u2.shared.service.ICreateServiceContextService;
import com.tmobile.u2.shared.service.IErrorLookupService;
import com.tmobile.u2.shared.service.ILogServiceContextService;
import com.tmobile.u2.shared.service.impl.CreateServiceContextServiceImpl;
import com.tmobile.u2.shared.service.impl.ErrorLookupServiceImpl;
import com.tmobile.u2.shared.service.impl.LogServiceContextServiceImpl;
import com.tmobile.u2.util.SharedServiceContext;

@ExtendWith(MockitoExtension.class)
public class CheckBlackListImplServiceImplTest {

	@InjectMocks
	private CheckBlackListImplServiceImpl checkBlackListImplServiceImpl;

	@Mock
	private IErrorLookupService iErrorLookupService;

	@Mock
	private ICreateServiceContextService iCreateServiceContextService;

	@Mock
	private ILogServiceContextService iLogServiceContextService;

	@Mock
	private SharedServiceContext sharedServiceContext;

	@Mock
	private IInvokeCheckBlackListService iInvokeCheckBlackListService;

	@Before
	public void setup() throws Exception {
		Mockito.doNothing().when(iErrorLookupService).executeMethod(Mockito.any(LegacyESOAErrorLookupStartInput.class),
				Mockito.any(ErrorLookupEndOutput.class));
		Mockito.doNothing().when(iCreateServiceContextService).executeMethod(
				Mockito.any(CreateServiceContextInput.class), Mockito.any(CreateServiceContextOutput.class));
		Mockito.doNothing().when(iLogServiceContextService)
				.executeMethod(Mockito.any(LogServiceContextStartInput.class));
		Mockito.when(sharedServiceContext.getRequestValue(Mockito.anyString())).thenReturn(Mockito.any());
		//Mockito.doNothing().when(iInvokeCheckBlackListService).executeMethod(Mockito.any(CheckBlackListRequest.class), Mockito.any(CheckBlackListResponse.class));
	}

	@Test
	public void testExecuteMethod() throws TMobileProcessException {
		Exception ex = null;
		try {
			CheckBlackListResponse checkBlackListResponse = new CheckBlackListResponse();
			CheckBlackListRequest checkBlackListRequest = new CheckBlackListRequest();
			checkBlackListImplServiceImpl.executeMethod(checkBlackListRequest, checkBlackListResponse);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetLogServiceContextService() throws TMobileProcessException {
		Exception ex = null;
		try {
			checkBlackListImplServiceImpl.setLogServiceContextService(new LogServiceContextServiceImpl());
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetCreateServiceContextService() throws TMobileProcessException {
		Exception ex = null;
		try {
			checkBlackListImplServiceImpl.setCreateServiceContextService(new CreateServiceContextServiceImpl());
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetInvokeCheckBlackListService() throws TMobileProcessException {
		Exception ex = null;
		try {
			checkBlackListImplServiceImpl.setInvokeCheckBlackListService(iInvokeCheckBlackListService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetErrorLookupService() throws TMobileProcessException {
		Exception ex = null;
		try {
			checkBlackListImplServiceImpl.setErrorLookupService(new ErrorLookupServiceImpl());
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetSharedServiceContext() throws TMobileProcessException {
		Exception ex = null;
		try {
			checkBlackListImplServiceImpl.setSharedServiceContext(sharedServiceContext);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@AfterClass
	public void tearDownClass() throws Exception {
	}
}
