package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.t_mobile.csi.device.queryimeiwarranty.CSIQueryIMEIWarrantyRequestType;
import com.t_mobile.csi.device.queryimeiwarranty.CSIQueryIMEIWarrantyResponseType;
import com.tmobile.u2.service.IQueryIMEIWarrantySQLService;
import com.tmobile.u2.service.ISerializationQueryIMEIWarrantyTarASFforCSIService;
import com.tmobile.u2.service.impl.InvokeQueryIMEIWarrantyServiceImpl;

import targetserialization.applications.serialization.adaptersimulationflows.warrantydetails.serializationqueryimeiwarrantytarasfforcsi_end_output.SerializationQueryIMEIWarrantyTarASFforCSIEndOutput;
import targetserialization.applications.serialization.adaptersimulationflows.warrantydetails.serializationqueryimeiwarrantytarasfforcsi_start_input.SerializationQueryIMEIWarrantyTarASFforCSIStartInput;

@ExtendWith(MockitoExtension.class)
public class InvokeQueryIMEIWarrantyServiceImplTest {

	@InjectMocks
	private InvokeQueryIMEIWarrantyServiceImpl invokeQueryIMEIWarrantyServiceImpl;

	@Mock
	private IQueryIMEIWarrantySQLService iQueryIMEIWarrantySQLService;

	@Mock
	private ISerializationQueryIMEIWarrantyTarASFforCSIService iSerializationQueryIMEIWarrantyTarASFforCSIService;

	@Before
	public void setup() throws Exception {
		Mockito.doNothing().when(iQueryIMEIWarrantySQLService).executeMethod(
				Mockito.any(CSIQueryIMEIWarrantyRequestType.class),
				Mockito.any(CSIQueryIMEIWarrantyResponseType.class));
		Mockito.doNothing().when(iSerializationQueryIMEIWarrantyTarASFforCSIService).executeMethod(
				Mockito.any(SerializationQueryIMEIWarrantyTarASFforCSIStartInput.class),
				Mockito.any(SerializationQueryIMEIWarrantyTarASFforCSIEndOutput.class));
	}

	@Test
	public void testExecuteMethod() {
		CSIQueryIMEIWarrantyRequestType queryIMEIWarrantyRequest = new CSIQueryIMEIWarrantyRequestType();
		CSIQueryIMEIWarrantyResponseType queryIMEIWarrantyResponse = new CSIQueryIMEIWarrantyResponseType();
		Exception ex = null;
		try {
			invokeQueryIMEIWarrantyServiceImpl.executeMethod(queryIMEIWarrantyRequest, queryIMEIWarrantyResponse);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetSerializationQueryIMEIWarrantyTarASFforCSIService() {
		Exception ex = null;
		try {
			invokeQueryIMEIWarrantyServiceImpl.setSerializationQueryIMEIWarrantyTarASFforCSIService(
					iSerializationQueryIMEIWarrantyTarASFforCSIService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetQueryIMEIWarrantySQLService() {
		Exception ex = null;
		try {
			invokeQueryIMEIWarrantyServiceImpl.setQueryIMEIWarrantySQLService(iQueryIMEIWarrantySQLService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@AfterClass
	public void tearDownClass() throws Exception {
	}
}
