package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import com.tmobile.u2.service.impl.ExecuteSpaceOpHttpsServiceImpl;

import targetcaasclient.applications.caas.endpoints.spaceops.executespaceophttps_end_output.Response;
import targetcaasclient.applications.caas.endpoints.spaceops.executespaceophttps_start_input.SpaceOps;

@ExtendWith(MockitoExtension.class)
public class ExecuteSpaceOpHttpsServiceImplTest {

    @InjectMocks
    private ExecuteSpaceOpHttpsServiceImpl executeSpaceOpHttpsServiceImpl;

    @Mock
    private RestTemplate restTemplate;

    @Before
    public void setup() throws Exception {
    }

    @Test
    public void testSetRestTemplate() {
        // TODO replace MethodName with actual method and return type as well if required;
        ////Mockito.when(restTemplate.MethodName(Mockito.any(clazz.class))).thenReturn(Mockito.any());
        RestTemplate restTemplate = new RestTemplate();
        Exception ex = null;
        try {
            executeSpaceOpHttpsServiceImpl.setRestTemplate(restTemplate);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testExecuteMethod() {
        // TODO replace MethodName with actual method and return type as well if required;
        ////Mockito.when(restTemplate.MethodName(Mockito.any(clazz.class))).thenReturn(Mockito.any());
        SpaceOps spaceOps = new SpaceOps();
        Response response = new Response();
        Exception ex = null;
        try {
            executeSpaceOpHttpsServiceImpl.executeMethod(spaceOps, response);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
