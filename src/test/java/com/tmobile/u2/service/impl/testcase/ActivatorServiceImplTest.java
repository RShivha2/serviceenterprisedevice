package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tmobile.u2.exception.TMobileProcessException;
import com.tmobile.u2.service.impl.ActivatorServiceImpl;
import com.tmobile.u2.shared.service.IPropertiesLoaderU2Service;
import com.tmobile.u2.shared.service.ISensitiveElementsInitializeService;
import com.tmobile.u2.shared.service.ISensitiveElementsXSLTInitializeService;

@ExtendWith(MockitoExtension.class)
public class ActivatorServiceImplTest {

	@InjectMocks
	private ActivatorServiceImpl activatorServiceImpl;

	@Mock
	private ISensitiveElementsInitializeService iSensitiveElementsInitializeService;

	@Mock
	private ISensitiveElementsXSLTInitializeService iSensitiveElementsXSLTInitializeService;

	@Mock
	private IPropertiesLoaderU2Service iPropertiesLoaderU2Service;

	@Before
	public void setup() throws Exception {
	}

	@Test
	public void testExecuteMethod() throws TMobileProcessException {
		Mockito.doNothing().when(iSensitiveElementsInitializeService).executeMethod();
		Mockito.doNothing().when(iSensitiveElementsXSLTInitializeService).executeMethod();
		Mockito.doNothing().when(iPropertiesLoaderU2Service).executeMethod();
		Exception ex = null;
		try {
			activatorServiceImpl.executeMethod();
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@AfterClass
	public void tearDownClass() throws Exception {
	}
}
