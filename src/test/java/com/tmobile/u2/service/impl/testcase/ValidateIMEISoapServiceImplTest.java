package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.t_mobile.erp.services.logistics.MTBlockingStatusCheckResponse;
import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.u2.service.impl.ValidateIMEISoapServiceImpl;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.shared.service.impl.LogProcessDetailsServiceImpl;
import com.tmobile.u2.util.BlockServiceSOAPConnector;
import com.tmobile.u2.util.ServiceEnterpriseSOAPConnector;

import targetsappi.applications.sappi.endpoints.device.validateimeisoap_start_input.ValidateIMEISoapStartInput;

@ExtendWith(MockitoExtension.class)
public class ValidateIMEISoapServiceImplTest {

    @InjectMocks
    private ValidateIMEISoapServiceImpl validateIMEISoapServiceImpl;

    @Mock
    private ILogProcessDetailsService iLogProcessDetailsService;

    @Mock
    private ServiceEnterpriseSOAPConnector serviceEnterpriseSOAPConnector;

    @Mock
    private BlockServiceSOAPConnector blockServiceSOAPConnector;

    @Before
    public void setup() throws Exception {
        // TODO replace MethodName with actual method and return type as well if required;
        Mockito.doNothing().when(iLogProcessDetailsService).executeMethod(Mockito.any(LogProcessDetailsStartInput.class));
        //Mockito.when(serviceEnterpriseSOAPConnector.MethodName(Mockito.any(clazz.class))).thenReturn(Mockito.any());
        //Mockito.when(blockServiceSOAPConnector.MethodName(Mockito.any(clazz.class))).thenReturn(Mockito.any());
    }

    @Test
    public void testExecuteMethod() {
        ValidateIMEISoapStartInput validateIMEISoapStartInput = new ValidateIMEISoapStartInput();
        MTBlockingStatusCheckResponse mtBlockingStatusCheckResponse = new MTBlockingStatusCheckResponse();
        Exception ex = null;
        try {
            validateIMEISoapServiceImpl.executeMethod(validateIMEISoapStartInput, mtBlockingStatusCheckResponse);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetLogProcessDetailsService() {
        Exception ex = null;
        try {
            validateIMEISoapServiceImpl.setLogProcessDetailsService(new LogProcessDetailsServiceImpl());
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetSOAPConnector() {
        Exception ex = null;
        try {
            validateIMEISoapServiceImpl.setSOAPConnector(serviceEnterpriseSOAPConnector);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
