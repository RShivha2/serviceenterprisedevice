package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.t_mobile.csi.device.getimei.CSIGetIMEIRequestType;
import com.t_mobile.csi.device.getimei.CSIGetIMEIResponseType;
import com.tmobile.u2.service.IGetIMEISQLService;
import com.tmobile.u2.service.IGetIMEIStatusJDBCService;
import com.tmobile.u2.service.impl.InvokeGetIMEIStatusServiceImpl;

import targetserialization.applications.serialization.endpoints.device.getimeistatusjdbc_end_output.GetIMEIStatusJDBCEndOutput;
import targetserialization.applications.serialization.endpoints.device.getimeistatusjdbc_start_input.GetIMEIStatusJDBCStartInput;

@ExtendWith(MockitoExtension.class)
public class InvokeGetIMEIStatusServiceImplTest {

    @InjectMocks
    private InvokeGetIMEIStatusServiceImpl invokeGetIMEIStatusServiceImpl;

    @Mock
    private IGetIMEISQLService iGetIMEISQLService;

    @Mock
    private IGetIMEIStatusJDBCService iGetIMEIStatusJDBCService;

    @Before
    public void setup() throws Exception {
        Mockito.doNothing().when(iGetIMEISQLService).executeMethod(Mockito.any(CSIGetIMEIRequestType.class), Mockito.any(CSIGetIMEIResponseType.class));
        Mockito.doNothing().when(iGetIMEIStatusJDBCService).executeMethod(Mockito.any(GetIMEIStatusJDBCStartInput.class), Mockito.any(GetIMEIStatusJDBCEndOutput.class));
    }

    @Test
    public void testExecuteMethod() {
        CSIGetIMEIRequestType csiGetIMEIRequest = new CSIGetIMEIRequestType();
        CSIGetIMEIResponseType csiGetIMEIResponse = new CSIGetIMEIResponseType();
        Exception ex = null;
        try {
            invokeGetIMEIStatusServiceImpl.executeMethod(csiGetIMEIRequest, csiGetIMEIResponse);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetGetIMEISQLService() {
        Exception ex = null;
        try {
            invokeGetIMEIStatusServiceImpl.setGetIMEISQLService(iGetIMEISQLService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetGetIMEIStatusJDBCService() {
        Exception ex = null;
        try {
            invokeGetIMEIStatusServiceImpl.setGetIMEIStatusJDBCService(iGetIMEIStatusJDBCService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
