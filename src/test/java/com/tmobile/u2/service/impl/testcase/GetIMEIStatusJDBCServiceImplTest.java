package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tibco.namespaces.tnt.plugins.jdbc_input.Inputs;
import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.u2.repositories.IGetIMEIStatusJDBCRepository;
import com.tmobile.u2.service.impl.GetIMEIStatusJDBCServiceImpl;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.shared.service.impl.LogProcessDetailsServiceImpl;

import targetserialization.applications.serialization.endpoints.device.getimeistatusjdbc_end_output.GetIMEIStatusJDBCEndOutput;
import targetserialization.applications.serialization.endpoints.device.getimeistatusjdbc_start_input.GetIMEIStatusJDBCStartInput;

@ExtendWith(MockitoExtension.class)
public class GetIMEIStatusJDBCServiceImplTest {

	@InjectMocks
	private GetIMEIStatusJDBCServiceImpl iMEIStatusJDBCServiceImpl;

	@Mock
	private IGetIMEIStatusJDBCRepository iGetIMEIStatusJDBCRepository;

	@Mock
	private ILogProcessDetailsService iLogProcessDetailsService;

	@Before
	public void setup() throws Exception {
		Mockito.when(iGetIMEIStatusJDBCRepository.getIMEIStatus(Mockito.any(Inputs.class))).thenReturn(Mockito.any());
		Mockito.doNothing().when(iLogProcessDetailsService)
				.executeMethod(Mockito.any(LogProcessDetailsStartInput.class));
	}

	@Test
	public void testExecuteMethod() {
		GetIMEIStatusJDBCStartInput getIMEIStatusJDBCStartInput = new GetIMEIStatusJDBCStartInput();
		GetIMEIStatusJDBCEndOutput getIMEIStatusJDBCEndOutput = new GetIMEIStatusJDBCEndOutput();
		Exception ex = null;
		try {
			iMEIStatusJDBCServiceImpl.executeMethod(getIMEIStatusJDBCStartInput, getIMEIStatusJDBCEndOutput);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetIGetIMEIStatusJDBCRepository() {
		Exception ex = null;
		try {
			iMEIStatusJDBCServiceImpl.setIGetIMEIStatusJDBCRepository(iGetIMEIStatusJDBCRepository);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetLogProcessDetailsService() {
		Exception ex = null;
		try {
			iMEIStatusJDBCServiceImpl.setLogProcessDetailsService(new LogProcessDetailsServiceImpl());
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@AfterClass
	public void tearDownClass() throws Exception {
	}
}
