package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.t_mobile.erp.services.logistics.MtQueryIMEIResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.GetDeviceDetailsRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.GetDeviceDetailsResponse;
import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.u2.service.IGetDeviceDetailsOrchService;
import com.tmobile.u2.service.IQueryIMEIForWholesaleSoapService;
import com.tmobile.u2.service.impl.InvokegetDeviceDetailsServiceImpl;
import com.tmobile.u2.shared.service.IHeaderValidationService;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.shared.service.impl.LogProcessDetailsServiceImpl;
import com.tmobile.u2.util.ModuleServiceContext;
import com.tmobile.u2.util.SharedServiceContext;

import sharedservicecontext.sharedprocesses.utilityprocesses.headervalidation_start_input.HeaderValidationStartInput;
import targetsappi.applications.sappi.endpoints.device.queryimeiforwholesalesoap_start_input.QueryIMEIForWholesaleSoapStartInput;

@ExtendWith(MockitoExtension.class)
public class InvokegetDeviceDetailsServiceImplTest {

    @InjectMocks
    private InvokegetDeviceDetailsServiceImpl invokegetDeviceDetailsServiceImpl;

    @Mock
    private IHeaderValidationService iHeaderValidationService;

    @Mock
    private IQueryIMEIForWholesaleSoapService iQueryIMEIForWholesaleSoapService;

    @Mock
    private IGetDeviceDetailsOrchService iGetDeviceDetailsOrchService;

    @Mock
    private ModuleServiceContext moduleServiceContext;

    @Mock
    private ILogProcessDetailsService iLogProcessDetailsService;

    @Mock
    private SharedServiceContext sharedServiceContext;

    @Before
    public void setup() throws Exception {
        // TODO replace MethodName with actual method and return type as well if required;
    	Mockito.when(sharedServiceContext.getRequestValue(Mockito.anyString())).thenReturn(Mockito.any());
    	Mockito.doNothing().when(iLogProcessDetailsService).executeMethod(Mockito.any(LogProcessDetailsStartInput.class));
        Mockito.doNothing().when(iHeaderValidationService).executeMethod(Mockito.any(HeaderValidationStartInput.class));;
        Mockito.doNothing().when(iQueryIMEIForWholesaleSoapService).executeMethod(Mockito.any(QueryIMEIForWholesaleSoapStartInput.class), Mockito.any(MtQueryIMEIResponse.class));
        Mockito.doNothing().when(iGetDeviceDetailsOrchService).executeMethod(Mockito.any(GetDeviceDetailsRequest.class), Mockito.any(GetDeviceDetailsResponse.class));
        Mockito.when(moduleServiceContext.getRequestValue(Mockito.anyString())).thenReturn(Mockito.any());
    }

    @Test
    public void testSetModuleServiceContext() {
        ModuleServiceContext moduleServiceContext = new ModuleServiceContext();
        Exception ex = null;
        try {
            invokegetDeviceDetailsServiceImpl.setModuleServiceContext(moduleServiceContext);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testExecuteMethod() {
        GetDeviceDetailsRequest getDeviceDetailsRequest = new GetDeviceDetailsRequest();
        GetDeviceDetailsResponse getDeviceDetailsResponse = new GetDeviceDetailsResponse();
        Exception ex = null;
        try {
            invokegetDeviceDetailsServiceImpl.executeMethod(getDeviceDetailsRequest, getDeviceDetailsResponse);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetHeaderValidationService() {
        Exception ex = null;
        try {
            invokegetDeviceDetailsServiceImpl.setHeaderValidationService(iHeaderValidationService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetGetDeviceDetailsOrchService() {
        Exception ex = null;
        try {
            invokegetDeviceDetailsServiceImpl.setGetDeviceDetailsOrchService(iGetDeviceDetailsOrchService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetQueryIMEIForWholesaleSoapService() {
        Exception ex = null;
        try {
            invokegetDeviceDetailsServiceImpl.setQueryIMEIForWholesaleSoapService(iQueryIMEIForWholesaleSoapService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetLogProcessDetailsService() {
        Exception ex = null;
        try {
            invokegetDeviceDetailsServiceImpl.setLogProcessDetailsService(new LogProcessDetailsServiceImpl());
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetSharedServiceContext() {
        Exception ex = null;
        try {
            invokegetDeviceDetailsServiceImpl.setSharedServiceContext(sharedServiceContext);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
