package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tmobile.services.productmanagement.deviceenterprise.v1.ManageAppleCareEnrollmentRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.ManageAppleCareEnrollmentResponse;
import com.tmobile.services.schema.dataobject.createservicecontextendoutput.CreateServiceContextOutput;
import com.tmobile.services.schema.dataobject.createservicecontextstartinput.CreateServiceContextInput;
import com.tmobile.services.schema.dataobject.errorlookupendoutput.ErrorLookupEndOutput;
import com.tmobile.services.schema.dataobject.legacyesoaerrorlookupstartinput.LegacyESOAErrorLookupStartInput;
import com.tmobile.services.schema.dataobject.logservicecontextstartinput.LogServiceContextStartInput;
import com.tmobile.u2.service.IInvokeManageAppleCareEnrollmentService;
import com.tmobile.u2.service.impl.ManageAppleCareEnrollmentImplServiceImpl;
import com.tmobile.u2.shared.service.ICreateServiceContextService;
import com.tmobile.u2.shared.service.IErrorLookupService;
import com.tmobile.u2.shared.service.ILogServiceContextService;
import com.tmobile.u2.shared.service.impl.CreateServiceContextServiceImpl;
import com.tmobile.u2.shared.service.impl.ErrorLookupServiceImpl;
import com.tmobile.u2.shared.service.impl.LogServiceContextServiceImpl;
import com.tmobile.u2.util.SharedServiceContext;

@ExtendWith(MockitoExtension.class)
public class ManageAppleCareEnrollmentImplServiceImplTest {

    @InjectMocks
    private ManageAppleCareEnrollmentImplServiceImpl manageAppleCareEnrollmentImplServiceImpl;

    @Mock
    private IErrorLookupService iErrorLookupService;

    @Mock
    private ICreateServiceContextService iCreateServiceContextService;

    @Mock
    private ILogServiceContextService iLogServiceContextService;

    @Mock
    private SharedServiceContext sharedServiceContext;

    @Mock
    private IInvokeManageAppleCareEnrollmentService iInvokeManageAppleCareEnrollmentService;

    @Before
    public void setup() throws Exception {
        // TODO replace MethodName with actual method and return type as well if required;
        Mockito.doNothing().when(iErrorLookupService).executeMethod(Mockito.any(LegacyESOAErrorLookupStartInput.class), Mockito.any(ErrorLookupEndOutput.class));
        Mockito.doNothing().when(iCreateServiceContextService).executeMethod(Mockito.any(CreateServiceContextInput.class), Mockito.any(CreateServiceContextOutput.class));
        Mockito.doNothing().when(iLogServiceContextService).executeMethod(Mockito.any(LogServiceContextStartInput.class));
        Mockito.when(sharedServiceContext.getRequestValue(Mockito.anyString())).thenReturn(Mockito.any());
        Mockito.doNothing().when(iInvokeManageAppleCareEnrollmentService).executeMethod(Mockito.any(ManageAppleCareEnrollmentRequest.class), Mockito.any(ManageAppleCareEnrollmentResponse.class));
    }

    @Test
    public void testExecuteMethod() {
        ManageAppleCareEnrollmentRequest manageAppleCareEnrollmentRequest = new ManageAppleCareEnrollmentRequest();
        ManageAppleCareEnrollmentResponse manageAppleCareEnrollmentResponse = new ManageAppleCareEnrollmentResponse();
        Exception ex = null;
        try {
            manageAppleCareEnrollmentImplServiceImpl.executeMethod(manageAppleCareEnrollmentRequest, manageAppleCareEnrollmentResponse);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetLogServiceContextService() {
        Exception ex = null;
        try {
            manageAppleCareEnrollmentImplServiceImpl.setLogServiceContextService(new LogServiceContextServiceImpl());
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetCreateServiceContextService() {
        Exception ex = null;
        try {
            manageAppleCareEnrollmentImplServiceImpl.setCreateServiceContextService(new CreateServiceContextServiceImpl());
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetInvokeManageAppleCareEnrollmentService() {
        Exception ex = null;
        try {
            manageAppleCareEnrollmentImplServiceImpl.setInvokeManageAppleCareEnrollmentService(iInvokeManageAppleCareEnrollmentService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetErrorLookupService() {
        Exception ex = null;
        try {
            manageAppleCareEnrollmentImplServiceImpl.setErrorLookupService(new ErrorLookupServiceImpl());
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetSharedServiceContext() {
        Exception ex = null;
        try {
            manageAppleCareEnrollmentImplServiceImpl.setSharedServiceContext(sharedServiceContext);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
