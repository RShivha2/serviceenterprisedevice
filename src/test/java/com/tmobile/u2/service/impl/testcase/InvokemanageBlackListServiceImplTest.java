package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tmobile.services.productmanagement.deviceenterprise.v1.ManageBlackListRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.ManageBlackListResponse;
import com.tmobile.u2.service.IManageBlackListService;
import com.tmobile.u2.service.impl.InvokemanageBlackListServiceImpl;
import com.tmobile.u2.shared.service.IHeaderValidationService;
import com.tmobile.u2.util.SharedServiceContext;

import sharedservicecontext.sharedprocesses.utilityprocesses.headervalidation_start_input.HeaderValidationStartInput;

@ExtendWith(MockitoExtension.class)
public class InvokemanageBlackListServiceImplTest {

    @InjectMocks
    private InvokemanageBlackListServiceImpl invokemanageBlackListServiceImpl;

    @Mock
    private IHeaderValidationService iHeaderValidationService;

    @Mock
    private IManageBlackListService iManageBlackListService;

    @Mock
    private SharedServiceContext sharedServiceContext;

    @Before
    public void setup() throws Exception {
        // TODO replace MethodName with actual method and return type as well if required;
    	Mockito.when(sharedServiceContext.getRequestValue(Mockito.anyString())).thenReturn(Mockito.any());
        Mockito.doNothing().when(iHeaderValidationService).executeMethod(Mockito.any(HeaderValidationStartInput.class));;
        Mockito.doNothing().when(iManageBlackListService).executeMethod(Mockito.any(ManageBlackListRequest.class), Mockito.any(ManageBlackListResponse.class));
    }

    @Test
    public void testExecuteMethod() {
        ManageBlackListRequest manageBlackListRequest = new ManageBlackListRequest();
        ManageBlackListResponse manageBlackListResponse = new ManageBlackListResponse();
        Exception ex = null;
        try {
            invokemanageBlackListServiceImpl.executeMethod(manageBlackListRequest, manageBlackListResponse);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetHeaderValidationService() {
        Exception ex = null;
        try {
            invokemanageBlackListServiceImpl.setHeaderValidationService(iHeaderValidationService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetSharedServiceContext() {
        Exception ex = null;
        try {
            invokemanageBlackListServiceImpl.setSharedServiceContext(sharedServiceContext);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetManageBlackListService() {
        Exception ex = null;
        try {
            invokemanageBlackListServiceImpl.setManageBlackListService(iManageBlackListService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
