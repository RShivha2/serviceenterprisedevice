package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.u2.service.impl.LogoutSOAPServiceImpl;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.shared.service.impl.LogProcessDetailsServiceImpl;
import com.tmobile.u2.util.AuthenticateSOAPConnector;

import targetgsx.applications.gsx.endpoints.warranty.logoutsoap_start_input.LogoutSOAPStartInput;

@ExtendWith(MockitoExtension.class)
public class LogoutSOAPServiceImplTest {

    @InjectMocks
    private LogoutSOAPServiceImpl logoutSOAPServiceImpl;

    @Mock
    private AuthenticateSOAPConnector authenticateSOAPConnector;

    @Mock
    private ILogProcessDetailsService iLogProcessDetailsService;

    @Before
    public void setup() throws Exception {
        // TODO replace MethodName with actual method and return type as well if required;
        //Mockito.when(authenticateSOAPConnector.MethodName(Mockito.any(clazz.class))).thenReturn(Mockito.any());
        Mockito.doNothing().when(iLogProcessDetailsService).executeMethod(Mockito.any(LogProcessDetailsStartInput.class));
    }

    @Test
    public void testExecuteMethod() {
        LogoutSOAPStartInput logoutSOAPStartInput = new LogoutSOAPStartInput();
        Exception ex = null;
        try {
            logoutSOAPServiceImpl.executeMethod(logoutSOAPStartInput);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetLogProcessDetailsService() {
        Exception ex = null;
        try {
            logoutSOAPServiceImpl.setLogProcessDetailsService(new LogProcessDetailsServiceImpl());
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetSOAPConnector() {
        Exception ex = null;
        try {
            logoutSOAPServiceImpl.setSOAPConnector(authenticateSOAPConnector);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
