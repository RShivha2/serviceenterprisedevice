package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tmobile.u2.service.impl.BlanketPOSyncSoapServiceImpl;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.util.ServiceEnterpriseSOAPConnector;

import targetsappi.applications.sappi.endpoints.order.blanketposyncsoap_end_output.BlanketPOSyncSoapEndOutput;
import targetsappi.applications.sappi.endpoints.order.blanketposyncsoap_start_input.BlanketPOSyncSoapStartInput;

@ExtendWith(MockitoExtension.class)
public class BlanketPOSyncSoapServiceImplTest {

    @InjectMocks
    private BlanketPOSyncSoapServiceImpl blanketPOSyncSoapServiceImpl;

    @Mock
    private ILogProcessDetailsService iLogProcessDetailsService;

    @Mock
    private ServiceEnterpriseSOAPConnector serviceEnterpriseSOAPConnector;

    @Before
    public void setup() throws Exception {
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }

    @Test
    public void testExecuteMethod() {
        BlanketPOSyncSoapStartInput blanketPOSyncSoapStartInput = new BlanketPOSyncSoapStartInput();
        BlanketPOSyncSoapEndOutput blanketPOSyncSoapEndOutput = new BlanketPOSyncSoapEndOutput();
        Exception ex = null;
        try {
            blanketPOSyncSoapServiceImpl.executeMethod(blanketPOSyncSoapStartInput, blanketPOSyncSoapEndOutput);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }
}
