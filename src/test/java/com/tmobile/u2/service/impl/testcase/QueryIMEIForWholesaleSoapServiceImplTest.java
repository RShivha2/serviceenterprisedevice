package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.t_mobile.erp.services.logistics.MtQueryIMEIResponse;
import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.u2.service.impl.QueryIMEIForWholesaleSoapServiceImpl;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.shared.service.impl.LogProcessDetailsServiceImpl;
import com.tmobile.u2.util.ServiceEnterpriseSOAPConnector;

import targetsappi.applications.sappi.endpoints.device.queryimeiforwholesalesoap_start_input.QueryIMEIForWholesaleSoapStartInput;

@ExtendWith(MockitoExtension.class)
public class QueryIMEIForWholesaleSoapServiceImplTest {

    @InjectMocks
    private QueryIMEIForWholesaleSoapServiceImpl queryIMEIForWholesaleSoapServiceImpl;

    @Mock
    private ILogProcessDetailsService iLogProcessDetailsService;

    @Mock
    private ServiceEnterpriseSOAPConnector serviceEnterpriseSOAPConnector;

    @Before
    public void setup() throws Exception {
        Mockito.doNothing().when(iLogProcessDetailsService).executeMethod(Mockito.any(LogProcessDetailsStartInput.class));
        //Mockito.when(serviceEnterpriseSOAPConnector.callWebService(Mockito.any(clazz.class))).thenReturn(Mockito.any());
    }

    @Test
    public void testExecuteMethod() {
        QueryIMEIForWholesaleSoapStartInput queryIMEIForWholesaleSoapStartInput = new QueryIMEIForWholesaleSoapStartInput();
        MtQueryIMEIResponse mtQueryimeiresponse = new MtQueryIMEIResponse();
        Exception ex = null;
        try {
            queryIMEIForWholesaleSoapServiceImpl.executeMethod(queryIMEIForWholesaleSoapStartInput, mtQueryimeiresponse);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetLogProcessDetailsService() {
        Exception ex = null;
        try {
            queryIMEIForWholesaleSoapServiceImpl.setLogProcessDetailsService(new LogProcessDetailsServiceImpl());
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetSOAPConnector() {
        Exception ex = null;
        try {
            queryIMEIForWholesaleSoapServiceImpl.setSOAPConnector(serviceEnterpriseSOAPConnector);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
