package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import com.tmobile.services.base.Header;
import com.tmobile.services.base.Sender;
import com.tmobile.services.productmanagement.deviceenterprise.v1.PostDeviceAssesmentResultsRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.PostDeviceAssesmentResultsResponse;
import com.tmobile.u2.service.impl.InvokePostDeviceAssesmentResultsServiceImpl;
import com.tmobile.u2.shared.service.IHeaderValidationService;
import com.tmobile.u2.util.SharedServiceContext;

import sharedservicecontext.sharedprocesses.utilityprocesses.headervalidation_start_input.HeaderValidationStartInput;

@ExtendWith(MockitoExtension.class)
public class InvokePostDeviceAssesmentResultsServiceImplTest {

    @InjectMocks
    private InvokePostDeviceAssesmentResultsServiceImpl invokePostDeviceAssesmentResultsServiceImpl;

    @Mock
    private JmsTemplate jmsTemplate;

    @Mock
    private IHeaderValidationService iHeaderValidationService;

    @Mock
    private SharedServiceContext sharedServiceContext;

    @Before
    public void setup() throws Exception {
        // TODO replace MethodName with actual method and return type as well if required;
    	Mockito.when(sharedServiceContext.getRequestValue(Mockito.anyString())).thenReturn(Mockito.any());
        Mockito.doNothing().when(jmsTemplate).send(Mockito.anyString(), Mockito.any(MessageCreator.class));
        Mockito.doNothing().when(iHeaderValidationService).executeMethod(Mockito.any(HeaderValidationStartInput.class));;
    }

    @Test
    public void testExecuteMethod() {
        PostDeviceAssesmentResultsRequest postDeviceAssesmentResultsRequest = new PostDeviceAssesmentResultsRequest();
		postDeviceAssesmentResultsRequest.setHeader(new Header());
		postDeviceAssesmentResultsRequest.getHeader().setSender(new Sender());
        PostDeviceAssesmentResultsResponse postDeviceAssesmentResultsResponse = new PostDeviceAssesmentResultsResponse();
        Exception ex = null;
        try {
            invokePostDeviceAssesmentResultsServiceImpl.executeMethod(postDeviceAssesmentResultsRequest, postDeviceAssesmentResultsResponse);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetHeaderValidationService() {
        Exception ex = null;
        try {
            invokePostDeviceAssesmentResultsServiceImpl.setHeaderValidationService(iHeaderValidationService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetJmsTemplate() {
        Exception ex = null;
        try {
            invokePostDeviceAssesmentResultsServiceImpl.setJmsTemplate(jmsTemplate);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetSharedServiceContext() {
        Exception ex = null;
        try {
            invokePostDeviceAssesmentResultsServiceImpl.setSharedServiceContext(sharedServiceContext);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
