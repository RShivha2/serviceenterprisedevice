package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tmobile.services.productmanagement.deviceenterprise.v1.RegisterDeviceForFirstUseRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.RegisterDeviceForFirstUseResponse;
import com.tmobile.u2.service.impl.RegisterDeviceForFirstUseDummyImplServiceImpl;

@ExtendWith(MockitoExtension.class)
public class RegisterDeviceForFirstUseDummyImplServiceImplTest {

	@InjectMocks
	private RegisterDeviceForFirstUseDummyImplServiceImpl registerDeviceForFirstUseDummyImplServiceImpl;

	@Before
	public void setup() throws Exception {
	}

	@Test
	public void testExecuteMethod() {
		RegisterDeviceForFirstUseRequest registerDeviceForFirstUseRequest = new RegisterDeviceForFirstUseRequest();
		RegisterDeviceForFirstUseResponse registerDeviceForFirstUseResponse = new RegisterDeviceForFirstUseResponse();
		Exception ex = null;
		try {
			registerDeviceForFirstUseDummyImplServiceImpl.executeMethod(registerDeviceForFirstUseRequest,
					registerDeviceForFirstUseResponse);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@AfterClass
	public void tearDownClass() throws Exception {
	}
}
