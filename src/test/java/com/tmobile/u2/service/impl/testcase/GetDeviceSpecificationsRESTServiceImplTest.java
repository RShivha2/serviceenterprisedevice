package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import com.example.namespaces.tns._1573645539824.ConvertJSONtoXMLRequest;
import com.example.namespaces.tns._1573646434454.ConvertJSONtoXMLResponse;
import com.t_mobile.schema.networkmanagement.union.resources.schemas.devicespecifications.DeviceSpecificationDetails;
import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.u2.service.IConvertJSONtoXMLService;
import com.tmobile.u2.service.impl.GetDeviceSpecificationsRESTServiceImpl;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.shared.service.impl.LogProcessDetailsServiceImpl;

import targetunion.applications.union.endpoints.getdevicespecificationsrest_start_input.GetDeviceSpecificationsRequest;

@ExtendWith(MockitoExtension.class)
public class GetDeviceSpecificationsRESTServiceImplTest {

	@InjectMocks
	private GetDeviceSpecificationsRESTServiceImpl deviceSpecificationsRESTServiceImpl;

	@Mock
	private RestTemplate restTemplate;

	@Mock
	private IConvertJSONtoXMLService iConvertJSONtoXMLService;

	@Mock
	private ILogProcessDetailsService iLogProcessDetailsService;

	@Before
	public void setup() throws Exception {
		// Mockito.when(restTemplate.MethodName(Mockito.any(clazz.class))).thenReturn(Mockito.any());
		Mockito.doNothing().when(iConvertJSONtoXMLService).executeMethod(Mockito.any(ConvertJSONtoXMLRequest.class),
				Mockito.any(ConvertJSONtoXMLResponse.class));
		Mockito.doNothing().when(iLogProcessDetailsService)
				.executeMethod(Mockito.any(LogProcessDetailsStartInput.class));
	}

	@Test
	public void testSetRestTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		Exception ex = null;
		try {
			deviceSpecificationsRESTServiceImpl.setRestTemplate(restTemplate);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testExecuteMethod() {
		GetDeviceSpecificationsRequest getDeviceSpecificationsRequest = new GetDeviceSpecificationsRequest();
		DeviceSpecificationDetails deviceSpecificationDetails = new DeviceSpecificationDetails();
		Exception ex = null;
		try {
			deviceSpecificationsRESTServiceImpl.executeMethod(getDeviceSpecificationsRequest,
					deviceSpecificationDetails);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetLogProcessDetailsService() {
		Exception ex = null;
		try {
			deviceSpecificationsRESTServiceImpl.setLogProcessDetailsService(new LogProcessDetailsServiceImpl());
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetConvertJSONtoXMLService() {
		Exception ex = null;
		try {
			deviceSpecificationsRESTServiceImpl.setConvertJSONtoXMLService(iConvertJSONtoXMLService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@AfterClass
	public void tearDownClass() throws Exception {
	}
}
