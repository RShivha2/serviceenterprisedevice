package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tmobile.services.productmanagement.deviceenterprise.v1.UpdateDeviceOrderRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.UpdateDeviceOrderResponse;
import com.tmobile.services.schema.dataobject.createservicecontextendoutput.CreateServiceContextOutput;
import com.tmobile.services.schema.dataobject.createservicecontextstartinput.CreateServiceContextInput;
import com.tmobile.services.schema.dataobject.errorlookupendoutput.ErrorLookupEndOutput;
import com.tmobile.services.schema.dataobject.legacyesoaerrorlookupstartinput.LegacyESOAErrorLookupStartInput;
import com.tmobile.services.schema.dataobject.logservicecontextstartinput.LogServiceContextStartInput;
import com.tmobile.u2.service.IInvokeUpdateDeviceOrderService;
import com.tmobile.u2.service.impl.UpdateDeviceOrderImplServiceImpl;
import com.tmobile.u2.shared.service.ICreateServiceContextService;
import com.tmobile.u2.shared.service.IErrorLookupService;
import com.tmobile.u2.shared.service.ILogServiceContextService;
import com.tmobile.u2.shared.service.impl.CreateServiceContextServiceImpl;
import com.tmobile.u2.shared.service.impl.ErrorLookupServiceImpl;
import com.tmobile.u2.shared.service.impl.LogServiceContextServiceImpl;
import com.tmobile.u2.util.SharedServiceContext;

@ExtendWith(MockitoExtension.class)
public class UpdateDeviceOrderImplServiceImplTest {

    @InjectMocks
    private UpdateDeviceOrderImplServiceImpl updateDeviceOrderImplServiceImpl;

    @Mock
    private IErrorLookupService iErrorLookupService;

    @Mock
    private ICreateServiceContextService iCreateServiceContextService;

    @Mock
    private ILogServiceContextService iLogServiceContextService;

    @Mock
    private SharedServiceContext sharedServiceContext;

    @Mock
    private IInvokeUpdateDeviceOrderService iInvokeUpdateDeviceOrderService;

    @Before
    public void setup() throws Exception {
        Mockito.doNothing().when(iErrorLookupService).executeMethod(Mockito.any(LegacyESOAErrorLookupStartInput.class), Mockito.any(ErrorLookupEndOutput.class));
        Mockito.doNothing().when(iCreateServiceContextService).executeMethod(Mockito.any(CreateServiceContextInput.class), Mockito.any(CreateServiceContextOutput.class));
        Mockito.doNothing().when(iLogServiceContextService).executeMethod(Mockito.any(LogServiceContextStartInput.class));
        Mockito.when(sharedServiceContext.getRequestValue(Mockito.anyString())).thenReturn(Mockito.any());
        Mockito.doNothing().when(iInvokeUpdateDeviceOrderService).executeMethod(Mockito.any(UpdateDeviceOrderRequest.class), Mockito.any(UpdateDeviceOrderResponse.class));
    }

    @Test
    public void testExecuteMethod() {
        UpdateDeviceOrderRequest updateDeviceOrderRequest = new UpdateDeviceOrderRequest();
        UpdateDeviceOrderResponse updateDeviceOrderResponse = new UpdateDeviceOrderResponse();
        Exception ex = null;
        try {
            updateDeviceOrderImplServiceImpl.executeMethod(updateDeviceOrderRequest, updateDeviceOrderResponse);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetInvokeUpdateDeviceOrderService() {
        Exception ex = null;
        try {
            updateDeviceOrderImplServiceImpl.setInvokeUpdateDeviceOrderService(iInvokeUpdateDeviceOrderService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetLogServiceContextService() {
        Exception ex = null;
        try {
            updateDeviceOrderImplServiceImpl.setLogServiceContextService(new LogServiceContextServiceImpl());
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetCreateServiceContextService() {
        Exception ex = null;
        try {
            updateDeviceOrderImplServiceImpl.setCreateServiceContextService(new CreateServiceContextServiceImpl());
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetErrorLookupService() {
        Exception ex = null;
        try {
            updateDeviceOrderImplServiceImpl.setErrorLookupService(new ErrorLookupServiceImpl());
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetSharedServiceContext() {
        Exception ex = null;
        try {
            updateDeviceOrderImplServiceImpl.setSharedServiceContext(sharedServiceContext);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
