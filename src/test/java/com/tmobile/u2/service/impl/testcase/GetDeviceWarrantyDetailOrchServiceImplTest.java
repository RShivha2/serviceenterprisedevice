package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.u2.service.IAuthenticateSOAPService;
import com.tmobile.u2.service.IGetUnitDetailsService;
import com.tmobile.u2.service.IGetWarrantyStatusHTTPService;
import com.tmobile.u2.service.ILogoutSOAPService;
import com.tmobile.u2.service.IRefreshAccessTokensService;
import com.tmobile.u2.service.impl.GetDeviceWarrantyDetailOrchServiceImpl;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.shared.service.impl.LogProcessDetailsServiceImpl;

import generated.GetUnitDetailsResponse;
import targetgsx.applications.gsx.endpoints.warranty.authenticatesoap_end_output.AuthenticateSOAPEndOutput;
import targetgsx.applications.gsx.endpoints.warranty.authenticatesoap_start_input.AuthenticateSOAPStartInput;
import targetgsx.applications.gsx.endpoints.warranty.getwarrantystatushttp_end_output.GetWarrantyStatusHTTPEndOutput;
import targetgsx.applications.gsx.endpoints.warranty.getwarrantystatushttp_start_input.GetWarrantyStatusHTTPStartInput;
import targetgsx.applications.gsx.endpoints.warranty.logoutsoap_start_input.LogoutSOAPStartInput;
import targetgsx.applications.gsx.orchestrations.warranty.getdevicewarrantydetailorch_end_output.GetDeviceDetailsOrchEndOutput;
import targetgsx.applications.gsx.orchestrations.warranty.getdevicewarrantydetailorch_start_input.GetDeviceDetailsOrchStartInput;

@ExtendWith(MockitoExtension.class)
public class GetDeviceWarrantyDetailOrchServiceImplTest {

	@InjectMocks
	private GetDeviceWarrantyDetailOrchServiceImpl deviceWarrantyDetailOrchServiceImpl;

	@Mock
	private IAuthenticateSOAPService iAuthenticateSOAPService;

	@Mock
	private IGetWarrantyStatusHTTPService iGetWarrantyStatusHTTPService;

	@Mock
	private ILogProcessDetailsService iLogProcessDetailsService;

	@Mock
	private ILogoutSOAPService iLogoutSOAPService;

	@Mock
	private IRefreshAccessTokensService iRefreshAccessTokensService;

	@Mock
	private IGetUnitDetailsService iGetUnitDetailsService;

	@Before
	public void setup() throws Exception {
		Mockito.doNothing().when(iLogProcessDetailsService)
				.executeMethod(Mockito.any(LogProcessDetailsStartInput.class));
		Mockito.doNothing().when(iAuthenticateSOAPService).executeMethod(Mockito.any(AuthenticateSOAPStartInput.class),
				Mockito.any(AuthenticateSOAPEndOutput.class));
		Mockito.doNothing().when(iGetWarrantyStatusHTTPService).executeMethod(
				Mockito.any(GetWarrantyStatusHTTPStartInput.class), Mockito.any(GetWarrantyStatusHTTPEndOutput.class));
		Mockito.doNothing().when(iLogoutSOAPService).executeMethod(Mockito.any(LogoutSOAPStartInput.class));
		Mockito.doNothing().when(iRefreshAccessTokensService)
				.executeMethod(Mockito.any(com.example.namespaces.tns._1593722054053.Element.class));
		Mockito.doNothing().when(iGetUnitDetailsService).executeMethod(
				Mockito.any(com.example.namespaces.tns._1593633254125.Root.class),
				Mockito.any(GetUnitDetailsResponse.class));
	}

	@Test
	public void testExecuteMethod() {
		GetDeviceDetailsOrchStartInput getDeviceWarrantyDetailOrchStartInput = new GetDeviceDetailsOrchStartInput();
		GetDeviceDetailsOrchEndOutput getDeviceWarrantyDetailOrchEndOutput = new GetDeviceDetailsOrchEndOutput();
		Exception ex = null;
		try {
			deviceWarrantyDetailOrchServiceImpl.executeMethod(getDeviceWarrantyDetailOrchStartInput,
					getDeviceWarrantyDetailOrchEndOutput);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetLogProcessDetailsService() {
		Exception ex = null;
		try {
			deviceWarrantyDetailOrchServiceImpl.setLogProcessDetailsService(new LogProcessDetailsServiceImpl());
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetGetUnitDetailsService() {
		Exception ex = null;
		try {
			deviceWarrantyDetailOrchServiceImpl.setGetUnitDetailsService(iGetUnitDetailsService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetGetWarrantyStatusHTTPService() {
		Exception ex = null;
		try {
			deviceWarrantyDetailOrchServiceImpl.setGetWarrantyStatusHTTPService(iGetWarrantyStatusHTTPService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetRefreshAccessTokensService() {
		Exception ex = null;
		try {
			deviceWarrantyDetailOrchServiceImpl.setRefreshAccessTokensService(iRefreshAccessTokensService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetLogoutSOAPService() {
		Exception ex = null;
		try {
			deviceWarrantyDetailOrchServiceImpl.setLogoutSOAPService(iLogoutSOAPService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetAuthenticateSOAPService() {
		Exception ex = null;
		try {
			deviceWarrantyDetailOrchServiceImpl.setAuthenticateSOAPService(iAuthenticateSOAPService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@AfterClass
	public void tearDownClass() throws Exception {
	}
}
