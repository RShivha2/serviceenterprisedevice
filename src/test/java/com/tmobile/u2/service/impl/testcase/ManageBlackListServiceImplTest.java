package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.t_mobile.erp.services.logistics.MTBlockingStatusCheckResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.ManageBlackListRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.ManageBlackListResponse;
import com.tmobile.u2.service.IManageBlackListSoapService;
import com.tmobile.u2.service.IManageBlackListWholesaleSoapService;
import com.tmobile.u2.service.IValidateIMEISoapService;
import com.tmobile.u2.service.impl.ManageBlackListServiceImpl;

import targetsappi.applications.sappi.endpoints.device.manageblacklistsoap_end_output.ManageBlackListSoapEndOutput;
import targetsappi.applications.sappi.endpoints.device.manageblacklistwholesalesoap_end_output.ManageBlackListWholesaleSoapEndOutput;
import targetsappi.applications.sappi.endpoints.device.manageblacklistwholesalesoap_start_input.Event;
import targetsappi.applications.sappi.endpoints.device.validateimeisoap_start_input.ValidateIMEISoapStartInput;

@ExtendWith(MockitoExtension.class)
public class ManageBlackListServiceImplTest {

    @InjectMocks
    private ManageBlackListServiceImpl manageBlackListServiceImpl;

    @Mock
    private IValidateIMEISoapService iValidateIMEISoapService;

    @Mock
    private IManageBlackListSoapService iManageBlackListSoapService;

    @Mock
    private IManageBlackListWholesaleSoapService iManageBlackListWholesaleSoapService;

    @Before
    public void setup() throws Exception {
        // TODO replace MethodName with actual method and return type as well if required;
        Mockito.doNothing().when(iValidateIMEISoapService).executeMethod(Mockito.any(ValidateIMEISoapStartInput.class), Mockito.any(MTBlockingStatusCheckResponse.class));;
        Mockito.doNothing().when(iManageBlackListSoapService).executeMethod(Mockito.any(Event.class), Mockito.any(ManageBlackListSoapEndOutput.class));
        Mockito.doNothing().when(iManageBlackListWholesaleSoapService).executeMethod(Mockito.any(Event.class), Mockito.any(ManageBlackListWholesaleSoapEndOutput.class));
    }

    @Test
    public void testExecuteMethod() {
        ManageBlackListRequest manageBlackListRequest = new ManageBlackListRequest();
        ManageBlackListResponse manageBlackListResponse = new ManageBlackListResponse();
        Exception ex = null;
        try {
            manageBlackListServiceImpl.executeMethod(manageBlackListRequest, manageBlackListResponse);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetValidateIMEISoapService() {
        Exception ex = null;
        try {
            manageBlackListServiceImpl.setValidateIMEISoapService(iValidateIMEISoapService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetManageBlackListWholesaleSoapService() {
        Exception ex = null;
        try {
            manageBlackListServiceImpl.setManageBlackListWholesaleSoapService(iManageBlackListWholesaleSoapService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetManageBlackListSoapService() {
        Exception ex = null;
        try {
            manageBlackListServiceImpl.setManageBlackListSoapService(iManageBlackListSoapService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
