package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.u2.service.impl.GetWarrantyStatusHTTPServiceImpl;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.shared.service.impl.LogProcessDetailsServiceImpl;

import targetgsx.applications.gsx.endpoints.warranty.getwarrantystatushttp_end_output.GetWarrantyStatusHTTPEndOutput;
import targetgsx.applications.gsx.endpoints.warranty.getwarrantystatushttp_start_input.GetWarrantyStatusHTTPStartInput;

@ExtendWith(MockitoExtension.class)
public class GetWarrantyStatusHTTPServiceImplTest {

	@InjectMocks
	private GetWarrantyStatusHTTPServiceImpl warrantyStatusHTTPServiceImpl;

	@Mock
	private RestTemplate restTemplate;

	@Mock
	private ILogProcessDetailsService iLogProcessDetailsService;

	@Before
	public void setup() throws Exception {
		// TODO replace MethodName with actual method and return type as well if
		// required;
		// Mockito.when(restTemplate.MethodName(Mockito.any(clazz.class))).thenReturn(Mockito.any());
		Mockito.doNothing().when(iLogProcessDetailsService)
				.executeMethod(Mockito.any(LogProcessDetailsStartInput.class));
	}

	@Test
	public void testSetRestTemplate() {
		Exception ex = null;
		try {
			warrantyStatusHTTPServiceImpl.setRestTemplate(restTemplate);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testExecuteMethod() {
		GetWarrantyStatusHTTPStartInput getWarrantyStatusHTTPStartInput = new GetWarrantyStatusHTTPStartInput();
		GetWarrantyStatusHTTPEndOutput getWarrantyStatusHTTPEndOutput = new GetWarrantyStatusHTTPEndOutput();
		Exception ex = null;
		try {
			warrantyStatusHTTPServiceImpl.executeMethod(getWarrantyStatusHTTPStartInput,
					getWarrantyStatusHTTPEndOutput);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetLogProcessDetailsService() {
		Exception ex = null;
		try {
			warrantyStatusHTTPServiceImpl.setLogProcessDetailsService(new LogProcessDetailsServiceImpl());
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@AfterClass
	public void tearDownClass() throws Exception {
	}
}
