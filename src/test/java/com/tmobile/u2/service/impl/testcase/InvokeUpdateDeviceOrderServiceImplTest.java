package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.oracle.xmlns.apps.mdm.customer.UpdateOrderOutput;
import com.t_mobile.u2.xml.tmolineordersio.ListOfTMOLineOrdersIO;
import com.tmobile.services.productmanagement.deviceenterprise.v1.UpdateDeviceOrderRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.UpdateDeviceOrderResponse;
import com.tmobile.u2.service.IUpdateOrderSoapService;
import com.tmobile.u2.service.impl.InvokeUpdateDeviceOrderServiceImpl;
import com.tmobile.u2.shared.service.IHeaderValidationService;
import com.tmobile.u2.util.SharedServiceContext;

import sharedservicecontext.sharedprocesses.utilityprocesses.headervalidation_start_input.HeaderValidationStartInput;

@ExtendWith(MockitoExtension.class)
public class InvokeUpdateDeviceOrderServiceImplTest {

    @InjectMocks
    private InvokeUpdateDeviceOrderServiceImpl invokeUpdateDeviceOrderServiceImpl;

    @Mock
    private IHeaderValidationService iHeaderValidationService;

    @Mock
    private IUpdateOrderSoapService iUpdateOrderSoapService;

    @Mock
    private SharedServiceContext sharedServiceContext;

    @Before
    public void setup() throws Exception {
    	Mockito.when(sharedServiceContext.getRequestValue(Mockito.anyString())).thenReturn(Mockito.any());
        Mockito.doNothing().when(iHeaderValidationService).executeMethod(Mockito.any(HeaderValidationStartInput.class));;
        Mockito.doNothing().when(iUpdateOrderSoapService).executeMethod(Mockito.any(ListOfTMOLineOrdersIO.class), Mockito.any(UpdateOrderOutput.class));
    }

    @Test
    public void testExecuteMethod() {
        UpdateDeviceOrderRequest updateDeviceOrderRequest = new UpdateDeviceOrderRequest();
        UpdateDeviceOrderResponse updateDeviceOrderResponse = new UpdateDeviceOrderResponse();
        Exception ex = null;
        try {
            invokeUpdateDeviceOrderServiceImpl.executeMethod(updateDeviceOrderRequest, updateDeviceOrderResponse);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetHeaderValidationService() {
        Exception ex = null;
        try {
            invokeUpdateDeviceOrderServiceImpl.setHeaderValidationService(iHeaderValidationService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetSharedServiceContext() {
        Exception ex = null;
        try {
            invokeUpdateDeviceOrderServiceImpl.setSharedServiceContext(sharedServiceContext);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetUpdateOrderSoapService() {
        Exception ex = null;
        try {
            invokeUpdateDeviceOrderServiceImpl.setUpdateOrderSoapService(iUpdateOrderSoapService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
