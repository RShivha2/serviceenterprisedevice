package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckAppleCareCompatabilityRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckAppleCareCompatabilityResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckBlackListRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckBlackListResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckDeviceCommissionEligiblityRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckDeviceCommissionEligiblityResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.GetAppleCareEnrollmentRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.GetAppleCareEnrollmentResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.GetDeviceDetailsRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.GetDeviceDetailsResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.ManageAppleCareEnrollmentRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.ManageAppleCareEnrollmentResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.ManageBlackListRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.ManageBlackListResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.PostDeviceAssesmentResultsRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.PostDeviceAssesmentResultsResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.RegisterDeviceForFirstUseRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.RegisterDeviceForFirstUseResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.SendDeviceUnlockCodeRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.SendDeviceUnlockCodeResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.UpdateDeviceOrderRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.UpdateDeviceOrderResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.UpdateUnlockEligibilityRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.UpdateUnlockEligibilityResponse;
import com.tmobile.u2.service.ICheckAppleCareCompatabilityImplService;
import com.tmobile.u2.service.ICheckBlackListImplService;
import com.tmobile.u2.service.ICheckDeviceCommissionEligiblityDummyImplService;
import com.tmobile.u2.service.IGetAppleCareEnrollmentImplService;
import com.tmobile.u2.service.IGetDeviceDetailsImplService;
import com.tmobile.u2.service.IManageAppleCareEnrollmentImplService;
import com.tmobile.u2.service.IManageBlackListImplService;
import com.tmobile.u2.service.IPostDeviceAssesmentResultsImplService;
import com.tmobile.u2.service.IRegisterDeviceForFirstUseDummyImplService;
import com.tmobile.u2.service.ISendDeviceUnlockCodeImplService;
import com.tmobile.u2.service.IUpdateDeviceOrderImplService;
import com.tmobile.u2.service.IUpdateUnlockEligibilityImplService;
import com.tmobile.u2.service.impl.DeviceEnterpriseServiceImpl;
import com.tmobile.u2.util.SharedServiceContext;

@ExtendWith(MockitoExtension.class)
public class DeviceEnterpriseServiceImplTest {

	@InjectMocks
	private DeviceEnterpriseServiceImpl deviceEnterpriseServiceImpl;

	@Mock
	private SharedServiceContext sharedServiceContext;

	@Mock
	private ISendDeviceUnlockCodeImplService iSendDeviceUnlockCodeImplService;

	@Mock
	private IUpdateDeviceOrderImplService iUpdateDeviceOrderImplService;

	@Mock
	private IGetAppleCareEnrollmentImplService iGetAppleCareEnrollmentImplService;

	@Mock
	private IManageBlackListImplService iManageBlackListImplService;

	@Mock
	private ICheckDeviceCommissionEligiblityDummyImplService iCheckDeviceCommissionEligiblityDummyImplService;

	@Mock
	private IManageAppleCareEnrollmentImplService iManageAppleCareEnrollmentImplService;

	@Mock
	private IUpdateUnlockEligibilityImplService iUpdateUnlockEligibilityImplService;

	@Mock
	private ICheckBlackListImplService iCheckBlackListImplService;

	@Mock
	private IPostDeviceAssesmentResultsImplService iPostDeviceAssesmentResultsImplService;

	@Mock
	private IGetDeviceDetailsImplService iGetDeviceDetailsImplService;

	@Mock
	private IRegisterDeviceForFirstUseDummyImplService iRegisterDeviceForFirstUseDummyImplService;

	@Mock
	private ICheckAppleCareCompatabilityImplService iCheckAppleCareCompatabilityImplService;

	@Before
	public void setup() throws Exception {
		Mockito.when(sharedServiceContext.getRequestValue(Mockito.anyString())).thenReturn(Mockito.any());
		Mockito.doNothing().when(iSendDeviceUnlockCodeImplService).executeMethod(
				Mockito.any(SendDeviceUnlockCodeRequest.class), Mockito.any(SendDeviceUnlockCodeResponse.class));
		Mockito.doNothing().when(iUpdateDeviceOrderImplService).executeMethod(
				Mockito.any(UpdateDeviceOrderRequest.class), Mockito.any(UpdateDeviceOrderResponse.class));
		Mockito.doNothing().when(iGetAppleCareEnrollmentImplService).executeMethod(
				Mockito.any(GetAppleCareEnrollmentRequest.class), Mockito.any(GetAppleCareEnrollmentResponse.class));
		Mockito.doNothing().when(iManageBlackListImplService).executeMethod(Mockito.any(ManageBlackListRequest.class),
				Mockito.any(ManageBlackListResponse.class));
		Mockito.doNothing().when(iCheckDeviceCommissionEligiblityDummyImplService).executeMethod(
				Mockito.any(CheckDeviceCommissionEligiblityRequest.class),
				Mockito.any(CheckDeviceCommissionEligiblityResponse.class));
		Mockito.doNothing().when(iManageAppleCareEnrollmentImplService).executeMethod(
				Mockito.any(ManageAppleCareEnrollmentRequest.class),
				Mockito.any(ManageAppleCareEnrollmentResponse.class));
		Mockito.doNothing().when(iUpdateUnlockEligibilityImplService).executeMethod(
				Mockito.any(UpdateUnlockEligibilityRequest.class), Mockito.any(UpdateUnlockEligibilityResponse.class));
		Mockito.doNothing().when(iCheckBlackListImplService).executeMethod(Mockito.any(CheckBlackListRequest.class),
				Mockito.any(CheckBlackListResponse.class));
		Mockito.doNothing().when(iPostDeviceAssesmentResultsImplService).executeMethod(
				Mockito.any(PostDeviceAssesmentResultsRequest.class),
				Mockito.any(PostDeviceAssesmentResultsResponse.class));
		Mockito.doNothing().when(iGetDeviceDetailsImplService).executeMethod(Mockito.any(GetDeviceDetailsRequest.class),
				Mockito.any(GetDeviceDetailsResponse.class));
		Mockito.doNothing().when(iRegisterDeviceForFirstUseDummyImplService).executeMethod(
				Mockito.any(RegisterDeviceForFirstUseRequest.class),
				Mockito.any(RegisterDeviceForFirstUseResponse.class));
		Mockito.doNothing().when(iCheckAppleCareCompatabilityImplService).executeMethod(
				Mockito.any(CheckAppleCareCompatabilityRequest.class),
				Mockito.any(CheckAppleCareCompatabilityResponse.class));
	}

	@Test
	public void testCheckBlackList() {
		CheckBlackListRequest request = new CheckBlackListRequest();
		CheckBlackListResponse response = new CheckBlackListResponse();
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.checkBlackList(request, response);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetSendDeviceUnlockCodeImplService() {
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.setSendDeviceUnlockCodeImplService(iSendDeviceUnlockCodeImplService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetGetAppleCareEnrollmentImplService() {
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.setGetAppleCareEnrollmentImplService(iGetAppleCareEnrollmentImplService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetManageAppleCareEnrollmentImplService() {
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.setManageAppleCareEnrollmentImplService(iManageAppleCareEnrollmentImplService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetCheckAppleCareCompatabilityImplService() {
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl
					.setCheckAppleCareCompatabilityImplService(iCheckAppleCareCompatabilityImplService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetUpdateUnlockEligibilityImplService() {
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.setUpdateUnlockEligibilityImplService(iUpdateUnlockEligibilityImplService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetPostDeviceAssesmentResultsImplService() {
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl
					.setPostDeviceAssesmentResultsImplService(iPostDeviceAssesmentResultsImplService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetRegisterDeviceForFirstUseDummyImplService() {
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl
					.setRegisterDeviceForFirstUseDummyImplService(iRegisterDeviceForFirstUseDummyImplService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetCheckDeviceCommissionEligiblityDummyImplService() {
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.setCheckDeviceCommissionEligiblityDummyImplService(
					iCheckDeviceCommissionEligiblityDummyImplService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetManageBlackListImplService() {
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.setManageBlackListImplService(iManageBlackListImplService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetUpdateDeviceOrderImplService() {
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.setUpdateDeviceOrderImplService(iUpdateDeviceOrderImplService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetCheckBlackListImplService() {
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.setCheckBlackListImplService(iCheckBlackListImplService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetGetDeviceDetailsImplService() {
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.setGetDeviceDetailsImplService(iGetDeviceDetailsImplService);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSendDeviceUnlockCode() {
		SendDeviceUnlockCodeRequest request = new SendDeviceUnlockCodeRequest();
		SendDeviceUnlockCodeResponse response = new SendDeviceUnlockCodeResponse();
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.sendDeviceUnlockCode(request, response);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testCheckDeviceCommissionEligiblity() {
		CheckDeviceCommissionEligiblityRequest request = new CheckDeviceCommissionEligiblityRequest();
		CheckDeviceCommissionEligiblityResponse response = new CheckDeviceCommissionEligiblityResponse();
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.checkDeviceCommissionEligiblity(request, response);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testRegisterDeviceForFirstUse() {
		RegisterDeviceForFirstUseRequest request = new RegisterDeviceForFirstUseRequest();
		RegisterDeviceForFirstUseResponse response = new RegisterDeviceForFirstUseResponse();
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.registerDeviceForFirstUse(request, response);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testManageAppleCareEnrollment() {
		ManageAppleCareEnrollmentRequest request = new ManageAppleCareEnrollmentRequest();
		ManageAppleCareEnrollmentResponse response = new ManageAppleCareEnrollmentResponse();
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.manageAppleCareEnrollment(request, response);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testUpdateDeviceOrder() {
		UpdateDeviceOrderRequest request = new UpdateDeviceOrderRequest();
		UpdateDeviceOrderResponse response = new UpdateDeviceOrderResponse();
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.updateDeviceOrder(request, response);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testUpdateUnlockEligibility() {
		UpdateUnlockEligibilityRequest request = new UpdateUnlockEligibilityRequest();
		UpdateUnlockEligibilityResponse response = new UpdateUnlockEligibilityResponse();
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.updateUnlockEligibility(request, response);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testPostDeviceAssesmentResults() {
		PostDeviceAssesmentResultsRequest request = new PostDeviceAssesmentResultsRequest();
		PostDeviceAssesmentResultsResponse response = new PostDeviceAssesmentResultsResponse();
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.postDeviceAssesmentResults(request, response);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testGetAppleCareEnrollment() {
		GetAppleCareEnrollmentRequest request = new GetAppleCareEnrollmentRequest();
		GetAppleCareEnrollmentResponse response = new GetAppleCareEnrollmentResponse();
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.getAppleCareEnrollment(request, response);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testSetSharedServiceContext() {
		SharedServiceContext sharedServiceContext = new SharedServiceContext();
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.setSharedServiceContext(sharedServiceContext);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testCheckAppleCareCompatability() {
		CheckAppleCareCompatabilityRequest request = new CheckAppleCareCompatabilityRequest();
		CheckAppleCareCompatabilityResponse response = new CheckAppleCareCompatabilityResponse();
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.checkAppleCareCompatability(request, response);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testGetDeviceDetails() {
		GetDeviceDetailsRequest request = new GetDeviceDetailsRequest();
		GetDeviceDetailsResponse response = new GetDeviceDetailsResponse();
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.getDeviceDetails(request, response);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@Test
	public void testManageBlackList() {
		ManageBlackListRequest request = new ManageBlackListRequest();
		ManageBlackListResponse response = new ManageBlackListResponse();
		Exception ex = null;
		try {
			deviceEnterpriseServiceImpl.manageBlackList(request, response);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@AfterClass
	public void tearDownClass() throws Exception {
	}
}
