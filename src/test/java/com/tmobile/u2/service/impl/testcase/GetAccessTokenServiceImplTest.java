package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import com.tibco.tns.bw.activity.jsonparser.xsd.output.GetAccessTokenOutput;
import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.u2.exception.TMobileProcessException;
import com.tmobile.u2.service.impl.GetAccessTokenServiceImpl;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.shared.service.impl.LogProcessDetailsServiceImpl;

@ExtendWith(MockitoExtension.class)
public class GetAccessTokenServiceImplTest {

    @InjectMocks
    private GetAccessTokenServiceImpl accessTokenServiceImpl;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private ILogProcessDetailsService iLogProcessDetailsService;

    @Before
    public void setup() throws Exception {
        // TODO replace MethodName with actual method and return type as well if required;
        //Mockito.when(restTemplate.MethodName(Mockito.any(clazz.class))).thenReturn(Mockito.any());
        Mockito.doNothing().when(iLogProcessDetailsService).executeMethod(Mockito.any(LogProcessDetailsStartInput.class));
    }

    @Test
    public void testSetRestTemplate() throws TMobileProcessException {
        RestTemplate restTemplate = new RestTemplate();
        Exception ex = null;
        try {
            accessTokenServiceImpl.setRestTemplate(restTemplate);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testExecuteMethod() throws TMobileProcessException {
        GetAccessTokenOutput getAccessTokenOutput = new GetAccessTokenOutput();
        Exception ex = null;
        try {
            accessTokenServiceImpl.executeMethod(getAccessTokenOutput);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetLogProcessDetailsService() throws TMobileProcessException {
        Exception ex = null;
        try {
            accessTokenServiceImpl.setLogProcessDetailsService(new LogProcessDetailsServiceImpl());
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
