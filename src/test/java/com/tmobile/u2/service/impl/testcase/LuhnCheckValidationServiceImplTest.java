package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tmobile.u2.service.impl.LuhnCheckValidationServiceImpl;

import serviceenterprisedevice.common.subprocesses.businessrules.luhncheckvalidation_end_output.LuhnCheckValidationEndOutput;
import serviceenterprisedevice.common.subprocesses.businessrules.luhncheckvalidation_start_input.LuhnCheckValidationStartInput;

@ExtendWith(MockitoExtension.class)
public class LuhnCheckValidationServiceImplTest {

    @InjectMocks
    private LuhnCheckValidationServiceImpl luhnCheckValidationServiceImpl;

    @Before
    public void setup() throws Exception {
    }

    @Test
    public void testExecuteMethod() {
        // TODO replace MethodName with actual method and return type as well if required;
        LuhnCheckValidationStartInput luhnCheckValidationStartInput = new LuhnCheckValidationStartInput();
        LuhnCheckValidationEndOutput luhnCheckValidationEndOutput = new LuhnCheckValidationEndOutput();
        Exception ex = null;
        try {
            luhnCheckValidationServiceImpl.executeMethod(luhnCheckValidationStartInput, luhnCheckValidationEndOutput);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
