package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import com.t_mobile.csi.device.queryimeiwarranty.CSIQueryIMEIWarrantyRequestType;
import com.t_mobile.csi.device.queryimeiwarranty.CSIQueryIMEIWarrantyResponseType;
import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.u2.service.impl.QueryIMEIWarrantySQLServiceImpl;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.shared.service.impl.LogProcessDetailsServiceImpl;

@ExtendWith(MockitoExtension.class)
public class QueryIMEIWarrantySQLServiceImplTest {

    @InjectMocks
    private QueryIMEIWarrantySQLServiceImpl queryIMEIWarrantySQLServiceImpl;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private ILogProcessDetailsService iLogProcessDetailsService;

    @Before
    public void setup() throws Exception {
        //Mockito.when(restTemplate.MethodName(Mockito.any(clazz.class))).thenReturn(Mockito.any());
        Mockito.doNothing().when(iLogProcessDetailsService).executeMethod(Mockito.any(LogProcessDetailsStartInput.class));
    }

    @Test
    public void testSetRestTemplate() {
        Exception ex = null;
        try {
            queryIMEIWarrantySQLServiceImpl.setRestTemplate(restTemplate);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testExecuteMethod() {
        CSIQueryIMEIWarrantyRequestType queryIMEIWarrantyRequest = new CSIQueryIMEIWarrantyRequestType();
        CSIQueryIMEIWarrantyResponseType queryIMEIWarrantyResponse = new CSIQueryIMEIWarrantyResponseType();
        Exception ex = null;
        try {
            queryIMEIWarrantySQLServiceImpl.executeMethod(queryIMEIWarrantyRequest, queryIMEIWarrantyResponse);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetLogProcessDetailsService() {
        Exception ex = null;
        try {
            queryIMEIWarrantySQLServiceImpl.setLogProcessDetailsService(new LogProcessDetailsServiceImpl());
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
