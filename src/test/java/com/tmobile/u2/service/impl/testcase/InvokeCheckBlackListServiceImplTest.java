package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.t_mobile.erp.services.logistics.MTBlockingStatusCheckResponse;
import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckBlackListRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckBlackListResponse;
import com.tmobile.u2.service.IValidateIMEISoapService;
import com.tmobile.u2.service.impl.InvokeCheckBlackListServiceImpl;
import com.tmobile.u2.shared.service.IHeaderValidationService;
import com.tmobile.u2.util.SharedServiceContext;

import sharedservicecontext.sharedprocesses.utilityprocesses.headervalidation_start_input.HeaderValidationStartInput;
import targetsappi.applications.sappi.endpoints.device.validateimeisoap_start_input.ValidateIMEISoapStartInput;

@ExtendWith(MockitoExtension.class)
public class InvokeCheckBlackListServiceImplTest {

    @InjectMocks
    private InvokeCheckBlackListServiceImpl invokeCheckBlackListServiceImpl;

    @Mock
    private IHeaderValidationService iHeaderValidationService;

    @Mock
    private IValidateIMEISoapService iValidateIMEISoapService;

    @Mock
    private SharedServiceContext sharedServiceContext;

    @Before
    public void setup() throws Exception {
        // TODO replace MethodName with actual method and return type as well if required;
    	Mockito.when(sharedServiceContext.getRequestValue(Mockito.anyString())).thenReturn(Mockito.any());
        Mockito.doNothing().when(iHeaderValidationService).executeMethod(Mockito.any(HeaderValidationStartInput.class));;
        Mockito.doNothing().when(iValidateIMEISoapService).executeMethod(Mockito.any(ValidateIMEISoapStartInput.class), Mockito.any(MTBlockingStatusCheckResponse.class));
    }

    @Test
    public void testExecuteMethod() {
        CheckBlackListRequest checkBlackListRequest = new CheckBlackListRequest();
        CheckBlackListResponse checkBlackListResponse = new CheckBlackListResponse();
        Exception ex = null;
        try {
            invokeCheckBlackListServiceImpl.executeMethod(checkBlackListRequest, checkBlackListResponse);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetHeaderValidationService() {
        Exception ex = null;
        try {
            invokeCheckBlackListServiceImpl.setHeaderValidationService(iHeaderValidationService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetValidateIMEISoapService() {
        Exception ex = null;
        try {
            invokeCheckBlackListServiceImpl.setValidateIMEISoapService(iValidateIMEISoapService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetSharedServiceContext() {
        Exception ex = null;
        try {
            invokeCheckBlackListServiceImpl.setSharedServiceContext(sharedServiceContext);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
