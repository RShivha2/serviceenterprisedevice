package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckAppleCareCompatabilityRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.CheckAppleCareCompatabilityResponse;
import com.tmobile.services.schema.dataobject.createservicecontextendoutput.CreateServiceContextOutput;
import com.tmobile.services.schema.dataobject.createservicecontextstartinput.CreateServiceContextInput;
import com.tmobile.services.schema.dataobject.errorlookupendoutput.ErrorLookupEndOutput;
import com.tmobile.services.schema.dataobject.guid.Guid;
import com.tmobile.services.schema.dataobject.legacyesoaerrorlookupstartinput.LegacyESOAErrorLookupStartInput;
import com.tmobile.services.schema.dataobject.logservicecontextstartinput.LogServiceContextStartInput;
import com.tmobile.u2.exception.TMobileProcessException;
import com.tmobile.u2.service.IInvokeCheckAppleCareCompatabilityService;
import com.tmobile.u2.service.impl.CheckAppleCareCompatabilityImplServiceImpl;
import com.tmobile.u2.shared.service.ICreateServiceContextService;
import com.tmobile.u2.shared.service.IErrorLookupService;
import com.tmobile.u2.shared.service.ILogServiceContextService;
import com.tmobile.u2.util.SharedServiceContext;
import com.tmobile.u2.util.TibcoToJavaUtil;

@ExtendWith(MockitoExtension.class)
public class CheckAppleCareCompatabilityImplServiceImplTest {

	@InjectMocks
	private CheckAppleCareCompatabilityImplServiceImpl checkAppleCareCompatabilityImplServiceImpl;

	@Mock
	private IErrorLookupService iErrorLookupService;

	@Mock
	private ICreateServiceContextService iCreateServiceContextService;

	@Mock
	private ILogServiceContextService iLogServiceContextService;

	@InjectMocks
	private SharedServiceContext sharedServiceContext;

	@Mock
	private IInvokeCheckAppleCareCompatabilityService iInvokeCheckAppleCareCompatabilityService;

	@Before
	public void setup() throws Exception {

		//Mockito.when(sharedServiceContext.getRequestValue(Mockito.anyString())).thenReturn(null);
		//sharedServiceContext.putRequestValue("GUID", TibcoToJavaUtil.createUUID());
		Mockito.doNothing().when(iErrorLookupService).executeMethod(Mockito.any(LegacyESOAErrorLookupStartInput.class), Mockito.any(ErrorLookupEndOutput.class));
		Mockito.doNothing().when(iCreateServiceContextService).executeMethod(Mockito.any(CreateServiceContextInput.class), Mockito.any(CreateServiceContextOutput.class));
		Mockito.doNothing().when(iLogServiceContextService).executeMethod(Mockito.any(LogServiceContextStartInput.class));
		Mockito.doNothing().when(iInvokeCheckAppleCareCompatabilityService).executeMethod(Mockito.any(CheckAppleCareCompatabilityRequest.class),
				Mockito.any(CheckAppleCareCompatabilityResponse.class));
	}

	@Test
	public void testExecuteMethod() throws TMobileProcessException {
		CheckAppleCareCompatabilityRequest checkAppleCareCompatabilityRequest = new CheckAppleCareCompatabilityRequest();
		CheckAppleCareCompatabilityResponse checkAppleCareCompatabilityResponse = new CheckAppleCareCompatabilityResponse();
		Exception ex = null;
		try {
			checkAppleCareCompatabilityImplServiceImpl.executeMethod(checkAppleCareCompatabilityRequest,
					checkAppleCareCompatabilityResponse);
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}

	@AfterClass
	public void tearDownClass() throws Exception {
	}
}
