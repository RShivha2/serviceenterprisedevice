package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.namespaces.tns._1573645539824.ConvertJSONtoXMLRequest;
import com.example.namespaces.tns._1573646434454.ConvertJSONtoXMLResponse;
import com.tmobile.u2.service.impl.ConvertJSONtoXMLServiceImpl;

@ExtendWith(MockitoExtension.class)
public class ConvertJSONtoXMLServiceImplTest {

    @InjectMocks
    private ConvertJSONtoXMLServiceImpl convertJSONtoXMLServiceImpl;

    @Before
    public void setup() throws Exception {
    }

    @Test
    public void testExecuteMethod() {
        // TODO replace MethodName with actual method and return type as well if required;
        ConvertJSONtoXMLRequest convertJSONtoXMLRequest = new ConvertJSONtoXMLRequest();
        ConvertJSONtoXMLResponse convertJSONtoXMLResponse = new ConvertJSONtoXMLResponse();
        Exception ex = null;
        try {
            convertJSONtoXMLServiceImpl.executeMethod(convertJSONtoXMLRequest, convertJSONtoXMLResponse);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
