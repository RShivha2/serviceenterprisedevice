package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.u2.service.impl.ManageBlackListSoapServiceImpl;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.shared.service.impl.LogProcessDetailsServiceImpl;
import com.tmobile.u2.util.BlockingStatusUpdateSOAPConnector;

import targetsappi.applications.sappi.endpoints.device.manageblacklistsoap_end_output.ManageBlackListSoapEndOutput;
import targetsappi.applications.sappi.endpoints.device.manageblacklistwholesalesoap_start_input.Event;

@ExtendWith(MockitoExtension.class)
public class ManageBlackListSoapServiceImplTest {

    @InjectMocks
    private ManageBlackListSoapServiceImpl manageBlackListSoapServiceImpl;

    @Mock
    private ILogProcessDetailsService iLogProcessDetailsService;

    @Mock
    private BlockingStatusUpdateSOAPConnector blockingStatusUpdateSOAPConnector;

    @Before
    public void setup() throws Exception {
        Mockito.doNothing().when(iLogProcessDetailsService).executeMethod(Mockito.any(LogProcessDetailsStartInput.class));
        //Mockito.when(blockingStatusUpdateSOAPConnector.callWebService(Mockito.any(clazz.class))).thenReturn(Mockito.any());
    }

    @Test
    public void testExecuteMethod() {
        Event event = new Event();
        ManageBlackListSoapEndOutput manageBlackListSoapEndOutput = new ManageBlackListSoapEndOutput();
        Exception ex = null;
        try {
            manageBlackListSoapServiceImpl.executeMethod(event, manageBlackListSoapEndOutput);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetLogProcessDetailsService() {
        Exception ex = null;
        try {
            manageBlackListSoapServiceImpl.setLogProcessDetailsService(new LogProcessDetailsServiceImpl());
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetSOAPConnector() {
        Exception ex = null;
        try {
            manageBlackListSoapServiceImpl.setSOAPConnector(blockingStatusUpdateSOAPConnector);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
