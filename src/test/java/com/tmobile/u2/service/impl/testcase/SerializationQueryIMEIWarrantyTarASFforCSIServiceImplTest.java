package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.u2.service.impl.SerializationQueryIMEIWarrantyTarASFforCSIServiceImpl;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.shared.service.impl.LogProcessDetailsServiceImpl;

import targetserialization.applications.serialization.adaptersimulationflows.warrantydetails.serializationqueryimeiwarrantytarasfforcsi_end_output.SerializationQueryIMEIWarrantyTarASFforCSIEndOutput;
import targetserialization.applications.serialization.adaptersimulationflows.warrantydetails.serializationqueryimeiwarrantytarasfforcsi_start_input.SerializationQueryIMEIWarrantyTarASFforCSIStartInput;

@ExtendWith(MockitoExtension.class)
public class SerializationQueryIMEIWarrantyTarASFforCSIServiceImplTest {

    @InjectMocks
    private SerializationQueryIMEIWarrantyTarASFforCSIServiceImpl serializationQueryIMEIWarrantyTarASFforCSIServiceImpl;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private ILogProcessDetailsService iLogProcessDetailsService;

    @Before
    public void setup() throws Exception {
        //Mockito.when(restTemplate.MethodName(Mockito.any(clazz.class))).thenReturn(Mockito.any());
        Mockito.doNothing().when(iLogProcessDetailsService).executeMethod(Mockito.any(LogProcessDetailsStartInput.class));
    }

    @Test
    public void testSetRestTemplate() {
        Exception ex = null;
        try {
            serializationQueryIMEIWarrantyTarASFforCSIServiceImpl.setRestTemplate(restTemplate);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testExecuteMethod() {
        SerializationQueryIMEIWarrantyTarASFforCSIStartInput serializationQueryIMEIWarrantyTarASFforCSIStartInput = new SerializationQueryIMEIWarrantyTarASFforCSIStartInput();
        SerializationQueryIMEIWarrantyTarASFforCSIEndOutput serializationQueryIMEIWarrantyTarASFforCSIEndOutput = new SerializationQueryIMEIWarrantyTarASFforCSIEndOutput();
        Exception ex = null;
        try {
            serializationQueryIMEIWarrantyTarASFforCSIServiceImpl.executeMethod(serializationQueryIMEIWarrantyTarASFforCSIStartInput, serializationQueryIMEIWarrantyTarASFforCSIEndOutput);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetLogProcessDetailsService() {
        Exception ex = null;
        try {
            serializationQueryIMEIWarrantyTarASFforCSIServiceImpl.setLogProcessDetailsService(new LogProcessDetailsServiceImpl());
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
