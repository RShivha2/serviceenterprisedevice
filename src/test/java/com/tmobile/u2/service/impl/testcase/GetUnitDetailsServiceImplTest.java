package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import com.example.namespaces.tns._1593633254125.Root;
import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.u2.service.impl.GetUnitDetailsServiceImpl;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.shared.service.impl.LogProcessDetailsServiceImpl;

import generated.GetUnitDetailsResponse;

@ExtendWith(MockitoExtension.class)
public class GetUnitDetailsServiceImplTest {

    @InjectMocks
    private GetUnitDetailsServiceImpl unitDetailsServiceImpl;

    @Mock
    private ILogProcessDetailsService iLogProcessDetailsService;

    @Mock
    private RestTemplate restTemplate;

    @Before
    public void setup() throws Exception {
    	// TODO replace MethodName with actual method and return type as well if required;
    	Mockito.doNothing().when(iLogProcessDetailsService).executeMethod(Mockito.any(LogProcessDetailsStartInput.class));
    	//Mockito.when(restTemplate.MethodName(Mockito.any(clazz.class))).thenReturn(Mockito.any());
    }

    @Test
    public void testSetRestTemplate() {
        Exception ex = null;
        try {
            unitDetailsServiceImpl.setRestTemplate(restTemplate);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testExecuteMethod() {
        Root root = new Root();
        GetUnitDetailsResponse getUnitDetailsResponse = new GetUnitDetailsResponse();
        Exception ex = null;
        try {
            unitDetailsServiceImpl.executeMethod(root, getUnitDetailsResponse);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetLogProcessDetailsService() {
        Exception ex = null;
        try {
            unitDetailsServiceImpl.setLogProcessDetailsService(new LogProcessDetailsServiceImpl());
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
