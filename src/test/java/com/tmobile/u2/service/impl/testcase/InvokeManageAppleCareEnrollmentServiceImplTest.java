package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.t_mobile.schema.apple.cancelorder.CancelOrderReqtype;
import com.t_mobile.schema.apple.cancelorder.CancelOrderRestype;
import com.t_mobile.schema.apple.createorder.CreateOrderRequesttype;
import com.t_mobile.schema.apple.createorder.CreateOrderResponsetype;
import com.tmobile.services.productmanagement.deviceenterprise.v1.ManageAppleCareEnrollmentRequest;
import com.tmobile.services.productmanagement.deviceenterprise.v1.ManageAppleCareEnrollmentResponse;
import com.tmobile.u2.service.ICancelOrderRESTService;
import com.tmobile.u2.service.ICreateOrderRESTService;
import com.tmobile.u2.service.impl.InvokeManageAppleCareEnrollmentServiceImpl;
import com.tmobile.u2.shared.service.IHeaderValidationService;
import com.tmobile.u2.util.SharedServiceContext;

import sharedservicecontext.sharedprocesses.utilityprocesses.headervalidation_start_input.HeaderValidationStartInput;

@ExtendWith(MockitoExtension.class)
public class InvokeManageAppleCareEnrollmentServiceImplTest {

    @InjectMocks
    private InvokeManageAppleCareEnrollmentServiceImpl invokeManageAppleCareEnrollmentServiceImpl;

    @Mock
    private IHeaderValidationService iHeaderValidationService;

    @Mock
    private ICreateOrderRESTService iCreateOrderRESTService;

    @Mock
    private ICancelOrderRESTService iCancelOrderRESTService;

    @Mock
    private SharedServiceContext sharedServiceContext;

    @Before
    public void setup() throws Exception {
    	Mockito.when(sharedServiceContext.getRequestValue(Mockito.anyString())).thenReturn(Mockito.any());
        Mockito.doNothing().when(iHeaderValidationService).executeMethod(Mockito.any(HeaderValidationStartInput.class));;
        Mockito.doNothing().when(iCreateOrderRESTService).executeMethod(Mockito.any(CreateOrderRequesttype.class), Mockito.any(CreateOrderResponsetype.class));
        Mockito.doNothing().when(iCancelOrderRESTService).executeMethod(Mockito.any(CancelOrderReqtype.class), Mockito.any(CancelOrderRestype.class));
    }

    @Test
    public void testExecuteMethod() {
        ManageAppleCareEnrollmentRequest manageAppleCareEnrollmentRequest = new ManageAppleCareEnrollmentRequest();
        ManageAppleCareEnrollmentResponse manageAppleCareEnrollmentResponse = new ManageAppleCareEnrollmentResponse();
        Exception ex = null;
        try {
            invokeManageAppleCareEnrollmentServiceImpl.executeMethod(manageAppleCareEnrollmentRequest, manageAppleCareEnrollmentResponse);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetHeaderValidationService() {
        Exception ex = null;
        try {
            invokeManageAppleCareEnrollmentServiceImpl.setHeaderValidationService(iHeaderValidationService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetSharedServiceContext() {
        Exception ex = null;
        try {
            invokeManageAppleCareEnrollmentServiceImpl.setSharedServiceContext(sharedServiceContext);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetCancelOrderRESTService() {
        Exception ex = null;
        try {
            invokeManageAppleCareEnrollmentServiceImpl.setCancelOrderRESTService(iCancelOrderRESTService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetCreateOrderRESTService() {
        Exception ex = null;
        try {
            invokeManageAppleCareEnrollmentServiceImpl.setCreateOrderRESTService(iCreateOrderRESTService);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
