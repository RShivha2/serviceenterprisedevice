package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.t_mobile.schema.apple.createorder.CreateOrderRequesttype;
import com.t_mobile.schema.apple.createorder.CreateOrderResponsetype;
import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.u2.exception.TMobileProcessException;
import com.tmobile.u2.service.impl.CreateOrderRESTServiceImpl;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.shared.service.impl.LogProcessDetailsServiceImpl;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class CreateOrderRESTServiceImplTest {

    @InjectMocks
    private CreateOrderRESTServiceImpl createOrderRESTServiceImpl;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private ILogProcessDetailsService iLogProcessDetailsService;

    @Before
    public void setup() throws Exception {
    }

    @Test
    public void testSetRestTemplate() throws TMobileProcessException {
        Exception ex = null;
        try {
            createOrderRESTServiceImpl.setRestTemplate(restTemplate);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testExecuteMethod() throws TMobileProcessException {
        //Mockito.when(restTemplate.exchange(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(Mockito.any());

    	ResponseEntity<String> responseEntity = new ResponseEntity<String>(HttpStatus.OK);
    	
		/*
		 * Mockito.when(restTemplate.exchange(
		 * "http://sentry.test.ext.t-mobile.com:6441/order-service/1.0/create-order/",
		 * HttpMethod.POST, new HttpEntity<>(Mockito.anyString(), new HttpHeaders()),
		 * String.class)) .thenReturn(responseEntity);
		 */
        Mockito.when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<String>>any()))
             .thenReturn(responseEntity);
    	
    	Mockito.doNothing().when(iLogProcessDetailsService).executeMethod(Mockito.any(LogProcessDetailsStartInput.class));
        CreateOrderRequesttype createOrderRequest = new CreateOrderRequesttype();
        CreateOrderResponsetype createOrderResponse = new CreateOrderResponsetype();
        Exception ex = null;
        try {
            createOrderRESTServiceImpl.executeMethod(createOrderRequest, createOrderResponse);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetLogProcessDetailsService() throws TMobileProcessException {
        Exception ex = null;
        try {
            createOrderRESTServiceImpl.setLogProcessDetailsService(new LogProcessDetailsServiceImpl());
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
