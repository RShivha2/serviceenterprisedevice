package com.tmobile.u2.service.impl.testcase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.namespaces.tns._1588947392231.QueryAllSOAPStartInput;
import com.example.namespaces.tns._1588947392231.Root;
import com.example.namespaces.tns._1588947392232.QueryAllSOAPEndOutput;
import com.tmobile.services.schema.dataobject.logprocessdetailsstartinput.LogProcessDetailsStartInput;
import com.tmobile.u2.service.impl.QueryAllSOAPServiceImpl;
import com.tmobile.u2.shared.service.ILogProcessDetailsService;
import com.tmobile.u2.shared.service.impl.LogProcessDetailsServiceImpl;
import com.tmobile.u2.util.QueryAllSOAPConnector;
import com.tmobile.u2.util.SharedServiceContext;

@ExtendWith(MockitoExtension.class)
public class QueryAllSOAPServiceImplTest {

    @InjectMocks
    private QueryAllSOAPServiceImpl queryAllSOAPServiceImpl;

    @Mock
    private QueryAllSOAPConnector queryAllSOAPConnector;

    @Mock
    private ILogProcessDetailsService iLogProcessDetailsService;

    @Mock
    private SharedServiceContext sharedServiceContext;

    @Before
    public void setup() throws Exception {
    	Mockito.when(sharedServiceContext.getRequestValue(Mockito.anyString())).thenReturn(Mockito.any());
        Mockito.doNothing().when(iLogProcessDetailsService).executeMethod(Mockito.any(LogProcessDetailsStartInput.class));
        //Mockito.when(queryAllSOAPConnector.callWebService(Mockito.any(clazz.class))).thenReturn(Mockito.any());
    }

    @Test
    public void testExecuteMethod() {
        QueryAllSOAPStartInput queryAllSOAPStartInput = new QueryAllSOAPStartInput();
        queryAllSOAPStartInput.getRoot().setQueryAll(new Root.QueryAll());
        QueryAllSOAPEndOutput queryAllSOAPEndOutput = new QueryAllSOAPEndOutput();
        Exception ex = null;
        try {
            queryAllSOAPServiceImpl.executeMethod(queryAllSOAPStartInput, queryAllSOAPEndOutput);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetLogProcessDetailsService() {
        Exception ex = null;
        try {
            queryAllSOAPServiceImpl.setLogProcessDetailsService(new LogProcessDetailsServiceImpl());
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetSOAPConnector() {
        Exception ex = null;
        try {
            queryAllSOAPServiceImpl.setSOAPConnector(queryAllSOAPConnector);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @Test
    public void testSetSharedServiceContext() {
        Exception ex = null;
        try {
            queryAllSOAPServiceImpl.setSharedServiceContext(sharedServiceContext);
        } catch (Exception e) {
            ex = e;
            e.printStackTrace();
        }
        //Assert.assertNull(ex);
    }

    @AfterClass
    public void tearDownClass() throws Exception {
    }
}
