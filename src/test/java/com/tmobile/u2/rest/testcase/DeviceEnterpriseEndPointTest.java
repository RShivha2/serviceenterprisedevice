package com.tmobile.u2.rest.testcase;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.ResourceUtils;

import com.tmobile.services.productmanagement.deviceenterprise.v1.UpdateUnlockEligibilityRequest;
import com.tmobile.u2.util.ServiceEnterpriseSOAPConnector;
import com.tmobile.u2.util.TibcoToJavaUtil;

@RunWith(SpringRunner.class)
//@ActiveProfiles(profiles = "test")
//@SpringBootTest(classes = TestConfig.class)
//@TestPropertySource("classpath:application-test.properties")
//@WebMvcTest(DeviceEnterpriseEndPoint.class)
//@ContextConfiguration(classes = {DeviceEnterpriseWebServiceConfig.class})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
public class DeviceEnterpriseEndPointTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private WebTestClient webClient;
	
	@MockBean
	private ServiceEnterpriseSOAPConnector sOAPConnector;

	//@Test
	public void testUpdateUnlockEligibility() {

		UpdateUnlockEligibilityRequest request = new UpdateUnlockEligibilityRequest();

		Exception ex = null;
		try {
			
			
			String soapURL = TibcoToJavaUtil
					.getProperty("TMO.Env.Endpoints.SAPPI.SOAP.Endpoint_SAPPI_UpdateStatus_Host") + ":"
					+ TibcoToJavaUtil.getProperty("TMO.Env.Endpoints.SAPPI.SOAP.Endpoint_SAPPI_UpdateStatus_Port")
					+ TibcoToJavaUtil.getProperty("TMO.Env.Endpoints.SAPPI.SOAP.Endpoint_SAPPI_UpdateStatus_URI");

			// MtStatusUpdateRequest mtStatusUpdateRequest =
			// JAXB.unmarshal(ResourceUtils.getFile("classpath:testData/mt_StatusUpdateRequest.xml"),MtStatusUpdateRequest.class);
			// MtStatusUpdateResponse mtStatusUpdateResponse =
			// JAXB.unmarshal(ResourceUtils.getFile("classpath:testData/mt_StatusUpdateResponse.xml"),MtStatusUpdateResponse.class);

			// JAXBElement<MtStatusUpdateResponse> soapResponse = new
			// com.t_mobile.erp.services.logistics.ObjectFactory().createMtStatusUpdateResponse(mtStatusUpdateResponse);

			// Mockito.when(sOAPConnector.callWebService(soapURL, mtStatusUpdateRequest,
			// null)).thenReturn(soapResponse);

			// File xml =
			// ResourceUtils.getFile("classpath:requestData/updateUnlockEligibilityRequest_SOAP.xml");
			// request = JAXB.unmarshal(xml, UpdateUnlockEligibilityRequest.class);
			// UpdateUnlockEligibilityResponse actual1 =
			// deviceEnterpriseEndPoint.updateUnlockEligibility(request);

			StringBuilder result = new StringBuilder();
			File soapxml = ResourceUtils.getFile("classpath:requestData/updateUnlockEligibilityRequest_SOAP.xml");
			try (BufferedReader reader = new BufferedReader(new FileReader(soapxml))) {
				String line;
				while ((line = reader.readLine()) != null) {
					result.append(line + System.lineSeparator());
				}
			}
			this.webClient.post().uri("/service/soap/ProductManagement/DeviceEnterprise/V1")
						.contentType(MediaType.TEXT_XML)
						.bodyValue(result.toString())
						.exchange().expectStatus().isOk();

			System.out.println("result :: " + result.toString());
			try {
				mockMvc.perform(MockMvcRequestBuilders
						.post("http://localhost:8080/service/soap/ProductManagement/DeviceEnterprise/V1")
						.content(result.toString()).contentType(MediaType.TEXT_XML))
						.andExpect(status().is2xxSuccessful());
			} catch (Exception e1) {
				e1.printStackTrace();
			}

		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
		//Assert.assertNull(ex);
	}
}
