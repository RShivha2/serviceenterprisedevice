package com.tmobile.services.EnterpriseDevice;

import static org.springframework.ws.test.server.RequestCreators.withPayload;
import static org.springframework.ws.test.server.ResponseMatchers.payload;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javax.xml.transform.Source;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.ResourceUtils;
import org.springframework.ws.test.server.MockWebServiceClient;
import org.springframework.xml.transform.StringSource;

import com.tmobile.u2.rest.DeviceEnterpriseWebServiceConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DeviceEnterpriseWebServiceConfig.class})
public class ServiceEnterpriseDeviceApplicationTests {
	@Autowired
	private ApplicationContext applicationContext;
	private MockWebServiceClient mockClient;

	@Before
	public void createClient() {
		mockClient = MockWebServiceClient.createClient(applicationContext);
		GenericApplicationContext ctx = (GenericApplicationContext) applicationContext;
		final XmlBeanDefinitionReader definitionReader = new XmlBeanDefinitionReader(ctx);
		definitionReader.setValidationMode(XmlBeanDefinitionReader.VALIDATION_NONE);
		definitionReader.setNamespaceAware(true);
	}

	@Test
	public void testCountryEndpoint() throws Exception {
		try {
			System.out.println("1");
			/*
			 * StringBuilder result = new StringBuilder(); File soapxml =
			 * ResourceUtils.getFile(
			 * "classpath:requestData/updateUnlockEligibilityRequest_SOAP.xml"); try
			 * (BufferedReader reader = new BufferedReader(new FileReader(soapxml))) {
			 * String line; while ((line = reader.readLine()) != null) { result.append(line
			 * + System.lineSeparator()); } }
			 */
			
			Source requestPayload = new StringSource("<v1:updateUnlockEligibilityRequest serviceTransactionId=\"?\" version=\"?\" xmlns:v1=\"http://services.tmobile.com/ProductManagement/DeviceEnterprise/V1\" xmlns:base=\"http://services.tmobile.com/base\"><base:header><base:sender><base:senderId>ERICSSON</base:senderId><base:channelId>RETAIL</base:channelId><base:applicationId>ECNM</base:applicationId><base:applicationUserId>?</base:applicationUserId><base:sessionId>PASS AS IF FROM THE SOURCE</base:sessionId><base:workflowId>PASS AS IF FROM THE SOURCE</base:workflowId><base:activityId>PASS AS IF FROM THE SOURCE</base:activityId><base:timestamp>2014-06-29T12:00:00</base:timestamp><base:storeId>PASS AS IF FROM THE SOURCE(484848)</base:storeId><base:dealerCode>PASS AS IF FROM THE SOURCE(0000002)</base:dealerCode><base:scope>PASS AS IF FROM THE SOURCE</base:scope><base:interactionId>PASS AS IF FROM THE SOURCE</base:interactionId></base:sender><base:providerId><base:id>ERIC123</base:id></base:providerId></base:header><v1:MSISDN>4042792847</v1:MSISDN><v1:imei>355873847928122</v1:imei><v1:imsi>234105563888549</v1:imsi><v1:overrideType>FRONTLINE</v1:overrideType><v1:overrideReason>International</v1:overrideReason><v1:deviceBrandId>T-Mobile</v1:deviceBrandId><v1:userId>gayathri.T</v1:userId></v1:updateUnlockEligibilityRequest>");
			Source responsePayload = new StringSource("<getCountryResponse xmlns='http://tutorialspoint/schemas'>"
					+ "<country>" + "<name>United States</name>" + "<population>46704314</population>"
					+ "<capital>Washington</capital>" + "<currency>USD</currency>" + "</country>"
					+ "</getCountryResponse>");
			mockClient.sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
