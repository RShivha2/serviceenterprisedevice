package com.hcl.atma.utgen.testcase.utils;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.*")
public class TestConfig {
}
